import * as React from "react";
import Badge from "@mui/material/Badge";

interface PropBadge {
  icon: any;
  numberInfo?: number;
  color?:
    | "primary"
    | "default"
    | "secondary"
    | "error"
    | "info"
    | "success"
    | "warning"
    | undefined;
  onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const CBadge = ({
  onClick,
  icon,
  numberInfo,
  color = "primary",
}: PropBadge) => {
  return (
    <Badge onClick={onClick} badgeContent={numberInfo} color={color}>
      {icon}
    </Badge>
  );
};

export default CBadge;
