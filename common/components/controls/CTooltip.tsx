import * as React from "react";
import Tooltip from "@mui/material/Tooltip";
type CTooltip = {
  child: any;
  title: string;
  placement?: "top" | "right" | "bottom" | "left";
};
const CTooltip = ({ child, title, placement = "right" }: CTooltip) => {
  return (
    <Tooltip title={title} placement={placement}>
      {child}
    </Tooltip>
  );
};

export default CTooltip;
