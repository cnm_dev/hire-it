import React from "react";
import Rating from "@mui/material/Rating";

interface PropRating {
  rate?: number;
  readOnly: boolean;
  setStar?: React.Dispatch<React.SetStateAction<number | null>>;
}

const CRating = ({ rate = 5, readOnly, setStar }: PropRating) => {
  const [value, setValue] = React.useState<number | null>(rate);
  return (
    <Rating
      name="simple-controlled"
      value={value}
      precision={0.5}
      readOnly={readOnly ? true : false}
      onChange={(event, newValue) => {
        setStar && setStar(newValue);
        setValue(newValue);
      }}
    />
  );
};

export default CRating;
