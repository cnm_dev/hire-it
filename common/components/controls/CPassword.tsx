import {
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import { Eye, EyeSlash } from "iconsax-react";
import React from "react";

interface CPassword {
  placeholder?: string | undefined;
  label: string;
  registerName?: any;
  className?: string | undefined;
  onBlur?:
    | React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>
    | undefined;
  aria_invalid?:
    | boolean
    | "true"
    | "false"
    | "grammar"
    | "spelling"
    | undefined;
}

const CPassword = ({
  placeholder,
  label,
  registerName,
  className,
  onBlur,
  aria_invalid,
}: CPassword) => {
  const [showPassword, setShowPassword] = React.useState(false);
  const handleClickShowPassword = () => setShowPassword((show) => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };
  return (
    <FormControl
      sx={{ m: 1, width: "25ch" }}
      variant="outlined"
      className={className}
    >
      <InputLabel htmlFor="outlined-adornment-password">{label}</InputLabel>
      <OutlinedInput
        {...registerName}
        aria-invalid={aria_invalid}
        onBlur={onBlur}
        id="outlined-adornment-password"
        type={showPassword ? "text" : "password"}
        placeholder={placeholder}
        endAdornment={
          <InputAdornment position="end">
            <IconButton
              aria-label="toggle password visibility"
              onClick={handleClickShowPassword}
              onMouseDown={handleMouseDownPassword}
              edge="end"
            >
              {showPassword ? (
                <Eye size="25" variant="Bold" color="rgba(0,0,0,0.1)" />
              ) : (
                <EyeSlash size="25" variant="Bold" color="rgba(0,0,0,0.1)" />
              )}
            </IconButton>
          </InputAdornment>
        }
        label={label}
      />
    </FormControl>
  );
};

export default CPassword;
