import * as React from "react";
import Button from "@mui/material/Button";

export type ColorButton = {
  color?:
    | "inherit"
    | "primary"
    | "secondary"
    | "success"
    | "error"
    | "info"
    | "warning"
    | undefined;
};

interface PropCButton {
  variant?: "text" | "outlined" | "contained" | undefined;
  text: string | any;
  borderRadius?: number;
  onClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
  color?: ColorButton["color"];
  className?: string;
  disabled?: boolean;
  type?: "button" | "submit" | "reset";
}

export default function CButton({
  variant = "contained",
  text,
  borderRadius,
  onClick,
  color = "primary",
  className,
  disabled = false,
  type,
}: PropCButton) {
  return (
    <Button
      onClick={onClick}
      variant={variant}
      style={{ borderRadius: borderRadius }}
      color={color}
      className={className}
      disabled={disabled}
      type={type}
    >
      {text}
    </Button>
  );
}
