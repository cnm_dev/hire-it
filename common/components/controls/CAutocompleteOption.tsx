import {
  Autocomplete,
  AutocompleteChangeDetails,
  AutocompleteChangeReason,
  AutocompleteValue,
  Box,
  InputAdornment,
  SxProps,
  TextField,
  Theme,
} from "@mui/material";
import React from "react";
import {
  Control,
  Controller,
  FieldValues,
  RegisterOptions,
} from "react-hook-form";
import { BsCheck2 } from "react-icons/bs";
interface propsCAutocompleteOption {
  values: Option[];
  placeholder?: string | undefined;
  clasName?: string;
  name: string | "";
  control?: Control<any> | undefined;
  rules?: Omit<
    RegisterOptions<FieldValues, string>,
    "disabled" | "valueAsNumber" | "valueAsDate" | "setValueAs"
  >;
  multiple?: boolean | undefined;
  onBlur?: (
    e: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement, Element>
  ) => void;
  isInline?: boolean | undefined;
  disableCloseOnSelect?: boolean | undefined;
  ListboxProps?:
    | (React.HTMLAttributes<HTMLUListElement> & {
        sx?: SxProps<Theme> | undefined;
      })
    | undefined;
  validateLenght?: boolean | undefined;
  onBlurAutocomplete?: React.FocusEventHandler<HTMLDivElement> | undefined;
  open?: boolean;
  defaultValue?: Option[];
}

export interface Option {
  id: number;
  name: string;
}

const CAutocompleteOption = ({
  values,
  placeholder,
  name,
  control,
  rules,
  multiple,
  onBlur,
  clasName,
  isInline,
  disableCloseOnSelect,
  ListboxProps,
  validateLenght,
  onBlurAutocomplete,
  open,
  defaultValue,
}: propsCAutocompleteOption) => {
  return (
    <Box sx={{ width: "100%" }}>
      <Controller
        name={name}
        control={control}
        rules={rules}
        defaultValue={defaultValue}
        render={({ field: { onChange, value }, fieldState: { error } }) => {
          return (
            <Autocomplete
              limitTags={6}
              open={open}
              className="CAutocomplete"
              multiple={multiple}
              options={values}
              disableCloseOnSelect={disableCloseOnSelect}
              ListboxProps={ListboxProps}
              getOptionLabel={(option) =>
                multiple ? option.name : option?.name ?? ""
              }
              isOptionEqualToValue={(option, value) => option.id === value.id}
              filterSelectedOptions
              value={!multiple ? (value ? value : null) : value ? value : []}
              onChange={(event, value) => {
                onChange(value);
              }}
              onBlur={onBlurAutocomplete}
              renderOption={(props, option) =>
                isInline ? (
                  <span
                    {...props}
                    tabIndex={1}
                    key={option.id}
                    className={clasName}
                  >
                    {option.name}
                  </span>
                ) : (
                  <li
                    {...props}
                    tabIndex={1}
                    key={option.id}
                    className={clasName}
                  >
                    {option.name}
                  </li>
                )
              }
              renderInput={(params) => {
                return (
                  <TextField
                    {...params}
                    value={value?.name}
                    error={error?.message !== undefined}
                    onBlur={onBlur}
                    placeholder={
                      value?.length !== 0 && value ? "" : placeholder
                    }
                    InputProps={{
                      ...params.InputProps,
                      endAdornment: (
                        <InputAdornment position="end">
                          {!validateLenght ? (
                            error?.message === undefined &&
                            value?.length !== 0 &&
                            value ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )
                          ) : error?.message === undefined &&
                            value?.length >= 4 &&
                            value?.length <= 6 &&
                            value ? (
                            <BsCheck2 fontSize={18} color="#30d69a" />
                          ) : (
                            ""
                          )}
                        </InputAdornment>
                      ),
                    }}
                  />
                );
              }}
              clearIcon={false}
              forcePopupIcon={false}
            />
          );
        }}
      />
    </Box>
  );
};

export default CAutocompleteOption;
