/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import React from "react";
import { IoIosCopy } from "react-icons/io";
import {
  FacebookIcon,
  FacebookShareButton,
  EmailIcon,
  EmailShareButton,
  TelegramIcon,
  TelegramShareButton,
  LineIcon,
  LineShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TwitterIcon,
  TwitterShareButton,
} from "react-share";
import CButtonIcon from "./CButtonIcon";

type CShareProps = {
  title: string;
};

export default function CShare({ title }: CShareProps) {
  const shareUrl = typeof window !== "undefined" ? window.location.href : "";
  const handelCoppied = () => {
    navigator.clipboard.writeText(shareUrl);
  };
  return (
    <div className="share">
      <div className="share-container">
        <div className="share-item">
          <FacebookShareButton
            url={shareUrl}
            quote={title}
            hashtag="#hireit.io"
          >
            <FacebookIcon size={32} round />
          </FacebookShareButton>
          <span>Facebook</span>
        </div>
        <div className="share-item">
          <EmailShareButton url={shareUrl} title={title} about={title}>
            <EmailIcon size={32} round />
          </EmailShareButton>
          <span>Email</span>
        </div>
        <div className="share-item">
          <LineShareButton url={shareUrl} title={title}>
            <LineIcon size={32} round />
          </LineShareButton>
          <span>Line</span>
        </div>
        <div className="share-item">
          <TelegramShareButton url={shareUrl} title={title}>
            <TelegramIcon size={32} round />
          </TelegramShareButton>
          <span>Telegram</span>
        </div>
        <div className="share-item">
          <LinkedinShareButton url={shareUrl} title={title}>
            <LinkedinIcon size={32} round />
          </LinkedinShareButton>
          <span>Linked</span>
        </div>
        <div className="share-item">
          <TwitterShareButton url={shareUrl} title={title}>
            <TwitterIcon size={32} round />
          </TwitterShareButton>
          <span>Twitter</span>
        </div>
      </div>
      <div className="share-container bottom">
        <div className="share-item">
          <CButtonIcon onclick={handelCoppied} icon={<IoIosCopy />} />
          <span>Coppy</span>
        </div>
      </div>
    </div>
  );
}
