import { Stack, Typography } from "@mui/material";
import { useMemo } from "react";

import CSpinner from "./CSpinner";

type LoadingProps = {
  size?: "small" | "medium" | "large";
  fullScreen?: boolean;
  label?: string;
};

const CLoading = ({ size = "medium", fullScreen, label }: LoadingProps) => {
  const labelSize = useMemo(
    () => (size === "small" ? size : undefined),
    [size]
  );

  return (
    <Stack
      paddingY={fullScreen ? 0 : 4}
      width="100%"
      height={fullScreen ? "50vh" : "unset"}
      justifyContent="flex-end"
      alignItems="center"
      spacing={2}
    >
      <CSpinner size={size} />
      {label && <Typography fontSize={labelSize}>{label}</Typography>}
    </Stack>
  );
};

export default CLoading;
