import * as React from "react";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import { toast } from "react-toastify";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  padding: 20,
  bgcolor: "background.paper",
  border: "0px solid #000",
  boxShadow: 24,
  p: 2,
  borderRadius: 4,
};

type CModalProps = {
  modal: any;
  button: any;
  isOpenModal?: boolean;
  setIsOpenModal?: React.Dispatch<React.SetStateAction<boolean>>;
  confirm?: boolean;
  disable?: boolean;
  textToast?: string;
};

export default function CModal({
  isOpenModal,
  modal,
  button,
  disable = false,
  setIsOpenModal,
  confirm: isConfirm,
  textToast,
}: CModalProps) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);

  const confirmClose = () => {
    let text = "You confirm cancle Form \nPress Cancel to continue or OK.";
    confirm(text) === true && setOpen(false);
  };

  const handleClose = React.useCallback(() => {
    if (isConfirm) {
      confirmClose();
    } else setOpen(false);
  }, [isConfirm]);

  React.useEffect(() => {
    if (isOpenModal === false) {
      setOpen(false);
    }
  }, [isOpenModal]);

  return (
    <div>
      <div
        onClick={() => {
          if (!disable) {
            handleOpen();
            setIsOpenModal && setIsOpenModal(true);
          } else {
            textToast && toast.warning(textToast);
          }
        }}
      >
        {button}
      </div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>{modal}</Box>
      </Modal>
    </div>
  );
}
