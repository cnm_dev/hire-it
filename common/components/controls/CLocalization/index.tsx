/* eslint-disable @next/next/no-img-element */
import React from "react";
import image from "@/common/assets/image/all-image";
import { ArrowDown2, ArrowUp2 } from "iconsax-react";
import { useDispatch } from "react-redux";
import CLocalizationSlice from "./CLocalizationSlice";
import { langSelector } from "@/redux/selector";
import { useSelector } from "react-redux";
type CLocalizationProps = {
  hideMenu: boolean;
};
const CLocalization = ({ hideMenu }: CLocalizationProps) => {
  const dispatch = useDispatch();
  const lang: "en" | "vn" = useSelector(langSelector);
  const [activeSelected, setActiveSelected] = React.useState<boolean>(false);

  const handelActiveSelected = (selected: string | undefined) => {
    setActiveSelected(!activeSelected);
    if (selected) {
      dispatch(CLocalizationSlice.actions.langSelectedChange(selected));
    }
  };

  React.useEffect(() => {
    const langCookie = document.cookie
      .split(";")
      .filter((lang) => lang.includes("lang="))[0];
    langCookie &&
      dispatch(
        CLocalizationSlice.actions.langSelectedChange(
          langCookie.trim().slice(5)
        )
      );
  }, [dispatch]);
  return (
    <div className="change-language">
      <div
        className="change-language__origin"
        onClick={() => handelActiveSelected("")}
      >
        {lang === "en" ? (
          <img src={image.language[0].en} alt="" />
        ) : (
          <img src={image.language[0].vn} alt="" />
        )}
        {hideMenu ? "" : lang === "en" ? "English" : "Việt Nam"}
        {hideMenu ? (
          ""
        ) : activeSelected ? (
          <ArrowUp2 size="20" />
        ) : (
          <ArrowDown2 size="20" />
        )}
      </div>
      <div
        className={`change-language__selected ${
          activeSelected ? "active" : ""
        }`}
      >
        <div
          className={`language-selected__item ${
            lang === "en" ? "active" : ""
          } ${hideMenu ? "hide" : ""}`}
          onClick={() => handelActiveSelected("en")}
        >
          <img src={image.language[0].en} alt="" /> {hideMenu ? "" : "English"}
        </div>
        <div
          className={`language-selected__item ${
            lang === "vn" ? "active" : ""
          } ${hideMenu ? "hide" : ""}`}
          onClick={() => handelActiveSelected("vn")}
        >
          <img src={image.language[0].vn} alt="" /> {hideMenu ? "" : "Việt Nam"}
        </div>
      </div>
    </div>
  );
};

export default CLocalization;
