import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "lang",
  initialState: {
    lang: "en",
  },
  reducers: {
    langSelectedChange: (state, action) => {
      // mutation
      state.lang = action.payload;
      document.cookie = `lang=${action.payload}`;
    },
  },
});
