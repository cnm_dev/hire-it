import * as React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import { FieldValues, UseFormRegister } from "react-hook-form";
interface PropTextField {
  label?: string;
  variant?: "outlined" | "standard" | "filled" | undefined;
  marginTop?: number;
  marginBottom?: number;
  placeholder?: string;
  borderRadius?: number;
  className?: string;
  type?: string;
  registerName?: any;
  name?: string;
  onChange?:
    | ((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => any)
    | undefined;
  defaultValue?: any;
  disable?: boolean;
}

export default function CTextField({
  name,
  registerName,
  label,
  variant = "outlined",
  placeholder,
  className,
  type,
  onChange,
  defaultValue,
  disable = false,
}: PropTextField) {
  return (
    <TextField
      defaultValue={defaultValue}
      onChange={onChange}
      {...registerName}
      name={name}
      className={className}
      id="outlined-basic"
      label={label}
      variant={variant}
      disabled={disable}
      placeholder={placeholder}
      type={type}
    />
  );
}
