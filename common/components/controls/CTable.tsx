import React, { useEffect, useState } from "react";
import { styled } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell, { tableCellClasses } from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Checkbox } from "@mui/material";

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: "#1b5198",
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  "&:nth-of-type(odd)": {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  "&:last-child td, &:last-child th": {
    border: 0,
  },
}));

type alignProps = {
  align: "center" | "left" | "right" | "inherit" | "justify" | undefined;
};

type CTable = {
  rows: any[];
  alignItem?: alignProps["align"][];
  alignTitle?: alignProps["align"][];
  label: string[];
  isCheck?: boolean;
  pagination?: number;
  lenghtFull?: number;
  setListIndexChosse?: React.Dispatch<React.SetStateAction<number[]>>;
  isClear?: boolean;
  setIsClear?: React.Dispatch<React.SetStateAction<boolean>>;
};

export default function CTable({
  pagination,
  rows,
  alignTitle,
  label,
  alignItem,
  isCheck,
  lenghtFull,
  setListIndexChosse,
  isClear,
  setIsClear,
}: CTable) {
  const [isCheckAll, setIsCheckAll] = useState<boolean | string>(false);
  const [listIndex, setListIndex] = useState<number[]>([]);

  useEffect(() => {
    if (pagination && isCheck) {
      if (lenghtFull && isCheckAll === "multiple") {
        if (listIndex.length - 1 === lenghtFull) {
          setIsCheckAll(true);
        }
      }
    }
  }, [isCheck, isCheckAll, lenghtFull, listIndex, pagination]);

  useEffect(() => {
    if (pagination && isCheck) {
      if (isCheckAll === true && lenghtFull) {
        let list: number[] = [];
        for (let i = 0; i < lenghtFull; i++) {
          list.push(i);
        }
        setListIndex(list);
      }
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isCheckAll, rows, pagination, lenghtFull]);

  useEffect(() => {
    setListIndexChosse && setListIndexChosse(listIndex);
  }, [listIndex, setListIndexChosse]);

  useEffect(() => {
    if (isClear && setIsClear) {
      setIsCheckAll(false);
      setListIndex([]);
      setListIndexChosse && setListIndexChosse([]);
      setIsClear(false);
    }
  }, [isClear, setIsClear, setListIndexChosse]);

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 700 }} aria-label="customized table">
        <TableHead>
          <TableRow>
            {isCheck && pagination && (
              <StyledTableCell>
                <Checkbox
                  indeterminate={isCheckAll === "multiple" ? true : false}
                  checked={isCheckAll === false ? false : true}
                  onChange={(e) => {
                    if (e.target.checked) {
                      setIsCheckAll(true);
                    } else {
                      if (isCheckAll === "multiple" || isCheckAll === true)
                        setListIndex([]);
                      setIsCheckAll(false);
                    }
                  }}
                />
              </StyledTableCell>
            )}
            {label.map((title, index) => (
              <StyledTableCell
                key={index}
                align={alignTitle && alignTitle[index]}
              >
                {title}
              </StyledTableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row: any[], index) => (
            <StyledTableRow
              className={
                pagination
                  ? `${
                      listIndex.includes(index + (pagination - 1) * 10)
                        ? "check"
                        : ""
                    } `
                  : ""
              }
              key={index}
              onClick={(e: any) => {
                const classClick = e.target.className.toString();
                if (
                  !"[object SVGAnimatedString]".includes(classClick) &&
                  !classClick.toString().includes("MuiButtonBase-root")
                ) {
                  if (pagination) {
                    if (listIndex.includes(index + (pagination - 1) * 10)) {
                      isCheckAll === true && setIsCheckAll("multiple");
                      setListIndex(
                        listIndex.filter(
                          (indexItem) =>
                            indexItem !== index + (pagination - 1) * 10
                        )
                      );
                    } else {
                      setListIndex([
                        ...listIndex,
                        index + (pagination - 1) * 10,
                      ]);
                    }
                  }
                }
              }}
            >
              {isCheck && pagination && (
                <StyledTableCell>
                  <Checkbox
                    checked={listIndex.includes(index + (pagination - 1) * 10)}
                    onChange={(e) => {
                      if (e.target.checked) {
                        setListIndex([
                          ...listIndex,
                          index + (pagination - 1) * 10,
                        ]);
                      } else {
                        isCheckAll === true && setIsCheckAll("multiple");
                        setListIndex(
                          listIndex.filter(
                            (indexItem) =>
                              indexItem !== index + (pagination - 1) * 10
                          )
                        );
                      }
                    }}
                    color="primary"
                  />
                </StyledTableCell>
              )}
              {row.map((rowItem, indexItem) => (
                <StyledTableCell
                  align={alignItem && alignItem[indexItem]}
                  key={indexItem}
                >
                  {rowItem}
                </StyledTableCell>
              ))}
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
