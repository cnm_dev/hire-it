import * as React from "react";
import IconButton from "@mui/material/IconButton";

type CButtonIconProps = {
  className?: string;
  icon: any;
  onclick?: any;
  disabled?: boolean;
};
export default function CButtonIcon({
  className,
  icon,
  onclick,
  disabled = false,
}: CButtonIconProps) {
  return (
    <IconButton disabled={disabled} className={className} onClick={onclick}>
      {icon}
    </IconButton>
  );
}
