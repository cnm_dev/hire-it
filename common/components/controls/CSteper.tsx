/* eslint-disable react-hooks/rules-of-hooks */
import * as React from "react";
import Box from "@mui/material/Box";
import Stepper from "@mui/material/Stepper";
import Step from "@mui/material/Step";
import StepLabel from "@mui/material/StepLabel";
import { Clock } from "iconsax-react";

const steps = [
  "Getting Started",
  "Professional Experience",
  "Freelancing Preferences",
  "Profile Setup",
];

interface PropsCSteper {
  steps: { title: string; time?: string }[];
  initialState: number;
  initialStateCompleted: {
    [k: number]: boolean;
  };
}

export default function CSteper({
  steps,
  initialState,
  initialStateCompleted,
}: PropsCSteper) {
  const [activeStep, setActiveStep] = React.useState(initialState);
  const [completed, setCompleted] = React.useState<{
    [k: number]: boolean;
  }>(initialStateCompleted);
  React.useEffect(() => {
    setActiveStep(initialState);
    setCompleted(initialStateCompleted);
  }, [initialState, initialStateCompleted]);

  return (
    <Box sx={{ width: "100%" }}>
      {/* <Stepper nonLinear activeStep={activeStep} className="stepper"> */}
      <Stepper activeStep={activeStep} className="stepper">
        {steps.map((label, index) => (
          <Step key={index} completed={initialStateCompleted[index]}>
            <StepLabel>
              <div className="label">
                <p className="title">{label.title}</p>
                {/* {label.time && (
                  <div className="time">
                    <Clock size="14" color="rgba(0, 0, 0, 0.4)" />
                    <p>{label.time}</p>
                  </div>
                )} */}
              </div>
            </StepLabel>
          </Step>
        ))}
      </Stepper>
    </Box>
  );
}
