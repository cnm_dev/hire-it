import { Position } from "@/types/models";
import {
  FormControl,
  MenuItem,
  OutlinedInput,
  Select,
  SelectChangeEvent,
  Theme,
  useTheme,
} from "@mui/material";
import React from "react";
function getStyles(name: string, personName: readonly string[], theme: Theme) {
  return {
    fontWeight:
      personName.indexOf(name) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightMedium,
  };
}

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

interface position {
  id: string;
  positionName: string;
}

interface CFormSelect {
  names: position[];
  placeholder?: String;
  registerName?: any;
  className?: string | undefined;
}

const CFormSelect = ({
  names,
  placeholder,
  registerName,
  className,
}: CFormSelect) => {
  const theme = useTheme();
  const [personName, setPersonName] = React.useState<string[]>([]);

  const handleChange = (event: SelectChangeEvent<typeof personName>) => {
    const {
      target: { value },
    } = event;
    setPersonName(
      // On autofill we get a stringified value.
      typeof value === "string" ? value.split(",") : value
    );
  };
  return (
    <FormControl className={className}>
      <Select
        displayEmpty
        value={personName}
        onChange={handleChange}
        input={<OutlinedInput {...registerName} />}
        renderValue={(selected: any) => {
          if (selected.length === 0) {
            return <em> {placeholder}</em>;
          }

          return selected.join(", ");
        }}
        MenuProps={MenuProps}
        inputProps={{ "aria-label": "Without label" }}
      >
        <MenuItem disabled value="">
          <em>{placeholder}</em>
        </MenuItem>
        {names.map((name) => (
          <MenuItem
            key={name.id}
            value={name.positionName}
            style={getStyles(name.positionName, personName, theme)}
          >
            {name.positionName}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default CFormSelect;
