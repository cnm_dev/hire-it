import * as React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete, {
  AutocompleteChangeDetails,
  AutocompleteChangeReason,
} from "@mui/material/Autocomplete";

interface Option {
  label: string;
}
interface PropsAutoComplete {
  option: any;
  label?: string;
  placeholder?: string;
  className?: string;
  defaultValue?: any;
  onChange?:
    | ((
        event: React.SyntheticEvent<Element, Event>,
        value: Option,
        reason: AutocompleteChangeReason,
        details?: AutocompleteChangeDetails<unknown> | undefined
      ) => void)
    | undefined;
}

export default function CAutocomplete({
  option = [],
  label,
  placeholder,
  className,
  onChange,
  defaultValue,
}: PropsAutoComplete) {
  const isOptionEqualToValue = (option: any, value: any) => {
    return option.label === value.label;
  };
  return (
    <Autocomplete
      value={defaultValue}
      className={className}
      disableClearable
      id="auto-complete"
      getOptionLabel={(option: Option) => option.label}
      onChange={onChange}
      options={option}
      isOptionEqualToValue={isOptionEqualToValue}
      renderInput={(params) => (
        <TextField {...params} label={label} placeholder={placeholder} />
      )}
    />
  );
}
