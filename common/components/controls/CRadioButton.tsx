import { Box, FormControlLabel, Radio } from "@mui/material";
import React from "react";

interface PropsCRadio {
  item?: String;
  name?: string | undefined;
  state?: String;
  setState: React.Dispatch<React.SetStateAction<string | undefined>>;
  label?: string;
  registerName?: any;
  onFocus?: React.FocusEventHandler<HTMLButtonElement> | undefined;
  onChange?:
    | ((event: React.ChangeEvent<HTMLInputElement>, checked: boolean) => void)
    | undefined;
}

const CRadioButton = ({
  item,
  name,
  state,
  setState,
  label,
  registerName,
  onFocus,
  onChange,
}: PropsCRadio) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState(event.target.value);
  };

  const handleBoxClick = () => {};

  return (
    <Box onClick={handleBoxClick}>
      <FormControlLabel
        className="cradio_button"
        control={
          <Radio
            {...registerName}
            onFocus={onFocus}
            size="small"
            checked={state === item}
            value={item}
            name={name}
            onChange={onChange ? onChange : handleChange}
            color="primary"
          />
        }
        label={label}
      />
    </Box>
  );
};

export default CRadioButton;
