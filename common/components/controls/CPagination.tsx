import * as React from "react";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";

interface PropPagination {
  count: number;
  SetPagination: React.Dispatch<React.SetStateAction<number>>;
}

const CPagination = ({ count, SetPagination }: PropPagination) => {
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    SetPagination(value);
  };

  return (
    <Stack>
      <Pagination
        boundaryCount={3}
        onChange={handleChange}
        count={count}
        variant="outlined"
        color="primary"
      />
    </Stack>
  );
};
export default CPagination;
