import React from "react";
import { Calendar, EventPropGetter, momentLocalizer } from "react-big-calendar";
import "react-big-calendar/lib/css/react-big-calendar.css";
import moment from "moment";
const localizer = momentLocalizer(moment);

export type TypeEvent = {
  title?: string;
  start?: Date;
  end?: Date;
  type?: string;
};

type CBigCalendarProps = {
  events: TypeEvent[];
  eventPropGetter: EventPropGetter<TypeEvent> | undefined;
};

const CBigCalendar = ({ events, eventPropGetter }: CBigCalendarProps) => {
  return (
    <Calendar
      localizer={localizer}
      events={events}
      defaultDate={moment().toDate()}
      startAccessor="start"
      endAccessor="end"
      style={{ height: 650 }}
      eventPropGetter={eventPropGetter}
    />
  );
};

export default CBigCalendar;
