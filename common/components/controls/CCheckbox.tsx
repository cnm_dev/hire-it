import React from "react";
import Checkbox from "@mui/material/Checkbox";
import FormGroup from "@mui/material/FormGroup";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormControl from "@mui/material/FormControl";

interface PropsCheckox {
  arrOption: {
    value: string;
    label: string;
  }[];
  setValue?: React.Dispatch<React.SetStateAction<boolean>>;
  setLabel?: React.Dispatch<React.SetStateAction<string[]>>;
  label?: string[];
}

const CCheckbox = ({ arrOption, setValue, setLabel, label }: PropsCheckox) => {
  return (
    <FormControl component="fieldset">
      <FormGroup
        aria-label="position"
        row
        onChange={(e: any) => {
          setValue && setValue(e.target.checked);
          if (setLabel && label) {
            if (e.target.checked) {
              setLabel([...label, e.target.value]);
            } else {
              setLabel(
                label.filter((labelItem) => labelItem !== e.target.value)
              );
            }
          }
        }}
      >
        {arrOption.map((option, index) => (
          <FormControlLabel
            key={index}
            value={option.value}
            control={<Checkbox />}
            label={option.label}
            labelPlacement="end"
          />
        ))}
      </FormGroup>
    </FormControl>
  );
};

export default CCheckbox;
