import * as React from "react";
import Box from "@mui/material/Box";
import TextField from "@mui/material/TextField";
import {
  Control,
  Controller,
  FieldValues,
  RegisterOptions,
  UseFormRegister,
} from "react-hook-form";
import { TextareaAutosize } from "@mui/material";
import { BsCheck2 } from "react-icons/bs";
interface PropTextField {
  placeholder?: string;
  className?: string;
  registerName?: any;
  name: string | "";
  minCharacter: number | 255;
  minRows?: number | undefined;
  onChange?:
    | ((e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => any)
    | undefined;
  onBlur?: React.FocusEventHandler<HTMLTextAreaElement> | undefined;
  control?: Control<any> | undefined;
  rules?: Omit<
    RegisterOptions<FieldValues, string>,
    "disabled" | "valueAsNumber" | "valueAsDate" | "setValueAs"
  >;
}

export default function CTextErea({
  name,
  registerName,
  placeholder,
  className,
  minCharacter,
  minRows,
  onBlur,
  control,
  rules,
}: PropTextField) {
  const [character, setCharacter] = React.useState(0);

  return (
    <Controller
      name={name}
      control={control}
      rules={rules}
      render={({ field: { onChange, value }, fieldState: { error } }) => {
        return (
          <div className="TextareaAutosize-contain">
            <TextareaAutosize
              onBlur={onBlur}
              value={value}
              onChange={(e) => {
                if (e.target.value.length === 0) {
                  setCharacter(e.target.value.length);
                } else if (e.target.value.length < minCharacter) {
                  setCharacter(minCharacter - e.target.value.length);
                } else {
                  setCharacter(e.target.value.length);
                }
                onChange(e.target.value);
              }}
              {...registerName}
              name={name ?? 3}
              aria-label="minimum height"
              minRows={minRows}
              className={
                error?.message !== undefined ? "error-textarea" : className
              }
              placeholder={placeholder}
              style={{ width: 200 }}
            />
            <span>
              {character === 0
                ? ""
                : character >= minCharacter
                ? character + " characters entered"
                : character + " characters left"}

              {error?.message === undefined &&
              value?.length !== 0 &&
              character !== 0 ? (
                <BsCheck2 fontSize={13} color="#30d69a" />
              ) : (
                ""
              )}
            </span>
          </div>
        );
      }}
    />
  );
}
