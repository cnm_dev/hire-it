/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import React from "react";

const CloadingModern = () => {
  return (
    <div className="loading-modern">
      <img src={image.loading} alt="" />
    </div>
  );
};

export default CloadingModern;
