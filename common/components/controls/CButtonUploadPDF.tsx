/* eslint-disable @next/next/no-img-element */
import { CircularProgress } from "@mui/material";
import { CloudNotif } from "iconsax-react";
import React, { useState, useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { toast } from "react-toastify";

interface propsResume {
  onDrop?: any;
  isLoading?: boolean;
}

const CButtonUploadPDF = ({ onDrop, isLoading }: propsResume) => {
  const handleDrop = useCallback(
    (acceptedFiles: any[], fileRejections: any[]) => {
      if (fileRejections.length > 0) {
        toast.error(
          "apache-tomcat-9.0.74.pdf is over the 5MB size limit. Please upload a different file."
        );
      } else {
        const file = acceptedFiles[0];
        onDrop(file);
      }
    },
    [onDrop]
  );

  const { getRootProps, getInputProps, fileRejections } = useDropzone({
    onDrop: handleDrop,
    accept: { "application/pdf": [".pdf"] },
    maxSize: 5 * 1024 * 1024,
  });

  return (
    <div className="CButtonUploadPDF" {...getRootProps()}>
      <input {...getInputProps()} />
      {isLoading ? (
        <CircularProgress color="inherit" size={15} />
      ) : (
        <span>Upload</span>
      )}
    </div>
  );
};

export default CButtonUploadPDF;
