/* eslint-disable @next/next/no-img-element */
import { CloudNotif } from "iconsax-react";
import React, { useState, useCallback } from "react";
import { useDropzone } from "react-dropzone";

interface propsAvatarDropzone {
  onDrop?: any;
  imgDefult?: any;
  isResume?: boolean;
}

const AvatarDropzone = ({ onDrop, imgDefult }: propsAvatarDropzone) => {
  const [fileUrl, setFileUrl] = useState<any>(null);

  const handleDrop = useCallback(
    (acceptedFiles: any[]) => {
      const file = acceptedFiles[0];
      const fileUrl = URL.createObjectURL(file);
      setFileUrl(fileUrl);
      onDrop(file);
    },
    [onDrop]
  );

  const { getRootProps, getInputProps } = useDropzone({
    onDrop: handleDrop,
    accept: {
      "image/*": [".jpeg", ".png"],
    },
  });

  return (
    <div className="CDropzoneImg" {...getRootProps()}>
      <input {...getInputProps()} />
      {fileUrl ? (
        <img src={fileUrl} alt="Avatar" />
      ) : (
        <img src={imgDefult} alt="Avatar" />
      )}
      <div className="CDropzoneImg-container">
        <CloudNotif color="#fff" size="28" />
        <span>Upload</span>
      </div>
    </div>
  );
};

export default AvatarDropzone;
