import { updateInformates } from "@/apis/Informates";
import { Informate } from "@/types/models";
import { intervalTime } from "@/utils";
import { MessageNotif, MessageRemove } from "iconsax-react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React from "react";
import { toast } from "react-toastify";

type CItemInformateProps = {
  informate: Informate;
  handleClose: () => void;
};

const CItemInformate = ({ informate, handleClose }: CItemInformateProps) => {
  const { data: session } = useSession();
  const router = useRouter();
  const handelUpdateRead = () => {
    updateInformates({
      userId: session?.user.id,
      informateId: informate.id,
      read: true,
    })
      .then(() => {
        handleClose();
        router.replace(informate.route);
      })
      .catch((err) => toast.error(err.message));
  };
  return (
    <div
      onClick={handelUpdateRead}
      className={`informate-item ${informate.read ? "" : "new"}`}
    >
      <div className="informate-item__svg">
        {informate.type === "default" ? (
          <MessageNotif size="30" color="#decd52" />
        ) : (
          <MessageRemove size="30" color="#e44d4d" />
        )}
      </div>
      <div className="informate-item__context">
        <div className="informate-context">
          <h4>{informate.title}</h4>
          <span>{intervalTime(new Date(informate.createdAt))} </span>
        </div>
        <div className="informate-context">
          <span>{informate.description}</span>
        </div>
      </div>
    </div>
  );
};

export default CItemInformate;
