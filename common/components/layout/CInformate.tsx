import * as React from "react";
import Menu from "@mui/material/Menu";
import CItemInformate from "./CItemInformate";
import CButtonIcon from "../controls/CButtonIcon";
import CBadge from "../controls/CBadge";
import CLoading from "../controls/CLoading";
import { useSelector } from "react-redux";
import { informatesSelector } from "@/redux/selector";
import { MessageNotif } from "iconsax-react";

type CInformateProps = {
  IconButton: any;
  isLoading: boolean;
};

export default function CInformate({ IconButton, isLoading }: CInformateProps) {
  const informates = useSelector(informatesSelector);
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const countInformateNotRead = React.useMemo(() => {
    const sum = informates.reduce((accumulator, informate) => {
      if (!informate.read) {
        return accumulator + 1;
      } else {
        return accumulator;
      }
    }, 0);
    return sum;
  }, [informates]);

  return (
    <div className="informate-container">
      <CBadge
        onClick={handleClick}
        color={"error"}
        numberInfo={countInformateNotRead}
        icon={<CButtonIcon icon={IconButton} />}
      />
      <Menu
        id="menu-formate"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
      >
        {isLoading ? (
          <CLoading />
        ) : (
          <div className="informate">
            {informates.length === 0 ? (
              <div className="not-informate">
                <MessageNotif size="30" color="#decd52" />
                <span>Not informates</span>
              </div>
            ) : (
              informates.map((informate, index) => (
                <CItemInformate handleClose={handleClose} informate={informate} key={index} />
              ))
            )}
          </div>
        )}
      </Menu>
    </div>
  );
}
