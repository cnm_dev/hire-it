/* eslint-disable @next/next/no-img-element */
import React, { useMemo } from "react";
// image

// icon
import { ArrowLeft2, ArrowRight2, LogoutCurve } from "iconsax-react";

// library

import image from "@/common/assets/image/all-image";
import { all_api } from "@/mock/all_api";
import { langs } from "@/mock/langs";
import { TypeLang } from "@/modules/Home/components/HotMember";
import { Button } from "@mui/material";
import { signOut, useSession } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import { Sessions } from "types/models";
import CButton from "../controls/CButton";
import CLocalization from "../controls/CLocalization/index";
import CTooltip from "../controls/CTooltip";
const Header = ({ lang }: TypeLang) => {
  const router = useRouter();
  const [hideMenu, setHideMenu] = React.useState<boolean>(false);
  const { status, data }: Sessions = useSession();
  const list_menu = useMemo(() => {
    if (data) {
      if (data.user?.type_user === "Admin") {
        return lang == "en" ? all_api.menu_admin : all_api.menu_admin_VN;
      } else if (data.user?.type_user === "Customer") {
        return lang == "en" ? all_api.menu_customer : all_api.menu_customer_VN;
      } else if (data.user?.type_user === "Employee") {
        return lang == "en" ? all_api.menu_staff : all_api.menu_staff_vn;
      }
    } else return all_api.menu_default;
  }, [data, lang]);

  return (
    <header className={hideMenu ? "hide" : ""}>
      <div className="header-container">
        <div className="header-top">
          <Link href="/">
            <div className="header-logo">
              <img src={image.logo_gif} alt="" />
              <div className="logo-content">
                <h3>HireIT.io</h3>
                <p>IUH Ho Chi Minh</p>
              </div>
            </div>
          </Link>
          {status !== "unauthenticated" && data && data.user && (
            <div className="header-profile">
              <img
                src={
                  data.user?.avatar ? data.user?.avatar : image.image_default
                }
                alt=""
              />
              <div className="header-profile__information">
                <h3>{data.user?.fullName}</h3>
                <span>
                  {data.user?.type_user === "Customer"
                    ? langs.header.typeCustomer[lang]
                    : data.user?.type_user === "Admin"
                    ? langs.header.typeAdmin[lang]
                    : langs.header.typeEmployee[lang]}
                </span>
              </div>
            </div>
          )}
        </div>
        <div className="header-body">
          <nav>
            {list_menu?.map((menu) => (
              <li key={menu.url}>
                <Link
                  className={`${
                    router.pathname === menu.url ||
                    (router.pathname.includes(menu.url) && menu.url !== "/")
                      ? "active"
                      : ""
                  } nav-link`}
                  href={menu.url}
                >
                  {hideMenu ? (
                    <CTooltip title={menu.label} child={menu.icon} />
                  ) : (
                    menu.icon
                  )}
                  <span>{menu.label}</span>
                </Link>
              </li>
            ))}
          </nav>

          <CLocalization hideMenu={hideMenu} />
        </div>
        <div className="header-bottom">
          {data && data.user?.type_user === "Employee" && (
            <>
              <div className="header-bottom__content">
                {/* <h3>{langs.header.appy[lang]}</h3>
                <p>{langs.header.join[lang]}</p> */}
              </div>
              <div className="header-bottom__image">
                {/* <Image src={menubottom} alt="" /> */}
              </div>
            </>
          )}
        </div>
      </div>
      <div className="header-hide">
        <CButton
          onClick={() => setHideMenu(!hideMenu)}
          text={hideMenu ? <ArrowRight2 /> : <ArrowLeft2 />}
        />
      </div>

      {data && data.user?.type_user !== "Customer" && (
        <div className="logout">
          <Button
            startIcon={<LogoutCurve size="22" color="#555555" />}
            onClick={() => {
              signOut({
                callbackUrl: "/auth/login",
              });
            }}
          >
            Log out
          </Button>
        </div>
      )}
    </header>
  );
};

export default Header;
