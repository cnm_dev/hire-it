/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable @next/next/no-img-element */
import React, { useState } from "react";
import CModal from "../controls/CModal";
import CButton from "../controls/CButton";

type COptionModelProps = {
  notice: string;
  buttonOpen: JSX.Element;
  handel: () => void;
  title?: string;
};

const COptionModel = ({
  notice,
  buttonOpen,
  handel,
  title,
}: COptionModelProps) => {
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
  const handelModalOption = (status: boolean) => {
    if (status) {
      handel();
    }
    setIsOpenModal(false);
  };
  return (
    <CModal
      setIsOpenModal={setIsOpenModal}
      isOpenModal={isOpenModal}
      modal={
        <div className="option-modal">
          <img src="https://res.cloudinary.com/hire-it/image/upload/v1684385658/People_Group_Logo_4_zspfkl.gif" />
          <span>{notice}</span>
          <p>{title}</p>
          <div className="option-modal__btn">
            <CButton
              color="error"
              text="No"
              onClick={() => handelModalOption(false)}
            />
            <CButton
              color="success"
              text="Yes"
              onClick={() => handelModalOption(true)}
            />
          </div>
        </div>
      }
      button={buttonOpen}
    />
  );
};

export default COptionModel;
