/* eslint-disable @next/next/no-img-element */
import React from "react";

// icon
import { Facebook, Youtube, Instagram, Send2, Sms } from "iconsax-react";
import CTextField from "../controls/CTextField";
import CButton from "../controls/CButton";
import { TypeLang } from "@/modules/Home/components/HotMember";
import { langs } from "@/mock/langs";
import image from "@/common/assets/image/all-image";
const CFooter = ({ lang }: TypeLang) => {
  return (
    <footer>
      <div className="footer-logo">
        <img src={image.logo_footer} alt="" />
        <h4>HireIT.io</h4>
      </div>
      <div className="footer-function">
        <div className="footer-function__title">
          {langs.footer.company[lang]}
        </div>
        <div className="footer-function__item">{langs.footer.about[lang]}</div>
        <div className="footer-function__item">
          {langs.footer.contact[lang]}
        </div>
        <div className="footer-function__item">
          {langs.footer.feature[lang]}
        </div>
        <div className="footer-function__item">
          {langs.footer.discover[lang]}
        </div>
      </div>
      <div className="footer-contact">
        <div className="footer-contact__email">
          <h5>{langs.footer.getour[lang]}</h5>
          <div className="footer-email__input">
            <Sms />
            <CTextField placeholder={langs.footer.youremai[lang]} />
            <CButton text={langs.footer.subcrite[lang]} />
          </div>
        </div>
        <div className="footer-contact__media">
          <h5>{langs.footer.joinour[lang]}</h5>
          <div className="footer-media__container">
            <Facebook /> <Youtube /> <Instagram /> <Send2 />
          </div>
        </div>
      </div>
    </footer>
  );
};

export default CFooter;
