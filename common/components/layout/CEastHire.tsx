/* eslint-disable @next/next/no-img-element */
import imageLogo from "@/modules/assets/imgLogo";
import Image from "next/image";
import React from "react";

const CEastHire = () => {
  return (
    <div className="main-CEastHire">
      <div className="show-companies">
        <p className="title">TRUSTED BY</p>
        <div className="container-logo">
          {imageLogo.logoGif.map((item, index) => (
            <div className="container-logo-hover" key={index}>
              <img className="image-logo" src={item.logo} alt="" />
              <img className="image-logoGif" src={item.logoGif} alt="" />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default CEastHire;
