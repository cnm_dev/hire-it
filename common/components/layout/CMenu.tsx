/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useRef, useState } from "react";

// icon
import {
  User,
  NotificationBing,
  Home as HomeIcon,
  Setting2,
  MessageQuestion,
  LogoutCurve,
  SearchNormal1,
} from "iconsax-react";

import image from "../../assets/image/all-image";
// component
import CDropMenu from "../controls/CDropMenu";
import CTextField from "@/common/components/controls/CTextField";
import CButton from "@/common/components/controls/CButton";
import { langs } from "@/mock/langs";

import { signOut, useSession } from "next-auth/react";
import Link from "next/link";
import CInformate from "./CInformate";
import useInformates from "@/hooks/useInformates";
import { mutate } from "swr";
import InformatesSlice from "@/redux/Slices/InformatesSlice";
import { useDispatch } from "react-redux";
interface interfacePropCmenu {
  isSearch?: boolean;
  lang: "en" | "vn";
}

const CMenu = ({ isSearch = true, lang }: interfacePropCmenu) => {
  const [valueSearch, setValueSearch] = useState<string>("");
  const { status, data: session } = useSession();
  const { informates, loading } = useInformates({
    userId: session?.user.id,
    typeUser: session?.user.type_user,
  });
  const dispatch = useDispatch();

  const myInterval = useRef<any>();

  useEffect(() => {
    if (status === "authenticated" && session) {
      myInterval.current = setInterval(() => {
        mutate(`/api/accounts/${session.user.id}/informate/${session.user.type_user}`);
      }, 3 * 1000);

      return () => {
        clearInterval(myInterval.current);
      };
    }
  }, [session, status]);

  useEffect(() => {
    if (!loading) {
      dispatch(InformatesSlice.actions.listInformats(informates));
    }
  }, [dispatch, informates, loading]);

  const handelLogOut = () => {
    signOut({
      callbackUrl: "/auth/login",
    });
  };

  return (
    <div className="home-header">
      <div className="home-header__account">
        <div className="header-account__avatar">
          <img className="avatar" src={image.avatar.avatar_1} alt="" />
          <img className="avatar" src={image.avatar.avatar_2} alt="" />
          <img className="avatar" src={image.avatar.avatar_3} alt="" />
          <img className="avatar" src={image.avatar.avatar_4} alt="" />
          <img className="avatar" src={image.avatar.avatar_5} alt="" />
          <img className="avatar" src={image.avatar.avatar_6} alt="" />
          <img className="avatar" src={image.avatar.avatar_7} alt="" />
          <img className="avatar" src={image.avatar.avatar_8} alt="" />
          <img className="avatar" src={image.avatar.avatar_9} alt="" />
        </div>
        {status !== "unauthenticated" ? (
          session?.user.type_user === "Employee" &&
          session?.user.employee.completionLevel < 4 ? (
            <Link href="/member/application_info/edit">
              <CButton
                className="btn-apply__freelancer"
                text="Appy Freelancer"
              />
            </Link>
          ) : (
            <div className="header-account__function">
              <CDropMenu
                arrMenu={
                  session?.user.type_user === "Admin"
                    ? [
                        {
                          title: langs.nav.setting[lang],
                          href: "/",
                          icon: <Setting2 />,
                        },
                        {
                          title: langs.nav.help[lang],
                          href: "/",
                          icon: <MessageQuestion />,
                        },
                        {
                          title: langs.nav.log[lang],
                          icon: <LogoutCurve />,
                          onClick: handelLogOut,
                        },
                      ]
                    : [
                        {
                          title: langs.nav.profile[lang],
                          href: "/auth/profile",
                          icon: <HomeIcon />,
                        },
                        {
                          title: langs.nav.setting[lang],
                          href: "/",
                          icon: <Setting2 />,
                        },
                        {
                          title: langs.nav.help[lang],
                          href: "/",
                          icon: <MessageQuestion />,
                        },
                        {
                          title: langs.nav.log[lang],
                          icon: <LogoutCurve />,
                          onClick: handelLogOut,
                        },
                      ]
                }
                icon={<User />}
              />
              <CInformate
                isLoading={loading}
                IconButton={<NotificationBing />}
              />
            </div>
          )
        ) : (
          <Link href="/auth/login" className="btn-menu__login">
            <CButton text="Login Now" />
          </Link>
        )}
      </div>
      {isSearch && (
        <div className="home-header__search">
          <div className="home-search__container">
            <SearchNormal1 className="home-search__svg" />
            <CTextField
              label=""
              variant="filled"
              placeholder={langs.home.place_search[lang]}
              borderRadius={4}
              onChange={(e) => {
                setValueSearch(e.target.value);
              }}
            />
          </div>
          <Link href={`member?name=${valueSearch}`}>
            <CButton text={langs.home.go[lang]} />
          </Link>
        </div>
      )}
    </div>
  );
};

export default CMenu;
