const image = {
  logo_gif:
    "https://res.cloudinary.com/hire-it/image/upload/v1679980845/KLTN/Logo_tz3pl0.gif",
  logo_footer:
    "https://res.cloudinary.com/hire-it/image/upload/v1683914009/Colorful_Abstract_Globe_Marketing_Logo_czzbei.gif",
  image_default:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679642671/avatar_default_fvqjeu.gif",
  avatar: {
    avatar_1:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410294/Hire-it/Group_1_awfpzr.png",
    avatar_2:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410294/Hire-it/Group_2_xqxzkv.png",
    avatar_3:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410294/Hire-it/Group_4_plbux7.png",
    avatar_4:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410294/Hire-it/Group_5_fyhqh8.png",
    avatar_5:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410294/Hire-it/Group_6_bln4uu.png",
    avatar_6:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410295/Hire-it/Group_7_ymj5o2.png",
    avatar_7:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410295/Hire-it/Group_8_yeogll.png",
    avatar_8:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410295/Hire-it/Group_9_ynbisj.png",
    avatar_9:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410295/Hire-it/Group_10_myrpsa.png",
    avatar_10:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410295/Hire-it/Group_11_v8jtol.png",
    avatar_11:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410295/Hire-it/Group_12_agb8g8.png",
    avatar_12:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410295/Hire-it/Group_13_ttswge.png",
  },
  header_home:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679410798/img_gif_alx5tg.gif",
  hot_member: [
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1602481182506-6e15d0d2e44b_zx1rfq.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1603775020644-eb8decd79994_hata6r.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1593529467220-9d721ceb9a78_tchvwv.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1611166819965-87dc032ed901_koaek2.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1591973669966-52d2534d9087_ept2uw.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1601412436009-d964bd02edbc_qbrrov.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750318/Hire-it/photo-1611695434398-4f4b330623e6_wcgupi.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1600349230078-13945eb9d51d_zwa92j.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750318/Hire-it/photo-1611594547712-9e5d7da58684_lp6vf4.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1611310424006-42cf1e064288_l0lrxs.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1595956553066-fe24a8c33395_amvaot.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1514846326710-096e4a8035e0_nqilkt.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750317/Hire-it/photo-1578489758854-f134a358f08b_a1j9be.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1529626455594-4ff0802cfb7e_kpkf6h.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1500648767791-00dcc994a43e_bqoku5.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1509868918748-a554ad25f858_dqxyrw.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1499996860823-5214fcc65f8f_ukdibd.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1506794778202-cad84cf45f1d_y24k3d.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1556942057-94aaf3ae5d6e_ggofz4.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1553514029-1318c9127859_bybprt.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750316/Hire-it/photo-1507003211169-0a1dd7228f2d_lnetmq.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750315/Hire-it/photo-1544348817-5f2cf14b88c8_omll6v.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750315/Hire-it/photo-1545167622-3a6ac756afa4_z0nhlz.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750315/Hire-it/photo-1549351512-c5e12b11e283_bux48c.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750315/Hire-it/photo-1552374196-c4e7ffc6e126_keikjb.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750314/Hire-it/photo-1622559924472-2c2f69abb854_dnc2mt.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750315/Hire-it/photo-1544005313-94ddf0286df2_jh84cb.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750314/Hire-it/photo-1615813967515-e1838c1c5116_eobzjy.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750314/Hire-it/photo-1618835962148-cf177563c6c0_plnotb.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750314/Hire-it/photo-1614880353165-e56fac2ea9a8_esvxiy.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682750314/Hire-it/photo-1623580890503-9f7dca11d8ae_kwzgpk.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1438761681033-6461ffad8d80_sifn1j.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1542909168-82c3e7fdca5c_ie9y8i.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1570295999919-56ceb5ecca61_zdnpw5.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1474176857210-7287d38d27c6_xirs14.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1573140247632-f8fd74997d5c_uqlukn.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1547425260-76bcadfb4f2c_czjulp.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1588701177361-c42359b29f68_avcvn9.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1593104547489-5cfb3839a3b5_fauqzi.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643368/photo-1615567964485-0ee76b5b3410_iys5of.avif",
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643369/photo-1615473967657-9dc21773daa3_s3r4ii.avif",
  ],
  language: [
    {
      vn: "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679457592/viet-nam_oqsfyn.svg",
      en: "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679457536/en_hnvlww.svg",
    },
  ],
  explore_marketplace: {
    CSharp:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643361/using-pub-sub-in-c-net-to-build-a-chat-app_2x_xg9h9b.webp",
    PHP: "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643360/9909a789ae4e101f098f7023bc9eb9a7_frhn0i.webp",
    JS: "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643361/original-f8f0e578682738dba350240628eaa730_gbnzso.webp",
    Android:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643361/8fb969cd1635c2a5c2ca67c4c2f609dd_k8fn2y.webp",
    SQL: "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643360/6535a009d7444fac5ecfa9236995e9cb_x85fqg.webp",
    NodeJS:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643361/f16c1a475ed9091bb4d11190fbfb52f3_uxsczj.webp",
    Kotlin:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643361/38cc15ea2e3bd9d36c4ea59cec3e6f52_fjhd9z.webp",
    IOS: "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679643360/c170a9a4d64ad4dff24bac58529d26bb_gyxvwq.webp",
  },
  parent: {
    image_1:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410296/Hire-it/ProductImage_ggx85l.png",
    image_2:
      "https://res.cloudinary.com/dvdejvb2x/image/upload/v1675410296/Hire-it/ProductImage_1_yz76j4.png",
  },
  logo: "https://res.cloudinary.com/dvdejvb2x/image/upload/v1676885372/colorfilter_lukekt.png",
  profile_background:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1677166755/Group_1841_reswvi.png",
  payment:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1678765615/Hire-it/Vector_xbtzxs.png",
  job_detail:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679323421/Rectangle_513_n8bir0.png",
  contract:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1679842970/Hire-it/contract_gkxczq.webp",
  not_found:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1680535000/notfound_nseqto.webp",
  loading:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1680964058/loading_contract_hhn3ss.gif",
  upload:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1682092260/upload_zzjmpr.gif",
  upload_contract:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1683283048/image_processing20230124-847-13oci1i_bz4mjl.gif",
  dpf_logo:
    "https://res.cloudinary.com/dvdejvb2x/image/upload/v1684382541/pdf_logo_jtiuyh.avif",
  coppy:
    "https://www.pngfind.com/pngs/m/530-5305318_copy-icon-png-transparent-png.png",
  card_atm:
    "https://cdn0.iconfinder.com/data/icons/cash-card-add-on-colored/48/JD-11-512.png",
  VNPay:
    "https://itviec.com/rails/active_storage/representations/proxy/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBd2w2SHc9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--86c9a5822c522ee441f5a6e55a9c9cfdc61b09bc/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJY0c1bkJqb0dSVlE2RkhKbGMybDZaVjkwYjE5c2FXMXBkRnNIYVFJc0FXa0NMQUU9IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--15c3f2f3e11927673ae52b71712c1f66a7a1b7bd/logo%20VNPAY-02.png",
  Payment:
    "https://cdn.dribbble.com/users/274482/screenshots/4828558/media/774456fdd41fb9ae14ec16da5ccfef62.gif",
};

export default image;
