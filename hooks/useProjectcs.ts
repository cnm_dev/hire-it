import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Project } from "../types/models";

const useProjects = () => {
  const { data, error } = useSWR<ApiResult<Project[]>>(
    "/api/projects",
    fetcher
  );

  return {
    projects: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useProjects;
