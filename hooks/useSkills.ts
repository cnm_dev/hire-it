import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Skill } from "../types/models";

const useSkills = () => {
  const { data, error } = useSWR<ApiResult<Skill[]>>("/api/skills", fetcher);
  3;
  return {
    skills: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useSkills;
