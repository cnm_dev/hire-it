import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, JobPosting } from "../types/models";

const useJobPostings = () => {
  const { data, error } = useSWR<ApiResult<JobPosting[]>>(
    "/api/job-postings",
    fetcher
  );

  return {
    jobpostings: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useJobPostings;
