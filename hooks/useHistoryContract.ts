import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, ContractHistory } from "../types/models";
import { getContractHistoryForm } from "@/forms/historyContract";

const useHistoryContract = (queryVariable: getContractHistoryForm) => {
  const { contractId } = queryVariable;
  const { data, error } = useSWR<ApiResult<ContractHistory[]>>(
    `/api/contracts/${contractId}/contract_histories`,
    fetcher
  );

  return {
    historyContract: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useHistoryContract;
