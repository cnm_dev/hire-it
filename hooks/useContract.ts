import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Contract } from "../types/models";
import { GetContractForm } from "@/forms/contracts";

const useContract = (queryVariable: GetContractForm) => {
  const { customerId, contractId } = queryVariable;
  console.log(customerId);
  

  const { data, error } = useSWR<ApiResult<Contract[]>>(
    `/api/customers/${customerId}/contracts/${contractId}`,
    fetcher
  );

  return {
    contract: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useContract;
