import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, JobPosting } from "../types/models";
import { GetJobPostingForm } from "@/forms/jobPosting";

const useJobPosting = (params: GetJobPostingForm) => {
  const { postId } = params;

  const { data, error } = useSWR<ApiResult<JobPosting[]>>(
    `/api/job-postings/${postId}`,
    fetcher
  );

  return {
    jobposting: data?.data,
    loading: !error && !data,
    error,
  };
};

export default useJobPosting;
