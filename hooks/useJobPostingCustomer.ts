import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, JobPosting } from "../types/models";

const useJobPostingCustomer = (userId: string | string[] | undefined) => {
  const { data, error } = useSWR<ApiResult<JobPosting[]>>(
    `/api/customers/${userId}/job-posting`,
    fetcher
  );

  return {
    jobpostings: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useJobPostingCustomer;
