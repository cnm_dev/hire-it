// /* eslint-disable react-hooks/rules-of-hooks */
// import useSWR from "swr";

// import { fetcher } from "../services";
// import { ApiResult, Informate } from "../types/models";
// import { GetInformatesForm } from "@/forms/informates";

// const useInformates = (queryVariable: GetInformatesForm) => {
//   const { userId, typeUser } = queryVariable;

//   if (!userId) {
//     return {
//       informates: [],
//       loading: false,
//     };
//   }

//   const { data, error } = useSWR<ApiResult<Informate[]>>(
//     typeUser === "Customer"
//       ? `/api/accounts/${userId}/informate`
//       : `/api/accounts/${userId}/informate/admin`,
//     fetcher
//   );

//   return {
//     informates: data?.data || [],
//     loading: !error && !data,
//     error,
//   };
// };

// export default useInformates;

/* eslint-disable react-hooks/rules-of-hooks */
import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Informate } from "../types/models";
import { GetInformatesForm } from "@/forms/informates";

const useInformates = (queryVariable: GetInformatesForm) => {
  const { userId, typeUser } = queryVariable;

  const { data, error } = useSWR<ApiResult<Informate[]>>(
    !userId ? "" : `/api/accounts/${userId}/informate/${typeUser}`,
    fetcher
  );

  return {
    informates: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useInformates;
