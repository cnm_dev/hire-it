import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Employee } from "../types/models";
import { GetEmployeeForm } from "@/forms/employees";

const useEmployee = (params: GetEmployeeForm) => {
  const { profileId } = params;
  const { data, error } = useSWR<ApiResult<Employee[]>>(
    `/api/employees/${profileId}`,
    fetcher
  );

  return {
    employees: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useEmployee;
