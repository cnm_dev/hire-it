import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Contract } from "../types/models";
import { GetContractsEmployee } from "@/forms/employees";
import { ContractEmployee } from "@/modules/Calendar/Calendar";

const useContractsEmployee = (queryVariable: GetContractsEmployee) => {
  const { userEmployeeId } = queryVariable;

  const { data, error } = useSWR<ApiResult<ContractEmployee[]>>(
    `/api/employees/${userEmployeeId}/contracts`,
    fetcher
  );

  return {
    contracts: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useContractsEmployee;
