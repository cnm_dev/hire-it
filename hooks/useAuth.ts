import { useSession } from "next-auth/react";
import { useEffect } from "react";

const useAuth = () => {
  const { data, status } = useSession();

  // TODO: Log for development
  useEffect(() => {
  }, [data]);

  return {
    ...data,
    status,
    session: data,
    loading: status !== "authenticated",
  };
};

export default useAuth;
