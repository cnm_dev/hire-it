import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Contract } from "../types/models";
import { GetContractsForm } from "@/forms/contracts";

const useContracts = (queryVariable: GetContractsForm) => {
  const { customerId } = queryVariable;

  const { data, error } = useSWR<ApiResult<Contract[]>>(
    `/api/customers/${customerId}/contracts`,
    fetcher
  );

  return {
    contracts: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useContracts;
