import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Users_permissions_user } from "../types/models";

const useAccounts = () => {
  const { data, error } = useSWR<ApiResult<Users_permissions_user[]>>(
    `/api/accounts`,
    fetcher
  );

  return {
    accounts: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useAccounts;
