import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Certification } from "../types/models";

const useCertification = () => {
  const { data, error } = useSWR<ApiResult<Certification[]>>(
    "/api/certifications",
    fetcher
  );
  3;
  return {
    certifications: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useCertification;
