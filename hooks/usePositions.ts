import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Position } from "../types/models";

const usePositions = () => {
  const { data, error } = useSWR<ApiResult<Position[]>>(
    "/api/positions",
    fetcher
  );

  return {
    positions: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default usePositions;
