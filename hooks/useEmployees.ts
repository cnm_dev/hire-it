import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Employee } from "../types/models";

const useEmployees = () => {
  const { data, error } = useSWR<ApiResult<Employee[]>>(
    "/api/employees",
    fetcher
  );

  return {
    employees: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useEmployees;
