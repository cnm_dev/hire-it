import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, PostingSave } from "../types/models";

const usePostingSaveCustomer = (
  userId: string | string[] | undefined,
  type: string
) => {
  const { data, error } = useSWR<ApiResult<PostingSave[]>>(
    userId
      ? `${
          type === "Customer"
            ? `/api/customers/${userId}/post-save`
            : `/api/employees/${userId}/post-save`
        }`
      : "",
    fetcher
  );

  return {
    postingSave: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default usePostingSaveCustomer;
