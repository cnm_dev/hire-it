/* eslint-disable react-hooks/rules-of-hooks */
import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, EmployeeChosse } from "../types/models";

const useEmployeeChosse = (params: {
  userId: string | string[] | undefined;
}) => {
  const { userId } = params;

  if (!userId) {
    return {
      employeeChosse: [],
      loading: false,
    };
  }

  const { data, error } = useSWR<ApiResult<EmployeeChosse[]>>(
    `/api/customers/${userId}/employee-chosse`,
    fetcher
  );

  return {
    employeeChosse: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useEmployeeChosse;
