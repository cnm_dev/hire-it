/* eslint-disable react-hooks/rules-of-hooks */
import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Customer } from "../types/models";
import { GetCustomerForm } from "@/forms/customer";

const useCustomer = (params: GetCustomerForm) => {
  const { customerId } = params;

  if (!customerId) {
    return {
      customer: [],
      loading: false,
    };
  }

  const { data, error } = useSWR<ApiResult<Customer[]>>(
    `/api/customers/${customerId}`,
    fetcher
  );
  return {
    customer: data?.data || [],
    loading: !error && !data && !customerId,
    error,
  };
};

export default useCustomer;
