import useSWR from "swr";

import { fetcher } from "../services";
import { ApiResult, Customer } from "../types/models";

const useCustomers = () => {
  const { data, error } = useSWR<ApiResult<Customer[]>>(
    `/api/customers`,
    fetcher
  );

  return {
    customers: data?.data || [],
    loading: !error && !data,
    error,
  };
};

export default useCustomers;
