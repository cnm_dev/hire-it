import { CreateReviewForm } from "@/forms/review";
import { creator } from "../services";

export const createPreview = (queryVariables: CreateReviewForm) =>
  creator("/api/preview", queryVariables);
