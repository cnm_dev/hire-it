import { creator, updater } from "../services";
import {
  CreateContractForm,
  UpdateContractForm,
  UpdateStatusContractForm,
} from "@/forms/contracts";

export const createContract = (queryVariables: CreateContractForm) =>
  creator("/api/contracts", queryVariables);

export const updateContract = (queryVariables: UpdateContractForm) => {
  const { contractId } = queryVariables;
  return updater(`/api/contracts/${contractId}`, queryVariables);
};

export const updateStatusContract = (
  queryVariables: UpdateStatusContractForm
) => {
  return updater("/api/contracts", queryVariables);
};
