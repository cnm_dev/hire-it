import { UpdatePropjectForm, createProjectForm } from "@/forms/project";
import { creator, updater } from "../services";

export const createEducation = (queryVariables: any) =>
  creator("/api/educations", queryVariables);

export const updateEducation = (queryVariables: any) =>
  updater("/api/educations", queryVariables);
