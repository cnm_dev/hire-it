import { createContractHistoryForm } from "@/forms/historyContract";
import { creator } from "@/services";

export const createContractHistory = (
  queryVariables: createContractHistoryForm
) => {
  const { contractId } = queryVariables;
  return creator(
    `/api/contracts/${contractId}/contract_histories`,
    queryVariables
  );
};
