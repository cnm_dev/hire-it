import { UpdatePropjectForm, createProjectForm } from "@/forms/project";
import { creator, updater } from "../services";

export const createProject = (queryVariables: createProjectForm) =>
  creator("/api/projects", queryVariables);

export const updateProject = (queryVariables: UpdatePropjectForm) =>
  updater("/api/projects", queryVariables);
