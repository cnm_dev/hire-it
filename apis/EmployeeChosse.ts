import {
  DeleteEmployeeChosseForm,
  createEmployeeChosseForm,
} from "@/forms/employeeChosse";
import { creator, updater } from "@/services";

export const deleteEmployeeChosse = (
  employeeForm: DeleteEmployeeChosseForm
) => {
  const { employeeChosseId, listIdEmployee } = employeeForm;

  return updater("/api/employee-chosse", {
    employeeChosseId,
    listIdEmployee,
  });
};

export const createEmployeeChoose = (
  valueVariable: createEmployeeChosseForm
) => {
  const { idCustomer, idEmployee } = valueVariable;

  return creator(`/api/customers/${idCustomer}/employee-chosse`, {
    idCustomer,
    idEmployee,
  });
};
