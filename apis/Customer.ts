import { UpdatePropjectForm, createProjectForm } from "@/forms/project";
import { creator, updater } from "../services";
import { UpdateProfileCustomerForm } from "@/forms/customer";

export const createEducation = (queryVariables: any) =>
  creator("/api/educations", queryVariables);

export const updateEducation = (queryVariables: any) =>
  updater("/api/educations", queryVariables);

export const updateProfileCustomer = (
  queryVariables: UpdateProfileCustomerForm
) => {
  const { idUser } = queryVariables;
  return updater(`/api/customers/${idUser}`, queryVariables);
};
