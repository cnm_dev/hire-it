import { CreateInformateForm, UpdateInformatesForm } from "@/forms/informates";
import { updater, creator } from "@/services";

export const updateInformates = (variableForm: UpdateInformatesForm) => {
  const { informateId, read, userId } = variableForm;

  return updater(`/api/accounts/${userId}/informate`, { informateId, read });
};

export const createInformates = (variableForm: CreateInformateForm) => {
  const { customerId, employeeId } = variableForm;

  return creator(
    `/api/accounts/${customerId ? customerId : employeeId}/informate`,
    { ...variableForm }
  );
};
