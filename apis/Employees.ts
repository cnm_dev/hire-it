import { filterContractEmployeeForm } from "@/forms/employees";
import { filterContractEmployee } from "@/services/employees";

export const filterEmployee = (
  employeeForm: filterContractEmployeeForm,
  token: { jwt: string }
) => {
  return filterContractEmployee(token, employeeForm);
};
