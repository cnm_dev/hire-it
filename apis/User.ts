import { fetcher } from "../services";

export const getUserByEmail = (queryVariables: { email: string }) =>
  fetcher(`/api/user/${queryVariables.email}`, queryVariables);
