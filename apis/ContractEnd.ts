import { createEndContractForm } from "@/forms/contract-end";
import { creator, updater } from "../services";

export const createContractEnd = (queryVariables: createEndContractForm) =>
  creator("/api/contract-end", queryVariables);
