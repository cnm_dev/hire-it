import { DeleteUserForm, UpdateBlockUserForm } from "@/forms/user";
import { updater, axiosDelete } from "../services";
export const updateBlockUser = (queryVariables: UpdateBlockUserForm) => {
  return updater("/api/accounts", queryVariables);
};

export const deleteUser = (queryVariable: DeleteUserForm) => {
  const { idUser } = queryVariable;
  return axiosDelete(`/api/accounts/${idUser}`, { idUser });
};
