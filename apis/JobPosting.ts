import { UpdateJobPostingForm, createJostPostingForm } from "@/forms/jobPosting";
import { creator, updater } from "../services";
export const createJobPosting = (queryVariables: createJostPostingForm) =>
  creator("/api/job-postings", queryVariables);

export const updateJopPosting = (queryVariables: UpdateJobPostingForm) =>
  updater("/api/job-postings", queryVariables);
