import { createPostingSaveForm, updatePostingSaveForm } from "@/forms/postingSave";
import { creator, updater } from "../services";

export const createPostingSave = (queryVariables: createPostingSaveForm) => {
  const { idCustomer, idEmployee } = queryVariables;
  if (idCustomer) {
    return creator(`/api/customers/${idCustomer}/post-save`, queryVariables);
  } else {
    return creator(`/api/employees/${idEmployee}/post-save`, queryVariables);
  }
};

export const updatePostingSave = (queryVariables: updatePostingSaveForm) => {
  const { idCustomer, idEmployee } = queryVariables;
  if (idCustomer) {
    return updater(`/api/customers/${idCustomer}/post-save`, queryVariables);
  } else {
    return updater(`/api/employees/${idEmployee}/post-save`, queryVariables);
  }
};
