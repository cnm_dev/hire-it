import { createPostingSpamForm } from "@/forms/postingSpam";
import { creator } from "../services";
export const createPostingSpam = (queryVariables: createPostingSpamForm) => {
  const { idCustomer } = queryVariables;
  if (idCustomer) {
    return creator(`/api/customers/${idCustomer}/post-spam`, queryVariables);
  } else {
    return creator("/api/customers/${idCustomer}/post-spam", queryVariables);
  }
};
