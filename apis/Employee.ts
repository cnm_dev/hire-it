import {
  DeleteEmployeeForm,
  UpdateStatusForm,
  formGetStartEmployee,
  formProfessionalExperience,
} from "@/forms/employee";
import { axiosDelete } from "@/services";
import { updater } from "@/services";
import { formFreelancingPreferencesEmployee } from "@/types/models";

export const updateStatusEmployee = (employeeForm: UpdateStatusForm) => {
  const { employeeId, status, joinDate } = employeeForm;

  return updater(`/api/employees/${employeeId}/status`, {
    employeeId,
    status,
    joinDate,
  });
};

export const updateEmployeeGettingStarted = (
  employeeForm: formGetStartEmployee
) => {
  const {
    id,
    employeeId,
    city,
    completionLevel,
    country,
    englishProficiency,
    fullName,
    nationality,
    reasonApplying,
  } = employeeForm;

  return updater(`/api/employees/${employeeId}/updateEmployeeGettingStarted`, {
    id,
    employeeId,
    city,
    completionLevel,
    country,
    englishProficiency,
    fullName,
    nationality,
    reasonApplying,
  });
};

export const updateEmployeeFreelancingPreferences = (
  employeeForm: formFreelancingPreferencesEmployee
) => {
  const {
    id,
    employeeId,
    careerInterests,
    interestedProductTypes,
    remoteSuccessKey,
    completionLevel,
  } = employeeForm;

  return updater(
    `/api/employees/${employeeId}/updateEmployeeFreelancingPreferences`,
    {
      id,
      employeeId,
      careerInterests,
      interestedProductTypes,
      remoteSuccessKey,
      completionLevel,
    }
  );
};

export const updateEmployeeProfessionalExperience = (
  employeeForm: formProfessionalExperience
) => {
  const {
    id,
    employeeId,
    completionLevel,
    certifications,
    yearsOfExperience,
    gitHub,
    hourlyRate,
    isFullTime,
    linkedIn,
    skills,
  } = employeeForm;
  const checkIsFulltime = isFullTime === "Full-time (40 hours/week)";

  return updater(
    `/api/employees/${employeeId}/updateEmployeeProfessionalExperience`,
    {
      id,
      employeeId,
      completionLevel,
      certifications,
      yearsOfExperience,
      gitHub,
      hourlyRate,
      isFullTime: checkIsFulltime,
      linkedIn,
      skills,
    }
  );
};

export const deleteEmployee = (employeeForm: DeleteEmployeeForm) => {
  const { employeeId } = employeeForm;

  return axiosDelete(`/api/employees/${employeeId}`, { employeeId });
};
