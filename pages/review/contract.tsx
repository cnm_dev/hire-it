import CButton from "@/common/components/controls/CButton";
import withPageProtected from "@/utils/withPageProtected";
import Link from "next/link";
import React from "react";

export default withPageProtected(["Customer", "Employee", "Admin"], () => {
  return (
    <Link href="/review" target="_blank" rel="noopener noreferrer" >
      <CButton text="Contract" />
    </Link>
  );
})
