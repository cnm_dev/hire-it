import withPageProtected from "@/utils/withPageProtected";
import dynamic from "next/dynamic";

const InvoicePDF = dynamic(
  () => import("../../modules/review/components/pdf"),
  {
    ssr: false,
  }
);

export default withPageProtected(["Customer", "Employee", "Admin"], () => {
  return (
    <div
      style={{
        width: "100%",
        height: "100vh",
        overflow: "hidden",
        margin: 0,
        padding: 0,
      }}
    >
      {/* <InvoicePDF arrMember={[]} inforCustomer={undefined} description={[]} priceMoney={0} date={""} /> */}
    </div>
  );
});
