/* eslint-disable react-hooks/rules-of-hooks */
import image from "@/common/assets/image/all-image";
import CLoading from "@/common/components/controls/CLoading";
import CEastHire from "@/common/components/layout/CEastHire";
import FormOption from "@/modules/hire/components/FormOption";
import { getStringStorage } from "@/utils";
import Head from "next/head";
import React, { useEffect, useState } from "react";

const commitment = () => {
  const [commitment, setCommitment] = useState<string>();
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    setCommitment(getStringStorage(localStorage.getItem("Commitment")));
    setIsLoading(false);
  }, []);
  const questionName =
    "What level of time commitment will you require from the developer?";
  const data = [
    {
      value: "true",
      label: "Full time (40 or more hrs/week)",
    },
    {
      value: "false",
      label: "Part time (Less than 40 hrs/week)",
    },
    {
      value: "none",
      label: "I'll decide later",
    },
  ];
  return (
    <>
      <Head>
        <title>Join as a Client</title>
        <meta name="description" content="Generated by Hire IT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={image.logo} />
      </Head>
      <div className="main-hire">
        {isLoading ? (
          <CLoading fullScreen />
        ) : (
          <>
            <FormOption
              options={data}
              questionName={questionName}
              href={"skills"}
              defaultValue={commitment}
              showBack={true}
              option={2}
            />
            <CEastHire />
          </>
        )}
      </div>
    </>
  );
};

export default commitment;
