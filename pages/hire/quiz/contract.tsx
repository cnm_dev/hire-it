/* eslint-disable react-hooks/rules-of-hooks */
import { filterEmployee } from "@/apis/Employees";
import CloadingModern from "@/common/components/controls/CloadingModern";
import CEastHire from "@/common/components/layout/CEastHire";
import { checkNoneOption } from "@/constants/contract";
import ContactContract from "@/modules/hire/components/ContactContract";
import ContractOption from "@/modules/hire/components/Contract";
import { Employee } from "@/types/models";
import { converStringBoolean, getStringStorage } from "@/utils";
import withPageHire from "@/utils/withPageHire";
import { useSession } from "next-auth/react";
import React, { useEffect, useMemo, useState } from "react";
import { toast } from "react-toastify";

// All local storage
// Commitment,CompanySize,ProductSpecs,ProjectLength,OpenForRemote,ReadyToStart,Situation,Position,

export default withPageHire(["Customer"], () => {
  const [loading, setLoading] = useState<boolean>(true);
  const [employee, setEmployee] = useState<Employee[]>();
  const [companySize, setCompanySize] = useState<number[]>();
  const { data: session } = useSession();

  const access_token = useMemo(() => {
    if (session) {
      return session?.user.access_token;
    }
  }, [session]);

  useEffect(() => {
    localStorage.setItem("email", JSON.stringify(session?.user.email));

    const budget = getStringStorage(localStorage.getItem("Budget"));
    const hourlyRate = getStringStorage(localStorage.getItem("Budget"))
      ?.split(",")
      .map(Number);
    const isFullTime = converStringBoolean(
      getStringStorage(localStorage.getItem("Commitment"))
    );
    const isRemote = converStringBoolean(
      getStringStorage(localStorage.getItem("OpenForRemote"))
    );
    const positionName = getStringStorage(localStorage.getItem("Position"));

    const ProjectLength = getStringStorage(
      localStorage.getItem("ProjectLength")
    );

    const ReadyToStart = getStringStorage(localStorage.getItem("ReadyToStart"));
    const size = getStringStorage(localStorage.getItem("CompanySize"))
      ?.split(",")
      .map(Number);

    setCompanySize(size);

    let skill = localStorage.getItem("Skills");
    let skillLocalStorge = skill && JSON.parse(skill);

    let skills: string[] = [];
    skillLocalStorge.map((course: any) => {
      skills.push(course.id);
    });

    const isNone = checkNoneOption(
      isFullTime,
      ProjectLength,
      isRemote,
      ReadyToStart,
      budget
    );

    !isNone
      ? hourlyRate &&
        filterEmployee(
          {
            hourlyRateFrom: hourlyRate[0],
            hourlyRateTo: hourlyRate[1],
            isFullTime: isFullTime,
            isRemote: isRemote,
            positionName: positionName,
            status: "confirm",
            skills,
          },
          { jwt: access_token }
        )
          .then((course) => {
            setEmployee(course.data);
            setLoading(false);
          })
          .catch((err) => {
            toast.error(err);
            setLoading(false);
          })
      : setLoading(false);
  }, [access_token, session?.user.email]);

  return (
    <>
      {!loading && session && companySize ? (
        <div className="main-hire">
          {employee && employee.length >= companySize[0] ? (
            <ContractOption
              session={session}
              employees={
                employee.length <= companySize[1]
                  ? employee
                  : employee
                      .sort(() => Math.random() - 0.5)
                      .slice(0, companySize[1])
              }
            />
          ) : (
            <ContactContract session={session} />
          )}
          <CEastHire />
        </div>
      ) : (
        <CloadingModern />
      )}
    </>
  );
});
