/* eslint-disable react-hooks/rules-of-hooks */
import image from "@/common/assets/image/all-image";
import CLoading from "@/common/components/controls/CLoading";
import CEastHire from "@/common/components/layout/CEastHire";
import FormOption from "@/modules/hire/components/FormOption";
import { getStringStorage } from "@/utils";
import Head from "next/head";
import { useEffect, useState } from "react";

const budgetEstimation = () => {
  const [budget, setBudget] = useState<string>();
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    setBudget(getStringStorage(localStorage.getItem("Budget")));
    setIsLoading(false);
  }, []);

  const questionName = "What is your budget for this role?";
  const data = [
    {
      value: "0,7",
      label: "Less than $7/hr",
    },
    {
      value: "7,9",
      label: "$7 - $9/hr",
    },

    {
      label: "$9.1 - $11/hr",
      value: "9.1,11",
    },
    {
      label: "More than $11/hr",
      value: "11,999999",
    },
    {
      value: "none",
      label: "Not sure on budget yet",
    },
  ];

  return (
    <>
      <Head>
        <title>Join as a Client</title>
        <meta name="description" content="Generated by Hire IT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={image.logo} />
      </Head>
      <div className="main-hire">
        {isLoading ? (
          <CLoading fullScreen />
        ) : (
          <>
            <FormOption
              options={data}
              questionName={questionName}
              href={"contract"}
              defaultValue={budget}
              showBack={true}
              option={1}
            />
            <CEastHire />
          </>
        )}
      </div>
    </>
  );
};

export default budgetEstimation;
