/* eslint-disable react-hooks/rules-of-hooks */
import image from "@/common/assets/image/all-image";
import CLoading from "@/common/components/controls/CLoading";
import CFooter from "@/common/components/layout/CFooter";
import CHeader from "@/common/components/layout/CHeader";
import CMenu from "@/common/components/layout/CMenu";
import useContracts from "@/hooks/useContracts";
import ManagerContract from "@/modules/Manager/Contract/ManagerContract";
import ContractsSlice from "@/redux/Slices/ContractsSlice";
import FilterContractAdminSlice from "@/redux/Slices/FilterContractAdminSlice";
import { langSelector } from "@/redux/selector";
import withPageProtected from "@/utils/withPageProtected";
import Head from "next/head";
import { useEffect, useMemo } from "react";
import { useDispatch, useSelector } from "react-redux";

export default withPageProtected(["Admin"], () => {
  const lang: "en" | "vn" = useSelector(langSelector);
  const { contracts, loading } = useContracts({ customerId: undefined });
  const dispatch = useDispatch();
  useEffect(() => {
    if (!loading) dispatch(ContractsSlice.actions.listContracts(contracts));
  }, [contracts, dispatch, loading]);
  useMemo(() => {
    dispatch(FilterContractAdminSlice.actions.contractName(""));
    dispatch(FilterContractAdminSlice.actions.status("all"));

    const currentDate = new Date();
    // Thiết lập ngày thành 1
    currentDate.setDate(1);
    // Trừ đi ba tháng
    currentDate.setMonth(currentDate.getMonth() - 3);

    dispatch(
      FilterContractAdminSlice.actions.dateFrom(currentDate.toISOString())
    );
    dispatch(FilterContractAdminSlice.actions.dateTo(new Date().toISOString()));
  }, [dispatch]);
  return (
    <>
      <Head>
        <title>Hire IT - Manager Contract</title>
        <meta name="description" content="Generated by Hire IT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={image.logo} />
      </Head>
      <main>
        <CHeader lang={lang} />
        <div className="main">
          <CMenu lang={lang} isSearch={false} />
          <div className="main-account">
            {loading ? <CLoading fullScreen /> : <ManagerContract />}
          </div>
        </div>
      </main>
    </>
  );
});
