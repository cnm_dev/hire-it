import store from "@/redux/store";
import "@/styles/globals.scss";
import type { AppProps } from "next/app";
import React from "react";
import { Provider } from "react-redux";
import { SessionProvider, SessionProviderProps } from "next-auth/react";
import CToast from "@/common/components/layout/CToast";

export default function App({
  Component,
  pageProps: { session, ...pageProps },
}: AppProps<SessionProviderProps>) {
  return (
    <Provider store={store}>
      <SessionProvider session={session}>
        <CToast />
        <Component {...pageProps} />
      </SessionProvider>
    </Provider>
  );
}
