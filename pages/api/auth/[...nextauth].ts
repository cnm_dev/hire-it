/* eslint-disable react-hooks/rules-of-hooks */
import image from "@/common/assets/image/all-image";
import { STRAPI_API_TOKEN } from "@/constants";
import NextAuth, { NextAuthOptions } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";

import { getInforUser, getTypeUser, login } from "../../../services/user";
import { getStatusEmployee } from "@/services/employees";
import { getCustomer } from "@/services/customer";

/**
 * @see https://medium.com/@tom555my/strapi-next-js-email-password-authentication-a8207f72b446
 */
export const authOptions: NextAuthOptions = {
  // Configure authentication providers
  providers: [
    CredentialsProvider({
      name: "Sign in with Email",
      credentials: {
        email: { label: "Email", type: "text" },
        password: { label: "Password", type: "password" },
      },
      async authorize(credentials, _req) {
        if (!credentials) return null;
        try {
          const { user, jwt }: any = await login({
            email: credentials.email,
            password: credentials.password,
          });
          return { ...user, jwt };
        } catch (err) {
          // login failed
          console.error(err);
          return null;
        }
      },
    }),

    GoogleProvider({
      clientId:
        process.env.GOOGLE_CLIENT_ID ||
        "509053713792-s309quktiknpqcebp96dtf8sfu0f54l3.apps.googleusercontent.com",
      clientSecret:
        process.env.GOOGLE_CLIENT_SECRET ||
        "509053713792-s309quktiknpqcebp96dtf8sfu0f54l3.apps.googleusercontent.com",
    }),
  ],

  // Handle login callback from providers
  callbacks: {
    session: async ({ session, token }) => {
      const {
        id,
        username,
        email,
        name,
        jwt,
        type_user,
        status,
        address,
        phoneNumber,
      } = token;
      const fullName = username || name;
      const access_token = jwt;
      const active = getInforUser(STRAPI_API_TOKEN, { email });

      const isActive = (await active).data[0].isActive;
      const avatar = (await active).data[0].avartar || image.image_default;
      let getEmployee;
      let getcustomer;
      if (jwt) {
        if (type_user === "Employee") {
          getEmployee = await getStatusEmployee({ jwt: STRAPI_API_TOKEN }, id);
        }
        if (type_user === "Customer") {
          getcustomer = await getCustomer(
            { jwt: STRAPI_API_TOKEN },
            { customerId: id }
          );
        }
      }

      const user = {
        id,
        fullName,
        email,
        avatar,
        access_token,
        type_user,
        isActive,
        status,
        address,
        phoneNumber,
        employee: getEmployee && getEmployee.data[0],
        customer: getcustomer && getcustomer.data[0],
      };

      return Promise.resolve({ ...session, user });
    },
    jwt: async ({ token, user }: any) => {
      if (user) {
        token.id = user.id;
        token.jwt = user.jwt;
        token.username = user.fullName || user.username;
        token.isActive = user.isActive;
        token.avartar = user.avartar;
        token.address = user.address;
        token.phoneNumber = user.phoneNumber;

        const email = user.email;
        const result = getTypeUser(STRAPI_API_TOKEN, { email });
        const type_userApi = (await result).data[0];
        if (type_userApi) {
          token.type_user = type_userApi.type_user.typeUserName;
        }

        let status;
        if (
          type_userApi &&
          type_userApi.type_user.typeUserName === "Employee"
        ) {
          await getStatusEmployee({ jwt: user.jwt }, user.id)
            .then((course) => {
              status = course.data[0].status;
            })
            .catch((err) => console.log(err));
        }
        token.status = status;
      }

      return Promise.resolve(token);
    },
  },
};

export default NextAuth(authOptions);
