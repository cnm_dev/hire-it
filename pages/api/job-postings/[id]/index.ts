import type { NextApiRequest, NextApiResponse } from "next";

import { STRAPI_API_TOKEN } from "@/constants";
import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";
import { GetJobPostingForm } from "@/forms/jobPosting";
import { getJobPosting } from "@/services/jobpostings";
import { ApiException } from "@/types/error";
import withExceptionHandler from "@/utils/withExceptionHandler";
export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const { id } = req.query;

    // Query members
    if (req.method === "GET") {
      const result = await getJobPosting({ jwt: STRAPI_API_TOKEN }, {
        postId: id,
      } as GetJobPostingForm);
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
