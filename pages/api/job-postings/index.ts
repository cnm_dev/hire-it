import type { NextApiRequest, NextApiResponse } from "next";
import { METHOD_NOT_ALLOWED_ERROR } from "../../../constants/error";
import { ApiException } from "../../../types/error";
import withExceptionHandler from "../../../utils/withExceptionHandler";
import { createJobPosting, getJobPostings, updateJopPosting } from "@/services/jobpostings";
import { STRAPI_API_TOKEN } from "@/constants";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    // Query members
    if (req.method === "GET") {
      const result = await getJobPostings({ jwt: STRAPI_API_TOKEN });
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "POST") {
      const body = await req.body;
      const result = await createJobPosting({ jwt: STRAPI_API_TOKEN }, body);
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "PUT") {
      const body = await req.body;
      const result = await updateJopPosting({ jwt: STRAPI_API_TOKEN }, body);
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
