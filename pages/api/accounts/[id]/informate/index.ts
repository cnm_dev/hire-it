import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "../../../../../constants/error";
import { ApiException } from "../../../../../types/error";
import withExceptionHandler from "../../../../../utils/withExceptionHandler";
import { STRAPI_API_TOKEN } from "@/constants";
import { createInformates, getInformates, updateInformates } from "@/services/informates";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    // Query members
    const { id } = req.query;

    if (req.method === "GET") {
      const result = await getInformates(
        { jwt: STRAPI_API_TOKEN },
        { userId: id }
      );
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "PUT") {
      const body = req.body;
      const result = await updateInformates({ jwt: STRAPI_API_TOKEN }, body);
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "POST") {
      const body = req.body;
      const result = await createInformates({ jwt: STRAPI_API_TOKEN }, body);
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
