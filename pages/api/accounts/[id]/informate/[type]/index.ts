import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "../../../../../../constants/error";
import { ApiException } from "../../../../../../types/error";
import withExceptionHandler from "../../../../../../utils/withExceptionHandler";
import { STRAPI_API_TOKEN } from "@/constants";
import { getInformates } from "@/services/informates";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    // Query members
    const { id, type } = req.query;

    if (req.method === "GET") {
      const result = await getInformates(
        { jwt: STRAPI_API_TOKEN },
        { userId: id, typeUser: type }
      );
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
