import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "../../../../constants/error";
import { ApiException } from "../../../../types/error";
import withExceptionHandler from "../../../../utils/withExceptionHandler";
import { STRAPI_API_TOKEN } from "@/constants";
import { deleteUser } from "@/services/accounts";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const token = { jwt: STRAPI_API_TOKEN };

    // Delete employee
    if (req.method === "DELETE") {
      const { id } = req.query;
      const token = { jwt: STRAPI_API_TOKEN };
      const result = await deleteUser(token, { idUser: id });
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
