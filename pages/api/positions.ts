import type { NextApiRequest, NextApiResponse } from "next";

import { STRAPI_API_TOKEN } from "@/constants";
import { getPositions } from "@/services/positions";
import withExceptionHandler from "@/utils/withExceptionHandler";
import { ApiException } from "@/types/error";
import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    // Query members
    if (req.method === "GET") {
      const result = await getPositions({ jwt: STRAPI_API_TOKEN });
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
