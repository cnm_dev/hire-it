import type { NextApiRequest, NextApiResponse } from "next";

import { STRAPI_API_TOKEN } from "@/constants";
import withExceptionHandler from "@/utils/withExceptionHandler";
import { ApiException } from "@/types/error";
import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";
import { getInforUserByEmail } from "@/services/user";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const { email } = req.query;
    // Query members
    if (req.method === "GET") {
      const result = await getInforUserByEmail(STRAPI_API_TOKEN, {
        email: email,
      });
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
