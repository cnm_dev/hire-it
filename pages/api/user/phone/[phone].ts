import type { NextApiRequest, NextApiResponse } from "next";

import { STRAPI_API_TOKEN } from "@/constants";
import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";
import { getInforUserByPhone } from "@/services/user";
import { ApiException } from "@/types/error";
import withExceptionHandler from "@/utils/withExceptionHandler";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const { phone } = req.query;

    // Query members
    if (req.method === "GET") {
      const result = await getInforUserByPhone(STRAPI_API_TOKEN, {
        phoneNumber: phone,
      });
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
