import type { NextApiRequest, NextApiResponse } from "next";

import { STRAPI_API_TOKEN } from "@/constants";
import {
  createEducation,
  deleteEducation,
  updateEducation,
} from "@/services/education";
import { METHOD_NOT_ALLOWED_ERROR } from "../../../constants/error";
import { ApiException } from "../../../types/error";
import withExceptionHandler from "../../../utils/withExceptionHandler";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === "POST") {
      const body = await req.body;

      const result = await createEducation(STRAPI_API_TOKEN, body);
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "PUT") {
      const body = await req.body;

      if (body.educationMajors) {
        const result = await updateEducation(STRAPI_API_TOKEN, body);
        res.status(200).json(result);
      } else {

        const result = await deleteEducation(STRAPI_API_TOKEN, body);

        res.status(200).json(result);
      }

      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
