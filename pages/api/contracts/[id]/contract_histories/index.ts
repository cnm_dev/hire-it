import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "../../../../../constants/error";
import { ApiException } from "../../../../../types/error";
import withExceptionHandler from "../../../../../utils/withExceptionHandler";
import { STRAPI_API_TOKEN } from "@/constants";
import {
  createContractHistory,
  getContractHistory,
} from "@/services/historyContract";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const token = { jwt: STRAPI_API_TOKEN };

    // Create device
    if (req.method === "GET") {
      const { id } = await req.query;
      const result = await getContractHistory(token, { contractId: id });
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "POST") {
      const body = await req.body;
      const result = await createContractHistory(token, body);
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
