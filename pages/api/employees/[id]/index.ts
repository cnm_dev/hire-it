import type { NextApiRequest, NextApiResponse } from "next";

import { STRAPI_API_TOKEN } from "@/constants";
import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";
import { GetEmployeeForm } from "@/forms/employees";
import { getEmployee } from "@/services/employees";
import { ApiException } from "@/types/error";
import withExceptionHandler from "@/utils/withExceptionHandler";
import { deleteEmployee } from "@/services/employee";
export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const { id } = req.query;

    // Query members
    if (req.method === "GET") {
      const result = await getEmployee({ jwt: STRAPI_API_TOKEN }, {
        profileId: id,
      } as GetEmployeeForm);
      res.status(200).json(result);
      return res.end();
    }

    // Delete employee
    if (req.method === "DELETE") {
      const token = { jwt: STRAPI_API_TOKEN };
      const result = await deleteEmployee(token, { employeeId: id });
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
