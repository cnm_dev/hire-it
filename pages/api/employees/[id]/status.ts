import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";
import { ApiException } from "@/types/error";
import withExceptionHandler from "@/utils/withExceptionHandler";
import { updateStatusEmployee } from "@/services/employee";
import { STRAPI_API_TOKEN } from "@/constants";
export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === "PUT") {
      const token = { jwt: STRAPI_API_TOKEN };

      const params = await req.body;
      const result = await updateStatusEmployee(token, { ...params });
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
