import type { NextApiRequest, NextApiResponse } from "next";

import { STRAPI_API_TOKEN } from "@/constants";
import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";
import { updateEmployeeFreelancingPreferences } from "@/services/employee";
import { ApiException } from "@/types/error";
import withExceptionHandler from "@/utils/withExceptionHandler";
export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    if (req.method === "PUT") {
      const params = await req.body;
      const result = await updateEmployeeFreelancingPreferences(
        { jwt: STRAPI_API_TOKEN },
        {
          ...params,
        }
      );
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
