import { STRAPI_API_TOKEN } from "@/constants";
import { getSkills } from "@/services/skills";
import type { NextApiRequest, NextApiResponse } from "next";
import { METHOD_NOT_ALLOWED_ERROR } from "../../../constants/error";
import { ApiException } from "../../../types/error";
import withExceptionHandler from "../../../utils/withExceptionHandler";
import { certificationName } from "@/services/certifications";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    // Query members
    if (req.method === "GET") {
      const result = await certificationName({ jwt: STRAPI_API_TOKEN });
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
