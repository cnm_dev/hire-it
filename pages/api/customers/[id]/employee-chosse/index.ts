import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";
import { ApiException } from "@/types/error";
import withExceptionHandler from "@/utils/withExceptionHandler";
import { STRAPI_API_TOKEN } from "@/constants";
import {
  createEmployeeChoose,
  getEmployeeChosse,
} from "@/services/employeeChosse";
export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    // Query members
    if (req.method === "GET") {
      const { id } = req.query;

      const result = await getEmployeeChosse(
        { jwt: STRAPI_API_TOKEN },
        { userId: id }
      );
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "POST") {
      const body = req.body;
      const result = await createEmployeeChoose(
        { jwt: STRAPI_API_TOKEN },
        body
      );
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
