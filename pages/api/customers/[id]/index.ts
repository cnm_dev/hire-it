import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "@/constants/error";
import { getCustomer, updateProfileCustomer } from "@/services/customer";
import { ApiException } from "@/types/error";
import withExceptionHandler from "@/utils/withExceptionHandler";
import { STRAPI_API_TOKEN } from "@/constants";
export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const token = { jwt: STRAPI_API_TOKEN };
    const { id } = req.query;

    // Query members
    if (req.method === "GET" && id !== "undefined") {
      const result = await getCustomer(token, { customerId: id });
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "PUT") {
      const body = await req.body;
      const result = await updateProfileCustomer(token, body);
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
