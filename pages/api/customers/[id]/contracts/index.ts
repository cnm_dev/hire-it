import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "../../../../../constants/error";
import { ApiException } from "../../../../../types/error";
import withExceptionHandler from "../../../../../utils/withExceptionHandler";
import { getContracts } from "@/services/contracts";
import { STRAPI_API_TOKEN } from "@/constants";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    // Query members
    const token = { jwt: STRAPI_API_TOKEN };
    const { id } = req.query;

    if (req.method === "GET") {
      const result = await getContracts(token, { customerId: id });
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
