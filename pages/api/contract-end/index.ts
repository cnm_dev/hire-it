import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "../../../constants/error";
import { ApiException } from "../../../types/error";
import withExceptionHandler from "../../../utils/withExceptionHandler";
import { STRAPI_API_TOKEN } from "@/constants";
import { createContractEnd } from "@/services/contract-end";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const token = { jwt: STRAPI_API_TOKEN };

    // Create device
    if (req.method === "POST") {
      const body = await req.body;
      const result = await createContractEnd(token, body);
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
