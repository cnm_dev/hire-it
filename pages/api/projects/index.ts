import type { NextApiRequest, NextApiResponse } from "next";

import { METHOD_NOT_ALLOWED_ERROR } from "../../../constants/error";
import { ApiException } from "../../../types/error";
import withExceptionHandler from "../../../utils/withExceptionHandler";
import {
  createProject,
  getProjects,
  updatePropject,
} from "@/services/projects";
import { STRAPI_API_TOKEN } from "@/constants";

export default withExceptionHandler(
  async (req: NextApiRequest, res: NextApiResponse) => {
    const token = { jwt: STRAPI_API_TOKEN };

    if (req.method === "GET") {
      const result = await getProjects(token);
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "POST") {
      const body = await req.body;

      const result = await createProject(token, body);
      res.status(200).json(result);
      return res.end();
    }

    if (req.method === "PUT") {
      const body = await req.body;

      const result = await updatePropject(token, body);
      res.status(200).json(result);
      return res.end();
    }

    throw new ApiException(405, METHOD_NOT_ALLOWED_ERROR);
  }
);
