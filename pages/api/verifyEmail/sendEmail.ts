// import { transporter } from "@/constants";
import { transporter } from "@/utils/email";
import type { NextApiRequest, NextApiResponse } from "next";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { email, html } = req.body;

  // Create an email message
  const emailMessage = {
    from: process.env.EMAIL_FROM,
    to: email,
    subject: "Email Verification",
    html: html,
  };
  try {
    // Send the email
    await transporter.sendMail(emailMessage);
    res.status(200).json({ success: true });
  } catch (error) {
    console.error(error);
    res.status(500).json({ success: false, message: "Failed to send email" });
  }
}
