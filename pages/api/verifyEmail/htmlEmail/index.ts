export const verifyEmailHtml = (verificationCode: any) => {
  return ` <table style="width: 100%;background-color:#ffffff">
    <tr>
        <td >
    <div style="background-color:#ffffff;color:#353740;padding:0 20px;text-align:left;max-width: 400px;">
        <img src="https://res.cloudinary.com/hire-it/image/upload/v1679987489/KLTN/logo_rvjdd6.png" alt="logo"  
    style="height: 100px;">
        <h1 style="color:#202123;font-size:32px">Verify your email address</h1>

        <p style="font-size:18px;">
          To continue setting up your HireIT account, please verify that this is your email address.
        </p>
        <p style="text-align:left;font-size: 20px;font-weight: bold;">
          Verify Code: ${verificationCode}
          </p>
          <p style="color: #444444;font-size: 16px;">This link will expire in 5 days. If you did not make this request, please disregard this email. For help, contact us through our <a href="">Help center.</a></p>
      </div> 
        </td>
    </tr>
</table>`;
};
