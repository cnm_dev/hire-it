export const emailInfomateCancel = (user: {
  name: string;
  phoneNumber: string;
  email: string;
  contractId: string;
  host: string;
}) => {
  return `<table style="width: 100%;background-color:#ffffff">
  <div style="background-color:#ffffff;color:#353740;padding:0 20px;text-align:left;max-width: 400px;">
      <img src="https://res.cloudinary.com/hire-it/image/upload/v1679987489/KLTN/logo_rvjdd6.png" alt="logo"  
  style="height: 100px;">
      <p>Subject: Contract Creation Successful - Pending Payment</p>
      <p>Dear <b>${user.name}</b></p>
      <p>We hereby notify that the contract ${
        user.contractId
      } between us and you has ended. This termination is done by mutual agreement or for some other reason.
      </p>
      <p>The terms and conditions of the contract are no longer valid as of ${new Date()}. The parties are no longer responsible and obligated to comply with the terms of the contract.
      </p>
We would like to thank you for your cooperation in the past time and regret the termination of this contract. If you have any questions or need assistance regarding this process, please feel free to contact us.
<p></p>
      <p><a href=${user.host}">Click the link to see details</a> </p>
      <p>Best regards,</p>
      <p>Vo Minh Hieu</p>
      <p>Administrators</p>
      <p>HireIT</p>
      </div>
</table>`;
};
