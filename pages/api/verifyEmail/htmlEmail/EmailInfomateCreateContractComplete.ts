export const emailInfomateComplete = (user: {
  name: string;
  phoneNumber: string;
  email: string;
  contractId: string;
  host: string;
}) => {
  return (
    user &&
    `<table style="width: 100%;background-color:#ffffff">
  <tr>
  <td>
  <div style="background-color:#ffffff;color:#353740;padding:0 20px;text-align:left;max-width: 400px;">
      <img src="https://res.cloudinary.com/hire-it/image/upload/v1679987489/KLTN/logo_rvjdd6.png" alt="logo"  
  style="height: 100px;">
      <p>
      Subject: Thank you for choosing our services!</p>
      <p>Dear <b>${user.name}</b></p>
      <p>We want to express our sincere gratitude for choosing our services. Thank you for placing your trust in us. We are committed to delivering exceptional service and ensuring your satisfaction.</p>
      <p>We greatly appreciate your business and the opportunity to serve you. Our team will be in touch with you soon to discuss your specific needs and provide further assistance.</p>
      <p>If you have any immediate questions or require further information, please don't hesitate to contact us at [Contact Number] or email us at [Email Address]. We are here to support you.</p>
      <p>Thank you once again for choosing [Your Company Name]. We value your patronage and look forward to serving you with utmost care.</p>
      <p> <a href="${user.host}">Click the link to see details</a></p>
      <p>Best regards,</p>
      <p>Vo Minh Hieu</p>
      <p>Administrators</p>
      <p>HireIT</p>
    </div>
    </td>
    </tr>
</table>`
  );
};
