export const emailInfomateExtendWait = (user: {
  name: string;
  phoneNumber: string;
  email: string;
  contractId: string;
  host: string;
  endDate: Date;
  endDateExtend: Date;
}) => {
  return `<table style="width: 100%;background-color:#ffffff">
  <div style="background-color:#ffffff;color:#353740;padding:0 20px;text-align:left;max-width: 400px;">
      <img src="https://res.cloudinary.com/hire-it/image/upload/v1679987489/KLTN/logo_rvjdd6.png" alt="logo"  
  style="height: 100px;">
      <p>
Subject: Contract Extension Pending Payment</p>
      <p>Dear <b>${user.name}</b></p>
      <p>We would like to inform you that your contract with [Your Company Name] has been successfully extended. However, we kindly remind you that payment for the contract extension is still pending.</p>
      <p>The details of the contract extension are as follows:</p>
      <p>Contract ID: ${user.contractId}</p>
      <p>Original Contract End Date: ${user.endDate} End Date]</p>
      <p>Extended Contract End Date: ${user.endDateExtend}</p>
      <p>To ensure the continuation of our services and to finalize the contract extension, we kindly request that you make the necessary payment as soon as possible. Please refer to the payment instructions provided in the attached invoice or contact our billing department for further assistance.</p>
      <p>We appreciate your prompt attention to this matter and your commitment to our partnership. If you have any questions or require any clarification regarding the payment process or any other aspect of the contract extension, please feel free to reach out to us. We are here to assist you.</p>
      <p>Thank you for your cooperation and continued trust in [Your Company Name]. We value our relationship with you and look forward to serving you further. </p>
      <p><a href=${user.host}">Click the link to see details</a> </p>
      <p>Best regards,</p>
      <p>Vo Minh Hieu</p>
      <p>Administrators</p>
      <p>HireIT</p>
      </div>
</table>`;
};
