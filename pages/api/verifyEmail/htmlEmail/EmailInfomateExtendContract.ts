export const emailInfomateExtend = (user: {
  name: string;
  phoneNumber: string;
  email: string;
  contractId: string;
  host: string;
  endDate: Date;
  endDateExtend: Date;
}) => {
  return `<table style="width: 100%;background-color:#ffffff">
  <div style="background-color:#ffffff;color:#353740;padding:0 20px;text-align:left;max-width: 400px;">
      <img src="https://res.cloudinary.com/hire-it/image/upload/v1679987489/KLTN/logo_rvjdd6.png" alt="logo"  
  style="height: 100px;">
      <p>
      Subject: Contract Extension Successful</p>
      <p>Dear <b>${user.name}</b></p>
      <p>We are pleased to inform you that your contract with [Your Company Name] has been successfully extended. We greatly appreciate your continued partnership and trust in our services.</p>
      <p>The details of the contract extension are as follows:</p>
      <p>Contract ID: ${user.contractId}</p>
      <p>Original Contract End Date: ${user.endDate} End Date]</p>
      <p>Extended Contract End Date: ${user.endDateExtend}</p>
      <p>We would like to take this opportunity to thank you for your ongoing support and commitment. We value our relationship with you and are dedicated to providing excellent service and meeting your needs.</p>
      <p>If you have any questions or require further information regarding the contract extension or any other matter, please do not hesitate to contact us. Our team is available to assist you.
      </p>
      <p>Once again, thank you for choosing [Your Company Name]. We look forward to a continued successful partnership.</p>
      <p><a href=${user.host}">Click the link to see details</a> </p>
      <p>Best regards,</p>
      <p>Vo Minh Hieu</p>
      <p>Administrators</p>
      <p>HireIT</p>
      </div>
</table>`;
};
