export const emailInfomateSuccess = (user: {
  name: string;
  phoneNumber: string;
  email: string;
  contractId: string;
  totalMoney: number;
  host: string;
}) => {
  return `<table style="width: 100%;background-color:#ffffff">
  <div style="background-color:#ffffff;color:#353740;padding:0 20px;text-align:left;max-width: 400px;">
      <img src="https://res.cloudinary.com/hire-it/image/upload/v1679987489/KLTN/logo_rvjdd6.png" alt="logo"  
  style="height: 100px;">
      <p>Subject: Contract Creation Successful - Pending Payment</p>
      <p>Dear <b>${user.name}</b></p>
      <p>We are delighted to inform you that your contract has been successfully created with [Your Company Name]. We appreciate your trust in our services and we are committed to ensuring a smooth and satisfactory experience for you.</p>
      <p>The details of your contract are as follows:</p>
      <p>Contract ID: ${user.contractId}</p>
      <p>Contract Start Date: [Start Date]</p>
      <p>Contract End Date: [End Date]</p>
      <p>Total Amount: ${user.totalMoney}</p>
      <p>Please note that your contract is currently in "Pending Payment" status. We kindly request you to proceed with the payment as per the agreed terms and conditions outlined in the contract.
      </p>
      <p>To complete the payment, please follow the instructions provided below:</p>
      <p>Payment Method: [Payment Method]</p>
      <p>Payment Due Date: [Due Date]</p>
      <p>Payment Amount: [Payment Amount]</p>
      <p>Payment Instructions: [Payment Instructions]</p>
      <p>Should you have any questions or require further assistance regarding the payment process or any other matter related to the contract, please feel free to reach out to our dedicated customer support team at [Contact Number] or email us at [Email Address]. We are here to help you.</p>
      <p>Thank you for choosing [Your Company Name]. We appreciate your business and look forward to a successful collaboration.
</p>
      <p><a href=${user.host}">Click the link to see details</a> </p>
      <p>Best regards,</p>
      <p>Vo Minh Hieu</p>
      <p>Administrators</p>
      <p>HireIT</p>
      </div>
</table>`;
};
