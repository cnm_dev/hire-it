/* eslint-disable react-hooks/rules-of-hooks */
import image from "@/common/assets/image/all-image";
import CFooter from "@/common/components/layout/CFooter";
import CHeader from "@/common/components/layout/CHeader";
import CMenu from "@/common/components/layout/CMenu";
import Head from "next/head";
import React, { useEffect, useState } from "react";
import Contract from "@/modules/Contract";

import { langSelector } from "@/redux/selector";
import { useDispatch, useSelector } from "react-redux";
import withPageProtected from "@/utils/withPageProtected";
import useContracts from "@/hooks/useContracts";
import { useSession } from "next-auth/react";
import { toast } from "react-toastify";
import CLoading from "@/common/components/controls/CLoading";
import ContractsSlice from "@/redux/Slices/ContractsSlice";
import ContractsOriginSlice from "@/redux/Slices/ContractsOriginSlice";

export default withPageProtected(["Customer", "Employee", "Admin"], () => {
  const lang: "en" | "vn" = useSelector(langSelector);
  const { data: session } = useSession();
  const dispatch = useDispatch();
  const { contracts, loading, error } = useContracts({
    customerId: session?.user.id,
  });
  error && toast.error(error);

  useEffect(() => {
    dispatch(ContractsSlice.actions.listContracts(contracts));
    dispatch(ContractsOriginSlice.actions.listContractsOrigin(contracts));
  }, [contracts, dispatch]);

  return (
    <>
      <Head>
        <title>Hire IT - Contract</title>
        <meta name="description" content="Generated by Hire IT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={image.logo} />
      </Head>
      <main>
        <CHeader lang={lang} />

        {!loading ? (
          <div className="main">
            <CMenu lang={lang} isSearch={false} />
            <div className="main-payment">
              <Contract />
            </div>
            <CFooter lang={lang} />
          </div>
        ) : (
          <CLoading fullScreen/>
        )}
      </main>
    </>
  );
});
