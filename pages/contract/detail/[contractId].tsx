/* eslint-disable react-hooks/rules-of-hooks */
import image from "@/common/assets/image/all-image";
import CFooter from "@/common/components/layout/CFooter";
import CHeader from "@/common/components/layout/CHeader";
import CMenu from "@/common/components/layout/CMenu";
import Head from "next/head";
import React, { useEffect, useState } from "react";

import { langSelector } from "@/redux/selector";
import { useDispatch, useSelector } from "react-redux";
import ContractDetail from "@/modules/ContractDetail/ContractDetail";
import withPageProtected from "@/utils/withPageProtected";
import { useRouter } from "next/router";
import useContract from "@/hooks/useContract";
import { useSession } from "next-auth/react";
import CLoading from "@/common/components/controls/CLoading";
import ContractDetailSlice from "@/redux/Slices/ContractDetailSlice";
export default withPageProtected(["Customer", "Admin"], () => {
  const lang: "en" | "vn" = useSelector(langSelector);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const router = useRouter();
  const dispatch = useDispatch();
  const { contractId } = router.query;
  const { data: session } = useSession();
  const { loading, contract } = useContract({
    customerId: session?.user.id,
    contractId,
  });
  useEffect(() => {
    if (!loading && contract[0]) {
      dispatch(ContractDetailSlice.actions.contractDetail(contract[0]));

      setIsLoading(false);
    }
  }, [contract, dispatch, loading]);

  return (
    <>
      <Head>
        <title>Hire IT - Contract</title>
        <meta name="description" content="Generated by Hire IT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={image.logo} />
      </Head>
      <main>
        <CHeader lang={lang} />

        {!loading && contract[0] && !isLoading ? (
          <div className="main">
            <CMenu lang={lang} isSearch={false} />
            <div className="main-payment">
              <ContractDetail />{" "}
            </div>
            <CFooter lang={lang} />
          </div>
        ) : (
          <CLoading fullScreen />
        )}
      </main>
    </>
  );
});
