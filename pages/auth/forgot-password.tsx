/* eslint-disable react/no-unescaped-entities */
import { getUserByEmail } from "@/apis/User";
import CButton from "@/common/components/controls/CButton";
import CTextField from "@/common/components/controls/CTextField";
import { STRAPI_API_TOKEN } from "@/constants";
import { verifyEmailHtml } from "@/pages/api/verifyEmail/htmlEmail";
import { updatePassword, updateUserActive } from "@/services/user";
import { Button } from "@mui/material";
import axios from "axios";
import { signOut, useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { BsCheck } from "react-icons/bs";
import { HiOutlineMail } from "react-icons/hi";
import { IoArrowBackOutline } from "react-icons/io5";
import OTPInput from "react-otp-input";
import { toast } from "react-toastify";
const ForgotPassword = () => {
  const [otp, setOtp] = React.useState("");
  const [value, setValue] = React.useState("");
  const [showInput, isShowInput] = React.useState(false);
  const [verified, isVerified] = React.useState(false);
  const [valueInput, setValueInput] = React.useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [idUser, setIdUser] = useState("");

  const router = useRouter();
  React.useEffect(() => {});
  const handelSendEmail = async () => {
    const email = valueInput;

    getUserByEmail({ email })
      .then(async (res) => {
        isShowInput(true);
        setIdUser(res.data[0].id);
        let verificationCode = "";
        for (let i = 0; i < 4; i++) {
          verificationCode += Math.floor(Math.random() * 10);
        }
        setValue(verificationCode);
        /**
         * Call api send email
         */
        const html = verifyEmailHtml(verificationCode);
        await axios.post("/api/verifyEmail/sendEmail", {
          email,
          html,
        });
      })
      .catch((err) => {
        isShowInput(false);
        toast.error("Email does not exist");
      });
  };

  const handelVerify = (valueInput: string) => {
    setOtp(valueInput);

    if (valueInput.length === 4) {
      if (valueInput === value) {
        isVerified(true);
      } else toast.error("Wrong verification codes");
    }
  };

  const handelUpadtePassword = () => {
    if (newPassword !== confirmPassword) {
      toast.error("Confirm password is not match");
      return;
    }
    if (newPassword.length < 8) {
      toast.error("Password must be at least 8 characters");
      return;
    }

    updatePassword(idUser, newPassword, STRAPI_API_TOKEN)
      .then((res) => {
        if (res.status === 200) {
          toast.success("Update password success");
          signOut({
            callbackUrl: "/auth/login",
          });
        }
      })
      .catch((err) => {
        toast.error("Update password fail");
      });
  };

  return (
    <main>
      <div className="root-verifyemail">
        {!verified ? (
          <div className="main-verifyemail">
            <div className="boder-icon">
              <div className="border-1">
                <HiOutlineMail
                  color="#7f56d9"
                  size={40}
                  className="icon-email"
                />
              </div>
            </div>
            <h2>Check your email</h2>
            <p className="text-label">We sent a verification link to</p>
            <CTextField
              disable={showInput}
              type="email"
              onChange={(e) => {
                setValueInput(e.target.value);
              }}
              className="text-password"
              placeholder="Enter your email"
            />
            {showInput ? (
              <>
                <div className="container-verifyemail">
                  <OTPInput
                    value={otp}
                    onChange={(data) => {
                      handelVerify(data);
                    }}
                    numInputs={4}
                    renderInput={(props) => (
                      <input {...props} className="verify-input" />
                    )}
                  />
                </div>
                <p className="text-label-receive">
                  Didn't receive the email? <span>Click to resend</span>
                </p>
              </>
            ) : (
              <Button
                className="button-submit verify-email"
                onClick={handelSendEmail}
                variant="contained"
              >
                Verify emaill
              </Button>
            )}
            <Button
              className="button_back"
              startIcon={<IoArrowBackOutline className="icon" />}
              onClick={() =>
                signOut({
                  callbackUrl: "/auth/login",
                })
              }
            >
              Back to login
            </Button>
          </div>
        ) : (
          <div className="form-veryfied">
            <div className="border-icon-1">
              <div className="border-icon-2">
                <div className="border-icon-3">
                  <BsCheck size={50} className="icon-check" color="#039855" />
                </div>
              </div>
            </div>
            <h2>Email verified</h2>
            <form action="">
              <div className="new-pass">
                <span>New password: </span>
                <CTextField
                  type="password"
                  onChange={(e) => setNewPassword(e.target.value)}
                  placeholder="Enter your new password"
                />
              </div>
              <div className="new-pass">
                <span>Confirm password: </span>
                <CTextField
                  type="password"
                  onChange={(e) => setConfirmPassword(e.target.value)}
                  placeholder="Enter your confirm password"
                />
              </div>
              <div className="new-pass__err">
                <span></span>
              </div>
              <div className="new-pass__btn">
                <Button
                  className="button-submit password"
                  onClick={() => {
                    handelUpadtePassword();
                  }}
                  variant="contained"
                >
                  Continue
                </Button>
              </div>
              <div className="new-pass__err">
                <span></span>
              </div>
            </form>
          </div>
        )}
      </div>
    </main>
  );
};

export default ForgotPassword;
