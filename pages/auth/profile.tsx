/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import Head from "next/head";
import image from "@/common/assets/image/all-image";
import CHeader from "../../common/components/layout/CHeader";


// redux
import { langSelector } from "@/redux/selector";
import { useSelector } from "react-redux";
import withPageProtected from "@/utils/withPageProtected";
import { useSession } from "next-auth/react";
import Profile from "@/modules/Auth/profile";
import ProfileCustomer from "@/modules/Auth/profile/ProfileCustomer";
export default withPageProtected(["Customer", "Employee", "Admin"], () => {
  const lang: "en" | "vn" = useSelector(langSelector);
  const { data: session } = useSession();
  return (
    <>
      <Head>
        <title>Hire IT - Profile</title>
        <meta name="description" content="Generated by Hire IT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={image.logo} />
      </Head>
      <main>
        <CHeader lang={lang} />
        <div className="main">
          <div className="main-profile">
            {session && session?.user.employee ? (
              <Profile type="user" lang={lang} />
            ) : (
              session && (
                <ProfileCustomer session={session} type="user" lang={lang} />
              )
            )}
          </div>
        </div>
      </main>
    </>
  );
});
