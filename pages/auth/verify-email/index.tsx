/* eslint-disable react/no-unescaped-entities */
import { verifyEmailHtml } from "@/pages/api/verifyEmail/htmlEmail";
import { updateUserActive } from "@/services/user";
import { Button } from "@mui/material";
import axios from "axios";
import { signOut, useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React from "react";
import { BsCheck } from "react-icons/bs";
import { HiOutlineMail } from "react-icons/hi";
import { IoArrowBackOutline } from "react-icons/io5";
import OTPInput from "react-otp-input";
import { toast } from "react-toastify";
import { Sessions } from "types/models";
const VerifyEmail = () => {
  const [otp, setOtp] = React.useState("");
  const [value, setValue] = React.useState("");
  const [showInput, isShowInput] = React.useState(false);
  const [verified, isVerified] = React.useState(false);
  const { status, data }: Sessions = useSession();

  const router = useRouter();
  React.useEffect(() => {});
  const onClick = async () => {
    const email = data.user?.email;

    isShowInput(true);
    let verificationCode = "";
    for (let i = 0; i < 4; i++) {
      verificationCode += Math.floor(Math.random() * 10);
    }
    setValue(verificationCode);

    /**
     * Call api send email
     */
    const html = verifyEmailHtml(verificationCode);
    await axios.post("/api/verifyEmail/sendEmail", {
      email,
      html,
    });
  };

  const handelVerify = (valueInput: string) => {
    setOtp(valueInput);

    if (valueInput.length === 4) {
      if (valueInput === value) {
        isVerified(true);
        document.cookie =
          "next-auth.session-token=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        updateUserActive(true, data.user?.id, data.user?.access_token)
          .then(async () => {
            data.user.isActive = true;
          })
          .catch((err) => console.log(err));
      } else toast.error("Wrong verification codes");
    }
  };
  return (
    <main>
      <div className="root-verifyemail">
        {!verified ? (
          <div className="main-verifyemail">
            <div className="boder-icon">
              <div className="border-1">
                <HiOutlineMail
                  color="#7f56d9"
                  size={40}
                  className="icon-email"
                />
              </div>
            </div>
            <h2>Check your email</h2>

            <p className="text-label">
              We sent a verification link to
              <br />
              {status !== "unauthenticated" && data && (
                <b>{data.user?.email}</b>
              )}
            </p>

            {showInput ? (
              <>
                <div className="container-verifyemail">
                  <OTPInput
                    value={otp}
                    onChange={(data) => {
                      handelVerify(data);
                    }}
                    numInputs={4}
                    renderInput={(props) => (
                      <input {...props} className="verify-input" />
                    )}
                  />
                </div>
                <p className="text-label-receive">
                  Didn't receive the email? <span>Click to resend</span>
                </p>
              </>
            ) : (
              <Button
                className="button-submit"
                onClick={onClick}
                variant="contained"
              >
                Enter code manually
              </Button>
            )}

            <Button
              className="button_back"
              startIcon={<IoArrowBackOutline className="icon" />}
              onClick={() =>
                signOut({
                  callbackUrl: "/auth/login",
                })
              }
            >
              Back to login
            </Button>
          </div>
        ) : (
          <div className="form-veryfied">
            <div className="border-icon-1">
              <div className="border-icon-2">
                <div className="border-icon-3">
                  <BsCheck size={50} className="icon-check" color="#039855" />
                </div>
              </div>
            </div>
            <h2>Email verified</h2>
            <p className="text-label">
              Your password has been successfully reset.
              <br />
              Click below to login magically
            </p>
            <Button
              className="button-submit"
              onClick={() => {
                router.replace("/");
              }}
              variant="contained"
            >
              Continue
            </Button>
          </div>
        )}
      </div>
    </main>
  );
};

export default VerifyEmail;
