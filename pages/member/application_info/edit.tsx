import image from "@/common/assets/image/all-image";
import ApplicationInfo from "@/modules/Member/components/ApplicationInfo";
import withPageEditInfo from "@/utils/withPageEditInfo";
import Head from "next/head";
import React from "react";

export default withPageEditInfo(["Employee"], () => {
  return (
    <>
      <Head>
        q<title>Hire IT - Application Infor</title>
        <meta name="description" content="Access to Hire IT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={image.logo} />
      </Head>
      <main>
        <div className="application-main">
          <ApplicationInfo />
        </div>
      </main>
    </>
  );
});
