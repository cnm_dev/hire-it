/* eslint-disable react-hooks/rules-of-hooks */
import image from "@/common/assets/image/all-image";
import CFooter from "@/common/components/layout/CFooter";
import CHeader from "@/common/components/layout/CHeader";
import CMenu from "@/common/components/layout/CMenu";
import Posting from "@/modules/JobPosting";
import Head from "next/head";
// redux
import { langSelector } from "@/redux/selector";
import { useSelector } from "react-redux";
import withPageProtected from "@/utils/withPageProtected";
import useCustomer from "@/hooks/useCustomer";
import { useSession } from "next-auth/react";
import CLoading from "@/common/components/controls/CLoading";

export default withPageProtected(["Customer", "Employee", "None"], () => {
  const { data: session } = useSession();
  const lang = useSelector(langSelector);
  const { customer, loading } = useCustomer({
    customerId: session?.user.id,
  });
  return (
    <>
      <Head>
        <title>Hire IT - Job Posting</title>
        <meta name="description" content="Generated by Hire IT" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href={image.logo} />
      </Head>

      {loading ? (
        <CLoading fullScreen />
      ) : (
        <main>
          <CHeader lang={lang} />
          <div className="main">
            <CMenu lang={lang} isSearch={false} />
            <div className="main-posting">
              <Posting customer={customer} lang={lang} />
            </div>
            <CFooter lang={lang} />
          </div>
        </main>
      )}
    </>
  );
});
