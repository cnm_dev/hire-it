export const langs = {
  global: {
    client: {
      en: "Client",
      vn: "Nhân viên",
    },
    staff: {
      en: "Staff",
      vn: "Nhân viên",
    },
    price: {
      en: "Price",
      vn: "Giá",
    },
    lowtohight: {
      en: "Price: Low to High",
      vn: "Giá: Thấp đến cao",
    },
    highttolow: {
      en: "Price: High to Low",
      vn: "Giá: Cap đến thấp",
    },
    amount: {
      en: "Amount hire",
      vn: "Tổng lượt thuê",
    },
    seeall: {
      en: "See all",
      vn: "Xem tất cả",
    },
    iamfree: {
      en: "I am freelancer",
      vn: "Tôi là freelancer",
    },
    iamhire: {
      en: "I want hire DEV",
      vn: "Tôi muốn thuê DEV",
    },
    free: {
      en: "free",
      vn: "Rảnh rỗi",
    },
    work: {
      en: "Working",
      vn: "Đang làm việc",
    },
    wait: {
      en: "Wait",
      vn: "Đang trong dự án chờ",
    },
    lableSkill: {
      en: "ALL",
      vn: "Đang trong dự án chờ",
    },
    inactive: {
      en: "Inactive",
      vn: "Không hoạt động",
    },
    reviews: {
      en: "Reviews",
      vn: "Đánh giá",
    },
    star: {
      en: "Star",
      vn: "Sao",
    },
    status: {
      en: "Status",
      vn: "Trạng thái",
    },
    currency: {
      en: "Currency",
      vn: "Tiền tệ",
    },
    min: {
      en: "Min",
      vn: "Nhỏ nhất",
    },
    to: {
      en: "to",
      vn: "Đến",
    },
    max: {
      en: "Max",
      vn: "Lớn nhất",
    },
    skillexp: {
      en: "Skill & Experience",
      vn: "Kỹ năng & Kinh nghiệm",
    },
    language: {
      en: "Language",
      vn: "Ngôn ngữ",
    },
    year: {
      en: "Year",
      vn: "Năm",
    },
    month: {
      en: "Month",
      vn: "tháng",
    },
    week: {
      en: "Week",
      vn: "Tuần",
    },
    day: {
      en: "Day",
      vn: "Ngày",
    },
    more: {
      en: "More",
      vn: "Hơn",
    },
    category: {
      en: "Category",
      vn: "Loại",
    },
    edit: { en: "Edit", vn: "Chỉnh sửa" },
    dev: {
      en: "Developer",
      vn: "Lập trình viên",
    },
    designer: {
      en: "Designer",
      vn: "Người thiết kế",
    },
    proman: {
      en: "Project Manager",
      vn: "Người quản lý dự án",
    },
    tester: {
      en: "Tester",
      vn: "Người kiểm thử",
    },
    total: {
      en: "Total",
      vn: "Tổng",
    },
    lastupdate: {
      en: "Last update",
      vn: "Cập nhật gần nhất",
    },
    hourseago: {
      en: "hourse ago",
      vn: "giờ trước",
    },
    minago: {
      en: "min ago",
      vn: "phút trước",
    },
    mostrecent: {
      en: "Most recent",
      vn: "Gần đây nhất",
    },
    latest: {
      en: "Latest",
      vn: "Cũ nhất",
    },
    sortby: {
      en: "Sort By",
      vn: "Sắp xếp",
    },
    all: {
      en: "All",
      vn: "Tất cả",
    },
    des: {
      en: "Description",
      vn: "Miêu tả",
    },
  },
  header: {
    typeEmployee: {
      en: "Your is: Employee",
      vn: "Bạn là: Nhân viên",
    },
    typeCustomer: {
      en: "Your is: Customer",
      vn: "Bạn là: Khách hàng",
    },
    typeAdmin: {
      en: "Your is: Admin",
      vn: "Bạn là: Quản trị viên",
    },
    home: {
      en: "Home",
      vn: "Trang Chủ",
    },
    member: {
      en: "Member",
      vn: "Nhân Viên",
    },
    jobpost: {
      en: "Job Posting",
      vn: "Bài đăng tuyển",
    },
    buyproject: {
      en: "Buy Project",
      vn: "Mua Dự án",
    },
    appy: {
      en: "Apply as a Freelancer",
      vn: "Đăng ký làm Freelancer",
    },
    join: {
      en: "Apply to join the World’s Top HireIT",
      vn: "Đăng ký tham gia Top HireIT hàng đầu thế giới",
    },
  },
  nav: {
    profile: {
      en: "Profile",
      vn: "Trang cá nhân",
    },
    setting: {
      en: "Setting",
      vn: "Cài đặt",
    },
    help: {
      en: "Help & Support",
      vn: "Hỗ trợ",
    },
    log: {
      en: "Logout",
      vn: "Đăng xuất",
    },
  },
  footer: {
    company: {
      en: "Company",
      vn: "Công ty",
    },
    about: {
      en: "About us",
      vn: "Về chúng tôi",
    },
    contact: {
      en: "Contact us",
      vn: "Liên hệ",
    },
    feature: {
      en: "Feature",
      vn: "Tính năng",
    },
    discover: {
      en: "Discover",
      vn: "Khám phá",
    },
    getour: {
      en: "Get our latest updates",
      vn: "Nhận thông tin cập nhật mới nhất của chúng tôi",
    },
    youremai: {
      en: "Your email",
      vn: "Email của bạn",
    },
    subcrite: {
      en: "Subcribe",
      vn: "Đăng ký",
    },
    joinour: {
      en: "Join our comunity",
      vn: "Tham gia cộng đồng của chúng tôi",
    },
  },
  home: {
    place_search: {
      en: "Search member",
      vn: "Tìm kím nhân viên",
    },
    go: {
      en: "Go",
      vn: "Tìm",
    },
    head: {
      en: "Simple human resource hire with",
      vn: "Thuê nhân sự cực kỳ đơn giản với",
    },
    des: {
      en: " is one of the leading providers of high-quality IT staffing services in Vietnam for both local and international businesses. With a team of experienced and knowledgeable experts who are dedicated to their work, we are committed to delivering the most optimal and effective solutions for our clients. Additionally, we guarantee a high level of security and reliability when working with our clients important information and data. Please contact us for the best advice and support!.",
      vn: "là một trong những nhà cung cấp dịch vụ nhân sự CNTT chất lượng cao hàng đầu tại Việt Nam cho các doanh nghiệp trong nước và quốc tế. Với đội ngũ chuyên gia giàu kinh nghiệm, am hiểu và tận tâm với công việc, chúng tôi cam kết mang đến những giải pháp tối ưu và hiệu quả nhất cho khách hàng. Ngoài ra, chúng tôi đảm bảo mức độ bảo mật và độ tin cậy cao khi làm việc với các thông tin và dữ liệu quan trọng của khách hàng. Hãy liên hệ với chúng tôi để được tư vấn và hỗ trợ tốt nhất!.",
    },
    hirenow: {
      en: "Hire IT Now",
      vn: "Thuê nhân sự ngay!",
    },
    hotmember: {
      en: "Hot Member",
      vn: "Nhân viên nổi bật",
    },
    job: {
      en: "Job",
      vn: "Dự án",
    },
    market: {
      en: "Explore the marketplace",
      vn: "Khám phá thị trường",
    },
    becom: {
      en: "Becom our partner",
      vn: "Trở thành đối tác với chúng tôi",
    },
    becomdes: {
      en: "Are you a freelancer or you are looking to hire people? Get in touch and join us!",
      vn: "Bạn có phải là một freelancer hay bạn đang muốn thuê người? Hãy liên lạc và tham gia với chúng tôi!",
    },
    easy: {
      en: "Easy and convenient to use",
      vn: "Đơn giản và thuận tiện khi sử dụng",
    },
    pay: {
      en: "Simple payment",
      vn: "Thanh toán dễ dàng",
    },
    paydes: {
      en: "Payment is extremely simple with many forms. Prestige, safety and many incentives for you.",
      vn: "Thanh toán cực kì đơn giản với nhiều hình thức. Uy tín, an toàn và nhiều ưu đãi cho bạn.",
    },
    interface: {
      en: "Intuitive interface",
      vn: "Giao diện trực quan",
    },
    interfacedes: {
      en: "The interface is designed to be simple, intuitive and extremely easy to use for newbies.",
      vn: "Giao diện được thiết kế đơn giản, trực quan và cực kỳ dễ dàng sử dụng cho những người mới.",
    },
    image: {
      en: "Upload file",
      vn: "Cập nhật file",
    },
    imgaedes: {
      en: "Upload your work (image, video, audio), add a title and description, properties, and unlockable content.",
      vn: "Tải lên tác phẩm của bạn (hình ảnh, video, âm thanh), thêm tiêu đề và mô tả, thuộc tính và nội dung.",
    },
    feedback: {
      en: "Feedback and reviews",
      vn: "Phản hồi và đánh giá",
    },
    feedbackdes: {
      en: "Easily evaluate your personal opinion through each use of our services and provide a more intuitive experience.",
      vn: "Dễ dàng đánh giá ý kiến của nhân của bạn qua mỗi lần sử dụng dịch vụ của chúng tôi và mang đến trải nghiệm trực quan hơn",
    },
  },
  member: {
    listmember: {
      en: "List Member",
      vn: "Danh sách nhân viên",
    },
    client: {
      en: "Client",
      vn: "Nhân viên",
    },
    reviews: {
      en: "Reviews",
      vn: "Đánh giá",
    },
  },
  profile: {
    history: {
      en: "History hire employments",
      vn: "Lịch sử thuê việc làm",
    },
    education: {
      en: "Education",
      vn: "Trình độ",
    },
    skill: {
      en: "Skill",
      vn: "Kỹ năng",
    },
    rvforstaff: {
      en: "reviews for staff",
      vn: "đánh giá cho nhân viên",
    },
  },
  posting: {
    jobpostL: {
      en: "Job Posting",
      vn: "Bài đăng tuyển",
    },
    companyName: {
      en: "Company Name",
      vn: "Tên công ty",
    },
    placeholderInputCompanyName: {
      en: "Enter your Company Name",
      vn: "Nhập tên công ty của bạn",
    },
    errorInputCompanyName: {
      en: "Company Name is required",
      vn: "Tên công ty không được để trống",
    },
    jobTitle: {
      en: "Job Title",
      vn: "Chức vụ",
    },
    jobTitleAbout: {
      en: "A Job title must describe one position only",
      vn: "Chức danh Công việc chỉ được mô tả một vị trí",
    },

    placeholderInputjobTitle: {
      en: "Enter your Title Job",
      vn: "Nhập vị trí công việc",
    },
    errorInputjobTitle: {
      en: "Title Job is required",
      vn: "Chức vụ không được để trống",
    },
    expiredTime: {
      en: "Expired time",
      vn: "Thời gian ứng tuyển",
    },
    expiredAbout: {
      en: "Time you will no longer recruit personnel for this post",
      vn: "Thời điểm bạn sẽ không tiếp tục tuyển dụng nhân viên cho vị trí này nữa",
    },
    aboutCompany: {
      en: "About company",
      vn: "Giới thiệu về công ty",
    },
    aboutCompanyDes: {
      en: "Basic description of your company to make this job more detailed",
      vn: "Để làm công việc này trở nên chi tiết hơn, hãy cung cấp một mô tả cơ bản về công ty của bạn.",
    },

    placeholderInputAboutCompany: {
      en: "Enter your about company",
      vn: "Nhập mô tả công ty của bạn",
    },
    errorInputAboutCompany: {
      en: "About company is required",
      vn: "Mô tả công ty không được để trống",
    },

    aboutLocationCompany: {
      en: "Location company",
      vn: "Địa chỉ công ty",
    },
    aboutLocationCompanyDes: {
      en: "Address where applicants can go to interview or work",
      vn: "Địa chỉ ứng viên có thể đến phỏng vấn hoặc làm việc",
    },
    placeholderaboutLocationCompany: {
      en: "Enter your Location company",
      vn: "Nhập địa chỉ công ty của bạn",
    },
    erroraboutLocationCompany: {
      en: "Location is required",
      vn: "Địa chỉ không được để trống",
    },
    companyAbout: {
      en: "Name company you want hire employee work in",
      vn: "Tên công ty bạn muốn thuê nhân viên làm việc trong",
    },
    paymentAbout: {
      en: "Pay money / employee",
      vn: "Tên công ty bạn muốn thuê nhân viên làm việc trong",
    },
    paymentAboutDes: {
      en: "The amount you will pay per employee",
      vn: "Số tiền bạn sẽ trả cho mỗi nhân viên",
    },
    paymentAboutPlace: {
      en: "Enter your Pay money",
      vn: "Nhập mức lương của bạn",
    },
    paymentAboutError: {
      en: "Pay money is required",
      vn: "Mức lương không thể để trống",
    },
    recruitmentAbout: {
      en: "Recruitment number (employee)",
      vn: "Số lượng tuyển dụng (Nhân viên)",
    },
    recruitmentAboutDes: {
      en: "Number of employees you want to recruit for the company",
      vn: "Số lượng nhân viên bạn muốn tuyển dụng cho công ty",
    },
    recruitmentAboutOlace: {
      en: "Enter your recruitment number",
      vn: "Nhập số lượng nhân viên",
    },
    recruitmentAboutError: {
      en: "Recruitment number is required",
      vn: "Số lượng nhân viên không được để trống",
    },

    gendertAbout: {
      en: "Employee gender",
      vn: "Giới tính nhân viên",
    },
    gendertAboutDes: {
      en: "The gender of the employee you want to hire",
      vn: "Giới tính của nhân viên bạn muốn thuê",
    },
    gendertAboutError: {
      en: "Gender is required",
      vn: "Giới tính không được để trống",
    },

    workTimeAbout: {
      en: "Working time",
      vn: "Thời gian làm việc",
    },
    workTimeAboutDes: {
      en: "Working time when employees come to work",
      vn: "Thời gian làm việc khi nhân viên đến làm",
    },
    workTimeAboutError: {
      en: "Working time is required",
      vn: "Thời gian làm việc không được để trống",
    },

    experienceAbout: {
      en: "Employee experience require",
      vn: "Kinh nghiệm nhân viên yêu cầu",
    },
    experienceAboutDes: {
      en: "The experience of the employee you want to hire",
      vn: "Kinh nghiệm của nhân viên mà bạn muốn thuê",
    },
    experienceAboutPlace: {
      en: "Enter your experience require",
      vn: "Nhập số năm kinh nghiệm",
    },
    experienceAboutError: {
      en: "Experience is required",
      vn: "Năm kinh nghiệm không được để trống",
    },

    posting: {
      en: "Posting",
      vn: "Bài đăng",
    },
    placesearch: {
      en: "Search company or language develop",
      vn: "Tìm kiếm công ty hoặc ngôn ngữ lập trình",
    },
    general: {
      en: "General information",
      vn: "Thông tin chung",
    },
    jobdes: {
      en: "Job description",
      vn: "Mô tả công việc",
    },
    candidate: {
      en: "Candidate Requirements:",
      vn: "Yêu cầu ứng tuyển",
    },
    send: {
      en: "Send CV",
      vn: "Gửi CV",
    },
  },
  payment: {
    contract: {
      en: "Contract Payment",
      vn: "Thanh toán hợp đồng",
    },
    choosemethod: {
      en: "Choose payment method",
      vn: "Chọn phương thức thanh toán",
    },
    connected: {
      en: "Connected",
      vn: "Đã kết nối",
    },
    waiting: {
      en: "Waiting",
      vn: "Đang chờ",
    },
    yourinfor: {
      en: "Your information",
      vn: "Thông tin của bạn",
    },
    namecompany: {
      en: "Name Company",
      vn: "Tên công ty",
    },
    name: {
      en: "Your Name",
      vn: "Tên của bạn",
    },
    faxcode: {
      en: "Fax Number",
      vn: "Số Fax",
    },
    location: {
      en: "Location",
      vn: "Địa chỉ",
    },
    contractterm: {
      en: "Contract terms",
      vn: "Điều khoản hợp đồng",
    },
    precontract: {
      en: "Preview Contract",
      vn: "Tổng quan hợp đồng",
    },
    amountmoney: {
      en: "Amount Money",
      vn: "Tổng tiền",
    },
    check: {
      en: "By checking this if you agree to the contract terms",
      vn: "Chọn vào ô này nếu bạn đồng ý với các điều khoản hợp đồng",
    },
    payment: {
      en: "Payment",
      vn: "Thanh toán",
    },
    help: {
      en: "Need help?",
      vn: "Bạn cần trợ giúp",
    },
    support: {
      en: "Support",
      vn: "Hỗ trợ",
    },
  },
  auth: {
    welcome: {
      en: "Welcome To HireIT",
      vn: "Chào mừng đến với HireIT",
    },
    please: {
      en: "Please enter your details",
      vn: "Vui lòng điền thông tin của bạn",
    },
    remember: {
      en: "Remember for 30 days",
      vn: "Ghi nhớ tôi trong 30 ngày",
    },
    forgot: {
      en: "Forgot password?",
      vn: "Quên mật khẩu",
    },
    sigin: {
      en: "Sign in",
      vn: "Đăng nhập",
    },
    withgoole: {
      en: "with Google",
      vn: "với Google",
    },
  },
};
