import axios from "axios";

export async function uploadImage(file: any) {
  const formData = new FormData();
  formData.append("file", file);
  formData.append("upload_preset", "hireit");
  formData.append("api_key", "784254792915144");
  const data = await axios.post(
    "https://api.cloudinary.com/v1_1/Hire-it/image/upload",
    formData,
    {
      headers: {
        "Content-Type": "multipart/form-data",
      },
    }
  );

  return data;
}

export async function uploadResume(file: any) {
  const formData = new FormData();
  formData.append("file", file);
  formData.append("upload_preset", "hire_it");
  formData.append("api_key", "263625892793981");
  const data = await axios.post(
    "https://api.cloudinary.com/v1_1/hire-it/image/upload",
    formData
  );

  return data;
}
