import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";

export async function getPositions(token: Token | { jwt: string }) {
  const query = gql`
    {
      positions {
        data {
          id
          attributes {
            positionName
          }
        }
      }
    }
  `;

  const { positions } = await fetcherQL(token.jwt, query);
  return transformer(positions);
}
