import { Token } from "@/types/nextAuth";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { transformer } from "@/utils/server";
import { createPostingSaveForm, updatePostingSaveForm } from "@/forms/postingSave";

export async function createPostingSave(
  token: Token | { jwt: string },
  queryVariable: createPostingSaveForm
) {
  const { idCustomer, idEmployee, idPosting } = queryVariable;

  const query = gql`
    mutation (
      $idCustomer: ID
      $idEmployee: ID
      $idPosting: [ID!]
      $publishedAt: DateTime!
    ) {
      createPostingSave(
        data: {
          customer: $idCustomer
          employee: $idEmployee
          job_postings: $idPosting
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;
  const { createPostingSave } = await fetcherQL(token.jwt, query, {
    idCustomer,
    idEmployee,
    idPosting,
    publishedAt: new Date(),
  });

  return transformer(createPostingSave);
}

export async function getPostingCustomer(
  token: Token | { jwt: string },
  queryVariable: { userId: string | string[] | undefined }
) {
  const { userId } = queryVariable;

  const query = gql`
    query ($userId: ID!) {
      postingSaves(
        filters: {
          customer: { users_permissions_user: { id: { eq: $userId } } }
        }
      ) {
        data {
          id
          attributes {
            customer {
              data {
                id
                attributes {
                  companyName
                }
              }
            }
            employee {
              data {
                id
              }
            }
            job_postings {
              data {
                id
                attributes {
                  jobPostingName
                  description
                  wage
                  requirement
                  aboutCompany
                  timeRemain
                  email
                  experience
                  gender
                  location
                  quantity
                  phone
                  timework
                  companyName
                  createdAt
                  image
                  customer {
                    data {
                      attributes {
                        companyName
                        users_permissions_user {
                          data {
                            attributes {
                              fullName
                              email
                              phoneNumber
                              avartar
                              address
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { postingSaves } = await fetcherQL(token.jwt, query, { userId });

  return transformer(postingSaves);
}

export async function getPostingEmployee(
  token: Token | { jwt: string },
  queryVariable: { userId: string | string[] | undefined }
) {
  const { userId } = queryVariable;

  const query = gql`
    query ($userId: ID!) {
      postingSaves(
        filters: {
          employee: { users_permissions_user: { id: { eq: $userId } } }
        }
      ) {
        data {
          id
          attributes {
            customer {
              data {
                id
                attributes {
                  companyName
                }
              }
            }
            employee {
              data {
                id
              }
            }
            job_postings {
              data {
                id
                attributes {
                  jobPostingName
                  description
                  wage
                  requirement
                  aboutCompany
                  timeRemain
                  email
                  experience
                  gender
                  location
                  quantity
                  phone
                  timework
                  companyName
                  createdAt
                  image
                  customer {
                    data {
                      attributes {
                        companyName
                        users_permissions_user {
                          data {
                            attributes {
                              fullName
                              email
                              phoneNumber
                              avartar
                              address
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { postingSaves } = await fetcherQL(token.jwt, query, { userId });

  return transformer(postingSaves);
}

export async function updatePostingSave(
  token: Token | { jwt: string },
  queryVariables: updatePostingSaveForm
) {
  const { idPosting, idCustomer, idEmployee, idPostSave } = queryVariables;

  const query = gql`
    mutation ($idPostSave: ID!, $idPosting: [ID!]) {
      updatePostingSave(id: $idPostSave, data: { job_postings: $idPosting }) {
        data {
          id
        }
      }
    }
  `;

  const { updatePostingSave } = await fetcherQL(token.jwt, query, {
    idPosting,
    idCustomer,
    idEmployee,
    idPostSave,
  });

  return transformer(updatePostingSave);
}
