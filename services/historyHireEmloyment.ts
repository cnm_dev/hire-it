import {} from "@/types";
import { HistoryHireEmployment } from "@/types/models";
import { transformer } from "@/utils/server";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { DeleteEmployeeForm } from "@/forms/employee";
export async function createHistoryEmployment(
  jwt: string,
  queryVariable: HistoryHireEmployment
) {
  const {
    employeeId,
    companyName,
    descriptionJob,
    technologie,
    workingPosition,
  } = queryVariable;

  let query = "";
  query = gql`
    mutation (
      $employeeId: ID!
      $companyName: String
      $descriptionJob: String
      $technologie: String
      $workingPosition: String
      $publishedAt: DateTime!
    ) {
      createHistoryHireEmployment(
        data: {
          employee: $employeeId
          companyName: $companyName
          descriptionJob: $descriptionJob
          technologie: $technologie
          workingPosition: $workingPosition
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { createHistoryEmployment } = await fetcherQL(jwt, query, {
    employeeId,
    companyName,
    descriptionJob,
    technologie,
    workingPosition,
    publishedAt: new Date(),
  });
  return transformer(createHistoryEmployment);
}

export async function updateHistoryEmployment(
  jwt: string,
  queryVariable: HistoryHireEmployment
) {
  const { id, companyName, descriptionJob, technologie, workingPosition } =
    queryVariable;
  let query = "";
  query = gql`
    mutation (
      $id: ID!
      $companyName: String
      $descriptionJob: String
      $technologie: String
      $workingPosition: String
    ) {
      updateHistoryHireEmployment(
        id: $id
        data: {
          companyName: $companyName
          descriptionJob: $descriptionJob
          technologie: $technologie
          workingPosition: $workingPosition
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { updateHistoryEmployment } = await fetcherQL(jwt, query, {
    id,
    companyName,
    descriptionJob,
    technologie,
    workingPosition,
  });
  return transformer(updateHistoryEmployment);
}

export async function deleteHistoryEmployment(
  jwt: string,
  queryVariables: {
    id: string | undefined;
  }
) {
  const { id } = queryVariables;
  const query = gql`
    mutation ($id: ID!) {
      updateHistoryHireEmployment(id: $id, data: { publishedAt: null }) {
        data {
          id
        }
      }
    }
  `;

  const { updateHistoryHireEmployment } = await fetcherQL(jwt, query, { id });

  return transformer(updateHistoryHireEmployment);
}
