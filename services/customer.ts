import { Token } from "@/types/nextAuth";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { transformer } from "@/utils/server";
import { GetCustomerForm, UpdateProfileCustomerForm } from "@/forms/customer";

export async function getCustomer(
  token: Token | { jwt: string },
  queryVariables: GetCustomerForm
) {
  const { customerId } = queryVariables;
  const query = gql`
    query ($customerId: ID!) {
      customers(
        filters: { users_permissions_user: { id: { eq: $customerId } } }
      ) {
        data {
          id
          attributes {
            companyName
            numberOfContracts
            codeFax
            users_permissions_user {
              data {
                id
                attributes {
                  fullName
                  email
                  avartar
                  address
                  phoneNumber
                  blocked
                }
              }
            }
          }
        }
      }
    }
  `;

  const { customers } = await fetcherQL(token.jwt, query, {
    customerId,
  });

  return transformer(customers);
}

export async function getCustomers(token: Token | { jwt: string }) {
  const query = gql`
    query {
      customers {
        data {
          id
          attributes {
            companyName
            numberOfContracts
            codeFax
            users_permissions_user {
              data {
                id
                attributes {
                  fullName
                  email
                  avartar
                  address
                  phoneNumber
                  blocked
                }
              }
            }
          }
        }
      }
    }
  `;

  const { customers } = await fetcherQL(token.jwt, query);

  return transformer(customers);
}

export async function updateProfileCustomer(
  token: Token | { jwt: string },
  queryVariables: UpdateProfileCustomerForm
) {
  const {
    customerId,
    idUser,
    address,
    avatar,
    codeFax,
    comanyName,
    currentPass,
    newPass,
    phoneNumber,
  } = queryVariables;

  const query = gql`
    mutation (
      $customerId: ID!
      $idUser: ID!
      $address: String
      $avatar: String
      $codeFax: String
      $comanyName: String
      $newPass: String
      $phoneNumber: String
    ) {
      updateCustomer(
        id: $customerId
        data: { codeFax: $codeFax, companyName: $comanyName }
      ) {
        data {
          id
        }
      }
      updateUsersPermissionsUser(
        id: $idUser
        data: {
          phoneNumber: $phoneNumber
          address: $address
          password: $newPass
          avartar: $avatar
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { updateCustomer } = await fetcherQL(token.jwt, query, {
    customerId,
    idUser,
    address,
    avatar,
    codeFax,
    comanyName,
    currentPass,
    newPass,
    phoneNumber,
  });

  return transformer(updateCustomer);
}
