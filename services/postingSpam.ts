import { Token } from "@/types/nextAuth";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { transformer } from "@/utils/server";
import { createPostingSpamForm } from "@/forms/postingSpam";

export async function createPostingSpam(
  token: Token | { jwt: string },
  queryVariable: createPostingSpamForm
) {
  const { idCustomer, idEmployee, idPosting, reason } = queryVariable;

  const query = gql`
    mutation (
      $idCustomer: ID
      $idEmployee: ID
      $idPosting: ID!
      $reason: String!
      $publishedAt: DateTime!
    ) {
      createPostingSpam(
        data: {
          Reason: $reason
          customer: $idCustomer
          employee: $idEmployee
          job_posting: $idPosting
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;
  const { createPostingSpam } = await fetcherQL(token.jwt, query, {
    idCustomer,
    idEmployee,
    idPosting,
    reason,
    publishedAt: new Date(),
  });

  return transformer(createPostingSpam);
}
