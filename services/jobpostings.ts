import { gql } from "graphql-request";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import {
  GetJobPostingForm,
  UpdateJobPostingForm,
  createJostPostingForm,
} from "@/forms/jobPosting";
import {} from "@/types";

export async function getJobPostings(token: Token | { jwt: string }) {
  const query = gql`
    {
      jobPostings(pagination: { limit: -1 }, sort: "createdAt:desc") {
        data {
          id
          attributes {
            jobPostingName
            description
            wage
            requirement
            aboutCompany
            timeRemain
            status
            email
            experience
            gender
            location
            quantity
            phone
            timework
            companyName
            createdAt
            image
            posting_spams {
              data {
                id
                attributes {
                  Reason
                  createdAt
                  employee {
                    data {
                      id
                      attributes {
                        users_permissions_user {
                          data {
                            id
                            attributes {
                              fullName
                              email
                              avartar
                            }
                          }
                        }
                      }
                    }
                  }
                  customer {
                    data {
                      id
                      attributes {
                        users_permissions_user {
                          data {
                            id
                            attributes {
                              fullName
                              email
                              avartar
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            customer {
              data {
                id
                attributes {
                  companyName
                  users_permissions_user {
                    data {
                      attributes {
                        fullName
                        email
                        phoneNumber
                        avartar
                        address
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { jobPostings } = await fetcherQL(token.jwt, query);
  return transformer(jobPostings);
}

export async function getJobPosting(
  token: Token | { jwt: string },
  queryVariable: GetJobPostingForm
) {
  const { postId } = queryVariable;
  const id = postId;
  let query = "";
  query = gql`
    query ($id: ID!) {
      jobPostings(filters: { id: { eq: $id } }) {
        data {
          id
          attributes {
            jobPostingName
            description
            wage
            requirement
            aboutCompany
            timeRemain
            email
            experience
            gender
            location
            quantity
            phone
            timework
            companyName
            image
            customer {
              data {
                attributes {
                  companyName

                  users_permissions_user {
                    data {
                      attributes {
                        fullName
                        email
                        phoneNumber
                        avartar
                        address
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { jobPostings } = await fetcherQL(token.jwt, query, { id });
  return transformer(jobPostings);
}

export async function createJobPosting(
  token: Token | { jwt: string },
  queryVariable: createJostPostingForm
) {
  const {
    aboutCompany,
    description,
    email,
    experience,
    gender,
    jobPostingName,
    location,
    phone,
    quantity,
    requirement,
    timeRemain,
    timework,
    wage,
    customerId,
    companyName,
    image,
  } = queryVariable;

  let query = "";
  query = gql`
    mutation (
      $jobPostingName: String
      $description: String
      $wage: Float
      $requirement: String
      $aboutCompany: String
      $customerId: ID!
      $timeRemain: DateTime
      $email: String
      $experience: String
      $gender: String
      $location: String
      $quantity: Int
      $phone: String
      $timework: String
      $publishedAt: DateTime
      $companyName: String
      $image: String
    ) {
      createJobPosting(
        data: {
          jobPostingName: $jobPostingName
          description: $description
          wage: $wage
          requirement: $requirement
          aboutCompany: $aboutCompany
          customer: $customerId
          timeRemain: $timeRemain
          email: $email
          experience: $experience
          gender: $gender
          location: $location
          quantity: $quantity
          phone: $phone
          timework: $timework
          publishedAt: $publishedAt
          companyName: $companyName
          image: $image
          status: "pending"
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { createJobPosting } = await fetcherQL(token.jwt, query, {
    aboutCompany,
    description,
    email,
    experience,
    gender,
    jobPostingName,
    location,
    phone,
    quantity,
    requirement,
    timeRemain,
    timework,
    wage,
    customerId,
    companyName,
    image,
    publishedAt: new Date(),
  });
  return transformer(createJobPosting);
}

export async function getJobPostingsCustomer(
  token: Token | { jwt: string },
  queryVariable: { userId: string | string[] | undefined }
) {
  const { userId } = queryVariable;
  const query = gql`
    query ($userId: ID!) {
      jobPostings(
        pagination: { limit: -1 }
        sort: "createdAt:desc"
        filters: {
          customer: { users_permissions_user: { id: { eq: $userId } } }
        }
      ) {
        data {
          id
          attributes {
            jobPostingName
            description
            wage
            requirement
            aboutCompany
            timeRemain
            email
            experience
            gender
            location
            quantity
            phone
            timework
            companyName
            createdAt
            image
            customer {
              data {
                attributes {
                  companyName
                  users_permissions_user {
                    data {
                      attributes {
                        fullName
                        email
                        phoneNumber
                        avartar
                        address
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { jobPostings } = await fetcherQL(token.jwt, query, { userId });
  return transformer(jobPostings);
}

export async function updateJopPosting(
  token: Token | { jwt: string },
  variableForm: UpdateJobPostingForm
) {
  const { idJposting, status } = variableForm;
  const query = gql`
    mutation ($idJposting: ID!, $status: String) {
      updateJobPosting(id: $idJposting, data: { status: $status }) {
        data {
          id
        }
      }
    }
  `;

  const { updateJobPosting } = await fetcherQL(token.jwt, query, {
    idJposting,
    status,
  });
  return transformer(updateJobPosting);
}
