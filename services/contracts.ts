import { gql } from "graphql-request";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import {
  CreateContractForm,
  GetContractsForm,
  UpdateStatusContractForm,
} from "@/forms/contracts";

export async function getContracts(
  token: Token | { jwt: string },
  queryVariable: GetContractsForm
) {
  const { customerId } = queryVariable;
  let query = "";
  let variables;
  if (customerId !== "undefined") {
    query = gql`
      query ($customerId: ID!) {
        contracts(
          pagination: { limit: -1 }
          sort: "updatedAt:desc"
          filters: {
            project: {
              customer: { users_permissions_user: { id: { eq: $customerId } } }
            }
          }
        ) {
          data {
            id
            attributes {
              status
              startDate
              endDate
              updatedAt
              createdAt
              isExtend
              project {
                data {
                  id
                  attributes {
                    projectName
                    budgetFrom
                    budgetTo
                    companySizeFrom
                    companySizeTo
                    openForRemote
                    position
                    commitment
                    productSpecs
                    projectLengthFrom
                    projectLengthTo
                    readyToStart
                    totalMoney
                    situation
                    publishedAt
                    customer {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                username
                                fullName
                                email
                              }
                            }
                          }
                          companyName
                          numberOfContracts
                          codeFax
                          totalSpending
                        }
                      }
                    }
                    employees {
                      data {
                        id
                        attributes {
                          statusWork
                          hourlyRate
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                fullName
                                email
                              }
                            }
                          }
                          skills {
                            data {
                              id
                              attributes {
                                skillName
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `;
    variables = { customerId };
  } else {
    query = gql`
      query {
        contracts(pagination: { limit: -1 }, sort: "createdAt:desc") {
          data {
            id
            attributes {
              status
              startDate
              endDate
              createdAt
              contract_ends {
                data {
                  id
                  attributes {
                    reason
                    createdAt
                  }
                }
              }
              project {
                data {
                  id
                  attributes {
                    projectName
                    budgetFrom
                    budgetTo
                    companySizeFrom
                    companySizeTo
                    openForRemote
                    position
                    commitment
                    productSpecs
                    projectLengthFrom
                    projectLengthTo
                    readyToStart
                    totalMoney
                    situation
                    publishedAt

                    customer {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                username
                                fullName
                                avartar
                              }
                            }
                          }
                          companyName
                          numberOfContracts
                          codeFax
                          totalSpending
                        }
                      }
                    }
                    employees {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                fullName
                                email
                              }
                            }
                          }
                          skills {
                            data {
                              id
                              attributes {
                                skillName
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `;
  }

  const { contracts } = await fetcherQL(token.jwt, query, variables);
  return transformer(contracts);
}

export async function createContract(
  token: Token | { jwt: string },
  queryVariables: CreateContractForm
) {
  const { status, projectId } = queryVariables;
  const query = gql`
    mutation ($status: String, $projectId: ID!, $publishedAt: DateTime!) {
      createContract(
        data: {
          status: $status
          project: $projectId
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { createContract } = await fetcherQL(token.jwt, query, {
    status,
    projectId,
    publishedAt: new Date(),
  });

  return transformer(createContract);
}

export async function updateStatusContract(
  token: Token | { jwt: string },
  queryVariables: UpdateStatusContractForm
) {
  const { status, contractId } = queryVariables;
  const query = gql`
    mutation ($contractId: ID!, $status: String!) {
      updateContract(id: $contractId, data: { status: $status }) {
        data {
          id
        }
      }
    }
  `;

  const { updateContract } = await fetcherQL(token.jwt, query, {
    status,
    contractId,
  });

  return transformer(updateContract);
}
