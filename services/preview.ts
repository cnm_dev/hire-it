import { gql } from "graphql-request";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import { CreateReviewForm } from "@/forms/review";

export async function createPreview(
  token: Token | { jwt: string },
  queryVariables: CreateReviewForm
) {
  const { content, contractId, customerId, employeeId, rate } = queryVariables;
  const query = gql`
    mutation (
      $content: String
      $contractId: ID!
      $customerId: ID!
      $employeeId: ID!
      $rate: Float!
      $publishedAt: DateTime!
    ) {
      createPreview(
        data: {
          content: $content
          contract: $contractId
          customer: $customerId
          employee: $employeeId
          rate: $rate
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { createPreview } = await fetcherQL(token.jwt, query, {
    content,
    contractId,
    customerId,
    employeeId,
    rate,
    publishedAt: new Date(),
  });

  return transformer(createPreview);
}
