import { gql } from "graphql-request";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import { GetContractForm, UpdateContractForm } from "@/forms/contracts";

export async function getContract(
  token: Token | { jwt: string },
  queryVariable: GetContractForm
) {
  const { customerId, contractId } = queryVariable;
  let query = "";

  if (customerId === "87") {
    query = gql`
      query ($contractId: ID!) {
        contracts(
          pagination: { limit: -1 }
          sort: "createdAt:desc"
          filters: { id: { eq: $contractId } }
        ) {
          data {
            id
            attributes {
              status
              startDate
              endDate
              isExtend
              project {
                data {
                  id
                  attributes {
                    projectName
                    budgetFrom
                    budgetTo
                    companySizeFrom
                    companySizeTo
                    openForRemote
                    position
                    commitment
                    productSpecs
                    projectLengthFrom
                    projectLengthTo
                    readyToStart
                    totalMoney
                    situation
                    publishedAt
                    customer {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                email
                                fullName
                                address
                                phoneNumber
                              }
                            }
                          }
                          companyName
                          numberOfContracts
                          codeFax
                          totalSpending
                        }
                      }
                    }
                    employees {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                fullName
                                avartar
                                phoneNumber
                                email
                                address
                              }
                            }
                          }
                          position {
                            data {
                              attributes {
                                positionName
                              }
                            }
                          }
                          skills {
                            data {
                              id
                              attributes {
                                skillName
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              previews {
                data {
                  id
                  attributes {
                    content
                    rate
                    employee {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                username
                                fullName
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `;
  } else {
    query = gql`
      query ($customerId: ID!, $contractId: ID!) {
        contracts(
          pagination: { limit: -1 }
          sort: "createdAt:desc"
          filters: {
            project: {
              customer: { users_permissions_user: { id: { eq: $customerId } } }
            }
            id: { eq: $contractId }
          }
        ) {
          data {
            id
            attributes {
              status
              startDate
              endDate
              isExtend
              project {
                data {
                  id
                  attributes {
                    projectName
                    budgetFrom
                    budgetTo
                    companySizeFrom
                    companySizeTo
                    openForRemote
                    position
                    commitment
                    productSpecs
                    projectLengthFrom
                    projectLengthTo
                    readyToStart
                    totalMoney
                    situation
                    publishedAt
                    customer {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                email
                                fullName
                                address
                                phoneNumber
                              }
                            }
                          }
                          companyName
                          numberOfContracts
                          codeFax
                          totalSpending
                        }
                      }
                    }
                    employees {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                fullName
                                avartar
                                phoneNumber
                                email
                                address
                              }
                            }
                          }
                          position {
                            data {
                              attributes {
                                positionName
                              }
                            }
                          }
                          skills {
                            data {
                              id
                              attributes {
                                skillName
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
              previews {
                data {
                  id
                  attributes {
                    content
                    rate
                    employee {
                      data {
                        id
                        attributes {
                          users_permissions_user {
                            data {
                              id
                              attributes {
                                username
                                fullName
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    `;
  }

  const { contracts } = await fetcherQL(token.jwt, query, {
    customerId,
    contractId,
  });
  return transformer(contracts);
}

export async function updateContract(
  token: Token | { jwt: string },
  queryVariable: UpdateContractForm
) {
  const { contractId, endDate, startDate, status, isExtend } = queryVariable;

  let query = "";
  if (isExtend !== undefined) {
    if (endDate) {
      query = gql`
        mutation ($contractId: ID!, $endDate: DateTime!, $isExtend: Boolean!) {
          updateContract(
            id: $contractId
            data: { isExtend: $isExtend, endDate: $endDate }
          ) {
            data {
              id
            }
          }
        }
      `;
    } else {
      query = gql`
        mutation ($contractId: ID!, $isExtend: Boolean!) {
          updateContract(id: $contractId, data: { isExtend: $isExtend }) {
            data {
              id
            }
          }
        }
      `;
    }
  } else {
    query = gql`
      mutation (
        $contractId: ID!
        $status: String!
        $startDate: DateTime!
        $endDate: DateTime!
      ) {
        updateContract(
          id: $contractId
          data: { status: $status, startDate: $startDate, endDate: $endDate }
        ) {
          data {
            id
          }
        }
      }
    `;
  }

  const { updateContract } = await fetcherQL(token.jwt, query, {
    endDate,
    startDate,
    status,
    contractId,
    isExtend,
  });
  return transformer(updateContract);
}
