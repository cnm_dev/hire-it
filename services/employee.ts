import { Token } from "@/types/nextAuth";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { transformer } from "@/utils/server";
import {
  DeleteEmployeeForm,
  UpdateStatusForm,
  formGetStartEmployee,
} from "@/forms/employee";
import {
  HistoryHireEmployment,
  formFreelancingPreferencesEmployee,
  formProfessionalExperienceEmployee,
} from "@/types/models";

export async function updateStatusEmployee(
  token: Token | { jwt: string },
  queryVariables: UpdateStatusForm
) {
  const { employeeId, status, joinDate } = queryVariables;

  const query = gql`
    mutation ($employeeId: ID!, $status: String!, $joinDate: DateTime) {
      updateEmployee(
        id: $employeeId
        data: { status: $status, joinDate: $joinDate }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { updateEmployee } = await fetcherQL(token.jwt, query, {
    employeeId,
    status,
    joinDate,
  });

  return transformer(updateEmployee);
}

export async function updateResumeEmployee(
  token: Token | { jwt: string },
  queryVariables: {
    employeeId: number;
    resume: string;
    completionLevel: number;
  }
) {
  const { employeeId, resume, completionLevel } = queryVariables;

  const query = gql`
    mutation ($employeeId: ID!, $resume: String!, $completionLevel: Int!) {
      updateEmployee(
        id: $employeeId
        data: { resume: $resume, completionLevel: $completionLevel }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { updateEmployee } = await fetcherQL(token.jwt, query, {
    employeeId,
    resume,
    completionLevel,
  });

  return transformer(updateEmployee);
}

export async function updateformGetStartEmployee(
  token: Token | { jwt: string },
  queryVariables: formGetStartEmployee
) {
  const {
    id,
    employeeId,
    fullName,
    country,
    city,
    nationality,
    englishProficiency,
    reasonApplying,
    completionLevel,
  } = queryVariables;

  const query = gql`
    mutation (
      $employeeId: ID!
      $id: ID!
      $fullName: String!
      $country: String!
      $city: String!
      $nationality: String!
      $englishProficiency: String!
      $reasonApplying: String!
      $completionLevel: Int!
    ) {
      updateEmployee(
        id: $employeeId
        data: {
          country: $country
          city: $city
          nationality: $nationality
          englishProficiency: $englishProficiency
          reasonApplying: $reasonApplying
          completionLevel: $completionLevel
        }
      ) {
        data {
          id
        }
      }
      updateUsersPermissionsUser(id: $id, data: { fullName: $fullName }) {
        data {
          id
        }
      }
    }
  `;

  const { updateEmployee } = await fetcherQL(token.jwt, query, {
    id,
    employeeId,
    fullName,
    country,
    city,
    nationality,
    englishProficiency,
    reasonApplying,
    completionLevel,
  });

  return transformer(updateEmployee);
}

export async function updateEmployeeFreelancingPreferences(
  token: Token | { jwt: string },
  queryVariables: formFreelancingPreferencesEmployee
) {
  const {
    employeeId,
    careerInterests,
    interestedProductTypes,
    remoteSuccessKey,
    completionLevel,
  } = queryVariables;

  const query = gql`
    mutation (
      $employeeId: ID!
      $careerInterests: String!
      $interestedProductTypes: String!
      $remoteSuccessKey: String!
      $completionLevel: Int!
    ) {
      updateEmployee(
        id: $employeeId
        data: {
          careerInterests: $careerInterests
          interestedProductTypes: $interestedProductTypes
          remoteSuccessKey: $remoteSuccessKey
          completionLevel: $completionLevel
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { updateEmployee } = await fetcherQL(token.jwt, query, {
    employeeId,
    careerInterests,
    interestedProductTypes,
    remoteSuccessKey,
    completionLevel,
  });

  return transformer(updateEmployee);
}

export async function updateProfessionalExperienceEmployee(
  token: Token | { jwt: string },
  queryVariables: formProfessionalExperienceEmployee
) {
  const {
    employeeId,
    certifications,
    yearsOfExperience,
    gitHub,
    hourlyRate,
    isFullTime,
    linkedIn,
    skills,
    completionLevel,
  } = queryVariables;

  const convertCertifications: number[] = [];
  certifications.map((v) => convertCertifications.push(parseInt(v)));
  const convertSkills: number[] = [];
  skills.map((v) => convertSkills.push(parseInt(v)));

  // const skillConnections = skills.map((id) => ({ id: parseInt(id) }));
  // const certificationConnections = certifications.map((id) => ({
  //   id: parseInt(id),
  // }));

  const query = gql`
    mutation (
      $employeeId: ID!
      $yearsOfExperience: Float!
      $hourlyRate: Float!
      $isFullTime: Boolean!
      $linkedIn: String
      $gitHub: String
      $completionLevel: Int!
      $skills: [ID!]!
      $certifications: [ID!]
    ) {
      updateEmployee(
        id: $employeeId
        data: {
          yearsOfExperience: $yearsOfExperience
          gitHub: $gitHub
          hourlyRate: $hourlyRate
          isFullTime: $isFullTime
          linkedIn: $linkedIn
          completionLevel: $completionLevel
          skills: $skills
          certifications: $certifications
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { updateEmployee } = await fetcherQL(token.jwt, query, {
    employeeId,
    completionLevel,
    skills,
    certifications,
    yearsOfExperience: Number(yearsOfExperience),
    gitHub,
    hourlyRate: Number(hourlyRate),
    isFullTime,
    linkedIn,
  });

  return transformer(updateEmployee);
}

export async function deleteEmployee(
  token: Token | { jwt: string },
  queryVariables: DeleteEmployeeForm
) {
  const { employeeId } = queryVariables;
  const query = gql`
    mutation ($employeeId: ID!) {
      updateEmployee(id: $employeeId, data: { publishedAt: null }) {
        data {
          id
        }
      }
    }
  `;

  const { updateEmployee } = await fetcherQL(token.jwt, query, { employeeId });

  return transformer(updateEmployee);
}

export async function updateCountryAndCityEmployee(
  jwt: string,
  queryVariables: {
    employeeId: string | string[] | undefined;
    country?: string;
    city?: string;
    avartar?: string;
    phoneNumber?: string;
    password?: string;
    id: string;
  }
) {
  const { employeeId, country, city, avartar, password, phoneNumber, id } =
    queryVariables;
  const query = gql`
    mutation (
      $employeeId: ID!
      $id: ID!
      $country: String
      $city: String
      $avartar: String
      $phoneNumber: String
      $password: String
    ) {
      updateEmployee(
        id: $employeeId
        data: { country: $country, city: $city }
      ) {
        data {
          id
        }
      }
      updateUsersPermissionsUser(
        id: $id
        data: {
          avartar: $avartar
          password: $password
          phoneNumber: $phoneNumber
        }
      ) {
        data {
          id
        }
      }
    }
  `;
  const { updateEmployee } = await fetcherQL(jwt, query, {
    employeeId,
    id,
    country,
    city,
    avartar,
    password,
    phoneNumber,
  });

  return transformer(updateEmployee);
}
