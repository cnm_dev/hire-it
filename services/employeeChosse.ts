import { Token } from "@/types/nextAuth";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { transformer } from "@/utils/server";
import {
  DeleteEmployeeChosseForm,
  createEmployeeChosseForm,
} from "@/forms/employeeChosse";

export async function createEmployeeChoose(
  token: Token | { jwt: string },
  queryVariable: createEmployeeChosseForm
) {
  const { idCustomer, idEmployee } = queryVariable;

  const query = gql`
    mutation ($idCustomer: ID!, $idEmployee: [ID!], $publishedAt: DateTime!) {
      createEmployeeChoose(
        data: {
          customer: $idCustomer
          employees: $idEmployee
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;
  const { createEmployeeChoose } = await fetcherQL(token.jwt, query, {
    idCustomer,
    idEmployee,
    publishedAt: new Date(),
  });

  return transformer(createEmployeeChoose);
}

export async function getEmployeeChosse(
  token: Token | { jwt: string },
  queryVariable: { userId: string | string[] | undefined }
) {
  const { userId } = queryVariable;
  const query = gql`
    query ($userId: ID!) {
      employeeChooses(
        pagination: { limit: -1 }
        filters: {
          customer: { users_permissions_user: { id: { eq: $userId } } }
        }
      ) {
        data {
          id
          attributes {
            customer {
              data {
                id
              }
            }
            employees(pagination: { limit: -1 }) {
              data {
                id
                attributes {
                  statusWork
                  hourlyRate
                  isRemote
                  position {
                    data {
                      id
                      attributes {
                        positionName
                      }
                    }
                  }
                  yearsOfExperience
                  start
                  users_permissions_user {
                    data {
                      id
                      attributes {
                        email
                        fullName
                        avartar
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { employeeChooses } = await fetcherQL(token.jwt, query, { userId });

  return transformer(employeeChooses);
}

export async function deleteEmployeeChosse(
  token: Token | { jwt: string },
  queryVariables: DeleteEmployeeChosseForm
) {
  const { employeeChosseId, listIdEmployee } = queryVariables;

  const query = gql`
    mutation ($employeeChosseId: ID!, $listIdEmployee: [ID!]) {
      updateEmployeeChoose(
        id: $employeeChosseId
        data: { employees: $listIdEmployee }
      ) {
        data {
          id
        }
      }
    }
  `;
  const { updateEmployeeChoose } = await fetcherQL(token.jwt, query, {
    employeeChosseId,
    listIdEmployee,
  });

  return transformer(updateEmployeeChoose);
}
