import {} from "@/types";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";

export async function certificationName(token: Token | { jwt: string }) {
  const query = gql`
    {
      certifications(pagination: { limit: -1 }) {
        data {
          id
          attributes {
            certificationName
          }
        }
      }
    }
  `;

  const { certifications } = await fetcherQL(token.jwt, query);
  return transformer(certifications);
}
