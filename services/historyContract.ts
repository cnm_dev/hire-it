import { gql } from "graphql-request";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import {
  createContractHistoryForm,
  getContractHistoryForm,
} from "@/forms/historyContract";

export async function createContractHistory(
  token: Token | { jwt: string },
  queryVariables: createContractHistoryForm
) {
  const { status, contractId, dateUpdate, endDate, totalMoney, type } =
    queryVariables;
  let query = "";
  if (endDate) {
    query = gql`
      mutation (
        $contractId: ID!
        $totalMoney: Float
        $dateUpdate: DateTime
        $endDate: DateTime
        $status: String
        $type: String
        $publishedAt: DateTime
      ) {
        createContractHistory(
          data: {
            contract: $contractId
            totalMoney: $totalMoney
            dateUpdate: $dateUpdate
            endDate: $endDate
            status: $status
            type: $type
            publishedAt: $publishedAt
          }
        ) {
          data {
            id
          }
        }
      }
    `;
  } else {
    query = gql`
      mutation (
        $contractId: ID!
        $totalMoney: Float
        $dateUpdate: DateTime
        $status: String
        $type: String
        $publishedAt: DateTime
      ) {
        createContractHistory(
          data: {
            contract: $contractId
            totalMoney: $totalMoney
            dateUpdate: $dateUpdate
            status: $status
            type: $type
            publishedAt: $publishedAt
          }
        ) {
          data {
            id
          }
        }
      }
    `;
  }

  const { createContractHistory } = await fetcherQL(token.jwt, query, {
    status,
    contractId,
    dateUpdate,
    endDate,
    totalMoney,
    type,
    publishedAt: new Date(),
  });

  return transformer(createContractHistory);
}

export async function getContractHistory(
  token: Token | { jwt: string },
  queryVariables: getContractHistoryForm
) {
  const { contractId } = queryVariables;
  const query = gql`
    query ($contractId: ID!) {
      contractHistories(
        pagination: { limit: -1 }
        sort: "createdAt:desc"
        filters: { contract: { id: { eq: $contractId } } }
      ) {
        data {
          id
          attributes {
            contract {
              data {
                id
                attributes {
                  status
                  project {
                    data {
                      id
                      attributes {
                        projectName
                      }
                    }
                  }
                }
              }
            }
            createdAt
            totalMoney
            dateUpdate
            endDate
            status
            type
          }
        }
      }
    }
  `;

  const { contractHistories } = await fetcherQL(token.jwt, query, {
    contractId,
  });

  return transformer(contractHistories);
}
