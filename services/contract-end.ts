import { Token } from "@/types/nextAuth";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { transformer } from "@/utils/server";
import { createEndContractForm } from "@/forms/contract-end";

export async function createContractEnd(
  token: Token | { jwt: string },
  queryVariables: createEndContractForm
) {
  const { contractId, customerId, reason } = queryVariables;
  const query = gql`
    mutation (
      $contractId: ID!
      $customerId: ID!
      $reason: String
      $publishedAt: DateTime!
    ) {
      createContractEnd(
        data: {
          contract: $contractId
          customer: $customerId
          reason: $reason
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { createContractEnd } = await fetcherQL(token.jwt, query, {
    contractId,
    customerId,
    reason,
    publishedAt: new Date(),
  });

  return transformer(createContractEnd);
}
