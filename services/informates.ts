import { gql } from "graphql-request";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import {
  CreateInformateForm,
  GetInformatesForm,
  UpdateInformatesForm,
} from "@/forms/informates";

export async function createInformates(
  token: Token | { jwt: string },
  variableForm: CreateInformateForm
) {
  const { description, title, customerId, employeeId, route, type, adminId } =
    variableForm;
  let query = "";
  if (customerId) {
    query = gql`
      mutation (
        $description: String
        $title: String
        $customerId: ID!
        $route: String
        $type: String
        $publishedAt: DateTime!
      ) {
        createInformate(
          data: {
            description: $description
            title: $title
            customer: $customerId
            route: $route
            type: $type
            publishedAt: $publishedAt
          }
        ) {
          data {
            id
          }
        }
      }
    `;
  } else if (employeeId) {
    query = gql`
      mutation (
        $description: String
        $title: String
        $employeeId: ID!
        $route: String
        $type: String
        $publishedAt: DateTime!
      ) {
        createInformate(
          data: {
            description: $description
            title: $title
            employee: $employeeId
            route: $route
            type: $type
            publishedAt: $publishedAt
          }
        ) {
          data {
            id
          }
        }
      }
    `;
  } else {
    query = gql`
      mutation (
        $description: String
        $title: String
        $adminId: ID!
        $route: String
        $type: String
        $publishedAt: DateTime!
      ) {
        createInformate(
          data: {
            description: $description
            title: $title
            admin: $adminId
            route: $route
            type: $type
            publishedAt: $publishedAt
          }
        ) {
          data {
            id
          }
        }
      }
    `;
  }

  const { createInformate } = await fetcherQL(token.jwt, query, {
    description,
    title,
    customerId,
    employeeId,
    route,
    type,
    adminId,
    publishedAt: new Date(),
  });
  return transformer(createInformate);
}

export async function getInformates(
  token: Token | { jwt: string },
  variableForm: GetInformatesForm
) {
  const { userId, typeUser } = variableForm;
  let query = "";
  if (typeUser === "Customer") {
    query = gql`
      query ($userId: ID!) {
        informates(
          pagination: { limit: -1 }
          filters: {
            customer: { users_permissions_user: { id: { eq: $userId } } }
          }
          sort: "createdAt:desc"
        ) {
          data {
            id
            attributes {
              title
              description
              type
              read
              route
              createdAt
            }
          }
        }
      }
    `;
  } else {
    query = gql`
      query ($userId: ID!) {
        informates(
          pagination: { limit: -1 }
          filters: {
            admin: { users_permissions_user: { id: { eq: $userId } } }
          }
          sort: "createdAt:desc"
        ) {
          data {
            id
            attributes {
              title
              description
              type
              read
              route
              createdAt
            }
          }
        }
      }
    `;
  }

  const { informates } = await fetcherQL(token.jwt, query, { userId });
  return transformer(informates);
}

export async function getInformatesAdmin(
  token: Token | { jwt: string },
  variableForm: GetInformatesForm
) {
  const { userId } = variableForm;
  const query = gql`
    query ($userId: ID!) {
      informates(
        pagination: { limit: -1 }
        filters: { admin: { users_permissions_user: { id: { eq: $userId } } } }
        sort: "createdAt:desc"
      ) {
        data {
          id
          attributes {
            title
            description
            type
            read
            route
            createdAt
          }
        }
      }
    }
  `;

  const { informates } = await fetcherQL(token.jwt, query, { userId });
  return transformer(informates);
}

export async function updateInformates(
  token: Token | { jwt: string },
  variableForm: UpdateInformatesForm
) {
  const { informateId, read } = variableForm;
  const query = gql`
    mutation ($informateId: ID!, $read: Boolean) {
      updateInformate(id: $informateId, data: { read: $read }) {
        data {
          id
        }
      }
    }
  `;

  const { updateInformate } = await fetcherQL(token.jwt, query, {
    informateId,
    read,
  });
  return transformer(updateInformate);
}
