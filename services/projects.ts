import { gql } from "graphql-request";

import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import { UpdatePropjectForm, createProjectForm } from "@/forms/project";

export async function getProjects(token: Token | { jwt: string }) {
  const query = gql`
    query {
      projects(pagination: { limit: -1 }, sort: "createdAt:desc") {
        data {
          id
          attributes {
            projectName
            budgetFrom
            description
            totalMoney
            timeRequireDeveloper
            budgetTo
            companySizeFrom
            companySizeTo
            openForRemote
            position
            commitment
            status
            productSpecs
            projectLengthFrom
            projectLengthTo
            skills {
              data {
                id
                attributes {
                  skillName
                }
              }
            }
            situation
            readyToStart
            createdAt
            employees {
              data {
                id
              }
            }
            customer {
              data {
                id
                attributes {
                  companyName
                  codeFax
                  users_permissions_user {
                    data {
                      id
                      attributes {
                        email
                        username
                        avartar
                        fullName
                        phoneNumber
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { projects } = await fetcherQL(token.jwt, query);

  return transformer(projects);
}

export async function createProject(
  token: Token | { jwt: string },
  queryVariables: createProjectForm
) {
  const {
    totalMoney,
    projectName,
    budgetFrom,
    budgetTo,
    companySizeFrom,
    companySizeTo,
    openForRemote,
    position,
    commitment,
    productSpecs,
    projectLengthFrom,
    projectLengthTo,
    readyToStart,
    situation,
    listIDEmployee,
    status,
    skills,
    customerId,
  } = queryVariables;

  const query = gql`
    mutation (
      $projectName: String
      $budgetFrom: Float
      $budgetTo: Float
      $companySizeFrom: Int
      $companySizeTo: Int
      $OpenForRemote: Boolean
      $position: String
      $commitment: Boolean
      $productSpecs: String
      $projectLengthFrom: Float
      $projectLengthTo: Float
      $readyToStart: String
      $totalMoney: Float
      $situation: String
      $status: String
      $publishedAt: DateTime!
      $listIDEmployee: [ID!]
      $customerId: ID!
      $skills: [ID!]
    ) {
      createProject(
        data: {
          projectName: $projectName
          budgetFrom: $budgetFrom
          budgetTo: $budgetTo
          companySizeFrom: $companySizeFrom
          companySizeTo: $companySizeTo
          openForRemote: $OpenForRemote
          position: $position
          commitment: $commitment
          productSpecs: $productSpecs
          projectLengthFrom: $projectLengthFrom
          projectLengthTo: $projectLengthTo
          readyToStart: $readyToStart
          totalMoney: $totalMoney
          situation: $situation
          publishedAt: $publishedAt
          status: $status
          employees: $listIDEmployee
          customer: $customerId
          skills: $skills
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { createProject } = await fetcherQL(token.jwt, query, {
    totalMoney,
    projectName,
    budgetFrom,
    budgetTo,
    companySizeFrom,
    companySizeTo,
    openForRemote,
    position,
    commitment,
    productSpecs,
    projectLengthFrom,
    projectLengthTo,
    readyToStart,
    situation,
    listIDEmployee,
    status,
    customerId,
    skills,
    publishedAt: new Date(),
  });

  return transformer(createProject);
}

export async function updatePropject(
  token: Token | { jwt: string },
  queryVariable: UpdatePropjectForm
) {
  const {
    projectId,
    totalMoney,
    budgetFrom,
    budgetTo,
    commitment,
    companySizeFrom,
    companySizeTo,
    listIDEmployee,
    openForRemote,
    position,
    projectLengthFrom,
    projectLengthTo,
    situation,
    skills,
    status,
  } = queryVariable;
  const query = gql`
    mutation (
      $projectId: ID!
      $totalMoney: Float
      $budgetFrom: Float
      $budgetTo: Float
      $companySizeFrom: Int
      $companySizeTo: Int
      $openForRemote: Boolean
      $position: String
      $commitment: Boolean
      $projectLengthFrom: Float
      $projectLengthTo: Float
      $situation: String
      $status: String
      $listIDEmployee: [ID]
      $skills: [ID]
    ) {
      updateProject(
        id: $projectId
        data: {
          budgetFrom: $budgetFrom
          budgetTo: $budgetTo
          companySizeFrom: $companySizeFrom
          companySizeTo: $companySizeTo
          openForRemote: $openForRemote
          position: $position
          commitment: $commitment
          projectLengthFrom: $projectLengthFrom
          projectLengthTo: $projectLengthTo
          totalMoney: $totalMoney
          situation: $situation
          status: $status
          employees: $listIDEmployee
          skills: $skills
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { updateProject } = await fetcherQL(token.jwt, query, {
    projectId,
    totalMoney,
    budgetFrom,
    budgetTo,
    commitment,
    companySizeFrom,
    companySizeTo,
    listIDEmployee,
    openForRemote,
    position,
    projectLengthFrom,
    projectLengthTo,
    situation,
    skills,
    status,
  });
  return transformer(updateProject);
}
