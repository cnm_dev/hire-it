import { gql } from "graphql-request";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import {
  GetContractsEmployee,
  GetEmployeeForm,
  filterContractEmployeeForm,
} from "@/forms/employees";

export async function getEmployees(token: Token | { jwt: string }) {
  const query = gql`
    {
      employees(
        filters: { users_permissions_user: { isActive: { eq: true } } }
        pagination: { limit: -1 }
        sort: "createdAt:desc"
      ) {
        data {
          id
          attributes {
            users_permissions_user {
              data {
                id
                attributes {
                  fullName
                  email
                  avartar
                  blocked
                }
              }
            }
            certifications {
              data{
                id
                attributes{
                  certificationName
                }
              }
            }
            resume
            reasonApplying
            careerInterests
            remoteSuccessKey
            interestedProductTypes
            linkedIn
            gitHub
            englishProficiency
            gender
            dateOfBirth
            hourlyRate
            nationality
            city
            country
            yearsOfExperience
            status
            statusWork
            start
            createdAt
            position {
              data {
                attributes {
                  positionName
                }
              }
            }
            skills {
              data {
                id
                attributes {
                  skillName
                }
              }
            }
            previews {
              data {
                id
                attributes {
                  content
                  rate
                  customer {
                    data {
                      id
                      attributes {
                        companyName
                      }
                    }
                  }
                }
              }
            }
            projects(pagination: { limit: 1 }, sort: "endDate:desc") {
              data {
                id
                attributes {
                  contracts(
                    pagination: { limit: 1 }
                    sort: "endDate:desc"
                    filters: {
                      or: [
                        { status: { eq: "active" } }
                        { status: { eq: "wait" } }
                      ]
                    }
                  ) {
                    data {
                      id
                      attributes {
                        status
                        endDate
                        startDate
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { employees } = await fetcherQL(token.jwt, query);
  return transformer(employees);
}

export async function getEmployee(
  token: Token | { jwt: string },
  queryVariable: GetEmployeeForm
) {
  const { profileId } = queryVariable;
  const query = gql`
    query ($profileId: ID!) {
      employees(filters: { id: { eq: $profileId } }) {
        data {
          id
          attributes {
            users_permissions_user {
              data {
                id
                attributes {
                  fullName
                  avartar
                  phoneNumber
                  email
                  address
                }
              }
            }
            position {
              data {
                attributes {
                  positionName
                }
              }
            }
            gender
            dateOfBirth
            hourlyRate
            nationality
            yearsOfExperience
            country
            city
            status
            start
            joinDate
            statusWork
            history_hire_employments {
              data {
                id
                attributes {
                  companyName
                  workingPosition
                  technologie
                  descriptionJob
                }
              }
            }
            educations {
              data {
                id
                attributes {
                  educationMajors
                  educationaPlace
                  educationTime
                  educationalInstitution
                }
              }
            }
            skills {
              data {
                id
                attributes {
                  skillName
                }
              }
            }
            previews {
              data {
                id
                attributes {
                  createdAt
                  content
                  rate
                  customer {
                    data {
                      id
                      attributes {
                        companyName
                        users_permissions_user {
                          data {
                            attributes {
                              fullName
                              avartar
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            projects {
              data {
                id
                attributes {
                  contracts {
                    data {
                      id
                      attributes {
                        status
                        endDate
                        startDate
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { employees } = await fetcherQL(token.jwt, query, { profileId });
  return transformer(employees);
}

export async function getStatusEmployee(
  token: Token | { jwt: string },
  userID: string | any
) {
  const query = gql`
    query ($userID: ID!) {
      employees(filters: { users_permissions_user: { id: { eq: $userID } } }) {
        data {
          id
          attributes {
            status
            completionLevel
            users_permissions_user {
              data {
                attributes {
                  fullName
                }
              }
            }
            position {
              data {
                attributes {
                  positionName
                }
              }
            }
          }
        }
      }
    }
  `;

  const { employees } = await fetcherQL(token.jwt, query, { userID });
  return transformer(employees);
}

export async function filterContractEmployee(
  token: Token | { jwt: string },
  queryVariables: filterContractEmployeeForm
) {
  const {
    status,
    isRemote,
    isFullTime,
    hourlyRateFrom,
    hourlyRateTo,
    positionName,
    skills,
  } = queryVariables;

  const query = gql`
    query (
      $status: String
      $isRemote: Boolean
      $isFullTime: Boolean
      $hourlyRateFrom: Float
      $hourlyRateTo: Float
      $positionName: String
      $skills: [ID!]
    ) {
      employees(
        filters: {
          and: [
            { status: { eq: $status } }
            { isRemote: { eq: $isRemote } }
            { isFullTime: { eq: $isFullTime } }
            { hourlyRate: { gte: $hourlyRateFrom } }
            { hourlyRate: { lte: $hourlyRateTo } }
            { statusWork: { eq: "free" } }
            { position: { positionName: { eq: $positionName } } }
            { skills: { id: { in: $skills } } }
          ]
        }
      ) {
        data {
          id
          attributes {
            hourlyRate
            picture
            start
            position {
              data {
                id
                attributes {
                  positionName
                }
              }
            }
            users_permissions_user {
              data {
                attributes {
                  fullName
                  email
                  avartar
                  address
                  phoneNumber
                }
              }
            }
          }
        }
      }
    }
  `;

  const { employees } = await fetcherQL(token.jwt, query, {
    status,
    isRemote,
    isFullTime,
    hourlyRateFrom,
    hourlyRateTo,
    positionName,
    skills,
  });

  return transformer(employees);
}

export async function getContractsEmployee(
  token: Token | { jwt: string },
  queryVariables: GetContractsEmployee
) {
  const { userEmployeeId } = queryVariables;

  const query = gql`
    query ($userEmployeeId: ID!) {
      employees(
        filters: { users_permissions_user: { id: { eq: $userEmployeeId } } }
      ) {
        data {
          id
          attributes {
            projects(
              filters: {
                contracts: {
                  or: [
                    { status: { eq: "complete" } }
                    { status: { eq: "active" } }
                  ]
                }
              }
            ) {
              data {
                id
                attributes {
                  projectName
                  contracts {
                    data {
                      id
                      attributes {
                        status
                        startDate
                        endDate
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  `;

  const { employees } = await fetcherQL(token.jwt, query, { userEmployeeId });

  return transformer(employees);
}
