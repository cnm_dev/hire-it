import { STRAPI_API_TOKEN, STRAPI_API_URL } from "@/constants";
import { LoginData, RegistryCustomer, RegistryStaff } from "@/types/models";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import axios from "axios";
import { transformer } from "@/utils/server";
import { CreateEmployeeForm } from "@/forms/employee";
import { Token } from "@/types/nextAuth";
import { CreateCustomerForm } from "@/forms/customers";

export async function login({ email, password }: LoginData) {
  const { data } = await axios.post(
    "https://hire-it-orcxw.ondigitalocean.app/api/auth/local",
    {
      identifier: email,
      password,
    }
  );
  return data;
}

export async function registerCustomer({
  email,
  phoneNumber,
  password,
  fullName,
}: RegistryCustomer) {
  const data = await axios
    .post(STRAPI_API_URL + "/auth/local/register", {
      username: email,
      email: email,
      phoneNumber: phoneNumber,
      fullName: fullName,
      password: password,
      type_user: 1,
    })
    .then((data) => {
      createCustomer(data.data.jwt, {
        users_permissions_user: data.data.user.id,
      });
    })
    .catch((error) => {
      // Handle error.
      console.log("An error occurred:", error.response);
    });
  return data;
}

export async function registerStaff({
  email,
  phoneNumber,
  password,
  position,
  fullName,
}: RegistryStaff) {
  const data = await axios
    .post(STRAPI_API_URL + "/auth/local/register", {
      username: email,
      email: email,
      phoneNumber: phoneNumber,
      fullName: fullName,
      password: password,
      type_user: 3,
    })
    .then((data) => {
      createEmployee(data.data.jwt, {
        position: position,
        users_permissions_user: data.data.user.id,
      });
    })
    .catch((error) => {
      // Handle error.
      console.log("An error occurred:", error.response);
    });

  return data;
}
// Create customer
export async function createCustomer(
  token: string,
  queryVariables: CreateCustomerForm
) {
  const query = gql`
    mutation ($users_permissions_user: ID, $publishedAt: DateTime!) {
      createCustomer(
        data: {
          users_permissions_user: $users_permissions_user
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { users_permissions_user } = queryVariables;
  const { createCustomer } = await fetcherQL(token, query, {
    users_permissions_user,
    publishedAt: new Date(),
  });
  return transformer(createCustomer);
}
//  create employee
export async function createEmployee(
  token: string,
  queryVariables: CreateEmployeeForm
) {
  const query = gql`
    mutation (
      $position: ID
      $users_permissions_user: ID
      $publishedAt: DateTime!
    ) {
      createEmployee(
        data: {
          position: $position
          users_permissions_user: $users_permissions_user
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { position, users_permissions_user } = queryVariables;
  const { createEmployee } = await fetcherQL(token, query, {
    position,
    users_permissions_user,
    publishedAt: new Date(),
  });
  return transformer(createEmployee);
}

export async function getTypeUser(
  token: string,
  queryVariables: { email: any }
) {
  const { email } = queryVariables;
  let query = "";

  query = gql`
    query ($email: String!) {
      usersPermissionsUsers(filters: { email: { eq: $email } }) {
        data {
          id
          attributes {
            email
            type_user {
              data {
                id
                attributes {
                  typeUserName
                }
              }
            }
          }
        }
      }
    }
  `;

  const { usersPermissionsUsers } = await fetcherQL(token, query, { email });

  return transformer(usersPermissionsUsers);
}

export async function getInforUserByEmail(
  token: string,
  queryVariables: { email: any }
) {
  const { email } = queryVariables;
  let query = "";

  query = gql`
    query ($email: String!) {
      usersPermissionsUsers(filters: { email: { eq: $email } }) {
        data {
          id
          attributes {
            phoneNumber
            email
          }
        }
      }
    }
  `;

  const { usersPermissionsUsers } = await fetcherQL(token, query, { email });

  return transformer(usersPermissionsUsers);
}

export async function getInforUserByPhone(
  token: string,
  queryVariables: { phoneNumber: any }
) {
  const { phoneNumber } = queryVariables;
  let query = "";

  query = gql`
    query ($phoneNumber: String!) {
      usersPermissionsUsers(filters: { phoneNumber: { eq: $phoneNumber } }) {
        data {
          id
          attributes {
            phoneNumber
            email
          }
        }
      }
    }
  `;

  const { usersPermissionsUsers } = await fetcherQL(token, query, {
    phoneNumber,
  });

  return transformer(usersPermissionsUsers);
}

export async function getInforUser(
  token: string,
  queryVariables: { email: any }
) {
  const { email } = queryVariables;
  let query = "";

  query = gql`
    query ($email: String!) {
      usersPermissionsUsers(filters: { email: { eq: $email } }) {
        data {
          id
          attributes {
            isActive
            fullName
            phoneNumber
            avartar
          }
        }
      }
    }
  `;

  const { usersPermissionsUsers } = await fetcherQL(token, query, { email });

  return transformer(usersPermissionsUsers);
}

export async function updateUserActive(
  isActive: boolean,
  userId: string,
  jwt: string
) {
  const data = axios.put(
    STRAPI_API_URL + `/users/${userId}`,
    { isActive },
    { headers: { Authorization: `Bearer ${jwt}` } }
  );

  return data;
}

export async function updateUserAvatar(
  avatar: string,
  userId: string,
  jwt: string
) {
  const data = axios.put(
    STRAPI_API_URL + `/users/${userId}`,
    { avartar: avatar },
    { headers: { Authorization: `Bearer ${jwt}` } }
  );

  return data;
}

export async function updatePassword(
  userId: string,
  password: string,
  jwt: string
) {
  const data = axios.put(
    STRAPI_API_URL + `/users/${userId}`,
    { password: password },
    { headers: { Authorization: `Bearer ${jwt}` } }
  );

  return data;
}
