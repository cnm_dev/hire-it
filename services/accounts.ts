import { gql } from "graphql-request";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";
import { fetcherQL } from ".";
import { DeleteUserForm, UpdateBlockUserForm } from "@/forms/user";

export async function getAccounts(token: Token | { jwt: string }) {
  const query = gql`
    query {
      usersPermissionsUsers(pagination: { limit: -1 }, sort: "createdAt:desc") {
        data {
          id
          attributes {
            username
            email
            fullName
            phoneNumber
            blocked
            type_user {
              data {
                id
                attributes {
                  typeUserName
                }
              }
            }
          }
        }
      }
    }
  `;

  const { usersPermissionsUsers } = await fetcherQL(token.jwt, query);
  return transformer(usersPermissionsUsers);
}

export async function updateBlockUser(
  token: Token | { jwt: string },
  queryVariable: UpdateBlockUserForm
) {
  const { idUser, block } = queryVariable;
  const query = gql`
    mutation ($idUser: ID!, $block: Boolean!) {
      updateUsersPermissionsUser(id: $idUser, data: { blocked: $block }) {
        data {
          id
        }
      }
    }
  `;

  const { updateUsersPermissionsUser } = await fetcherQL(token.jwt, query, {
    idUser,
    block,
  });
  return transformer(updateUsersPermissionsUser);
}

export async function deleteUser(
  token: Token | { jwt: string },
  queryVariable: DeleteUserForm
) {
  const { idUser } = queryVariable;
  const query = gql`
    mutation ($idUser: ID!) {
      deleteUsersPermissionsUser(id: $idUser) {
        data {
          id
        }
      }
    }
  `;

  const { deleteUsersPermissionsUser } = await fetcherQL(token.jwt, query, {
    idUser,
  });
  return transformer(deleteUsersPermissionsUser);
}
