import {} from "@/types";
import { transformer } from "@/utils/server";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { Education } from "@/types/models";
export async function createEducation(jwt: string, queryVariable: Education) {
  const {
    employeeId,
    educationMajors,
    educationTime,
    educationaPlace,
    educationalInstitution,
  } = queryVariable;

  let query = "";
  query = gql`
    mutation (
      $employeeId: ID!
      $educationMajors: String
      $educationTime: String
      $educationaPlace: String
      $educationalInstitution: String
      $publishedAt: DateTime!
    ) {
      createEducation(
        data: {
          employee: $employeeId
          educationMajors: $educationMajors
          educationTime: $educationTime
          educationaPlace: $educationaPlace
          educationalInstitution: $educationalInstitution
          publishedAt: $publishedAt
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { createEducation } = await fetcherQL(jwt, query, {
    employeeId,
    educationMajors,
    educationTime,
    educationaPlace,
    educationalInstitution,
    publishedAt: new Date(),
  });
  return transformer(createEducation);
}

export async function updateEducation(jwt: string, queryVariable: Education) {
  const {
    id,
    educationMajors,
    educationTime,
    educationaPlace,
    educationalInstitution,
  } = queryVariable;
  let query = "";
  query = gql`
    mutation (
      $id: ID!
      $educationMajors: String
      $educationTime: String
      $educationaPlace: String
      $educationalInstitution: String
    ) {
      updateEducation(
        id: $id
        data: {
          educationMajors: $educationMajors
          educationTime: $educationTime
          educationaPlace: $educationaPlace
          educationalInstitution: $educationalInstitution
        }
      ) {
        data {
          id
        }
      }
    }
  `;

  const { updateEducation } = await fetcherQL(jwt, query, {
    id,
    educationMajors,
    educationTime,
    educationaPlace,
    educationalInstitution,
  });
  return transformer(updateEducation);
}

export async function deleteEducation(
  jwt: string,
  queryVariables: {
    id: string | undefined;
  }
) {
  const { id } = queryVariables;
  const query = gql`
    mutation ($id: ID!) {
      updateEducation(id: $id, data: { publishedAt: null }) {
        data {
          id
        }
      }
    }
  `;

  const { updateEducation } = await fetcherQL(jwt, query, { id });

  return transformer(updateEducation);
}
