import {} from "@/types";
import { gql } from "graphql-request";
import { fetcherQL } from ".";
import { Token } from "../types/nextAuth";
import { transformer } from "../utils/server";

export async function getSkills(token: Token | { jwt: string }) {
  const query = gql`
    {
      skills(pagination: { limit: -1 }) {
        data {
          id
          attributes {
            skillName
            position {
              data {
                id
                attributes {
                  positionName
                }
              }
            }
          }
        }
      }
    }
  `;

  const { skills } = await fetcherQL(token.jwt, query);
  return transformer(skills);
}
