/** Strapi API URL, default: `http://localhost:1337/api` */
export const STRAPI_API_URL =
  process.env.STRAPI_API_URL || "https://hire-it-orcxw.ondigitalocean.app/api";

/** Strapi GRAPHQL URL, default: `http://localhost:1337/graphql` */
export const STRAPI_GRAPHQL_URL =
  process.env.STRAPI_GRAPHQL_URL ||
  "https://hire-it-orcxw.ondigitalocean.app/graphql";

export const STRAPI_API_TOKEN =
  process.env.STRAPI_API_TOKEN ||
  "425a62bfb382b19a3e9a97a6f80dc75eacaff63f3a160fe060e0638c4a8839f5827faddd2ed8c0096993713b226b42415fd2e6b04ea034e2525a808f4d3df644462faf465022148cf11928e3642e5d98b754a23d24c251c8f0c2062c9e6698f826e45bd81f084fc4664075b070523b38bdcc56ef5106b7318767c1875551ecde";

export const URL_VNPAY = "https://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
export const VNP_SECUREHASH = "RGOZXXFLJHLJZNFBBDXOBNNBRIUMUEYT";
