import { URL_VNPAY, VNP_SECUREHASH } from ".";
import dateFormat from "dateformat";
import { createHmac } from "crypto";
import QueryString from "qs";
// export const url_payment = (
//   vnp_Amount: string,
//   vnp_CreateDate: Date,
//   vnp_OrderInfo: string,
//   vnp_ReturnUrl: string
// ) => {
//   const createDate = dateFormat(vnp_CreateDate, "yyyymmddHHMMss");
//   const vnp_ExpireDate = dateFormat(
//     new Date(new Date().setMinutes(new Date().getMinutes() + 15)),
//     "yyyymmddHHMMss"
//   );
//   const amount_money = 10000;
//   const vnp_TxnRef = dateFormat(vnp_CreateDate, "HHMMss");
//   let vnpUrl = URL_VNPAY;

//   let vnp_Params: any = {};
//   vnp_Params["vnp_Version"] = "2.1.0";
//   vnp_Params["vnp_Command"] = "pay";
//   vnp_Params["vnp_TmnCode"] = "NC1YW7TX";
//   vnp_Params["vnp_Locale"] = "vn";
//   vnp_Params["vnp_CurrCode"] = "VND";
//   vnp_Params["vnp_TxnRef"] = vnp_TxnRef;
//   vnp_Params["vnp_OrderInfo"] = vnp_OrderInfo;
//   vnp_Params["vnp_OrderType"] = "other";
//   vnp_Params["vnp_Amount"] = amount_money * 100;
//   vnp_Params["vnp_ReturnUrl"] = "http://localhost:3000/payment";
//   vnp_Params["vnp_IpAddr"] = "14.160.87.124:443";
//   vnp_Params["vnp_CreateDate"] = createDate;
//   vnp_Params["vnp_ExpireDate"] = vnp_ExpireDate;

//   sortObject(vnp_Params);

//   var signData = QueryString.stringify(vnp_Params, { encode: false });

//   var hmac = createHmac("sha512", VNP_SECUREHASH);
//   var signed = hmac.update(new Buffer(signData, "utf-8")).digest("hex");
//   vnp_Params["vnp_SecureHash"] = signed;
//   console.log(signed);

//   vnpUrl += "?" + QueryString.stringify(vnp_Params, { encode: false });
//   return vnpUrl;
// };

function sortObject(obj: any) {
  return Object.keys(obj)
    .sort()
    .reduce((a: any, v) => {
      a[v] = obj[v];
      return a;
    }, {});
}

export const url_payment = (
  vnp_Amount: number,
  vnp_CreateDate: Date,
  vnp_OrderInfo: string,
  vnp_ReturnUrl: string
) => {
  const createDate = dateFormat(vnp_CreateDate, "yyyymmddHHMMss");

  const vnp_TxnRef = dateFormat(vnp_CreateDate, "HHMMss");
  let vnpUrl = URL_VNPAY;

  let vnp_Params: any = {};
  vnp_Params["vnp_Version"] = "2.1.0";
  vnp_Params["vnp_Command"] = "pay";
  vnp_Params["vnp_TmnCode"] = "NC1YW7TX";
  vnp_Params["vnp_Locale"] = "vn";
  vnp_Params["vnp_CurrCode"] = "VND";
  vnp_Params["vnp_TxnRef"] = vnp_TxnRef;
  vnp_Params["vnp_OrderInfo"] = vnp_OrderInfo;
  vnp_Params["vnp_OrderType"] = "other";
  vnp_Params["vnp_Amount"] = (vnp_Amount * 2348250).toFixed(0);
  vnp_Params["vnp_ReturnUrl"] = "http%3A%2F%2Fhire-it-ten.vercel.app%2Fpayment";
  vnp_Params["vnp_IpAddr"] = "127.0.0.1";
  vnp_Params["vnp_CreateDate"] = createDate;

  let new_Vnp = sortObject(vnp_Params);
  var signData = QueryString.stringify(new_Vnp, { encode: false });
  const hmac = createHmac("sha512", VNP_SECUREHASH);
  hmac.update(Buffer.from(signData, "utf-8"));
  const HMAC_string = hmac.digest("hex");
  vnp_Params["vnp_SecureHash"] = HMAC_string;

  vnpUrl += "?" + QueryString.stringify(vnp_Params, { encode: false });
  return vnpUrl;
};

export const informate_payment = (vnp_ResponseCode: string | string[]) => {
  if (vnp_ResponseCode === "00") {
    return "Giao dịch thành công";
  } else if (vnp_ResponseCode === "07") {
    return "Trừ tiền thành công. Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường).";
  } else if (vnp_ResponseCode === "09") {
    return "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.";
  } else if (vnp_ResponseCode === "10") {
    return "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần";
  } else if (vnp_ResponseCode === "11") {
    return "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.";
  } else if (vnp_ResponseCode === "12") {
    return "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.";
  } else if (vnp_ResponseCode === "13") {
    return "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.";
  } else if (vnp_ResponseCode === "24") {
    return "Giao dịch không thành công do: Khách hàng hủy giao dịch";
  } else if (vnp_ResponseCode === "51") {
    return "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch.";
  } else if (vnp_ResponseCode === "65") {
    return "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày.";
  } else if (vnp_ResponseCode === "75") {
    return "Ngân hàng thanh toán đang bảo trì.";
  } else if (vnp_ResponseCode === "79") {
    return "Giao dịch không thành công do: KH nhập sai mật khẩu thanh toán quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch";
  } else {
    return "Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)";
  }
};
