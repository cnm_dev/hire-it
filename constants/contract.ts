import { Contract, Employee } from "@/types/models";
import { converStringBoolean, getStringStorage, randomString } from "@/utils";
import dateFormat from "dateformat";

// Position
export const PositionContract = (position: string | undefined = undefined) => {
  if (!position) {
    return `You hire staff position: ${getStringStorage(
      localStorage.getItem("Position")
    )}`;
  } else return `You hire staff position: ${position}`;
};

// Situation
export const SituationContract = (
  situation: string | undefined = undefined
) => {
  if (!situation) {
    return `Type of project you hiring for : ${getStringStorage(
      localStorage.getItem("Situation")
    )}`;
  } else return `Type of project you hiring for : ${situation}`;
};

// Commitment
export const CommitmentContract = (
  commitment: boolean | string | undefined = undefined
) => {
  if (commitment === undefined) {
    const Commitment = getStringStorage(localStorage.getItem("Commitment"));
    if (Commitment === "none") {
      return "I'll decide later";
    } else {
      if (converStringBoolean(Commitment))
        return "Full time (40 or more hrs/week)";
      return "Part time (Less than 40 hrs/week)";
    }
  } else {
    if (commitment) return "Full time (40 or more hrs/week)";
    else return "Part time (Less than 40 hrs/week)";
  }
};

// OpenForRemote
export const OpenForRemoteContract = (
  openForRemote: boolean | string | undefined = undefined
) => {
  if (openForRemote === undefined) {
    const OpenForRemote = getStringStorage(
      localStorage.getItem("OpenForRemote")
    );

    if (OpenForRemote === "none") {
      return "You not sure";
    } else {
      if (converStringBoolean(OpenForRemote))
        return `Wrking with a remote developer: True`;
      return "Wrking with a remote developer: False";
    }
  } else {
    if (openForRemote) return `Wrking with a remote developer: True`;
    return "Wrking with a remote developer: False";
  }
};

// ProductSpecs
export const ProductSpecsContract = (
  productSpecs: string | undefined = undefined
) => {
  if (!productSpecs) {
    return `Product specifications ready: ${getStringStorage(
      localStorage.getItem("ProductSpecs")
    )}`;
  } else return `Product specifications ready: ${productSpecs}`;
};

// ReadyToStart
export const ReadyToStartContract = (
  readyToStart: string | undefined = undefined
) => {
  if (!readyToStart) {
    return `Developer to start word: ${getStringStorage(
      localStorage.getItem("ReadyToStart")
    )}`;
  } else return `Developer to start word: ${readyToStart}`;
};

// CompanySize
export const CompanySizeContract = (
  companySizeFrom: number | null = null,
  companySizeTo: number | null = null
) => {
  const title = "Number of employees you want to hire:";
  if (companySizeFrom === null || companySizeTo === null) {
    const CompanySize = getStringStorage(localStorage.getItem("CompanySize"))
      ?.split(",")
      .map(Number);

    if (CompanySize) {
      if (CompanySize[0] === 1) {
        return `${title} Less than ${CompanySize[1]} staff`;
      } else if (CompanySize[1] === 99999999) {
        return `${title} More than ${CompanySize[0]} staff`;
      }
      return `${title} ${CompanySize[0]} - ${CompanySize[1]} staff`;
    }
  } else {
    if (companySizeFrom === 1) {
      return `${title} Less than ${companySizeTo} staff`;
    } else if (companySizeTo === 99999999) {
      return `${title} More than ${companySizeFrom} staff`;
    }
    return `${title} ${companySizeFrom} - ${companySizeTo} staff`;
  }
};

// ProjectLength
export const ProjectLengthContract = (
  projectLenghtFrom: number | null = null,
  projectLenghtTo: number | null = null
) => {
  const title = "You need the developer word in:";

  if (projectLenghtFrom === null || projectLenghtTo === null) {
    const ProjectLength = getStringStorage(
      localStorage.getItem("ProjectLength")
    )
      ?.split(",")
      .map(Number);

    if (ProjectLength) {
      if (ProjectLength[0] === 0) {
        return `${title} Less than ${converMonthWeek(ProjectLength[1])} week`;
      } else if (ProjectLength[1] === 99999999999) {
        return `${title} Longer than ${converMonthWeek(ProjectLength[0])}`;
      } else if (
        getStringStorage(localStorage.getItem("ProjectLength")) === "none"
      ) {
        return "I'll decide later";
      }
      return `${title} ${converMonthWeek(ProjectLength[0])} - ${converMonthWeek(
        ProjectLength[1]
      )}`;
    }
  } else {
    if (projectLenghtFrom === 0) {
      return `${title} Less than ${converMonthWeek(projectLenghtTo)} week`;
    } else if (projectLenghtTo === 99999999999) {
      return `${title} Longer than ${converMonthWeek(projectLenghtFrom)}`;
    } else if (
      getStringStorage(localStorage.getItem("ProjectLength")) === "none"
    ) {
      return "I'll decide later";
    }
    return `${title} ${converMonthWeek(projectLenghtFrom)} - ${converMonthWeek(
      projectLenghtTo
    )}`;
  }
};

// NameProject
export const NameContract = (nameProject: string | undefined = undefined) => {
  if (!nameProject) {
    const name = `hireIT_${randomString(6)}`;
    localStorage.setItem("NameProject", name);
    return `${name}.pdf`;
  } else {
    return `${nameProject}.pdf`;
  }
};

export const converMonthWeek = (time: number) => {
  if (time <= 4) {
    return `${time} week`;
  } else {
    return `${time / 4} month`;
  }
};

// Budget
export const BudgetContract = (
  budgetContractFrom: number | null = null,
  budgetContractTo: number | null = null
) => {
  const title = "Your budget for each employee:";
  if (budgetContractFrom === null || budgetContractTo === null) {
    const Budget = getStringStorage(localStorage.getItem("Budget"))
      ?.split(",")
      .map(Number);
    if (Budget) {
      if (Budget[0] === 0) {
        return `${title} Less than ${Budget[1]}/hr`;
      } else if (Budget[1] === 999999) {
        return `${title} More than ${Budget[0]}/hr`;
      } else if (getStringStorage(localStorage.getItem("Budget")) === "none") {
        return "Not sure on budget yet";
      }
      return `${title} $${Budget[0]} - $${Budget[1]}/hr`;
    }
  } else {
    if (budgetContractFrom === 0) {
      return `${title} Less than ${budgetContractTo}/hr`;
    } else if (budgetContractTo === 999999) {
      return `${title} More than ${budgetContractFrom}/hr`;
    } else if (getStringStorage(localStorage.getItem("Budget")) === "none") {
      return "Not sure on budget yet";
    }
    return `${title} $${budgetContractFrom} - $${budgetContractTo}/hr`;
  }
};

export const PriceMoney = (employees: Employee[]) => {
  let money = 0;
  employees.map((employee) => {
    money += employee.hourlyRate;
  });
  return money * 40;
};

export const CheckOption = () => {
  if (
    !localStorage.getItem("Budget") &&
    !localStorage.getItem("Commitment") &&
    !localStorage.getItem("OpenForRemote") &&
    !localStorage.getItem("Position") &&
    !localStorage.getItem("ProjectLength") &&
    !localStorage.getItem("ReadyToStart")
  )
    return true;
  else return false;
};

export const RemoveStorageOption = () => {
  localStorage.removeItem("Commitment");
  localStorage.removeItem("CompanySize");
  localStorage.removeItem("ProductSpecs");
  localStorage.removeItem("ProjectLength");
  localStorage.removeItem("OpenForRemote");
  localStorage.removeItem("ReadyToStart");
  localStorage.removeItem("Situation");
  localStorage.removeItem("Position");
  localStorage.removeItem("Budget");
  localStorage.removeItem("Skills");
};

export const valueAllOption = () => {
  const projectName = getStringStorage(localStorage.getItem("NameProject"));
  const budgetFrom = getStringStorage(localStorage.getItem("Budget"))
    ?.split(",")
    .map(Number)[0];
  const budgetTo = getStringStorage(localStorage.getItem("Budget"))
    ?.split(",")
    .map(Number)[1];
  const companySizeFrom = getStringStorage(localStorage.getItem("CompanySize"))
    ?.split(",")
    .map(Number)[0];
  const companySizeTo = getStringStorage(localStorage.getItem("CompanySize"))
    ?.split(",")
    .map(Number)[1];
  const openForRemote = converStringBoolean(
    getStringStorage(localStorage.getItem("OpenForRemote"))
  );
  const position = getStringStorage(localStorage.getItem("Position"));
  const commitment = converStringBoolean(
    getStringStorage(localStorage.getItem("Commitment"))
  );

  const productSpecs = getStringStorage(localStorage.getItem("ProductSpecs"));
  const projectLengthFrom = getStringStorage(
    localStorage.getItem("ProjectLength")
  )
    ?.split(",")
    .map(Number)[0];
  const projectLengthTo = getStringStorage(
    localStorage.getItem("ProjectLength")
  )
    ?.split(",")
    .map(Number)[1];

  const readyToStart = getStringStorage(localStorage.getItem("ReadyToStart"));
  const situation = getStringStorage(localStorage.getItem("Situation"));

  let skill = localStorage.getItem("Skills");
  let skillLocalStorge = skill && JSON.parse(skill);

  let skills: string[] = [];
  skillLocalStorge.map((course: any) => {
    skills.push(course.id);
  });

  return {
    projectName,
    budgetFrom,
    budgetTo,
    companySizeFrom,
    companySizeTo,
    openForRemote,
    position,
    commitment,
    productSpecs,
    projectLengthFrom,
    projectLengthTo,
    readyToStart,
    situation,
    skills,
  };
};

export const checkNoneOption = (
  Commitment: string | boolean | undefined,
  ProjectLength: string | undefined,
  OpenForRemote: string | boolean | undefined,
  ReadyToStart: string | undefined,
  budget: string | undefined
) => {
  if (
    Commitment === "none" ||
    ProjectLength === "none" ||
    OpenForRemote === "none" ||
    ReadyToStart === "none" ||
    budget === "none"
  )
    return true;
  return false;
};

export const arrayDataMoneyContract = (
  arrayStatus: number[],
  month: string | Date,
  money: number
) => {
  const indexArr = new Date(month).getMonth();
  arrayStatus[indexArr] += money;
};

export const totalMoneyContractYear = (contracts: Contract[]) => {
  const arrMoneyActive = Array.from({ length: 12 }, () => 0);
  const arrMoneyWait = Array.from({ length: 12 }, () => 0);
  const arrMoneyCancel = Array.from({ length: 12 }, () => 0);
  const arrMoneyComplete = Array.from({ length: 12 }, () => 0);

  contracts.map((contract) => {
    if (
      new Date(contract.createdAt).getFullYear() === new Date().getFullYear()
    ) {
      if (contract.status === "active") {
        if (
          new Date(contract.startDate).getFullYear() ===
          new Date().getFullYear()
        ) {
          arrayDataMoneyContract(
            arrMoneyActive,
            contract.startDate,
            contract.project.totalMoney
          );
        }
      } else if (contract.status === "wait") {
        arrayDataMoneyContract(
          arrMoneyWait,
          contract.createdAt,
          contract.project.totalMoney
        );
      } else if (contract.status === "cancel") {
        arrayDataMoneyContract(
          arrMoneyCancel,
          contract.createdAt,
          contract.project.totalMoney
        );
      } else {
        arrayDataMoneyContract(
          arrMoneyComplete,
          contract.startDate,
          contract.project.totalMoney
        );
      }
    }
  });

  return {
    arrMoneyActive,
    arrMoneyWait,
    arrMoneyCancel,
    arrMoneyComplete,
  };
};

export const arrayDataContract = (
  arrayStatus: number[],
  month: string | Date,
  size: number
) => {
  const indexArr = new Date(month).getMonth();
  arrayStatus[indexArr] += size;
};

export const totalContractYear = (contracts: Contract[]) => {
  const arrSizeActive = Array.from({ length: 12 }, () => 0);
  const arrSizeWait = Array.from({ length: 12 }, () => 0);
  const arrSizeCancel = Array.from({ length: 12 }, () => 0);
  const arrSizeComplete = Array.from({ length: 12 }, () => 0);

  contracts.map((contract) => {
    if (
      new Date(contract.createdAt).getFullYear() === new Date().getFullYear()
    ) {
      if (contract.status === "active") {
        if (
          new Date(contract.startDate).getFullYear() ===
          new Date().getFullYear()
        ) {
          arrayDataContract(arrSizeActive, contract.startDate, 1);
        }
      } else if (contract.status === "wait") {
        arrayDataContract(arrSizeWait, contract.createdAt, 1);
      } else if (contract.status === "cancel") {
        arrayDataContract(arrSizeCancel, contract.createdAt, 1);
      } else {
        arrayDataContract(arrSizeComplete, contract.createdAt, 1);
      }
    }
  });

  return {
    arrSizeActive,
    arrSizeWait,
    arrSizeCancel,
    arrSizeComplete,
  };
};
