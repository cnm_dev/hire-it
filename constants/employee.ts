import { EmployeeReviews } from "@/types/models";

export const averageStar = (previews: EmployeeReviews[]) => {
  if (previews.length > 0) {
    let avergae = 0;
    previews.map((review) => {
      avergae += review.rate;
    });

    return avergae / previews.length;
  }
  return 5;
};
