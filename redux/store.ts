import CLocalizationSlice from "@/common/components/controls/CLocalization/CLocalizationSlice";
import BudgetSlice from "@/modules/hire/components/FormOption/BudgetSlice";
import CommitmentSlice from "@/modules/hire/components/FormOption/CommitmentSlice";
import CompanySizeSlice from "@/modules/hire/components/FormOption/CompanySizeSlice";
import ContactsSlice from "@/modules/hire/components/FormOption/ContactsSlice";
import OpenForRemoteSlice from "@/modules/hire/components/FormOption/OpenForRemoteSlice";
import PositionSlice from "@/modules/hire/components/FormOption/PositionSlice";
import ProductSpecsSlice from "@/modules/hire/components/FormOption/ProductSpecsSlice";
import ProjectLengthSlide from "@/modules/hire/components/FormOption/ProjectLengthSlice";
import ReadyToStartSlice from "@/modules/hire/components/FormOption/ReadyToStartSlice";
import SituationSlice from "@/modules/hire/components/FormOption/SituationSlice";
import SkillsSlice from "@/modules/hire/components/FormOption/SkillsSlice";
import { configureStore } from "@reduxjs/toolkit";
import EmployeesSlice from "./Slices/EmployeesSlice";
import JobPostingSlice from "./Slices/JobPostingSlice";
import ContractsSlice from "./Slices/ContractsSlice";
import ContractsOriginSlice from "./Slices/ContractsOriginSlice";
import ContractDetailSlice from "./Slices/ContractDetailSlice";
import JobPostingFilterSlice from "./Slices/JobPostingFilterSlice";
import FilterMemberSlice from "./Slices/FilterMemberSlice";
import FilterEmployeeAdminSlice from "./Slices/FilterEmployeeAdminSlice";
import InformatesSlice from "./Slices/InformatesSlice";
import FilterCustomerAdminSlice from "./Slices/FilterCustomerAdminSlice";
import CustomersSlice from "./Slices/CustomersSlice";
import FilterContractAdminSlice from "./Slices/FilterContractAdminSlice";

const store = configureStore({
  reducer: {
    langs: CLocalizationSlice.reducer,
    budget: BudgetSlice.reducer,
    commitment: CommitmentSlice.reducer,
    companySize: CompanySizeSlice.reducer,
    contacts: ContactsSlice.reducer,
    openForRemote: OpenForRemoteSlice.reducer,
    position: PositionSlice.reducer,
    productSpecs: ProductSpecsSlice.reducer,
    projectLength: ProjectLengthSlide.reducer,
    readyToStart: ReadyToStartSlice.reducer,
    situation: SituationSlice.reducer,
    skills: SkillsSlice.reducer,
    employees: EmployeesSlice.reducer,
    customers: CustomersSlice.reducer,
    jobPostings: JobPostingSlice.reducer,
    contracts: ContractsSlice.reducer,
    contractsOrigin: ContractsOriginSlice.reducer,
    contractDetail: ContractDetailSlice.reducer,
    jobPostingFilters: JobPostingFilterSlice.reducer,
    filterMembers: FilterMemberSlice.reducer,
    filterEmployeeAdmin: FilterEmployeeAdminSlice.reducer,
    filterCustomerAdmin: FilterCustomerAdminSlice.reducer,
    filterContractAdmin: FilterContractAdminSlice.reducer,
    informates: InformatesSlice.reducer,
  },
});
export default store;
