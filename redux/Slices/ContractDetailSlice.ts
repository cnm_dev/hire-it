import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "contractDetail",
  initialState: {
    contractDetail: [],
  },
  reducers: {
    contractDetail: (state, action) => {
      state.contractDetail = action.payload;
    },
  },
});
