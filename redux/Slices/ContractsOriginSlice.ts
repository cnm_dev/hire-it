import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "contractsOrigin",
  initialState: {
    contractsOrigin: [],
  },
  reducers: {
    listContractsOrigin: (state, action) => {
      state.contractsOrigin = action.payload;
    },
  },
});
