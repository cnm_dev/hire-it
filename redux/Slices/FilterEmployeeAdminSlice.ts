import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "filterEmployeeAdmin",
  initialState: {
    employeeName: "",
    status: "all",
  },
  reducers: {
    employeeName: (state, action) => {
      state.employeeName = action.payload;
    },
    status: (state, action) => {
      state.status = action.payload;
    },
  },
});
