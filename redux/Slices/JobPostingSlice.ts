import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "jobPosting",
  initialState: {
    jobPostings: [],
  },
  reducers: {
    listJobPosting: (state, action) => {
      // mutation
      state.jobPostings = action.payload;
    },
  },
});
