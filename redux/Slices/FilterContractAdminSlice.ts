import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "filterContractAdmin",
  initialState: {
    contractName: "",
    dateFrom: "",
    dateTo: "",
    status: "all",
  },
  reducers: {
    contractName: (state, action) => {
      state.contractName = action.payload;
    },
    dateFrom: (state, action) => {
      state.dateFrom = action.payload;
    },
    dateTo: (state, action) => {
      state.dateTo = action.payload;
    },
    status: (state, action) => {
      state.status = action.payload;
    },
  },
});
