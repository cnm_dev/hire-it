import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "FilterMembers",
  initialState: {
    memberName: "",
    start: 0,
    sortPrice: 0,
    statusMember: [],
    priceForm: 0,
    priceTo: Number.MAX_VALUE,
    skill: "ALL",
    experience: 0,
    Category: "ALL",
  },
  reducers: {
    memberName: (state, action) => {
      // mutation
      state.memberName = action.payload;
    },
    start: (state, action) => {
      // mutation
      state.start = action.payload;
    },
    sortPrice: (state, action) => {
      // mutation
      state.sortPrice = action.payload;
    },
    statusMember: (state, action) => {
      // mutation
      state.statusMember = action.payload;
    },
    priceTo: (state, action) => {
      // mutation
      state.priceTo = action.payload;
    },
    priceForm: (state, action) => {
      // mutation
      state.priceForm = action.payload;
    },
    skill: (state, action) => {
      // mutation
      state.skill = action.payload;
    },
    experience: (state, action) => {
      // mutation
      state.experience = action.payload;
    },
    Category: (state, action) => {
      // mutation
      state.Category = action.payload;
    },
  },
});
