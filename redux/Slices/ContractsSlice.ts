import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "contracts",
  initialState: {
    contracts: [],
  },
  reducers: {
    listContracts: (state, action) => {
      state.contracts = action.payload;
    },
  },
});
