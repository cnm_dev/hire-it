import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "informates",
  initialState: {
    informates: [],
  },
  reducers: {
    listInformats: (state, action) => {
      // mutation
      state.informates = action.payload;
    },
  },
});
