import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "customers",
  initialState: {
    customers: [],
  },
  reducers: {
    listCustomers: (state, action) => {
      // mutation
      state.customers = action.payload;
    },
  },
});
