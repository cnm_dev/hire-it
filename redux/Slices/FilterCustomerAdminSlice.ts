import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "filterCustomerAdmin",
  initialState: {
    customerName: "",
    status: "all",
  },
  reducers: {
    customerName: (state, action) => {
      state.customerName = action.payload;
    },
    status: (state, action) => {
      state.status = action.payload;
    },
  },
});
