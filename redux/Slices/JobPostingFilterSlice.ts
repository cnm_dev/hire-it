import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "jobPostingFilter",
  initialState: {
    jobPostingFilters: [],
  },
  reducers: {
    listPostingFilter: (state, action) => {
      // mutation
      state.jobPostingFilters = action.payload;
    },
  },
});
