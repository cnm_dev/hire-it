import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "employees",
  initialState: {
    employees: [],
  },
  reducers: {
    listEmployees: (state, action) => {
      // mutation
      state.employees = action.payload;
    },
  },
});
