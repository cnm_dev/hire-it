import {
  Contract,
  Customer,
  Employee,
  Informate,
  JobPosting,
  filterMembers,
} from "@/types/models";
import { checkSkillIsContain, getStart } from "@/utils";
import { createSelector } from "@reduxjs/toolkit";

export const langSelector = (state: { langs: { lang: "en" | "vn" } }) =>
  state.langs.lang;
// option selector
export const budgetSelector = (state: { budget: { budget: string } }) =>
  state.budget.budget;
export const commitmentSelector = (state: {
  commitment: { commitment: string };
}) => state.commitment.commitment;
export const companySizeSelector = (state: {
  companySize: { companySize: string };
}) => state.companySize.companySize;
export const contactsSelector = (state: { contacts: { contacts: string } }) =>
  state.contacts.contacts;
export const openForRemoteSelector = (state: {
  openForRemote: { openForRemote: string };
}) => state.openForRemote.openForRemote;
export const positionSelector = (state: { position: { position: string } }) =>
  state.position.position;
export const productSpecsSelector = (state: {
  productSpecs: { productSpecs: string };
}) => state.productSpecs.productSpecs;
export const projectLengthSelector = (state: {
  projectLength: { projectLength: string };
}) => state.projectLength.projectLength;
export const readyToStartSelector = (state: {
  readyToStart: { readyToStart: string };
}) => state.readyToStart.readyToStart;
export const situationSelector = (state: {
  situation: { situation: string };
}) => state.situation.situation;
export const skillsSelector = (state: { skills: { skills: string[] } }) =>
  state.skills.skills;

export const todoOptionContract = createSelector(
  budgetSelector,
  commitmentSelector,
  companySizeSelector,
  contactsSelector,
  openForRemoteSelector,
  positionSelector,
  productSpecsSelector,
  projectLengthSelector,
  readyToStartSelector,
  situationSelector,
  skillsSelector,
  (
    budget,
    commitment,
    companySize,
    contacts,
    openForRemote,
    position,
    productSpecs,
    projectLength,
    readyToStart,
    situation,
    skills
  ) => {
    return {
      budget,
      commitment,
      companySize,
      contacts,
      openForRemote,
      position,
      productSpecs,
      projectLength,
      readyToStart,
      situation,
      skills,
    };
  }
);

// employees
export const employeesSelector = (state: {
  employees: { employees: Employee[] };
}) => state.employees.employees;
// customers
export const customersSelector = (state: {
  customers: { customers: Customer[] };
}) => state.customers.customers;
//jobposting
export const jobPostingsSelector = (state: {
  jobPostings: { jobPostings: JobPosting[] };
}) => state.jobPostings.jobPostings;
// contracts Customer
export const contractsSelector = (state: {
  contracts: { contracts: Contract[] };
}) => state.contracts.contracts;
export const contractsOriginSelector = (state: {
  contractsOrigin: { contractsOrigin: Contract[] };
}) => state.contractsOrigin.contractsOrigin;
export const contractsDetailSelector = (state: {
  contractDetail: { contractDetail: Contract };
}) => state.contractDetail.contractDetail;
//jobposting filter
export const jobPostingFiltersSelector = (state: {
  jobPostingFilters: { jobPostingFilters: JobPosting[] };
}) => state.jobPostingFilters.jobPostingFilters;
// informates
export const informatesSelector = (state: {
  informates: { informates: Informate[] };
}) => state.informates.informates;

// Fillter member
export const FilterMemberNameSelector = (state: {
  filterMembers: {
    memberName: string;
  };
}) => state.filterMembers.memberName;

export const FilterMemberstartSelector = (state: {
  filterMembers: {
    start: number;
  };
}) => state.filterMembers.start;

export const FilterMembersortPriceSelector = (state: {
  filterMembers: {
    sortPrice: number;
  };
}) => state.filterMembers.sortPrice;

export const FilterMemberstatusMemberSelector = (state: {
  filterMembers: {
    statusMember: string[];
  };
}) => state.filterMembers.statusMember;

export const FilterMemberpriceToSelector = (state: {
  filterMembers: {
    priceTo: number;
  };
}) => state.filterMembers.priceTo;

export const FilterMemberpriceFormSelector = (state: {
  filterMembers: {
    priceForm: number;
  };
}) => state.filterMembers.priceForm;

export const FilterMemberskillSelector = (state: {
  filterMembers: {
    skill: string;
  };
}) => state.filterMembers.skill;

export const FilterMemberexperienceSelector = (state: {
  filterMembers: {
    experience: number;
  };
}) => state.filterMembers.experience;

export const FilterMemberCategorySelector = (state: {
  filterMembers: {
    Category: string;
  };
}) => state.filterMembers.Category;

export const FilterMembersSelector = (state: {
  filterMembers: { filterMembers: filterMembers };
}) => state.filterMembers.filterMembers;

export const todoRemainingSelector = createSelector(
  employeesSelector,
  FilterMemberNameSelector,
  FilterMemberstartSelector,
  FilterMembersortPriceSelector,
  FilterMemberstatusMemberSelector,
  FilterMemberpriceToSelector,
  FilterMemberpriceFormSelector,
  FilterMemberskillSelector,
  FilterMemberexperienceSelector,
  FilterMemberCategorySelector,
  (
    employees,
    memberName,
    start,
    sortPrice,
    statusMember,
    priceTo,
    priceForm,
    skill,
    experience,
    Category
  ) => {
    if (
      memberName === "" &&
      start === 0 &&
      sortPrice == 0 &&
      statusMember.length === 0 &&
      priceTo === Number.MAX_VALUE &&
      priceForm === 0 &&
      skill === "ALL" &&
      experience === 0 &&
      Category === "ALL"
    ) {
      return employees;
    } else {
      return sortPrice === 0
        ? employees
            .filter((data) => {
              if (statusMember.length === 0) {
                if (skill === "ALL") {
                  if (Category === "ALL") {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience
                    );
                  } else {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      data.position.positionName === Category
                    );
                  }
                } else {
                  if (Category === "ALL") {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      checkSkillIsContain(data.skills, skill)
                    );
                  } else {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      checkSkillIsContain(data.skills, skill) &&
                      data.position.positionName === Category
                    );
                  }
                }
              } else {
                if (skill === "ALL") {
                  if (Category === "ALL") {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      statusMember.includes(data.statusWork) &&
                      data.yearsOfExperience >= experience
                    );
                  } else {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      statusMember.includes(data.statusWork) &&
                      data.position.positionName === Category
                    );
                  }
                } else {
                  if (Category === "ALL") {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      statusMember.includes(data.statusWork) &&
                      checkSkillIsContain(data.skills, skill)
                    );
                  } else {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      checkSkillIsContain(data.skills, skill) &&
                      statusMember.includes(data.statusWork) &&
                      data.position.positionName === Category
                    );
                  }
                }
              }
            })
            .sort((a, b) => a.hourlyRate - b.hourlyRate)
        : employees
            .filter((data) => {
              if (statusMember.length === 0) {
                if (skill === "ALL") {
                  if (Category === "ALL") {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience
                    );
                  } else {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      data.position.positionName === Category
                    );
                  }
                } else {
                  if (Category === "ALL") {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      checkSkillIsContain(data.skills, skill)
                    );
                  } else {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      checkSkillIsContain(data.skills, skill) &&
                      data.position.positionName === Category
                    );
                  }
                }
              } else {
                if (skill === "ALL") {
                  if (Category === "ALL") {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      statusMember.includes(data.statusWork) &&
                      data.yearsOfExperience >= experience
                    );
                  } else {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      statusMember.includes(data.statusWork) &&
                      data.position.positionName === Category
                    );
                  }
                } else {
                  if (Category === "ALL") {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      statusMember.includes(data.statusWork) &&
                      checkSkillIsContain(data.skills, skill)
                    );
                  } else {
                    return (
                      data.users_permissions_user.fullName
                        .toLowerCase()
                        .indexOf(memberName.toLowerCase()) !== -1 &&
                      data.start >= start &&
                      data.hourlyRate <= priceTo &&
                      data.hourlyRate >= priceForm &&
                      data.yearsOfExperience >= experience &&
                      checkSkillIsContain(data.skills, skill) &&
                      statusMember.includes(data.statusWork) &&
                      data.position.positionName === Category
                    );
                  }
                }
              }
            })
            .sort((a, b) => b.hourlyRate - a.hourlyRate);
    }
  }
);

export const FilterEmployeeNameAndEmailSelector = (state: {
  filterEmployeeAdmin: {
    employeeName: string;
  };
}) => state.filterEmployeeAdmin.employeeName;

export const FilterEmployeeStatusSelector = (state: {
  filterEmployeeAdmin: {
    status: string;
  };
}) => state.filterEmployeeAdmin.status;

export const todoEmployeeAdminSelector = createSelector(
  employeesSelector,
  FilterEmployeeNameAndEmailSelector,
  FilterEmployeeStatusSelector,
  (employees, employeeName, status) => {
    return employees.filter((data) => {
      if (status === "all") {
        return (
          data.users_permissions_user.fullName
            .toLowerCase()
            .indexOf(employeeName.toLowerCase()) !== -1 ||
          data.users_permissions_user.email
            .toLowerCase()
            .indexOf(employeeName.toLowerCase()) !== -1
        );
      } else {
        return (
          data.status === status &&
          (data.users_permissions_user.fullName
            .toLowerCase()
            .indexOf(employeeName.toLowerCase()) !== -1 ||
            data.users_permissions_user.email
              .toLowerCase()
              .indexOf(employeeName.toLowerCase()) !== -1)
        );
      }
    });
  }
);

export const FilterCustomerNameAndEmailSelector = (state: {
  filterCustomerAdmin: {
    customerName: string;
  };
}) => state.filterCustomerAdmin.customerName;
export const FilterCustomerStatusSelector = (state: {
  filterCustomerAdmin: {
    status: string;
  };
}) => state.filterCustomerAdmin.status;

export const todoCustomerAdminSelector = createSelector(
  customersSelector,
  FilterCustomerNameAndEmailSelector,
  FilterCustomerStatusSelector,
  (customers, customerName, status) => {
    return customers.filter((data) => {
      if (status === "all") {
        return (
          data.users_permissions_user.fullName
            .toLowerCase()
            .indexOf(customerName.toLowerCase()) !== -1 ||
          data.users_permissions_user.email
            .toLowerCase()
            .indexOf(customerName.toLowerCase()) !== -1 ||
          (data.companyName &&
            data.companyName
              .toLowerCase()
              .indexOf(customerName.toLowerCase()) !== -1)
        );
      } else {
        const t = status === "active" ? false : true;
        return (
          (data.users_permissions_user.fullName
            .toLowerCase()
            .indexOf(customerName.toLowerCase()) !== -1 ||
            data.users_permissions_user.email
              .toLowerCase()
              .indexOf(customerName.toLowerCase()) !== -1 ||
            data.companyName
              .toLowerCase()
              .indexOf(customerName.toLowerCase()) !== -1) &&
          data.users_permissions_user.blocked === t
        );
      }
    });
  }
);

export const FilterContractNameSelector = (state: {
  filterContractAdmin: {
    contractName: string;
  };
}) => state.filterContractAdmin.contractName;

export const FilterContractDateFromSelector = (state: {
  filterContractAdmin: {
    dateFrom: Date;
  };
}) => state.filterContractAdmin.dateFrom;

export const FilterContractDateToSelector = (state: {
  filterContractAdmin: {
    dateTo: Date;
  };
}) => state.filterContractAdmin.dateTo;

export const FilterContractStatusSelector = (state: {
  filterContractAdmin: {
    status: string;
  };
}) => state.filterContractAdmin.status;

export const todoContractAdminSelector = createSelector(
  contractsSelector,
  FilterContractNameSelector,
  FilterContractDateFromSelector,
  FilterContractDateToSelector,
  FilterContractStatusSelector,
  (contracts, contractName, dateFrom, dateTo, status) => {
    return contracts.filter((data) => {
      if (data.createdAt) {
        const createdAt = new Date(data.createdAt);
        const dFrom = new Date(dateFrom);
        const dTo = new Date(dateTo);

        if (status === "all") {
          return (
            data.project.customer.users_permissions_user.fullName
              .toLowerCase()
              .indexOf(contractName.toLowerCase()) !== -1 &&
            createdAt >= dFrom &&
            createdAt <= dTo
          );
        } else {
          return (
            data.status === status &&
            data.project.customer.users_permissions_user.fullName
              .toLowerCase()
              .indexOf(contractName.toLowerCase()) !== -1 &&
            createdAt >= dFrom &&
            createdAt <= dTo
          );
        }
      } else {
        if (status === "all") {
          return (
            data.project.customer.users_permissions_user.fullName
              .toLowerCase()
              .indexOf(contractName.toLowerCase()) !== -1
          );
        } else {
          return (
            data.status === status &&
            data.project.customer.users_permissions_user.fullName
              .toLowerCase()
              .indexOf(contractName.toLowerCase()) !== -1
          );
        }
      }
    });
  }
);
