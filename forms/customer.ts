export type GetCustomerForm = {
  customerId: string | string[] | undefined | any;
};

export type UpdateProfileCustomerForm = {
  customerId: string;
  idUser: string;
  phoneNumber?: string;
  codeFax?: string;
  comanyName?: string;
  address?: string;
  currentPass?: string;
  newPass?: string;
  avatar?: string;
};
