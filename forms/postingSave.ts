export type createPostingSaveForm = {
  idCustomer?: string;
  idEmployee?: string;
  idPosting: string[];
};

export type updatePostingSaveForm = {
  idCustomer?: string;
  idEmployee?: string;
  idPosting: string[];
  idPostSave: string;
};
