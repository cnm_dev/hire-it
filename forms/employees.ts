export type GetEmployeeForm = {
  profileId: string;
};

export type filterContractEmployeeForm = {
  status: string;
  isRemote?: boolean | string;
  isFullTime?: boolean | string;
  hourlyRateFrom?: number;
  hourlyRateTo: number;
  positionName?: string;
  skills?: string[];
};

export type GetContractsEmployee = {
  userEmployeeId: string | string[] | undefined;
};
