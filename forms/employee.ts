export type UpdateStatusForm = {
  employeeId: string;
  status: string;
  joinDate?: Date;
};

export type CreateEmployeeForm = {
  position: string;
  users_permissions_user: string;
};

export type DeleteEmployeeForm = {
  employeeId: string | string[] | undefined;
};
export type formGetStartEmployee = {
  id?: number;
  employeeId?: number;
  fullName: string;
  country: string;
  city: string;
  nationality: string;
  englishProficiency: string;
  reasonApplying: string;
  completionLevel: number;
};

export type formProfessionalExperience = {
  id?: number;
  employeeId?: number;
  skills: string[];
  certifications: string[];
  yearsOfExperience: number;
  linkedIn: string;
  gitHub: string;
  isFullTime: string;
  hourlyRate: number;
  completionLevel: number;
};

export type formProfileSetup = {
  avatar?: any;
  resume?: any;
};
