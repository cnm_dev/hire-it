export type CreateReviewForm = {
  content: string;
  rate: number;
  customerId: string;
  employeeId: string;
  contractId: string;
};
