export type createProjectForm = {
  budgetFrom?: number | undefined;
  budgetTo?: number | undefined;
  companySizeFrom?: number | undefined;
  companySizeTo?: number | undefined;
  position?: string | undefined;
  productSpecs?: string | undefined;
  totalMoney: number | undefined;
  projectName: string | undefined;
  openForRemote: boolean | string | undefined;
  commitment: boolean | string | undefined;
  projectLengthFrom: number | undefined;
  projectLengthTo: number | undefined;
  readyToStart: string | undefined;
  situation: string | undefined;
  listIDEmployee: string[];
  status: string;
  customerId: string;
  skills?: string[];
};

export type UpdatePropjectForm = {
  projectId: string,
  budgetFrom?: Number,
  budgetTo?: Number,
  companySizeFrom?: Number,
  companySizeTo?: Number,
  openForRemote?: Boolean,
  position?: string,
  commitment?: Boolean,
  projectLengthFrom?: Number,
  projectLengthTo?: Number,
  totalMoney?: Number,
  situation?: string,
  status?: string,
  listIDEmployee?: string[],
  skills?: string[],
};
