export type GetInformatesForm = {
  userId: string | string[] | undefined;
  typeUser?: string | string[] | undefined;
};

export type UpdateInformatesForm = {
  userId: string;
  informateId: string;
  read: boolean;
};

export type CreateInformateForm = {
  title: string;
  description: string;
  type?: "default" | "warning";
  route?: string;
  customerId?: string;
  employeeId?: string;
  adminId?: string;
};
