import { YubObjectSchema } from "@/types";
import * as yup from "yup";
export type GetJobPostingForm = {
  postId: string;
};
export const getJobPostingSchema = yup.object<
  YubObjectSchema<GetJobPostingForm>
>({
  id: yup.number().required(),
});

export type createJostPostingForm = {
  companyName: string;
  jobPostingName: string;
  description: string;
  wage: number;
  requirement: string;
  aboutCompany: string;
  timeRemain: Date;
  email: string;
  experience: string;
  gender: string;
  location: string;
  quantity: number;
  phone: string;
  timework: string;
  customerId: string;
  image: string;
};

export type UpdateJobPostingForm = {
  idJposting: string;
  status: string;
};
