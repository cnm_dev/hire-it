export type createPostingSpamForm = {
  idCustomer?: string;
  idEmployee?: string;
  idPosting: string;
  reason: string;
};
