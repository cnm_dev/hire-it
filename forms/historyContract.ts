export type createContractHistoryForm = {
  contractId: string;
  totalMoney: number;
  dateUpdate: Date;
  endDate?: Date;
  status: string;
  type: string;
};

export type getContractHistoryForm = {
  contractId: string | string[] | undefined;
};
