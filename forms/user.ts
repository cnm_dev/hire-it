export type userEmailForm = {
  email?: any;
  // status: string;
};

export type userPhoneForm = {
  phone?: any;
  // status: string;
};

export type UpdateBlockUserForm = {
  idUser: string;
  block: boolean;
};

export type DeleteUserForm = {
  idUser: string | string[] | undefined;
};
