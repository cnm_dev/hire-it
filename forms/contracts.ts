export type GetContractsForm = {
  customerId: string | string[] | undefined;
};

export type GetContractForm = {
  customerId: string | string[] | undefined;
  contractId: string | string[] | undefined;
};

export type CreateContractForm = {
  status: "wait" | "cancel" | "complete" | "active" | "inactive" | "end";
  projectId: String;
};

export type UpdateContractForm = {
  contractId: string;
  status?: "wait" | "cancel" | "complete" | "active" | "inactive" | "end";
  startDate?: Date;
  endDate?: Date;
  isExtend?: boolean;
};

export type UpdateStatusContractForm = {
  contractId: string;
  status: string;
};

export type updateExtendContract = {
  contractId: string;
  isExtend: boolean;
};
