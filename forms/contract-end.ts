export type createEndContractForm = {
  customerId: string;
  contractId: string;
  reason: string | undefined;
};
