export type GraphResultData<T = any> = {
  data: T;
  meta?: Meta;
};

export type GraphResult<T = any> = {
  [key: string]: GraphResultData<T>;
};

export type ApiResult<T = any> = {
  data: T;
  meta?: Meta;
};

export type Meta = {
  pagination: Pagination;
};

export type Pagination = {
  page: number;
  pageSize: number;
  pageCount: number;
  total: number;
};

export type CommonAttributes = {
  id: string;
  createdAt: string;
  updatedAt: string;
  publishedAt: Date;
};

// user
export type RegistryCustomer = {
  email: string;
  phoneNumber: string;
  fullName: string;
  password: string;
};

export type RegistryStaff = {
  email: string;
  phoneNumber: string;
  fullName: string;
  password: string;
  position: string;
};

export type LoginData = {
  email: string;
  password?: string;
};

export type UserValidate = {
  id: string;
  email: string;
  phoneNumber?: string;
};

export type Users = {
  email: string;
  id: string;
  username: string;
};

// auth

export type Logins = {
  email: string;
  password: string;
};

export type Registrys = {
  position: string;
  fullName: string;
  email: string;
  phoneNumber: string;
  password: string;
  confirmPassword: string | undefined;
  isAgree: boolean;
};

export type formInfoEmployee = {
  codeFax(
    phoneNumber: string | undefined,
    codeFax: any,
    comanyName: any,
    address: any,
    newPass: any,
    avatar: any
  ): unknown;
  comanyName(
    phoneNumber: string | undefined,
    codeFax: any,
    comanyName: any,
    address: any,
    newPass: any,
    avatar: any
  ): unknown;
  address(
    phoneNumber: string | undefined,
    codeFax: any,
    comanyName: any,
    address: any,
    newPass: any,
    avatar: any
  ): unknown;
  newPass(
    phoneNumber: string | undefined,
    codeFax: any,
    comanyName: any,
    address: any,
    newPass: any,
    avatar: any
  ): unknown;
  avatar(
    phoneNumber: string | undefined,
    codeFax: any,
    comanyName: any,
    address: any,
    newPass: any,
    avatar: any
  ): unknown;
  phoneNumber?: string;
  country?: string;
  city?: string;
  currentPassword?: string;
  newPassword?: string;
  confirmNewPassword?: string;
  historyHireEmployments?: string;
  education?: string;
};

export type formInfoCustomer = {
  phoneNumber?: string;
  companyName?: string;
  codeFax?: string;
  currentPassword?: string;
  newPassword?: string;
  confirmNewPassword?: string;
  address?: string;
};

export type formGetStartEmployee = {
  fullName: string;
  country: string;
  city: string;
  nationality: string;
  englishProficiency: string;
  reasonApplying: string;
};
export type formProfessionalExperienceEmployee = {
  id?: number;
  employeeId?: number;
  skills: string[];
  certifications: string[];
  yearsOfExperience: number;
  linkedIn: string;
  gitHub: string;
  isFullTime: boolean;
  hourlyRate: number;
  completionLevel: number;
};

export type formFreelancingPreferencesEmployee = {
  id?: number;
  employeeId?: number;
  interestedProductTypes: string;
  careerInterests: string;
  remoteSuccessKey: string;
  completionLevel: number;
};

export type formProfileSetupEmployee = {
  id?: number;
  employeeId?: number;
  avatar: string;
  resume: string;
};

export type Sessions = {
  status: "authenticated" | "unauthenticated" | "loading";
  data: any;
};

export type type_user = {
  role: Array<"Admin" | "Customer" | "Employee" | "None">;
};

export type Users_permissions_user = CommonAttributes & {
  username: string;
  email: string;
  provider: string;
  password: string;
  blocked: boolean;
  fullName: string;
  phoneNumber: string;
  isActive: string;
  type_user: TypeUser;
  avartar: string;
  address?: string;
};

export type Project = CommonAttributes & {
  projectName: string;
  description: string;
  feedbacks: string;
  totalMoney: number;
  contracts: Contract[];
  customer: Customer;
  employees: Employee[];
  budgetFrom: number;
  skills: Skill[];
  budgetTo: number;
  companySizeFrom: number;
  companySizeTo: number;
  openForRemote: boolean | string;
  position: string;
  commitment: boolean | string;
  productSpecs: string;
  projectLengthFrom: number;
  projectLengthTo: number;
  readyToStart: string;
  situation: string;
  listIDEmployee: string[];
  customerId: string;
  status: string;
};
export type TypeUser = CommonAttributes & {
  typeUserName: string;
};

export type Position = CommonAttributes & {
  positionName: string;
  description?: string;
  employees?: Employee[];
};

export type Customer = CommonAttributes & {
  companyName: string;
  numberOfContracts: number;
  codeFax: string;
  totalSpending: number;
  previews: Review[];
  users_permissions_user: Users_permissions_user;
  projects: Project[];
  informate: Informate[];
};

export type Skill = CommonAttributes & {
  id: number;
  skillName: string;
  description: string;
  position?: Position;
};

export type Certification = CommonAttributes & {
  id: number;
  certificationName: string;
  employees?: Employee[];
};

export type Review = CommonAttributes & {
  content: string;
  rate: number;
  customer: Customer;
  employee: Employee;
};

export type EmployeeReviews = CommonAttributes & {
  content: Review["content"];
  rate: Review["rate"];
  customer: Customer;
};

export type Employee = CommonAttributes & {
  id: number;
  users_permissions_user: Users_permissions_user;
  dateOfBirth: string;
  hourlyRate: number;
  nationality: string;
  yearsOfExperience: number;
  status: string;
  skills: Skill[];
  previews?: EmployeeReviews[];
  isRemote: boolean;
  gender: string;
  isFullTime: boolean;
  isAdmin: boolean;
  position: Position;
  projects: Project[];
  totalJob: number;
  start: number;
  country: string;
  city: string;
  englishProficiency: string;
  reasonApplying: string;
  certifications: string[];
  linkedIn: string;
  gitHub: string;
  completionLevel: number;
  interestedProductTypes: string;
  careerInterests: string;
  remoteSuccessKey: string;
  resume: string;
  statusWork: string;
  informate: Informate[];
  joinDate: Date;
  history_hire_employments?: HistoryHireEmployment[];
  educations?: Education[];
};

export type HistoryHireEmployment = {
  id?: string;
  companyName: string;
  workingPosition: string;
  technologie: string;
  descriptionJob: string;
  employeeId?: string;
};

export type Education = {
  id?: string;
  educationMajors: string;
  educationaPlace: string;
  educationTime: string;
  educationalInstitution: string;
  employeeId?: string;
};

export type CustomerJobPosting = {
  id: string;
  companyName: Customer["companyName"];
  users_permissions_user: UserCustomerJobPosting;
};

export type UserCustomerJobPosting = {
  fullName: Users_permissions_user["fullName"];
  email: Users_permissions_user["email"];
  phoneNumber: Users_permissions_user["phoneNumber"];
  avartar: Users_permissions_user["avartar"];
  address?: Users_permissions_user["address"];
};

export type PostingSave = CommonAttributes & {
  customer: Customer;
  employee: Employee;
  job_postings: JobPosting[];
};

export type PostingSpam = CommonAttributes & {
  customer: Customer;
  employee: Employee;
  job_posting: JobPosting;
  Reason: string;
};

export type ContractEnd = CommonAttributes & {
  customer: Customer;
  contract: Contract;
  reason: string;
};

export type JobPosting = CommonAttributes & {
  companyName: string;
  jobPostingName: string;
  description: string;
  wage: number;
  requirement: string;
  aboutCompany: string;
  timeRemain: Date;
  email: string;
  experience: string;
  gender: string;
  location: string;
  quantity: number;
  phone: string;
  timework: string;
  id: string;
  isBlock: boolean;
  customer: CustomerJobPosting;
  image: string;
  status: string;
  posting_spams: PostingSpam[];
};

export type Contract = CommonAttributes & {
  id: string;
  project: Project;
  status: string;
  startDate: Date;
  endDate: Date;
  previews: Review[];
  isExtend: boolean;
  contract_ends: ContractEnd[];
  contract_histories: ContractHistory[];
};

export type ContractHistory = CommonAttributes & {
  id: string;
  contract: Contract;
  totalMoney: number;
  dateUpdate: Date;
  endDate: Date;
  status: string;
  type: string;
};

export type Reviews = CommonAttributes & {
  content: string;
  rate: number;
  customer: Customer;
  employee: Employee;
  contract: Contract;
};

export type filterMembers = {
  memberName: string;
  start: number;
  sortPrice: number;
  statusMember: string;
  priceTo: number;
  priceForm: number;
  skill: string;
  experience: number;
  Category: string;
};

export type EmployeeChosse = CommonAttributes & {
  customer: Customer;
  employees: Employee[];
};

export type Informate = CommonAttributes & {
  customer: Customer[];
  employee: Employee[];
  title: string;
  description: string;
  type: string;
  read: boolean;
  route: string;
};
