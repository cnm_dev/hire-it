import NextAuth from "next-auth";

declare module "next-auth" {
  interface Session {
    user: {
      access_token: string;
      avatar: string;
      email: string;
      fullName: string;
      id: number;
      type_user: "Admin" | "Customer" | "Employee";
    } & DefaultSession["user"];
  }
}
