import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useSession } from "next-auth/react";
import { ReactNode, useEffect } from "react";
import CLoading from "@/common/components/controls/CLoading";
import { useRouter } from "next/router";

type PageProtectedHandlerProps =
  InferGetServerSidePropsType<GetServerSideProps>;

const withPageAuthorization = (
  handler: (_props: PageProtectedHandlerProps) => ReactNode
) =>
  function PageProtectedHandler(props: PageProtectedHandlerProps) {
    const router = useRouter();
    const { status, data: session } = useSession();

    useEffect(() => {
      if (session && !session.user.isActive) {
        router.replace("/auth/verify-email");
      }
    }, [session, router, status]);

    if (status === "loading" && !session) {
      return <CLoading fullScreen />;
    } else if (session && !session.user.isActive) {
      return <CLoading fullScreen />;
    }

    const Component: any = handler;
    return <Component {...{ ...session, ...props }} />;
  };

export default withPageAuthorization;
