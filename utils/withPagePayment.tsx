import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { ReactNode, useEffect } from "react";
import CLoading from "@/common/components/controls/CLoading";
import { type_user } from "@/types/models";

type PageProtectedHandlerProps =
  InferGetServerSidePropsType<GetServerSideProps>;

const withPagePayment = (
  role: type_user["role"],
  handler: (_props: PageProtectedHandlerProps) => ReactNode
) =>
  function PageProtectedHandler(props: PageProtectedHandlerProps) {
    const router = useRouter();
    const { data: session, status } = useSession();

    useEffect(() => {
      if (status === "unauthenticated") {
        router.replace("/auth/login");
      } else if (
        status === "authenticated" &&
        session &&
        !session.user.isActive
      ) {
        router.replace("/auth/verify-email");
      } else if (session && !role.includes(session.user.type_user)) {
        router.replace("/");
      } else if (!localStorage.getItem("IdContract")) router.replace("/hire");
    }, [router, session, status]);

    if (
      status !== "authenticated" ||
      (session && !session.user.isActive) ||
      (session?.user && !role.includes(session.user.type_user)) ||
      !localStorage.getItem("IdContract")
    ) {
      return <CLoading fullScreen/>;
    }

    const Component: any = handler;
    return <Component {...{ ...session, ...props }} />;
  };

export default withPagePayment;
