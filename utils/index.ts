import isString from "lodash/isString";
import dateFormat from "dateformat";
import date from "date-and-time";
import { EmployeeReviews, Skill } from "@/types/models";

export const ensureString = (value: any): string => {
  try {
    if (value !== undefined && value !== null) {
      if (isString(value)) {
        return value;
      } else if (typeof value === "object") {
        return JSON.stringify(value);
      } else {
        return `${value}`;
      }
    }
  } catch (error) {
    return "";
  }
  return "";
};

export const formatDate = (date: string | Date) => {
  // return dateFormat(date, "dddd, mmmm d, yyyy");
  return dateFormat(new Date(date), "dddd, mm/dd/yyyy");
};

export const formatNormalDate = (
  date: string | Date,
  isTime: boolean = false
) => {
  if (isTime) {
    return dateFormat(new Date(date), "mm/dd/yyyy HH:MM:ss");
  } else return dateFormat(new Date(date), "mm/dd/yyyy");
};

export const intervalTime = (dateTime: Date | string) => {
  const dateSubtract = date.subtract(new Date(), new Date(dateTime));
  if (dateSubtract.toSeconds() < 60) {
    return `Just recently`;
  } else if (dateSubtract.toMinutes() < 60) {
    return `${Math.floor(dateSubtract.toMinutes())} minutes ago`;
  }
  if (dateSubtract.toHours() < 24) {
    return `${Math.floor(dateSubtract.toHours())} hour ago`;
  } else return `${Math.floor(dateSubtract.toDays())} day ago`;
};

export const timeRemain = (dateTime: Date | string) => {
  return date.subtract(new Date(dateTime), new Date()).toSeconds();
};

export const countDay = (dateStart: Date | string, dateEnd: Date | string) => {
  const newDateEnd = new Date(new Date(dateEnd).setHours(0, 0, 0, 0));
  const newDateStart = new Date(new Date(dateStart).setHours(0, 0, 0, 0));

  return date.subtract(newDateEnd, newDateStart).toDays();
};

export const timeLeaft = (dateTime: Date | string) => {
  const dateSubtract = date.subtract(new Date(dateTime), new Date());
  if (dateSubtract.toSeconds() < 60) {
    return `${Math.floor(dateSubtract.toSeconds())}s lefts`;
  } else if (dateSubtract.toMinutes() < 60) {
    return `${Math.floor(dateSubtract.toMinutes())} minutes lefts`;
  }
  if (dateSubtract.toHours() < 24) {
    return `${Math.floor(dateSubtract.toHours())} hour lefts`;
  } else return `${Math.floor(dateSubtract.toDays())} day lefts`;
};

export const getStringStorage = (stringStorage: string | null) => {
  if (stringStorage) {
    const storageLenght = stringStorage.length - 1;
    return stringStorage.slice(1, storageLenght);
  }
};

export const converStringBoolean = (stringConver: string | undefined) => {
  if (stringConver === "true") return true;
  else if (stringConver === "false") return false;
  else return stringConver;
};

export const addWeekstoDate = (date: Date = new Date(), numberWeek: number) => {
  return new Date(date.setDate(date.getDate() + numberWeek * 7));
};

export const randomString = (length: number) => {
  var text = "";
  var possible =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < length; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};

export const getStart = (reviews: EmployeeReviews[]) => {
  let total = 0;

  if (reviews.length > 0) {
    reviews.forEach((data) => {
      total += data.rate;
    });
    return total / reviews.length;
  }
  return total;
};

export const checkSkillIsContain = (v1: Skill[], v2: string) => {
  let temp = false;
  v1.map((data) => {
    if (data.skillName.includes(v2)) temp = true;
    else temp = false;
  });
  return temp;
};

export const convertListValue = (listValue: string, key: string) => {
  let arrValue: string[] = [];
  listValue.split(key).map((value) => arrValue.push(value.trim()));
  return arrValue;
};

export const formatNumber = (
  number: number,
  fixed: number = 2,
  isFormat: boolean = true
) => {
  if (number === 0) {
    return number;
  } else {
    if (isFormat) {
      return number.toLocaleString("en-US", {
        minimumFractionDigits: fixed,
        maximumFractionDigits: fixed,
      });
    } else {
      return number.toFixed(fixed);
    }
  }
};
