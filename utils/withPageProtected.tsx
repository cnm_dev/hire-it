import { GetServerSideProps, InferGetServerSidePropsType } from "next";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { ReactNode, useEffect } from "react";
import CLoading from "@/common/components/controls/CLoading";
import { type_user } from "@/types/models";

type PageProtectedHandlerProps =
  InferGetServerSidePropsType<GetServerSideProps>;

const withPageProtected = (
  role: type_user["role"],
  handler: (_props: PageProtectedHandlerProps) => ReactNode
) =>
  function PageProtectedHandler(props: PageProtectedHandlerProps) {
    const router = useRouter();
    const { data: session, status } = useSession();

    useEffect(() => {
      if (!role.includes("None")) {
        if (status === "unauthenticated") {
          router.replace("/auth/login");
        } else if (
          status === "authenticated" &&
          session &&
          !session.user.isActive
        ) {
          router.replace("/auth/verify-email");
        } else if (session && !role.includes(session.user.type_user)) {
          router.replace("/");
        }
      }
    }, [router, session, status]);

    if (!role.includes("None") || status === "loading") {
      if (
        status !== "authenticated" ||
        (session && !session.user.isActive) ||
        (session?.user && !role.includes(session.user.type_user))
      ) {
        return <CLoading fullScreen />;
      }
    }

    const Component: any = handler;
    return <Component {...{ ...session, ...props }} />;
  };

export default withPageProtected;
