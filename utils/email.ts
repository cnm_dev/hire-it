// import { transporter } from "@/constants";
import nodemailer from "nodemailer";

interface MailOptions {
  from: string;
  to: string;
  subject: string;
  html: string;
}

export  const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: Number.parseInt(process.env.SMTP_PORT || "465"),
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASSWORD,
  },
  secure: true,
  tls: {
    rejectUnauthorized: false,
  },
});

export async function sendEmail({ from, to, subject, html }: MailOptions) {

  const mailOptions = { from, to, subject, html };

  const info = await transporter.sendMail(mailOptions);

  console.log(`Email sent: ${info.messageId}`);
}
