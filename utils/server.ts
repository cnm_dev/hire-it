import { GraphResultData } from "@/types/models";
import isObject from "lodash/isObject";
import isArray from "lodash/isArray";
import has from "lodash/has";
import { NextApiRequest } from "next";
import { getToken } from "next-auth/jwt";
import { ApiError, ApiException } from "@/types/error";
import { Token } from "@/types/nextAuth";
import { UNAUTHORIZED_ERROR } from "@/constants/error";
import { AnySchema, ValidationError } from "yup";
import { AnyObject } from "@/types";

export const getCurrentUserToken = async (req: NextApiRequest) => {
  const nextAuthSecret =
    process.env.NEXTAUTH_SECRET || "0fc05497-48ff-4dfe-b979-ba4a41654ae7";
  const token = await getToken({ req, secret: nextAuthSecret });

  if (!token) {
    throw new ApiException(401, UNAUTHORIZED_ERROR);
  }
  return token as Token;
};

export const getErrorInfo = (err: any) => {
  const { message, code, stack, name } = err;
  const error: any = {
    status: 500,
    ...{ code },
    ...{ name },
    message,
  };
  if (process.env.NODE_ENV !== "production") {
    error.stack = stack;
  }
  return { error };
};

const flattenAttributes = (data: any): any => {
  if (has(data, "attributes")) {
    return {
      id: data.id,
      ...data.attributes,
    };
  }
  return data;
};

const recursiveTransformer = (data: any): any | null => {
  if (isArray(data)) {
    return data.map((item) => recursiveTransformer(item));
  }

  if (isObject(data) as boolean) {
    let dataTemp: any | null;
    if (isArray(data.data)) {
      dataTemp = [...data.data];
    } else if (isObject(data.data)) {
      dataTemp = flattenAttributes({ ...data.data });
    } else if (data.data === null) {
      dataTemp = null;
    } else {
      dataTemp = flattenAttributes(data);
    }
    for (const key in dataTemp) {
      dataTemp[key] = recursiveTransformer(dataTemp[key]);
    }
    return dataTemp;
  }

  return data;
};

export const transformer = <T = any>(graphResultData: GraphResultData<T>) => {
  const data = graphResultData.data as any;
  return {
    data: recursiveTransformer(data) as T,
    ...{ meta: graphResultData.meta },
  };
};

/**
 * Validate data from request
 *
 * @param schema Yub validation schema
 * @param req Next Request
 * @param data Object data
 * @returns Object data or undefined
 */
export const validateInput = async <T = any>(
  schema: AnySchema,
  req: NextApiRequest,
  data?: AnyObject
) => {
  try {
    data = { ...req.body, ...req.query, ...data };
    await schema.validate(data);
    return data as T;
  } catch (err) {
    const { type, name, message, stack } = err as ValidationError;
    const error: ApiError = {
      status: 400,
      code: type,
      name,
      message,
    };
    if (process.env.NODE_ENV !== "production") {
      error.stack = stack;
    }
    throw new ApiException(400, error);
  }
};
