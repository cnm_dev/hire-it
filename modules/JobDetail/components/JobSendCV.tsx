import { langs } from "@/mock/langs";
import { TypeLang } from "@/modules/Home/components/HotMember";
import { DirectboxNotif, Mobile } from "iconsax-react";
import React from "react";
interface JobSendCVProps {
  lang: TypeLang["lang"];
  email?: string;
  phone?: string;
}
const JobSendCV = ({ lang, email, phone }: JobSendCVProps) => {
  return (
    <div className="job-send">
      <h3>{langs.posting.send[lang]}</h3>
      <div className="job-send__container">
        <div className="job-send__item">
          <DirectboxNotif size={28} variant="Outline" />
          <span>{email}</span>
        </div>
        <div className="job-send__item">
          <Mobile size={28} variant="Outline" />
          <span>{phone}</span>
        </div>
      </div>
    </div>
  );
};

export default JobSendCV;
