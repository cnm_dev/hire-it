import { langs } from "@/mock/langs";
import { TypeLang } from "@/modules/Home/components/HotMember";
import {
  Award,
  BrifecaseTimer,
  EmptyWallet,
  Profile2User,
  Sagittarius,
} from "iconsax-react";
import React from "react";

interface JobPostGlobalProps {
  lang: TypeLang["lang"];
  wage?: number;
  quantity?: number;
  gender?: string;
  experience?: string;
  timework?: string;
}

const JobPostInfor = ({
  lang,
  wage,
  quantity,
  gender,
  experience,
  timework,
}: JobPostGlobalProps) => {
  return (
    <div className="post-infor">
      <h3>{langs.posting.general[lang]}:</h3>
      <div className="post-infor__list">
        <div className="post-infor__item">
          <EmptyWallet size={28} variant="Outline" />
          <span>{wage} $</span>
        </div>
        <div className="post-infor__item">
          <Profile2User size={28} variant="Outline" />
          <span>{quantity} people</span>
        </div>
        <div className="post-infor__item">
          <Sagittarius size={28} variant="Outline" />
          <span>{gender}</span>
        </div>
        <div className="post-infor__item">
          <Award size={28} variant="Outline" />
          <span>Experience: {experience}</span>
        </div>
        <div className="post-infor__item">
          <BrifecaseTimer size={28} variant="Outline" />
          <span>{timework}</span>
        </div>
      </div>
    </div>
  );
};

export default JobPostInfor;
