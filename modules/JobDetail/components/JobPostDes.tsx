import { langs } from "@/mock/langs";
import { TypeLang } from "@/modules/Home/components/HotMember";
import React from "react";
interface JobPostDesProps {
  description?: string[];
  lang: TypeLang["lang"];
}

const JobPostDes = ({ lang, description }: JobPostDesProps) => {
  return (
    <div className="job-des">
      <h3>{langs.posting.jobdes[lang]}:</h3>
      <ul>
        {description &&
          description.map((data, index) => <li key={index}>{data}</li>)}
      </ul>
    </div>
  );
};

export default JobPostDes;
