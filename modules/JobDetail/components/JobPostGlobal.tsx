/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useRef, useState } from "react";
import { IoShareOutline } from "react-icons/io5";
import { AiFillHeart } from "react-icons/ai";
import { TypeLang } from "@/modules/Home/components/HotMember";
import { formatDate } from "@/utils";
import { Warning2 } from "iconsax-react";
import CModal from "@/common/components/controls/CModal";
import CShare from "@/common/components/controls/CShare";
import { createPostingSave, updatePostingSave } from "@/apis/PostingSave";
import { JobPosting, PostingSave } from "@/types/models";
import { useSession } from "next-auth/react";
import { mutate } from "swr";
import CRadioButton from "@/common/components/controls/CRadioButton";
import CTextField from "@/common/components/controls/CTextField";
import CButton from "@/common/components/controls/CButton";
import { toast } from "react-toastify";
import { createPostingSpam } from "@/apis/PostingSpam";
import CSpinner from "@/common/components/controls/CSpinner";
import { createInformates } from "@/apis/Informates";
import { useRouter } from "next/router";
import CLoading from "@/common/components/controls/CLoading";

interface JobPostGlobalProps {
  lang: TypeLang["lang"];
  listAboutCompany?: string[];
  jobPostingName: string;
  addressCompany?: string;
  companyName: string;
  timeRemain: Date;
  image: string;
  jobposting: JobPosting;
  listIdPostingSave: string[];
  postingSave: PostingSave[];
}
const radioSpam = [
  {
    label: "Bài đăng tuyển dụng chứa thông tin sai lệch",
  },
  { label: "Bài đăng tuyển dụng không cung cấp đủ thông tin" },
  { label: "Bài đăng tuyển dụng có nội dung không phù hợp" },
  { label: "Bài đăng tuyển dụng có dấu hiệu lừa đảo" },
  { label: "Bài đăng tuyển dụng không liên quan đến công việc" },
  { label: "Khác" },
];

const JobPostGlobal = ({
  postingSave,
  listIdPostingSave,
  jobposting,
  listAboutCompany,
  jobPostingName,
  addressCompany,
  companyName,
  timeRemain,
  image,
}: JobPostGlobalProps) => {
  const [valueSpam, setValueSpam] = useState<string>();
  const { data: session, status } = useSession();
  const router = useRouter();

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [activeModal, setActiveModal] = useState<boolean>(false);
  const [valueInputSpam, setValueInputSpam] = useState<string>("");
  const handelSaveJob = () => {
    if (session) {
      setIsLoading(true);
      if (session.user.type_user === "Customer") {
        if (postingSave.length === 0) {
          createPostingSave({
            idPosting: [jobposting.id],
            idCustomer: session.user.customer.id,
          })
            .then(() => {
              setIsLoading(false);
              mutate(`/api/customers/${session.user.id}/post-save`);
            })
            .catch(() => {
              setIsLoading(false);
            });
        } else {
          let newListIdPosting = listIdPostingSave.includes(jobposting.id)
            ? listIdPostingSave.filter((idPost) => idPost !== jobposting.id)
            : [...listIdPostingSave, jobposting.id];
          updatePostingSave({
            idCustomer: session.user.customer.id,
            idPostSave: postingSave[0].id,
            idPosting: newListIdPosting,
          })
            .then(() => {
              setIsLoading(false);
              mutate(`/api/customers/${session.user.id}/post-save`);
            })
            .catch(() => {
              setIsLoading(false);
            });
        }
      } else {
        if (postingSave.length === 0) {
          createPostingSave({
            idPosting: [jobposting.id],
            idEmployee: session.user.employee.id,
          })
            .then(() => {
              setIsLoading(false);
              mutate(`/api/employees/${session.user.id}/post-save`);
            })
            .catch(() => {
              setIsLoading(false);
            });
        } else {
          let newListIdPosting = listIdPostingSave.includes(jobposting.id)
            ? listIdPostingSave.filter((idPost) => idPost !== jobposting.id)
            : [...listIdPostingSave, jobposting.id];
          updatePostingSave({
            idEmployee: session.user.employee.id,
            idPostSave: postingSave[0].id,
            idPosting: newListIdPosting,
          })
            .then(() => {
              setIsLoading(false);
              mutate(`/api/employees/${session.user.id}/post-save`);
            })
            .catch(() => {
              setIsLoading(false);
            });
        }
      }
    } else {
      router.replace("/auth/login");
    }
  };

  const handelSpam = () => {
    if (valueSpam === undefined) {
      toast.warning("Vui lòng chọn lý do spam");
    } else if (valueSpam === "Khác") {
      if (valueInputSpam === "") {
        toast.warning("Vui lòng nhập lý do spam");
      } else {
        setIsLoading(true);
        if (session) {
          if (session.user.type_user === "Customer") {
            createPostingSpam({
              idPosting: jobposting.id,
              reason: valueInputSpam,
              idCustomer: session.user.customer.id,
            }).then(() => {
              createInformates({
                title: "Spam Posting",
                description: `Posting ${jobposting.jobPostingName} has been spam`,
                adminId: "1",
                route: "/manager/job-posting",
              })
                .then(() => {
                  setIsLoading(false);
                  setOpenModal(false);
                  toast.success("Spam success");
                })
                .catch(() => {
                  setIsLoading(false);
                });
            });
          } else {
            createPostingSpam({
              idPosting: jobposting.id,
              reason: valueInputSpam,
              idEmployee: session.user.employee.id,
            })
              .then(() => {
                createInformates({
                  title: "Spam Posting",
                  description: `Posting ${jobposting.jobPostingName} has been spam`,
                  adminId: "1",
                  route: "/manager/job-posting",
                }).then(() => {
                  setIsLoading(false);
                  setOpenModal(false);
                  toast.success("Spam success");
                });
              })
              .catch(() => {
                setIsLoading(false);
              });
          }
        }
      }
    } else {
      if (session && valueSpam) {
        if (session.user.type_user === "Customer") {
          createPostingSpam({
            idPosting: jobposting.id,
            reason: valueSpam,
            idCustomer: session.user.customer.id,
          })
            .then(() => {
              createInformates({
                title: "Spam Posting",
                description: `Posting ${jobposting.jobPostingName} has been spam`,
                adminId: "1",
                route: "/manager/job-posting",
              }).then(() => {
                setIsLoading(false);
                setOpenModal(false);
                toast.success("Spam success");
              });
            })
            .catch(() => {
              setIsLoading(false);
            });
        } else {
          createPostingSpam({
            idPosting: jobposting.id,
            reason: valueSpam,
            idEmployee: session.user.employee.id,
          })
            .then(() => {
              createInformates({
                title: "Spam Posting",
                description: `Posting ${jobposting.jobPostingName} has been spam`,
                adminId: "1",
                route: "/manager/job-posting",
              }).then(() => {
                setIsLoading(false);
                setOpenModal(false);
                toast.success("Spam success");
              });
            })
            .catch(() => {
              setIsLoading(false);
            });
        }
      }
    }
  };
  return (
    <div className="post-global">
      <div className="post-global__img">
        <img src={image} alt="" />
        <div className="post-global__container">
          <div className="jobInfor">
            <h2>{jobPostingName}</h2>
            <h3>{companyName}</h3>
            <p>Hạn nộp hồ sơ: {formatDate(timeRemain)}</p>
            <div className="post-global__media">
              <CModal
                modal={<CShare title={jobPostingName} />}
                button={
                  <div className="post-media__container">
                    <IoShareOutline />
                    <span>Share</span>
                  </div>
                }
              />
              <CModal
                setIsOpenModal={setOpenModal}
                isOpenModal={openModal}
                modal={
                  !activeModal ? (
                    <CLoading />
                  ) : (
                    <div className="modal-spam">
                      <h4>Report</h4>
                      {radioSpam.map((label, index) => (
                        <CRadioButton
                          key={index}
                          setState={setValueSpam}
                          name="spam"
                          item={label.label}
                          label={label.label}
                          state={valueSpam}
                          onChange={(e) => setValueSpam(e.target.value)}
                        />
                      ))}
                      <CTextField
                        defaultValue={valueInputSpam}
                        onChange={(e) => setValueInputSpam(e.target.value)}
                        placeholder="Enter your reason spam"
                        disable={valueSpam !== "Khác" ? true : false}
                      />
                      <div className="modal-spam__btn">
                        <CButton
                          text="Cancel"
                          onClick={() => setOpenModal(false)}
                        />
                        <CButton
                          disabled={isLoading}
                          text={
                            isLoading ? <CSpinner size="small" /> : "Submit"
                          }
                          onClick={handelSpam}
                        />
                      </div>
                    </div>
                  )
                }
                button={
                  <div
                    className="post-media__container"
                    onClick={() => {
                      status !== "authenticated"
                        ? router.replace("/auth/login")
                        : setActiveModal(true);
                    }}
                  >
                    <Warning2 />
                    <span>Spam</span>
                  </div>
                }
              />
              <div
                className="post-media__container"
                onClick={() => !isLoading && handelSaveJob()}
              >
                <AiFillHeart
                  color={`${
                    listIdPostingSave.includes(jobposting.id) ? "#f44242" : ""
                  }`}
                />
                <span>Save</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="post-global__content">
        <h3 className="title_global">About company:</h3>
        <ul>
          {listAboutCompany &&
            listAboutCompany.map((value: string, index: number) => (
              <li key={index}>
                <p>{value}</p>
              </li>
            ))}
        </ul>
        <p>
          <u>Location</u> : {addressCompany}
        </p>
      </div>
    </div>
  );
};

export default JobPostGlobal;
