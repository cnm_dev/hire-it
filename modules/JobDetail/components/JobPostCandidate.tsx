import { langs } from "@/mock/langs";
import { TypeLang } from "@/modules/Home/components/HotMember";
import React from "react";

interface JobPostGlobalProps {
  lang: TypeLang["lang"];
  requirement?: string[];
}

const JobPostCandidate = ({ lang, requirement }: JobPostGlobalProps) => {

  return (
    <div className="job-candidate">
      <h3>{langs.posting.candidate[lang]}</h3>
      <ul>
        {requirement &&
          requirement.map((data, index) => <li key={index}>{data}</li>)}
      </ul>
    </div>
  );
};

export default JobPostCandidate;
