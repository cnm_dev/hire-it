/* eslint-disable react-hooks/rules-of-hooks */
import CButton from "@/common/components/controls/CButton";
import { langs } from "@/mock/langs";
import React, { useMemo } from "react";
import { TypeLang } from "../Home/components/HotMember";
import JobPostCandidate from "./components/JobPostCandidate";
import JobPostDes from "./components/JobPostDes";
import JobPostGlobal from "./components/JobPostGlobal";
import JobPostInfor from "./components/JobPostInfor";
import JobSendCV from "./components/JobSendCV";
import useJobPosting from "@/hooks/useJobPosting";
import CLoading from "@/common/components/controls/CLoading";
import { convertListValue } from "@/utils";
import { PostingSave } from "@/types/models";

type JobDetailProps = {
  lang: TypeLang["lang"];
  postId: string;
  postingSave: PostingSave[];
  isShowHeader?: boolean;
};

const index = ({
  lang,
  postId,
  postingSave,
  isShowHeader = true,
}: JobDetailProps) => {
  const listIdPostingSave = useMemo(() => {
    let listId: string[] = [];

    if (postingSave.length > 0) {
      postingSave[0].job_postings.map((item) => listId.push(item.id));
    }

    return listId;
  }, [postingSave]);

  const { jobposting, loading } = useJobPosting({ postId: postId });

  const listAboutCompany = useMemo(() => {
    if (jobposting) {
      return convertListValue(jobposting[0].aboutCompany, "\n");
    }
  }, [jobposting]);

  const listDesJob = useMemo(() => {
    if (jobposting) {
      return convertListValue(jobposting[0].description, "\n");
    }
  }, [jobposting]);

  const listReq = useMemo(() => {
    if (jobposting) {
      return convertListValue(jobposting[0].requirement, "\n");
    }
  }, [jobposting]);

  return loading ? (
    <CLoading fullScreen />
  ) : (
    <div className="posting">
      {isShowHeader && (
        <div className="posting-header">
          <h3>{langs.posting.jobpostL[lang]}</h3>
          <div className="posting-header__statis">
            <CButton text={`3+ ${langs.posting.posting[lang]}`} />
            <span>
              3+ {langs.posting.posting[lang]} / {langs.global.day[lang]}
            </span>
          </div>
        </div>
      )}
      {jobposting ? (
        <div className="posting-container">
          <JobPostGlobal
            postingSave={postingSave}
            listIdPostingSave={listIdPostingSave}
            lang={lang}
            jobposting={jobposting[0]}
            listAboutCompany={listAboutCompany}
            image={jobposting[0].image}
            jobPostingName={jobposting[0].jobPostingName}
            companyName={jobposting[0].companyName}
            addressCompany={jobposting[0].location}
            timeRemain={jobposting[0].timeRemain}
          />
          <JobPostInfor
            lang={lang}
            wage={jobposting[0].wage}
            quantity={jobposting[0].quantity}
            gender={jobposting[0].gender}
            experience={jobposting[0].experience}
            timework={jobposting[0].timework}
          />
          <JobPostDes lang={lang} description={listDesJob} />
          <JobPostCandidate lang={lang} requirement={listReq} />
          <JobSendCV
            lang={lang}
            phone={jobposting[0].phone}
            email={jobposting[0].email}
          />
        </div>
      ) : (
        <CLoading fullScreen/>
      )}
    </div>
  );
};

export default index;
