/* eslint-disable @next/next/no-img-element */
import React from "react";

// icon
import { Verify } from "iconsax-react";
import CButton from "@/common/components/controls/CButton";
import image from "@/common/assets/image/all-image";
import { TypeLang } from "./HotMember";
import { langs } from "@/mock/langs";
import { useSession } from "next-auth/react";
import Link from "next/link";
const Partner = ({ lang }: TypeLang) => {
  const { data: session, status } = useSession();
  return (
    <div className="partner">
      <div className="partner-image">
        <img src={image.parent.image_1} alt="" />
        <div className="partner-image__name">
          <h4>Awkward Astronauts</h4>
          <Verify variant="Bold" />
        </div>
        <p>pinochio_2</p>
      </div>
      <div className="partner-image">
        <img src={image.parent.image_2} alt="" />
        <div className="partner-image__name">
          <h4>Awkward Astronauts</h4>
          <Verify variant="Bold" />
        </div>
        <p>pinochio_2</p>
      </div>
      <div className="partner-context">
        <h3>{langs.home.becom[lang]}</h3>
        <p>{langs.home.becomdes[lang]}</p>
        <div className="partner-context__button">
          <CButton
            disabled={session?.user.type_user === "Customer"}
            text={
              <Link
                href={`${
                  status === "unauthenticated"
                    ? "/auth/signup"
                    : "/auth/profile"
                }`}
              >
                {langs.global.iamfree[lang]}
              </Link>
            }
          />

          <CButton
            disabled={session?.user.type_user === "Employee"}
            text={
              <Link
                href={`${
                  status === "unauthenticated" ? "/auth/signup" : "/hire"
                }`}
              >
                {langs.global.iamhire[lang]}
              </Link>
            }
          />
        </div>
      </div>
    </div>
  );
};

export default Partner;
