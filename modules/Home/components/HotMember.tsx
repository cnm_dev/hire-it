/* eslint-disable @next/next/no-img-element */
import CAutocomplete from "@/common/components/controls/CAutocomplete";
import CButton from "@/common/components/controls/CButton";
import React from "react";
import { ArrowLeft, ArrowRight } from "iconsax-react";
// icon
import { MagicStar } from "iconsax-react";
// api
import { all_api } from "@/mock/all_api";

// swiper
// core version + navigation, pagination modules:
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Autoplay, Navigation, Pagination } from "swiper";
// import Swiper and modules styles
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/autoplay";
import image from "@/common/assets/image/all-image";
import { langs } from "@/mock/langs";
import Link from "next/link";

// swiper
SwiperCore.use([Navigation, Pagination, Autoplay]);

export type TypeLang = {
  lang: "en" | "vn";
};
const HotMember = ({ lang }: TypeLang) => {
  return (
    <div className="hotMember">
      <div className="hotMember-header">
        <div className="hotMember-header__left">
          <h3> {langs.home.hotmember[lang]}</h3>
        </div>
        <div className="hotMember-header__right">
          <Link href="/member">
            <CButton text={langs.global.seeall[lang]} variant="outlined" />
          </Link>
          <div className="swiper-button-prev">
            <CButton text={<ArrowLeft />} variant="outlined" />
          </div>
          <div className="swiper-button-next">
            <CButton text={<ArrowRight />} variant="outlined" />
          </div>
        </div>
      </div>
      <div className="hotMember-slides">
        <Swiper
          slidesPerView={3.5}
          spaceBetween={30}
          autoplay={{
            delay: 1500,
          }}
          modules={[Autoplay]}
          className="mySwiper"
          navigation={{
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
          }}
        >
          {all_api.hot_member.map((member, index) => (
            <SwiperSlide key={index}>
              <div className="hotMember-slides__item">
                <img
                  className="hotMember-slides__img"
                  src={member.image}
                  alt=""
                />
                <div className="hotMemeber-item__information">
                  <div className="hotMember-information__title">
                    <span> {langs.global.price[lang]}</span>
                    <span>${member.price}/1h</span>
                  </div>
                  <div className="hotMember-information__content">
                    <span>{langs.global.amount[lang]}</span>
                    <span>
                      {member.mount_job} {langs.home.job[lang]}
                    </span>
                  </div>
                </div>
                <div className="hotMember-item__star">
                  <div className="hotMember-star__information">
                    <img
                      className="hotMember-star__avatar"
                      src={member.avatar}
                      alt=""
                    />
                    <h5>{member.name}</h5>
                  </div>
                  <div className="hotMember-star__mount">
                    <MagicStar variant="Bold" />
                    <span>{member.star}</span>
                  </div>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  );
};

export default HotMember;
