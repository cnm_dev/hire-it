/* eslint-disable @next/next/no-img-element */
import CButton from "@/common/components/controls/CButton";
import React from "react";

interface PropItemMarketplace {
  item: {
    title: string;
    image: any;
  };
}

const ItemMarketplace = ({ item }: PropItemMarketplace) => {
  return (
    <div className="item-maketplace">
      <img src={item.image} alt="" />
      <CButton text={item.title}/>
    </div>
  );
};

export default ItemMarketplace;
