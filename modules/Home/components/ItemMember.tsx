/* eslint-disable @next/next/no-img-element */
import React from "react";

interface PropItemMember {
  avatar: string;
  name: string;
  profession: string;
}

const ItemMember = ({ avatar, name, profession }: PropItemMember) => {
  return (
    <div className="home-itemMember">
      <img width={45} height={45} src={avatar} alt="" />
      <div className="home-itemMember__infor">
        <h4>{name}</h4>
        <span>{profession}</span>
      </div>
    </div>
  );
};

export default ItemMember;
