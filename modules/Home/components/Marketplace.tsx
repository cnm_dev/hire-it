import React from "react";
import ItemMarketplace from "./ItemMarketplace";
import { all_api } from "@/mock/all_api";
import { TypeLang } from "./HotMember";
import { langs } from "@/mock/langs";
import Link from "next/link";

const Marketplace = ({ lang }: TypeLang) => {
  return (
    <div className="maketplace">
      <h2>{langs.home.market[lang]}</h2>
      <div className="maketplace-list">
        {all_api.marketplace.map((item, index) => (
          <Link href={`/member?skill=${item.title.split(" ")[0]}`} key={index}>
            <ItemMarketplace item={item} />
          </Link>
        ))}
      </div>
    </div>
  );
};

export default Marketplace;
