import React from "react";

// icon
import { EmptyWallet, Category, GalleryImport, Send2 } from "iconsax-react";
import { TypeLang } from "./HotMember";
import { langs } from "@/mock/langs";
const Convenience = ({ lang }: TypeLang) => {
  return (
    <div className="convenience">
      <h2>{langs.home.easy[lang]}</h2>
      <div className="convenience-content">
        <div className="convenience-content__item">
          <div className="convenience-item__icon">
            <EmptyWallet variant="Bold" />
          </div>
          <h4>{langs.home.pay[lang]}</h4>
          <p>{langs.home.paydes[lang]}</p>
        </div>
        <div className="convenience-content__item">
          <div className="convenience-item__icon">
            <Category variant="Bold" />
          </div>
          <h4>{langs.home.interface[lang]}</h4>
          <p>{langs.home.interfacedes[lang]}</p>
        </div>
        <div className="convenience-content__item">
          <div className="convenience-item__icon">
            <GalleryImport variant="Bold" />
          </div>
          <h4>{langs.home.image[lang]}</h4>
          <p>{langs.home.imgaedes[lang]}</p>
        </div>
        <div className="convenience-content__item">
          <div className="convenience-item__icon">
            <Send2 variant="Bold" />
          </div>
          <h4>{langs.home.feedback[lang]}</h4>
          <p>{langs.home.feedbackdes[lang]}</p>
        </div>
      </div>
    </div>
  );
};

export default Convenience;
