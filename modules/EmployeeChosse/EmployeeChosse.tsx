import CButton from "@/common/components/controls/CButton";
import React, { useMemo, useState } from "react";
import NotFoundValue from "../NotFoundValue";
import CTable from "@/common/components/controls/CTable";
import { Employee } from "@/types/models";
import CPagination from "@/common/components/controls/CPagination";
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import {
  DocumentText,
  InfoCircle,
  TickCircle,
  TrushSquare,
} from "iconsax-react";
import CTooltip from "@/common/components/controls/CTooltip";
import CBadge from "@/common/components/controls/CBadge";
import { deleteEmployeeChosse } from "@/apis/EmployeeChosse";
import { toast } from "react-toastify";
import { mutate } from "swr";
import { useSession } from "next-auth/react";
import CModal from "@/common/components/controls/CModal";
import QuickHire from "../hire/QuickHire/QuickHire";

type EmployeeChosseProps = {
  listChosse: Employee[];
  employeeChosseId: string;
};

const EmployeeChosse = ({
  listChosse,
  employeeChosseId,
}: EmployeeChosseProps) => {
  const [pagination, SetPagination] = React.useState(1);
  const [listmember, setListMember] = React.useState<Employee[]>(
    listChosse.slice(10)
  );
  const [listIndexChosse, setListIndexChosse] = React.useState<number[]>([]);
  const { data: session } = useSession();
  const [isClear, setIsClear] = useState<boolean>(false);
  React.useEffect(() => {
    setListMember(listChosse.slice((pagination - 1) * 10, pagination * 10));
  }, [listChosse, pagination]);

  const handelDeleteEmployeeChosse = (isToast: boolean = true) => {
    let list: number[] = [];
    for (let i = 0; i < listChosse.length; i++) {
      list.push(i);
    }
    const result = list.filter(
      (element: number) => !listIndexChosse.includes(element)
    );
    let listIdEmployee: string[] = [];
    result.forEach((indexChosse) => {
      listIdEmployee.push(listChosse[indexChosse].id);
    });

    deleteEmployeeChosse({ employeeChosseId, listIdEmployee })
      .then(() => {
        setIsClear(true);
        mutate(`/api/customers/${session?.user.id}/employee-chosse`);
        setListIndexChosse([]);
        isToast && toast.success("Delete chosse employee complete");
      })
      .catch((err) => toast.error(err.message));
  };

  const listEmployeeSelected = useMemo(() => {
    let listEmployee: Employee[] = [];
    listIndexChosse.map((indexSelected) => {
      listEmployee.push(listChosse[indexSelected]);
    });
    return listEmployee;
  }, [listChosse, listIndexChosse]);
  const rows = useMemo(() => {
    let valueRows: any = [];
    listmember.map((employee, index) => {
      valueRows.push([
        employee.users_permissions_user.email,
        employee.users_permissions_user.fullName,
        employee.position.positionName,
        employee.hourlyRate,
        employee.yearsOfExperience,
        <span
          key={index}
          style={{
            color: `${employee.start < 3 ? "red" : "#22e257"}`,
            fontWeight: "600",
          }}
        >
          {employee.start}
        </span>,
        employee.isRemote ? (
          <TickCircle size="24" color="#51cb1f" />
        ) : (
          <InfoCircle size="24" color="#FF8A65" />
        ),
        <span
          key={index}
          style={{
            textTransform: "capitalize",
            fontWeight: "600",
            color: `${employee.statusWork === "free" ? "#51cb1f" : "#FF8A65"}`,
          }}
        >
          {employee.statusWork}
        </span>,
      ]);
    });
    return valueRows;
  }, [listmember]);
  return (
    <div className="employee-choose">
      <div className="employee-choose__header">
        <h3>
          Employee Chosse
          <CButton text={`${listChosse.length} employee`} />
        </h3>
        <div className="employee-choose__function">
          {listIndexChosse.length > 0 && listChosse.length > 0 && (
            <div className="function-chosse__container">
              <CBadge
                color={"success"}
                numberInfo={
                  listIndexChosse.length > 1 ? listIndexChosse.length : 0
                }
                icon={
                  <CModal
                    confirm={true}
                    disable={listEmployeeSelected.some(
                      (employee) => employee.statusWork !== "free"
                    )}
                    textToast="You are selecting employees who are busy working"
                    modal={
                      <QuickHire
                        handelDeleteEmployeeChosse={handelDeleteEmployeeChosse}
                        listEmployee={listEmployeeSelected}
                      />
                    }
                    button={
                      <CButtonIcon
                        icon={
                          <CTooltip
                            placement="bottom"
                            title="Create Contact"
                            child={
                              <span>
                                <DocumentText size="27" color="green" />
                              </span>
                            }
                          />
                        }
                      />
                    }
                  />
                }
              />

              <CBadge
                color={"error"}
                numberInfo={
                  listIndexChosse.length > 1 ? listIndexChosse.length : 0
                }
                icon={
                  <CButtonIcon
                    onclick={() => handelDeleteEmployeeChosse()}
                    icon={
                      <CTooltip
                        placement="bottom"
                        title="Remove"
                        child={
                          <span>
                            <TrushSquare size="27" color="red" />
                          </span>
                        }
                      />
                    }
                  />
                }
              />
            </div>
          )}
        </div>
      </div>

      <div className="employee-choose__main">
        {listChosse.length === 0 ? (
          <NotFoundValue />
        ) : (
          rows && (
            <CTable
              lenghtFull={listChosse.length}
              rows={rows}
              alignTitle={[
                "left",
                "left",
                "left",
                "center",
                "center",
                "center",
                "center",
                "center",
              ]}
              alignItem={[
                "left",
                "left",
                "left",
                "center",
                "center",
                "center",
                "center",
                "center",
              ]}
              label={[
                "Email",
                "Full name",
                "Position",
                "Hourly Rate",
                "Experience",
                "Star",
                "Remote",
                "Status Work",
              ]}
              isCheck={true}
              pagination={pagination}
              setListIndexChosse={setListIndexChosse}
              isClear={isClear}
              setIsClear={setIsClear}
            />
          )
        )}
      </div>
      {listChosse.length !== 0 && (
        <div className="member-chosse__pagination">
          <CPagination
            SetPagination={SetPagination}
            count={
              listChosse.length <= 10 ? 1 : Math.ceil(listChosse.length / 10)
            }
          />
        </div>
      )}
    </div>
  );
};

export default EmployeeChosse;
