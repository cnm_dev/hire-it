import Image from "next/image";
import React from "react";
import FormJobSelection from "./components/FormJobSelection";
import FormSignUp from "./components/FormSignUp";

const SignUp = () => {
  const [isShowForm, setShowForm] = React.useState(true);
  const [value, setValue] = React.useState("");
  return (
    <div className="form-job-selecttion-main">
      {/* <Image className="image-logo" src={item.logo} alt="" /> */}
      {isShowForm ? (
        <FormJobSelection setShowForm={setShowForm} setValue={setValue} />
      ) : (
        <FormSignUp value={value} />
      )}
    </div>
  );
};

export default SignUp;
