/* eslint-disable @next/next/no-img-element */
import CFormSelect from "@/common/components/controls/CFormSelect";
import CLoading from "@/common/components/controls/CLoading";
import CPassword from "@/common/components/controls/CPassword";
import usePositions from "@/hooks/usePositions";
import imageLogo from "@/modules/assets/imgLogo";
import { registerCustomer, registerStaff } from "@/services/user";
import { Registrys } from "@/types/models";
import {
  Button,
  Checkbox,
  CircularProgress,
  FormControlLabel,
  TextField,
  Typography,
} from "@mui/material";
import axios from "axios";
import { signIn } from "next-auth/react";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-toastify";

interface PropsFormSignUp {
  value?: String;
}

const names = [
  "Designer",
  "Developer",
  "Finance Expert",
  "Product Manager",
  "Project Manager",
];

const FormSignUp = ({ value }: PropsFormSignUp) => {
  const [loadingNext, setLoadingNext] = React.useState<boolean>(false);
  const [loadingBack, setLoadingBack] = React.useState<boolean>(false);
  const { positions, loading } = usePositions();
  const [isClient, setClient] = React.useState<Boolean>(
    value === "client" ? true : false
  );
  const router = useRouter();

  const onclickChangeForm = () => {
    setClient(!isClient);
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    watch,
  } = useForm<Registrys>({ reValidateMode: "onSubmit" });

  const password = watch("password", "");
  const confirmPassword = watch("confirmPassword", "");

  // handle validate format email
  const handleBlurEmail = async (event: React.FocusEvent<HTMLInputElement>) => {
    const email = event.target.value;
    const emailRegex = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
    if (email) {
      if (!emailRegex.test(email)) {
        setError("email", {
          type: "onBlur",
          message: "*Invalid email format",
        });
      } else {
        const res = await fetch(`/api/user/${email}`);
        const json = await res.json();
        if (json.data[0]) {
          setError("email", {
            type: "onBlur",
            message: "*Email already exists",
          });
        } else {
          setError("email", {
            type: "onBlur",
            message: "",
          });
        }
      }
    } else {
      setError("email", {
        type: "onBlur",
        message: "",
      });
    }
  };

  // handle validate format full name
  const handleBlurName = (event: React.FocusEvent<HTMLInputElement>) => {
    const name = event.target.value;
    const nameRegex = /^[\p{L} \.'-]+$/u;
    if (name) {
      if (!nameRegex.test(name)) {
        setError("fullName", {
          type: "onBlur",
          message: "*Invalid name format",
        });
      } else {
        setError("fullName", {
          type: "onBlur",
          message: "",
        });
      }
    } else {
      setError("fullName", {
        type: "onBlur",
        message: "",
      });
    }
  };
  // handle validate format PhoneNumber

  const handleBlurPhoneNumber = async (
    event: React.FocusEvent<HTMLInputElement>
  ) => {
    const phone = event.target.value;
    const phoneRegex = /^\+?[0-9]{1,3}\s?[0-9]{9,}$/gm;
    if (phone) {
      if (!phoneRegex.test(phone)) {
        setError("phoneNumber", {
          type: "onBlur",
          message: "*Phone number must be at most 9 digits",
        });
      } else {
        const res = await fetch(`/api/user/phone/${phone}`);
        const json = await res.json();
        if (json.data[0]) {
          setError("phoneNumber", {
            type: "onBlur",
            message: "*Phone number already exists",
          });
        } else {
          setError("phoneNumber", {
            type: "onBlur",
            message: "",
          });
        }
      }
    } else {
      setError("phoneNumber", {
        type: "onBlur",
        message: "",
      });
    }
  };
  // handle validate format password
  const handleBlurPassword = (event: React.FocusEvent<HTMLInputElement>) => {
    const password = event.target.value;
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (password) {
      if (!passwordRegex.test(password)) {
        setError("password", {
          type: "onBlur",
          message:
            "*Password must be at least 8 characters and must contain both letters and numbers",
        });
      } else {
        setError("password", {
          type: "onBlur",
          message: "",
        });
      }
    } else {
      setError("password", {
        type: "onBlur",
        message: "",
      });
    }
  };
  // handle validate format confirm password
  const handleBlurConfirmPassword = (
    password: string,
    confirmPassword: string
  ) => {
    if (confirmPassword) {
      if (confirmPassword !== password) {
        setError("confirmPassword", {
          type: "onBlur",
          message: "*Confirm password is incorrect",
        });
      } else {
        setError("confirmPassword", {
          type: "onBlur",
          message: "",
        });
      }
    } else {
      setError("confirmPassword", {
        type: "onBlur",
        message: "",
      });
    }
  };

  const onRegistryCustomer: SubmitHandler<Registrys> = (data) => {
    setLoadingNext(true);
    registerCustomer({
      ...data,
    })
      .then(async () => {
        const result = await signIn("credentials", {
          redirect: false,
          ...data,
        });
        result && setLoadingNext(false);

        if (result?.ok) {
          router.replace("/auth/verify-email");
        } else {
          toast.error(result?.error);
        }
      })
      .catch((err) => {
        toast.error(err.response.data.error.message);
        setLoadingNext(false);
      });
  };

  const onRegistryStaff: SubmitHandler<Registrys> = async (data, e) => {
    setLoadingNext(true);
    const result = positions.filter(
      (value) => value.positionName === data.position[0]
    );
    registerStaff({
      ...data,
      position: result[0].id,
    })
      .then(async () => {
        const result = await signIn("credentials", {
          redirect: false,
          ...data,
        });
        result && setLoadingNext(false);
        if (result?.ok) {
          router.replace("/auth/verify-email");
        } else {
          toast.error(result?.error);
        }
      })
      .catch((err) => {
        toast.error(err.response.data.error.message);
        setLoadingNext(false);
      });
  };

  return (
    <div className="form-signup-main">
      <p className="title">
        {isClient
          ? "Apply to Hiring" + "\n" + "the World's Top Talent Network"
          : "Apply to Join" + "\n" + " the World'sTop Talent Network"}
      </p>

      <div className="container">
        <div className="form-signup">
          <div
            className=""
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              marginLeft: "25%",
            }}
          >
            <Button
              className="form-login-button-google"
              variant="outlined"
              color="primary"
              onClick={() => {
                setLoadingBack(!loadingBack);
              }}
            >
              {loadingBack ? (
                <CircularProgress size={30} color="inherit" />
              ) : (
                <Link
                  href="/auth/login"
                  style={{
                    width: "100%",
                    height: "100%",
                    color: "#fff",
                    marginTop: 5,
                  }}
                >
                  Continute with HireI
                </Link>
              )}
            </Button>
            <p
              style={{
                fontSize: 13,
                width: "75%",
                opacity: 0.7,
                paddingTop: 5,
              }}
            >
              If you already have an account, click to log in with HireIT.
            </p>
            <p className="line-text">or</p>
          </div>
          <form
            className="form"
            onSubmit={handleSubmit(
              isClient ? onRegistryCustomer : onRegistryStaff
            )}
          >
            {isClient ? null : (
              <>
                <div className="container-error">
                  <p className="error">
                    {errors.position?.type === "required" &&
                      "*Position cannot be empty"}
                  </p>
                  {loading ? (
                    <CLoading />
                  ) : (
                    <CFormSelect
                      names={positions}
                      className="textfield"
                      placeholder="I’m applying as a..."
                      registerName={{
                        ...register("position", { required: true }),
                      }}
                    />
                  )}
                </div>
              </>
            )}

            <div
              className=""
              style={{ display: "flex", width: "100%", gap: 10 }}
            >
              <div className="container-error">
                <p className="error">
                  {errors.fullName?.type === "required"
                    ? "*Full name cannot be empty"
                    : errors.fullName && <p>{errors.fullName.message}</p>}
                </p>
                <TextField
                  {...register("fullName", {
                    required: true,
                    pattern: {
                      value: /^[\p{L} \.'-]+$/u,
                      message: "*Invalid name format",
                    },
                  })}
                  aria-invalid={errors.fullName ? "true" : "false"}
                  onBlur={handleBlurName}
                  className="textfield"
                  label="Full name"
                  variant="outlined"
                  placeholder="Enter your Full name"
                  type="text"
                />
              </div>
              <div className="container-error">
                <p className="error">
                  {errors.phoneNumber?.type === "required"
                    ? "*Phone number cannot be empty"
                    : errors.phoneNumber && <p>{errors.phoneNumber.message}</p>}
                </p>
                <TextField
                  {...register("phoneNumber", {
                    required: true,
                    pattern: {
                      value: /^\+?[0-9]{1,3}\s?[0-9]{9,}$/gm,
                      message: "*Phone number must be at most 9 digits",
                    },
                    validate: async (value) => {
                      const res = await fetch(`/api/user/phone/${value}`);
                      const json = await res.json();
                      if (json.data[0]) {
                        return "*Phone number already exists";
                      }
                      return true;
                    },
                    onBlur(event) {
                      handleBlurPhoneNumber(event);
                    },
                  })}
                  aria-invalid={errors.phoneNumber ? "true" : "false"}
                  className="textfield"
                  label="Phone number"
                  variant="outlined"
                  placeholder="Number Of Contact"
                  type="text"
                />
              </div>
            </div>
            <div className="container-error">
              <p className="error">
                {errors.email?.type === "required"
                  ? "*Email cannot be empty"
                  : errors.email && <p>{errors.email.message}</p>}
              </p>
              <TextField
                {...register("email", {
                  required: true,
                  pattern: {
                    value: /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/,
                    message: "*Invalid email format",
                  },
                  validate: async (value) => {
                    const res = await fetch(`/api/user/${value}`);
                    const json = await res.json();
                    if (json.data[0]) {
                      return "*Email already exists";
                    }
                    return true;
                  },
                  onBlur(event) {
                    handleBlurEmail(event);
                  },
                })}
                aria-invalid={errors.email ? "true" : "false"}
                className="textfield"
                label="Email"
                variant="outlined"
                placeholder="Work email address"
                type="email"
              />
            </div>
            <div className="container-error">
              <p className="error">
                {errors.password?.type === "required"
                  ? "*Password cannot be empty"
                  : errors.password && <p>{errors.password.message}</p>}
              </p>
              <CPassword
                className="password"
                registerName={{
                  ...register("password", {
                    required: true,
                    pattern: {
                      value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                      message:
                        "*Password must be at least 8 characters and must contain both letters and numbers",
                    },
                  }),
                }}
                aria-invalid={errors.password ? "true" : "false"}
                onBlur={handleBlurPassword}
                label={"Password"}
                placeholder={"Password (8 or more characters)"}
              />
            </div>
            <div className="container-error">
              <p className="error">
                {errors.confirmPassword?.type === "required"
                  ? "*Confirm password cannot be empty"
                  : errors.confirmPassword && (
                      <p>{errors.confirmPassword.message}</p>
                    )}
              </p>
              <CPassword
                className="password"
                label={"Confirm Password"}
                placeholder={"Enter your Confirm password"}
                registerName={{
                  ...register("confirmPassword", {
                    required: true,
                    validate: (value) => {
                      return value === password
                        ? true
                        : "*Confirm password is incorrect";
                    },
                  }),
                }}
                aria-invalid={errors.confirmPassword ? "true" : "false"}
                onBlur={() => {
                  handleBlurConfirmPassword(password, confirmPassword!);
                }}
              />
            </div>
            <div className="container-error">
              <p className="error">
                {errors.isAgree?.type === "required" &&
                  "*Please tick the box to agree to our terms and conditions to continue with the registration"}
              </p>
              <FormControlLabel
                control={
                  <Checkbox
                    defaultChecked={false}
                    color="warning"
                    {...register("isAgree", { required: true })}
                  />
                }
                label={
                  <span className="label-Checkbox">
                    Yes, I understand and agree to the Upwork Terms of Service ,
                    including the User Agreement and Privacy Policy .
                  </span>
                }
              />
            </div>
            <Button
              className="button"
              variant="contained"
              type="submit"
              color="success"
            >
              {loadingNext ? (
                <CircularProgress size={30} color="inherit" />
              ) : (
                "Apply to Join HireIT"
              )}
            </Button>
          </form>
          <p className="change-form-singup">
            {isClient ? " Looking for work?" : "Here to hire talent?"}
            <span onClick={onclickChangeForm}>
              {isClient ? "Apply as talent" : "Join as a Client"}
            </span>
          </p>
        </div>
        <div className="show-companies">
          <p style={{ fontSize: 22, fontWeight: 700, opacity: 0.8 }}>
            Work with top companies
          </p>
          <div className="container-logo">
            {imageLogo.logo.map((item, index) => (
              <div className="container-logo-hover" key={index}>
                <img className="image-logo" src={item.logo} alt="" />
                <img className="image-logoGif" src={item.logoGif} alt="" />
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormSignUp;
