import { Button, Radio } from "@mui/material";
import Link from "next/link";
import React from "react";
import RadioButtonCustom from "./RadioButtonCustom";
interface PropsFormJobSelection {
  setShowForm: React.Dispatch<React.SetStateAction<boolean>>;
  setValue: React.Dispatch<React.SetStateAction<string>>;
}

const FormJobSelection = ({ setShowForm, setValue }: PropsFormJobSelection) => {
  const [selectedValue, setSelectedValue] = React.useState("");
  const handleSelectionJob = () => {
    setValue(selectedValue);
    setShowForm(false);
  };
  return (
    <div className="form-job-selecttion__container">
      <h1>Join as a client or freelancer</h1>
      <div className="container-option">
        <RadioButtonCustom
          item={"client"}
          name="position"
          state={selectedValue}
          setState={setSelectedValue}
          label="I’m a client, hiring for a project"
        />
        <RadioButtonCustom
          item={"freelancer"}
          name="position"
          state={selectedValue}
          setState={setSelectedValue}
          label="I’m a freelancer, looking for work"
        />
      </div>
      <Button
        className="button"
        variant="contained"
        disabled={selectedValue === ""}
        onClick={handleSelectionJob}
      >
        {selectedValue === ""
          ? "Create Account"
          : selectedValue === "client"
          ? "Join as a Client"
          : "Apply as a Freelancer"}
      </Button>
      <p className="label-bottom">
        Already have an account? <Link href="/auth/login">Log In</Link>
      </p>
    </div>
  );
};

export default FormJobSelection;
