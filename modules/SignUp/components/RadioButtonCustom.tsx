import { Box, FormControlLabel, Radio } from "@mui/material";
import React from "react";

interface PropsRadio {
  item?: String;
  name?: string | undefined;
  state: String;
  setState: React.Dispatch<React.SetStateAction<string>>;
  label?: string;
}

const RadioButtonCustom = ({
  item,
  name,
  state,
  setState,
  label,
}: PropsRadio) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState(event.target.value);
  };

  const handleBoxClick = () => {};

  return (
    <Box onClick={handleBoxClick}>
      <FormControlLabel
        className="radio-button-custom__container"
        control={
          <div className="container">
            <Radio
              checked={state === item}
              value={item}
              name={name}
              onChange={handleChange}
              sx={{
                alignSelf: "flex-end",
                "& .MuiSvgIcon-root": {
                  fontSize: 32,
                },
                color: "rgba(0,0,0,0.4)",
                "&.Mui-checked": {
                  color: "rgba(33,111,182,0.8)",
                },
              }}
            />
            <p className="label">{label}</p>
          </div>
        }
        label=""
      />
    </Box>
  );
};

export default RadioButtonCustom;
