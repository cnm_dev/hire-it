/* eslint-disable @next/next/no-img-element */
import React, { useCallback, useState } from "react";
// assets

import AvatarDropzone from "@/common/components/controls/CDropzoneImg";
import CLoading from "@/common/components/controls/CLoading";
import { uploadImage } from "@/services/image";

import { toast } from "react-toastify";

import { TypeLang } from "@/modules/Home/components/HotMember";
import { login } from "@/services/user";
import { formInfoCustomer, formInfoEmployee } from "@/types/models";
import { Button, InputAdornment, TextField } from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import { BsCheck2 } from "react-icons/bs";
import useCustomer from "@/hooks/useCustomer";
import { Session } from "next-auth";
import image from "@/common/assets/image/all-image";
import { updateProfileCustomer } from "@/apis/Customer";
import { UpdateProfileCustomerForm } from "@/forms/customer";
import CSpinner from "@/common/components/controls/CSpinner";
import { Warning2 } from "iconsax-react";
import { mutate } from "swr";

interface ProfileProps {
  lang: TypeLang["lang"];
  type?: string;
  session: Session;
}

const ProfileCustomer = ({ session }: ProfileProps) => {
  const [imageUpload, setImageUpload] = useState<any>();
  const [loadingUpload, setLoadingUpload] = useState<boolean>(false);
  const [isLoadingUpdate, setIsLoadingUpdate] = useState<boolean>(false);

  const { customer, loading } = useCustomer({
    customerId: session.user.id,
  });

  const handleAvatarUpload = useCallback((file: any) => {
    setImageUpload(file);
  }, []);
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    watch,
  } = useForm<formInfoCustomer>({ reValidateMode: "onSubmit" });

  const password = watch("newPassword", "");
  const confirmPassword = watch("confirmNewPassword", "");
  const phoneNumber = watch("phoneNumber", "");
  const companyName = watch("companyName", "");
  const codeFax = watch("codeFax", "");
  const address = watch("address", "");

  const handleBlurPhoneNumber = async (
    event: React.FocusEvent<HTMLInputElement>
  ) => {
    const phone = event.target.value;
    const phoneRegex = /^\+?[0-9]{1,3}\s?[0-9]{9,}$/gm;
    if (phoneNumber != session?.user.phoneNumber) {
      if (phone) {
        if (!phoneRegex.test(phone)) {
          setError("phoneNumber", {
            type: "onBlur",
            message: "*Phone number must be at most 9 digits",
          });
        } else {
          const res = await fetch(`/api/user/phone/${phone}`);
          const json = await res.json();
          if (json.data[0]) {
            setError("phoneNumber", {
              type: "onBlur",
              message: "*Phone number already exists",
            });
          } else {
            setError("phoneNumber", {
              type: "onBlur",
              message: undefined,
            });
          }
        }
      } else {
        setError("phoneNumber", {
          type: "onBlur",
          message: "*This field is required.",
        });
      }
    }
  };

  const handleBlurPassword = (event: React.FocusEvent<HTMLInputElement>) => {
    const password = event.target.value;
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (password) {
      if (!passwordRegex.test(password)) {
        setError("newPassword", {
          type: "onBlur",
          message:
            "*Password must be at least 8 characters and must contain both letters and numbers",
        });
      } else {
        setError("newPassword", {
          type: "onBlur",
          message: undefined,
        });
      }
    } else {
      setError("newPassword", {
        type: "onBlur",
        message: undefined,
      });
    }
  };

  const handleBlurCurrentPassword = (
    event: React.FocusEvent<HTMLInputElement>
  ) => {
    const password = event.target.value;
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (password) {
      if (!passwordRegex.test(password)) {
        setError("currentPassword", {
          type: "onBlur",
          message:
            "*Password must be at least 8 characters and must contain both letters and numbers",
        });
      } else {
        setError("currentPassword", {
          type: "onBlur",
          message: undefined,
        });
      }
    }
  };

  const handleBlurConfirmPassword = (
    password: string,
    confirmPassword: string
  ) => {
    if (confirmPassword) {
      if (confirmPassword !== password) {
        setError("confirmNewPassword", {
          type: "onBlur",
          message: "*Confirm password is incorrect",
        });
      } else {
        setError("confirmNewPassword", {
          type: "onBlur",
          message: undefined,
        });
      }
    }
  };

  const handelFunctionUpdate = (
    phoneNumber?: string,
    codeFax?: string,
    comanyName?: string,
    address?: string,
    newPass?: string,
    avatar?: string
  ) => {
    if (customer && customer.length > 0) {
      let variableCustomer: UpdateProfileCustomerForm = {
        customerId: customer[0].id,
        idUser: session.user.id,
      };
      (variableCustomer.phoneNumber = phoneNumber),
        (variableCustomer.codeFax = codeFax),
        (variableCustomer.address = address);
      variableCustomer.comanyName = comanyName;
      newPass && (variableCustomer.newPass = newPass);
      avatar && (variableCustomer.avatar = avatar);
      updateProfileCustomer(variableCustomer)
        .then(() => {
          mutate(`/api/customers/${session.user.id}`);
          session.user.phoneNumber = phoneNumber;
          session.user.customer.codeFax = codeFax;
          session.user.avatar = avatar;
          session.user.address = address;

          session.user.customer.codeFax = codeFax;
          session.user.customer.companyName = comanyName;
          session.user.customer.users_permissions_user.address = address;
          session.user.customer.users_permissions_user.phoneNumber = phoneNumber;
          session.user.customer.users_permissions_user.avartar = avatar;
          toast.success("Update profile customer complete");
          setIsLoadingUpdate(false);
        })
        .catch((err) => {
          toast.error(err.message);
          setIsLoadingUpdate(false);
        });
    }
  };

  const handleUpdateProfile: SubmitHandler<formInfoCustomer> = (data) => {
    setIsLoadingUpdate(true);
    if (imageUpload) {
      setLoadingUpload(true);
      setImageUpload(imageUpload);
      uploadImage(imageUpload)
        .then((image) => {
          setLoadingUpload(false);
          if (data.newPassword) {
            data &&
              handelFunctionUpdate(
                data.phoneNumber,
                data.codeFax,
                data.companyName,
                data.address,
                data.newPassword,
                image.data.url
              );
          } else {
            handelFunctionUpdate(
              data.phoneNumber,
              data.codeFax,
              data.companyName,
              data.address,
              undefined,
              image.data.url
            );
          }
        })
        .catch((err) => {
          setImageUpload(false);
          toast.error(err.message);
        });
    } else {
      if (data.newPassword) {
        data &&
          handelFunctionUpdate(
            data.phoneNumber,
            data.codeFax,
            data.companyName,
            data.address,
            data.newPassword
          );
      } else {
        handelFunctionUpdate(
          data.phoneNumber,
          data.codeFax,
          data.companyName,
          data.address
        );
      }
    }
  };

  return !loading && customer.length !== 0 ? (
    <div className="profile-customer">
      <form
        className="form-edit-info"
        onSubmit={handleSubmit(handleUpdateProfile)}
      >
        <div className="profile-header">
          <div className="profile-header__left">
            <div className="header-left__avatar">
              <div className="avatar-img__upload">
                <AvatarDropzone
                  onDrop={handleAvatarUpload}
                  imgDefult={
                    session?.user.avatar
                      ? session?.user.avatar
                      : image.image_default
                  }
                />
                {loadingUpload && (
                  <div className="loading-upload__avatar">
                    <CLoading />
                  </div>
                )}
              </div>
              <span>{session?.user.fullName}</span>
            </div>

            <div className="info-user">
              <div className="form-edit-info">
                <h3>Personal Information</h3>
                {(!customer[0].companyName ||
                  !customer[0].codeFax ||
                  !customer[0].users_permissions_user.address) && (
                  <span className="warning-infor">
                    <Warning2 size="32" color="#FF8A65" />
                    You have not updated your company information.
                  </span>
                )}

                <div className="inline">
                  <div className="lable-input">
                    <p>Phone number</p>
                    <div className="container-error">
                      <TextField
                        type="number"
                        className="phone-number"
                        placeholder="Phone number"
                        defaultValue={session?.user.phoneNumber}
                        error={errors.phoneNumber?.message !== undefined}
                        {...register("phoneNumber", {
                          required: true,
                          pattern: {
                            value: /^\+?[0-9]{1,3}\s?[0-9]{9,}$/gm,
                            message: "*Phone number must be at most 9 digits",
                          },
                          validate: async (value) => {
                            if (value && value != session?.user.phoneNumber) {
                              const res = await fetch(
                                `/api/user/phone/${value}`
                              );
                              const json = await res.json();
                              if (json.data[0]) {
                                return "*Phone number already exists";
                              }
                              return true;
                            }
                            return true;
                          },
                          onBlur(event) {
                            handleBlurPhoneNumber(event);
                          },
                        })}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              {!!errors.phoneNumber &&
                              errors.phoneNumber?.message === undefined &&
                              phoneNumber !== session?.user.phoneNumber ? (
                                <BsCheck2 fontSize={18} color="#30d69a" />
                              ) : (
                                ""
                              )}
                            </InputAdornment>
                          ),
                        }}
                      />
                      <p className="error">
                        {errors.phoneNumber?.type === "required"
                          ? "*This field is required."
                          : errors.phoneNumber && (
                              <span>{errors.phoneNumber.message}</span>
                            )}
                      </p>
                    </div>
                  </div>
                  <div className="lable-input">
                    <p>Code Fax</p>
                    <div className="container-error">
                      <TextField
                        defaultValue={customer[0].codeFax}
                        placeholder="Code Fax"
                        className="codeFax"
                        error={errors.codeFax?.message !== undefined}
                        {...register("codeFax", {
                          required: true,
                          onBlur(event) {
                            if (event.target.value != customer[0].codeFax) {
                              event.target.value
                                ? setError("codeFax", {
                                    type: "onBlur",
                                    message: undefined,
                                  })
                                : setError("codeFax", {
                                    type: "onBlur",
                                    message: "*City cannot be empty",
                                  });
                            }
                          },
                        })}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              {!!errors.codeFax &&
                              errors.codeFax?.message === undefined &&
                              codeFax != customer[0].codeFax ? (
                                <BsCheck2 fontSize={18} color="#30d69a" />
                              ) : (
                                ""
                              )}
                            </InputAdornment>
                          ),
                        }}
                      />
                      <p className="error">
                        {errors.codeFax?.type === "required"
                          ? "*City cannot be empty"
                          : errors.codeFax && (
                              <span>{errors.codeFax.message}</span>
                            )}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="lable-input">
                  <p>Company Name</p>
                  <div className="container-error">
                    <TextField
                      className="companyName"
                      defaultValue={customer[0].companyName}
                      placeholder="Company name"
                      error={errors.companyName?.message !== undefined}
                      {...register("companyName", {
                        required: true,
                        onBlur(event) {
                          if (customer[0].companyName != event.target.value) {
                            event.target.value
                              ? setError("companyName", {
                                  type: "onBlur",
                                  message: undefined,
                                })
                              : setError("companyName", {
                                  type: "onBlur",
                                  message: "*This field is required",
                                });
                          } else {
                          }
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.companyName &&
                            errors.companyName?.message === undefined &&
                            companyName != customer[0].companyName ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.companyName?.type === "required"
                        ? "*This field is required"
                        : errors.companyName && (
                            <span>{errors.companyName.message}</span>
                          )}
                    </p>
                  </div>
                </div>
                <div className="lable-input">
                  <p>Address company</p>
                  <div className="container-error">
                    <TextField
                      className="companyName"
                      defaultValue={customer[0].users_permissions_user.address}
                      placeholder="Address company"
                      error={errors.address?.message !== undefined}
                      {...register("address", {
                        required: true,
                        onBlur(event) {
                          if (
                            customer[0].users_permissions_user.address !=
                            event.target.value
                          ) {
                            event.target.value
                              ? setError("address", {
                                  type: "onBlur",
                                  message: undefined,
                                })
                              : setError("address", {
                                  type: "onBlur",
                                  message: "*This field is required",
                                });
                          } else {
                          }
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.address &&
                            errors.address?.message === undefined &&
                            address !=
                              customer[0].users_permissions_user.address ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.companyName?.type === "required"
                        ? "*This field is required"
                        : errors.companyName && (
                            <span>{errors.companyName.message}</span>
                          )}
                    </p>
                  </div>
                </div>
              </div>

              <div className="form-edit-info">
                <h3>Password</h3>
                <div className="lable-input">
                  <p>Current Password</p>
                  <div className="container-error">
                    <TextField
                      type="password"
                      placeholder="Password (8 or more characters)"
                      error={errors.currentPassword?.message !== undefined}
                      {...register("currentPassword", {
                        pattern: {
                          value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                          message:
                            "*Password must be at least 8 characters and must contain both letters and numbers",
                        },
                        validate: async (value) => {
                          if (value) {
                            try {
                              const res = await login({
                                email: session?.user.email,
                                password: value,
                              });
                              if (res) {
                                return true;
                              }
                              return "*Current password is not correct";
                            } catch (error) {
                              return "*Current password is not correct";
                            }
                          }
                          return true;
                        },
                        onBlur: (even) => {
                          handleBlurCurrentPassword(even);
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.currentPassword &&
                            errors.currentPassword?.message === undefined ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.currentPassword?.type === "required"
                        ? "*This field is required."
                        : errors.currentPassword && (
                            <span>{errors.currentPassword.message}</span>
                          )}
                    </p>
                  </div>
                </div>
                <div className="lable-input">
                  <p>New Password</p>
                  <div className="container-error">
                    <TextField
                      type="password"
                      placeholder="Password (8 or more characters)"
                      error={errors.newPassword?.message !== undefined}
                      {...register("newPassword", {
                        pattern: {
                          value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                          message:
                            "*Password must be at least 8 characters and must contain both letters and numbers",
                        },
                        onBlur: (even) => {
                          handleBlurPassword(even);
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.newPassword &&
                            errors.newPassword?.message === undefined &&
                            password !== "" ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.newPassword?.type === "required"
                        ? "*This field is required."
                        : errors.newPassword && (
                            <span>{errors.newPassword.message}</span>
                          )}
                    </p>
                  </div>
                </div>
                <div className="lable-input">
                  <p>Confirm Password</p>
                  <div className="container-error">
                    <TextField
                      type="password"
                      placeholder="Enter your Confirm password"
                      error={errors.confirmNewPassword?.message !== undefined}
                      {...register("confirmNewPassword", {
                        required: password ? true : false,
                        validate: (value) => {
                          if (value) {
                            return value === password
                              ? true
                              : "*Confirm password is incorrect";
                          }
                          return true;
                        },
                        onBlur: (even) => {
                          handleBlurConfirmPassword(
                            password ? password : "",
                            confirmPassword!
                          );
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.confirmNewPassword &&
                            errors.confirmNewPassword?.message === undefined ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.confirmNewPassword?.type === "required"
                        ? "*This field is required."
                        : errors.confirmNewPassword && (
                            <span>{errors.confirmNewPassword.message}</span>
                          )}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Button
            variant="contained"
            type="submit"
            disabled={
              (phoneNumber === session?.user.phoneNumber &&
                companyName === customer[0].companyName &&
                codeFax === customer[0].codeFax &&
                password === "" &&
                !imageUpload) ||
              isLoadingUpdate
            }
          >
            {isLoadingUpdate ? <CSpinner size="small" /> : "Save Changes"}
          </Button>
        </div>
      </form>
    </div>
  ) : (
    <CLoading fullScreen />
  );
};

export default ProfileCustomer;
