import { createEducation, updateEducation } from "@/apis/Education";
import CButton from "@/common/components/controls/CButton";
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import { STRAPI_API_TOKEN } from "@/constants";

import { Education } from "@/types/models";
import { Button, TextField } from "@mui/material";
import { Add, ArrowDown2, ArrowUp2, Minus } from "iconsax-react";
import { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { mutate } from "swr";

type FormAboutEducation = {
  arr: Education[];
  arrRemove?: Education[];
  handelAddListFieldAbout: () => void;
  handelRemoveListFieldAbout: (id: string) => void;
  employeeId?: string;
  profileId?: string;
  // control: Control<FieldValues, any>;
};

const FormAboutEducation = ({
  arr,
  handelAddListFieldAbout,
  handelRemoveListFieldAbout,
  employeeId,
  arrRemove,
  profileId,
}: FormAboutEducation) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    watch,
    control,
  } = useForm({ reValidateMode: "onSubmit" });

  const [activeHistory, setActiveHistory] = useState<boolean>(true);
  const handelActiveInformation = (type: string) => {
    if (type === "history") {
      setActiveHistory(!activeHistory);
    }
  };
  const handelUpdateEducation = (value: any) => {
    try {
      arr.map((data) => {
        if (data.id && employeeId) {
          if (data.id.split("\n")[1] === "create") {
            createEducation({
              educationalInstitution:
                value[`educationalInstitution_${data.id}`],
              educationaPlace: value[`educationaPlace_${data.id}`],
              employeeId: employeeId,
              educationMajors: value[`educationMajors_${data.id}`],
              educationTime: value[`educationTime_${data.id}`],
            });
          } else {
            updateEducation({
              id: data.id.split("\n")[1],
              educationalInstitution:
                value[`educationalInstitution_${data.id}`],
              educationaPlace: value[`educationaPlace_${data.id}`],
              educationMajors: value[`educationMajors_${data.id}`],
              educationTime: value[`educationTime_${data.id}`],
            });
          }
        }
      });
      arrRemove?.map((data) => {
        if (data.id) {
          if (data.id.split("\n")[1] !== "create") {
            updateEducation({
              id: data.id.split("\n")[1],
            });
          }
        }
      });
    } catch (error) {
      toast.error("Update faild");
    } finally {
      mutate(`/api/employees/${profileId}`);
      toast.success("update complete");
    }
  };
  return (
    <div className="form-about__field">
      <form
        className="form-about__field"
        onSubmit={handleSubmit(handelUpdateEducation)}
      >
        <div className="form-about__title">
          <h4>Educations</h4>
          <span>University or Training Institution</span>
        </div>
        <div className="form-about__text">
          {arr &&
            arr.map((v, index) => (
              <div className="fieldItem" key={index}>
                <div
                  className="show"
                  style={{ width: !activeHistory ? "105.7%" : "100%" }}
                >
                  <h4>
                    {v.educationMajors
                      ? v.educationMajors
                      : "Item " + (index + 1)}
                  </h4>
                  <CButtonIcon
                    onclick={() => handelActiveInformation("history")}
                    icon={
                      activeHistory ? (
                        <ArrowUp2 size={20} />
                      ) : (
                        <ArrowDown2 size={20} />
                      )
                    }
                  />
                </div>
                {activeHistory && (
                  <>
                    <TextField
                      {...register(`educationMajors_${v.id}`, {
                        required: true,
                      })}
                      placeholder="Enter your Education majors"
                      defaultValue={v.educationMajors}
                    />
                    <TextField
                      {...register(`educationalInstitution_${v.id}`, {
                        required: true,
                      })}
                      placeholder="Enter your Name of educational institution or Name of training institute"
                      defaultValue={v.educationalInstitution}
                    />
                    <TextField
                      {...register(`educationaPlace_${v.id}`, {
                        required: true,
                      })}
                      placeholder="Enter your Approach to education (e.g. Online or Atlanta, GA) "
                      defaultValue={v.educationaPlace}
                    />
                    <TextField
                      {...register(`educationTime_${v.id}`, { required: true })}
                      placeholder="Enter your Education time (e.g. 2022-2023)"
                      defaultValue={v.educationTime}
                    />

                    <CButton
                      className="remove"
                      text={
                        <>
                          <Minus size="18" color="#FF8A65" />
                          <a>Remove field</a>
                        </>
                      }
                      onClick={() => handelRemoveListFieldAbout(v.id + "")}
                    ></CButton>
                  </>
                )}
              </div>
            ))}
          {activeHistory && (
            <CButton
              className="add"
              text={
                <>
                  <Add size="20" color="rgba(56, 182, 140, 1)" />
                  <a>Add new field history hire employment</a>
                </>
              }
              onClick={handelAddListFieldAbout}
            ></CButton>
          )}
        </div>
        {activeHistory && (
          <Button variant="contained" type="submit">
            Save Changes
          </Button>
        )}
      </form>
    </div>
  );
};

export default FormAboutEducation;
