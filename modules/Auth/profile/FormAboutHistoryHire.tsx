import CButton from "@/common/components/controls/CButton";
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import { STRAPI_API_TOKEN } from "@/constants";
import {
  createHistoryEmployment,
  deleteHistoryEmployment,
  updateHistoryEmployment,
} from "@/services/historyHireEmloyment";
import { HistoryHireEmployment } from "@/types/models";
import { Box, Button, TextField, TextareaAutosize } from "@mui/material";
import { Add, ArrowDown2, ArrowUp2, CloseCircle, Minus } from "iconsax-react";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { mutate } from "swr";

type FormAboutHistoryHire = {
  arr: HistoryHireEmployment[];
  arrRemove?: HistoryHireEmployment[];
  handelAddListFieldAbout: () => void;
  handelRemoveListFieldAbout: (id: string) => void;
  employeeId?: string;
  profileId?: string;
  // control: Control<FieldValues, any>;
};

const FormAboutHistoryHire = ({
  arr,
  handelAddListFieldAbout,
  handelRemoveListFieldAbout,
  employeeId,
  arrRemove,
  profileId,
}: FormAboutHistoryHire) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    watch,
    control,
  } = useForm({ reValidateMode: "onSubmit" });

  const [activeHistory, setActiveHistory] = useState<boolean>(true);
  const handelActiveInformation = (type: string) => {
    if (type === "history") {
      setActiveHistory(!activeHistory);
    }
  };
  const handelUpdateHis = (value: any) => {
    try {
      arr.map((data) => {
        if (data.id && employeeId) {
          if (data.id.split("\n")[1] === "create") {
            createHistoryEmployment(STRAPI_API_TOKEN, {
              companyName: value[`companyName_${data.id}`],
              descriptionJob: value[`descriptionJob_${data.id}`],
              employeeId: employeeId,
              technologie: value[`technologie_${data.id}`],
              workingPosition: value[`workingPosition_${data.id}`],
            });
          } else {
            updateHistoryEmployment(STRAPI_API_TOKEN, {
              id: data.id.split("\n")[1],
              companyName: value[`companyName_${data.id}`],
              descriptionJob: value[`descriptionJob_${data.id}`],
              technologie: value[`technologie_${data.id}`],
              workingPosition: value[`workingPosition_${data.id}`],
            });
          }
        }
      });
      arrRemove?.map((data) => {
        if (data.id) {
          if (data.id.split("\n")[1] !== "create") {
            deleteHistoryEmployment(STRAPI_API_TOKEN, {
              id: data.id.split("\n")[1],
            });
          }
        }
      });
    } catch (error) {
      toast.error("Update faild");
    } finally {
      mutate(`/api/employees/${profileId}`);
      toast.success("update complete");
    }
  };
  return (
    <div className="form-about__field">
      <form
        className="form-about__field"
        onSubmit={handleSubmit(handelUpdateHis)}
      >
        <div className="form-about__title">
          <h4>History hire employments</h4>
          <span>Companies that you have joined</span>
        </div>
        <div className="form-about__text">
          {arr &&
            arr.map((v, index) => (
              <div className="fieldItem" key={index}>
                <div
                  className="show"
                  style={{ width: !activeHistory ? "105.7%" : "100%" }}
                >
                  <h4>{v.companyName}</h4>
                  <CButtonIcon
                    onclick={() => handelActiveInformation("history")}
                    icon={
                      activeHistory ? (
                        <ArrowUp2 size={20} />
                      ) : (
                        <ArrowDown2 size={20} />
                      )
                    }
                  />
                </div>
                {activeHistory && (
                  <>
                    <TextField
                      {...register(`companyName_${v.id}`, { required: true })}
                      placeholder="Enter your company name "
                      defaultValue={v.companyName}
                    />
                    <TextField
                      {...register(`workingPosition_${v.id}`, {
                        required: true,
                      })}
                      placeholder="Enter your working position"
                      defaultValue={v.workingPosition}
                    />
                    <TextField
                      {...register(`technologie_${v.id}`, { required: true })}
                      placeholder="Enter your technologies "
                      defaultValue={v.technologie}
                    />

                    <TextareaAutosize
                      aria-label="minimum height"
                      minRows={4}
                      placeholder="Enter your description Job..."
                      style={{ width: "100%" }}
                      {...register(`descriptionJob_${v.id}`, {
                        required: true,
                      })}
                      defaultValue={v.descriptionJob}
                    />
                    <CButton
                      className="remove"
                      text={
                        <>
                          <Minus size="18" color="#FF8A65" />
                          <a>Remove field</a>
                        </>
                      }
                      onClick={() => handelRemoveListFieldAbout(v.id + "")}
                    ></CButton>
                  </>
                )}
              </div>
            ))}
          {activeHistory && (
            <CButton
              className="add"
              text={
                <>
                  <Add size="20" color="rgba(56, 182, 140, 1)" />
                  <a>Add new field history hire employment</a>
                </>
              }
              onClick={handelAddListFieldAbout}
            ></CButton>
          )}
        </div>
        {activeHistory && (
          <Button variant="contained" type="submit">
            Save Changes
          </Button>
        )}
      </form>
    </div>
  );
};

export default FormAboutHistoryHire;
