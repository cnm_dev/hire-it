/* eslint-disable @next/next/no-img-element */
import React, { useCallback, useState } from "react";
// assets

import AvatarDropzone from "@/common/components/controls/CDropzoneImg";
import CLoading from "@/common/components/controls/CLoading";
import { uploadImage } from "@/services/image";
import { updateUserAvatar } from "@/services/user";
import { useSession } from "next-auth/react";
import { toast } from "react-toastify";

import useEmployee from "@/hooks/useEmployee";
import { TypeLang } from "@/modules/Home/components/HotMember";
import { updateCountryAndCityEmployee } from "@/services/employee";
import { login } from "@/services/user";
import { formInfoEmployee } from "@/types/models";
import { Button, InputAdornment, TextField } from "@mui/material";
import { SubmitHandler, useForm } from "react-hook-form";
import { BsCheck2 } from "react-icons/bs";
import FormAboutHistoryHire from "./FormAboutHistoryHire";
import { randomString } from "@/utils";
import FormAboutEducation from "./FormAboutEducation";

interface ProfileProps {
  lang: TypeLang["lang"];
  type?: string;
}

const Profile = ({ lang, type }: ProfileProps) => {
  const [imageUpload, setImageUpload] = useState<any>();
  const [loadingUpload, setLoadingUpload] = useState<boolean>(false);
  const [historyHire, setHistoryHire] = useState<any[]>([]);
  const [education, setEducation] = useState<any[]>([]);
  const [historyHireRemove, setHistoryHireRemove] = useState<any[]>([]);
  const [educationRemove, setEducationRemove] = useState<any[]>([]);

  const { data: session } = useSession();

  const { employees, loading } = useEmployee({
    profileId: session?.user.employee.id,
  });

  React.useMemo(() => {
    if (!loading) {
      let t: any[] = [];
      employees[0].history_hire_employments?.map((data, index) => {
        t.push({
          id: randomString(5) + "\n" + data.id,
          companyName: data.companyName,
          workingPosition: data.workingPosition,
          technologie: data.technologie,
          descriptionJob: data.descriptionJob,
        });
      });
      setHistoryHire(t);

      let t1: any[] = [];
      employees[0].educations?.map((data, index) => {
        t1.push({
          id: randomString(5) + "\n" + data.id,
          educationMajors: data.educationMajors,
          educationTime: data.educationTime,
          educationaPlace: data.educationaPlace,
          educationalInstitution: data.educationalInstitution,
        });
      });
      setEducation(t1);
    }
  }, [employees, loading]);

  const handleAvatarUpload = useCallback((file: any) => {
    setImageUpload(file);
  }, []);
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    watch,
    control,
  } = useForm<formInfoEmployee>({ reValidateMode: "onSubmit" });

  const password = watch("newPassword", "");
  const confirmPassword = watch("confirmNewPassword", "");
  const phoneNumber = watch("phoneNumber", "");
  const country = watch("country", "");
  const city = watch("city", "");

  const handleBlurPhoneNumber = async (
    event: React.FocusEvent<HTMLInputElement>
  ) => {
    const phone = event.target.value;
    const phoneRegex = /^\+?[0-9]{1,3}\s?[0-9]{9,}$/gm;
    if (phoneNumber != session?.user.phoneNumber) {
      if (phone) {
        if (!phoneRegex.test(phone)) {
          setError("phoneNumber", {
            type: "onBlur",
            message: "*Phone number must be at most 9 digits",
          });
        } else {
          const res = await fetch(`/api/user/phone/${phone}`);
          const json = await res.json();
          if (json.data[0]) {
            setError("phoneNumber", {
              type: "onBlur",
              message: "*Phone number already exists",
            });
          } else {
            setError("phoneNumber", {
              type: "onBlur",
              message: undefined,
            });
          }
        }
      } else {
        setError("phoneNumber", {
          type: "onBlur",
          message: "*This field is required.",
        });
      }
    }
  };

  const handleBlurPassword = (event: React.FocusEvent<HTMLInputElement>) => {
    const password = event.target.value;
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (password) {
      if (!passwordRegex.test(password)) {
        setError("newPassword", {
          type: "onBlur",
          message:
            "*Password must be at least 8 characters and must contain both letters and numbers",
        });
      } else {
        setError("newPassword", {
          type: "onBlur",
          message: undefined,
        });
      }
    } else {
      setError("newPassword", {
        type: "onBlur",
        message: undefined,
      });
    }
  };

  const handleBlurCurrentPassword = (
    event: React.FocusEvent<HTMLInputElement>
  ) => {
    const password = event.target.value;
    const passwordRegex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    if (password) {
      if (!passwordRegex.test(password)) {
        setError("currentPassword", {
          type: "onBlur",
          message:
            "*Password must be at least 8 characters and must contain both letters and numbers",
        });
      } else {
        setError("currentPassword", {
          type: "onBlur",
          message: undefined,
        });
      }
    }
  };

  const handleBlurConfirmPassword = (
    password: string,
    confirmPassword: string
  ) => {
    if (confirmPassword) {
      if (confirmPassword !== password) {
        setError("confirmNewPassword", {
          type: "onBlur",
          message: "*Confirm password is incorrect",
        });
      } else {
        setError("confirmNewPassword", {
          type: "onBlur",
          message: undefined,
        });
      }
    }
  };

  const [listHistory, setListHistory] = useState<string[]>();

  const handelAddHis = () => {
    setHistoryHire([
      ...historyHire,
      {
        id: randomString(5) + "\n" + "create",
        companyName: "",
        workingPosition: "",
        technologie: "",
        descriptionJob: "",
      },
    ]);
  };
  const handelRemoveHis = (id: string) => {
    setHistoryHire(historyHire.filter((value) => value.id !== id));
    setHistoryHireRemove([
      ...historyHireRemove,
      historyHire.filter((value) => value.id === id)[0],
    ]);
  };

  const handelAddEducation = () => {
    setEducation([
      ...education,
      {
        id: randomString(5) + "\n" + "create",
        educationMajors: "",
        educationTime: "",
        educationaPlace: "",
        educationalInstitution: "",
      },
    ]);
  };
  const handelRemoveEducation = (id: string) => {
    setEducation(education.filter((value) => value.id !== id));
    setEducationRemove([
      ...educationRemove,
      education.filter((value) => value.id === id)[0],
    ]);
  };

  const handleUpdateProfile: SubmitHandler<formInfoEmployee> = (data) => {
    if (password) {
      if (imageUpload) {
        setLoadingUpload(true);
        setImageUpload(imageUpload);
        uploadImage(imageUpload)
          .then((course) => {
            updateCountryAndCityEmployee(session?.user.access_token, {
              id: session?.user.id,
              employeeId: employees[0].id,
              avartar: course.data.url,
              phoneNumber: data.phoneNumber,
              password: data.newPassword,
              city: data.city,
              country: data.country,
            })
              .then((course) => {
                toast.success("Update complete");
                setLoadingUpload(false);
              })
              .catch((err) => {
                setLoadingUpload(false);
                toast.error(err);
              });
            setImageUpload("");
          })
          .catch((err) => {
            setLoadingUpload(false);
            toast.error(err);
          });
      } else {
        updateCountryAndCityEmployee(session?.user.access_token, {
          id: session?.user.id,
          employeeId: employees[0].id,
          phoneNumber: data.phoneNumber,
          password: data.newPassword,
          city: data.city,
          country: data.country,
        })
          .then((course) => {
            toast.success("Update complete");
            setLoadingUpload(false);
          })
          .catch((err) => {
            setLoadingUpload(false);
            toast.error(err);
          });
      }
    } else {
      if (imageUpload) {
        setLoadingUpload(true);
        setImageUpload(imageUpload);
        uploadImage(imageUpload)
          .then((course) => {
            updateCountryAndCityEmployee(session?.user.access_token, {
              id: session?.user.id,
              employeeId: employees[0].id,
              phoneNumber: data.phoneNumber,
              avartar: course.data.url,
              city: data.city,
              country: data.country,
            })
              .then((course) => {
                toast.success("Update complete");
                setLoadingUpload(false);
              })
              .catch((err) => {
                setLoadingUpload(false);
                toast.error(err);
              });
            setImageUpload("");
          })
          .catch((err) => {
            setLoadingUpload(false);
            toast.error(err);
          });
      } else {
        updateCountryAndCityEmployee(session?.user.access_token, {
          id: session?.user.id,
          employeeId: employees[0].id,
          phoneNumber: data.phoneNumber,
          city: data.city,
          country: data.country,
        })
          .then((course) => {
            toast.success("Upload complete");
            setLoadingUpload(false);
          })
          .catch((err) => {
            setLoadingUpload(false);
            toast.error(err);
          });
      }
    }
  };
  return loading ? (
    <CLoading fullScreen />
  ) : (
    <div className="profile-uer">
      <form
        className="form-edit-info"
        onSubmit={handleSubmit(handleUpdateProfile)}
      >
        <div className="profile-header">
          <div className="profile-header__left">
            <div className="header-left__avatar">
              <div className="avatar-img__upload">
                <AvatarDropzone
                  onDrop={handleAvatarUpload}
                  imgDefult={session?.user.avatar}
                />
                {loadingUpload && (
                  <div className="loading-upload__avatar">
                    <CLoading />
                  </div>
                )}
              </div>
              <span>{session?.user.fullName}</span>
            </div>

            <div className="info-user">
              <div className="form-edit-info">
                <h3>Personal Information</h3>
                <div className="lable-input">
                  <p>Phone number</p>
                  <div className="container-error">
                    <TextField
                      type="number"
                      placeholder="Phone number"
                      defaultValue={session?.user.phoneNumber}
                      error={errors.phoneNumber?.message !== undefined}
                      {...register("phoneNumber", {
                        required: true,
                        pattern: {
                          value: /^\+?[0-9]{1,3}\s?[0-9]{9,}$/gm,
                          message: "*Phone number must be at most 9 digits",
                        },
                        validate: async (value) => {
                          if (value && value != session?.user.phoneNumber) {
                            const res = await fetch(`/api/user/phone/${value}`);
                            const json = await res.json();
                            if (json.data[0]) {
                              return "*Phone number already exists";
                            }
                            return true;
                          }
                          return true;
                        },
                        onBlur(event) {
                          handleBlurPhoneNumber(event);
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.phoneNumber &&
                            errors.phoneNumber?.message === undefined &&
                            phoneNumber !== session?.user.phoneNumber ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.phoneNumber?.type === "required"
                        ? "*This field is required."
                        : errors.phoneNumber && (
                            <span>{errors.phoneNumber.message}</span>
                          )}
                    </p>
                  </div>
                </div>
                <div className="lable-input">
                  <p>Country</p>
                  <div className="container-error">
                    <TextField
                      defaultValue={employees[0].country}
                      placeholder="Country"
                      error={errors.country?.message !== undefined}
                      {...register("country", {
                        required: true,
                        onBlur(event) {
                          if (employees[0].country != event.target.value) {
                            event.target.value
                              ? setError("country", {
                                  type: "onBlur",
                                  message: undefined,
                                })
                              : setError("country", {
                                  type: "onBlur",
                                  message: "*This field is required",
                                });
                          } else {
                          }
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.country &&
                            errors.country?.message === undefined &&
                            country != employees[0].country ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.country?.type === "required"
                        ? "*This field is required"
                        : errors.country && (
                            <span>{errors.country.message}</span>
                          )}
                    </p>
                  </div>
                </div>
                <div className="lable-input">
                  <p>City</p>
                  <div className="container-error">
                    <TextField
                      defaultValue={employees[0].city}
                      placeholder="City"
                      className="city"
                      error={errors.city?.message !== undefined}
                      {...register("city", {
                        required: true,
                        onBlur(event) {
                          if (event.target.value != employees[0].city) {
                            event.target.value
                              ? setError("city", {
                                  type: "onBlur",
                                  message: undefined,
                                })
                              : setError("city", {
                                  type: "onBlur",
                                  message: "*City cannot be empty",
                                });
                          }
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.city &&
                            errors.city?.message === undefined &&
                            city != employees[0].city ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.city?.type === "required"
                        ? "*City cannot be empty"
                        : errors.city && <span>{errors.city.message}</span>}
                    </p>
                  </div>
                </div>
              </div>
              <div className="form-edit-info">
                <h3>Password</h3>
                <div className="lable-input">
                  <p>Current Password</p>
                  <div className="container-error">
                    <TextField
                      type="password"
                      placeholder="Password (8 or more characters)"
                      error={errors.currentPassword?.message !== undefined}
                      {...register("currentPassword", {
                        pattern: {
                          value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                          message:
                            "*Password must be at least 8 characters and must contain both letters and numbers",
                        },
                        validate: async (value) => {
                          if (value) {
                            try {
                              const res = await login({
                                email: session?.user.email,
                                password: value,
                              });
                              if (res) {
                                return true;
                              }
                              return "*Current password is not correct";
                            } catch (error) {
                              return "*Current password is not correct";
                            }
                          }
                          return true;
                        },
                        onBlur: (even) => {
                          handleBlurCurrentPassword(even);
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.currentPassword &&
                            errors.currentPassword?.message === undefined ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.currentPassword?.type === "required"
                        ? "*This field is required."
                        : errors.currentPassword && (
                            <span>{errors.currentPassword.message}</span>
                          )}
                    </p>
                  </div>
                </div>
                <div className="lable-input">
                  <p>New Password</p>
                  <div className="container-error">
                    <TextField
                      type="password"
                      placeholder="Password (8 or more characters)"
                      error={errors.newPassword?.message !== undefined}
                      {...register("newPassword", {
                        pattern: {
                          value: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
                          message:
                            "*Password must be at least 8 characters and must contain both letters and numbers",
                        },
                        onBlur: (even) => {
                          handleBlurPassword(even);
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.newPassword &&
                            errors.newPassword?.message === undefined &&
                            password !== "" ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.newPassword?.type === "required"
                        ? "*This field is required."
                        : errors.newPassword && (
                            <span>{errors.newPassword.message}</span>
                          )}
                    </p>
                  </div>
                </div>
                <div className="lable-input">
                  <p>Confirm Password</p>
                  <div className="container-error">
                    <TextField
                      type="password"
                      placeholder="Enter your Confirm password"
                      error={errors.confirmNewPassword?.message !== undefined}
                      {...register("confirmNewPassword", {
                        required: password ? true : false,
                        validate: (value) => {
                          if (value) {
                            return value === password
                              ? true
                              : "*Confirm password is incorrect";
                          }
                          return true;
                        },
                        onBlur: (even) => {
                          handleBlurConfirmPassword(
                            password ? password : "",
                            confirmPassword!
                          );
                        },
                      })}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            {!!errors.confirmNewPassword &&
                            errors.confirmNewPassword?.message === undefined ? (
                              <BsCheck2 fontSize={18} color="#30d69a" />
                            ) : (
                              ""
                            )}
                          </InputAdornment>
                        ),
                      }}
                    />
                    <p className="error">
                      {errors.confirmNewPassword?.type === "required"
                        ? "*This field is required."
                        : errors.confirmNewPassword && (
                            <span>{errors.confirmNewPassword.message}</span>
                          )}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Button
            variant="contained"
            type="submit"
            disabled={
              phoneNumber === session?.user.phoneNumber &&
              country === employees[0].country &&
              city === employees[0].city &&
              password === "" &&
              !imageUpload
            }
          >
            Save Changes
          </Button>
        </div>
      </form>
      <div className="form-about-employee">
        <FormAboutHistoryHire
          arr={historyHire}
          arrRemove={historyHireRemove}
          handelAddListFieldAbout={handelAddHis}
          handelRemoveListFieldAbout={handelRemoveHis}
          employeeId={employees[0].id + ""}
          profileId={session?.user.employee.id}
        />
      </div>
      <div className="form-about-employee">
        <FormAboutEducation
          arr={education}
          arrRemove={educationRemove}
          handelAddListFieldAbout={handelAddEducation}
          handelRemoveListFieldAbout={handelRemoveEducation}
          employeeId={employees[0].id + ""}
          profileId={session?.user.employee.id}
        />
      </div>
    </div>
  );
};

export default Profile;
