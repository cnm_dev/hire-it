/* eslint-disable @next/next/no-img-element */
import CButton from "@/common/components/controls/CButton";
import React, { useEffect, useMemo } from "react";
import CChart from "@/common/components/controls/CChart";
import { all_api } from "@/mock/all_api";
// date
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import useContracts from "@/hooks/useContracts";
import useEmployees from "@/hooks/useEmployees";
import useCustomers from "@/hooks/useCustomers";
import CLoading from "@/common/components/controls/CLoading";
import { totalMoneyContractYear } from "@/constants/contract";
import Link from "next/link";
import { all_video } from "@/common/assets/videos";

const HomeAdmin = () => {
  const { contracts, loading: loadingContracts } = useContracts({
    customerId: undefined,
  });
  const { employees, loading: loadingEmployees } = useEmployees();
  const { customers, loading: LoadingCustomers } = useCustomers();
  const [employeeSize, setEmployeeSize] = React.useState<number>(0);

  useEffect(() => {
    if (employees) {
      let temp = 0;
      employees.map((data) => {
        if (
          data.status !== "wait" &&
          data.status !== "cancel" &&
          data.status !== "invited"
        ) {
          temp += 1;
        }
      });

      setEmployeeSize(temp);
    }
  }, [employees]);

  const datasets = useMemo(() => {
    if (contracts.length !== 0) {
      const ObjectMoneyContract = totalMoneyContractYear(contracts);

      return [
        {
          label: "Total money contract Active",
          data: ObjectMoneyContract.arrMoneyActive,
          fill: false,
          borderColor: "rgb(46, 220, 84)",
          tension: 0.1,
        },
        {
          label: "Total money contract Wait",
          data: ObjectMoneyContract.arrMoneyWait,
          fill: false,
          borderColor: "rgb(220, 214, 46)",
          tension: 0.1,
        },
        {
          label: "Total money contract Cancel",
          data: ObjectMoneyContract.arrMoneyCancel,
          fill: false,
          borderColor: "rgb(220, 46, 46)",
          tension: 0.1,
        },
        {
          label: "Total money contract Complete",
          data: ObjectMoneyContract.arrMoneyComplete,
          fill: false,
          borderColor: "rgb(75, 192, 192)",
          tension: 0.1,
        },
      ];
    }
  }, [contracts]);
  return (
    <div className="home-admin">
      {!loadingContracts &&
      !loadingEmployees &&
      !LoadingCustomers &&
      datasets ? (
        <div className="home-admin__left">
          <div className="admin-leaft__dashboard">
            <div className="admin-dasboard__container">
              <div className="admin-dasboard__item">
                <video src={all_video.employee} loop autoPlay muted />
                <h3>Staff</h3>
                <span>
                  {employeeSize} <span>(user)</span>
                </span>
                <p>Total number of current employees in the company</p>
                <Link href="/manager/staff">
                  <CButton variant="outlined" text="Detail" />
                </Link>
              </div>
              <div className="admin-dasboard__item">
                <video src={all_video.customer} loop autoPlay muted />
                <h3>Customer</h3>
                <span>
                  {customers.length} <span>(user)</span>
                </span>
                <p>Total number of current Customer connect with the company</p>
                <Link href="/manager/customer">
                  <CButton variant="outlined" text="Detail" />
                </Link>
              </div>
              <div className="admin-dasboard__item">
                <video src={all_video.contract} loop autoPlay muted />
                <h3>Contract</h3>
                <span>
                  {contracts.length} <span>(contract)</span>
                </span>
                <p>Total number of contracts the company has signed</p>
                <Link href="/manager/contract">
                  <CButton variant="outlined" text="Detail" />
                </Link>
              </div>
            </div>
            <div className="admin-right__date">
              <Calendar />
            </div>
          </div>
          <div className="admin-leaft__chart">
            <CChart datasets={datasets} labels={all_api.chart_month} />
          </div>
        </div>
      ) : (
        <CLoading fullScreen />
      )}
    </div>
  );
};

export default HomeAdmin;
