import React from "react";
import { TypeLang } from "../Home/components/HotMember";
import { BackgroundLogin } from "./components/BackgroundLogin";
import { FormLogin } from "./components/FormLogin";

const Auth = ({ lang }: TypeLang) => {
  return (
    <>
      <div className="form-login">
        <FormLogin lang={lang} />
      </div>
      <div className="background-right">
        <BackgroundLogin />
      </div>
    </>
  );
};

export default Auth;
