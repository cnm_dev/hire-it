import CTextField from "@/common/components/controls/CTextField";
import { Button, Checkbox, FormControlLabel, Typography } from "@mui/material";
import { FcGoogle } from "react-icons/fc";
import React from "react";
import { TypeLang } from "@/modules/Home/components/HotMember";
import { langs } from "@/mock/langs";
import { SubmitHandler, useForm } from "react-hook-form";
import { Logins } from "@/types/models";
import { signIn } from "next-auth/react";
import { useRouter } from "next/router";
import CLoading from "@/common/components/controls/CLoading";
import { toast } from "react-toastify";
import Link from "next/link";

export const FormLogin = ({ lang }: TypeLang) => {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [loadingGoogle, setLoadingGoole] = React.useState<boolean>(false);
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Logins>();

  const onLogin: SubmitHandler<Logins> = async (data) => {
    setLoading(true);
    const result = await signIn("credentials", {
      redirect: false,
      ...data,
    });

    result && setLoading(false);

    if (result?.ok) {
      router.replace("/");
    } else {
      toast.error(result?.error);
    }
  };

  const handelLoginGoogle = async () => {
    const result = await signIn("google", {
      redirect: false,
    });

    !result ? setLoadingGoole(true) : setLoadingGoole(false);

    if (result?.ok) {
    } else {
      toast.error(result?.error);
    }
  };

  return (
    <>
      <div className="form-login__left">
        <div className="form-login">
          <h1>{langs.auth.welcome[lang]}</h1>
          <h3>{langs.auth.please[lang]}</h3>

          <form onSubmit={handleSubmit(onLogin)}>
            <CTextField
              registerName={{
                ...register("email", { required: true }),
              }}
              name="email"
              className="form-login_email"
              label="Email"
              variant="outlined"
              placeholder="Enter your email"
              type="email"
            />
            <p className="error">
              {errors.email?.type === "required" && "Email is not null"}
            </p>

            <CTextField
              registerName={{ ...register("password", { required: true }) }}
              name="password"
              className="form-login_password"
              label="Password"
              variant="outlined"
              type="password"
            />
            <p className="error">
              {errors.password?.type === "required" && "Password is not null"}
            </p>
            <div className="form-login-RememberPassword">
              <FormControlLabel
                control={<Checkbox defaultChecked={false} color="warning" />}
                label={
                  <Typography className="label-Checkbox">
                    {langs.auth.remember[lang]}
                  </Typography>
                }
              />
              <Link href="/auth/forgot-password">
                <p>{langs.auth.forgot[lang]}?</p>
              </Link>
            </div>
            <Button
              type="submit"
              className="form-login-button"
              variant="contained"
            >
              {loading ? <CLoading /> : langs.auth.sigin[lang]}
            </Button>
          </form>
          <Button
            onClick={handelLoginGoogle}
            className="form-login-button-google"
            variant="outlined"
            color="primary"
            startIcon={<FcGoogle size={32} />}
          >
            {loadingGoogle ? (
              <CLoading />
            ) : (
              `${langs.auth.sigin[lang]} ${langs.auth.withgoole[lang]}`
            )}
          </Button>
          <p className="form-login-bottom__text">
            Sign up for free.
            <Link href="/auth/signup">Don’t have an account?</Link>
          </p>
        </div>
      </div>
    </>
  );
};
