/* eslint-disable @next/next/no-img-element */
import React from "react";

export const BackgroundLogin = () => {
  return (
    <>
      <div className="background-drop__right">
        <img
          src="https://res.cloudinary.com/hire-it/image/upload/v1680115052/KLTN/background_bdyguk.png"
          alt="background"
          className="background-drop__image"
        />
      </div>
    </>
  );
};
