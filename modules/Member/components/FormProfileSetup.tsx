/* eslint-disable @next/next/no-img-element */
import CButtonUploadPDF from "@/common/components/controls/CButtonUploadPDF";
import AvatarDropzone from "@/common/components/controls/CDropzoneImg";
import CLoading from "@/common/components/controls/CLoading";
import { formProfessionalExperience, formProfileSetup } from "@/forms/employee";
import { updateResumeEmployee } from "@/services/employee";
import { uploadImage, uploadResume } from "@/services/image";
import { updateUserAvatar } from "@/services/user";
import { Button, CircularProgress } from "@mui/material";
import { Link21, Trash } from "iconsax-react";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React, { useCallback, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-toastify";
interface FormProfileSetup {
  handleComplete: () => void;
  id?: number;
  employeeId: number;
}
const FormProfileSetup = ({
  handleComplete,
  id,
  employeeId,
}: FormProfileSetup) => {
  const [loading, setLoading] = useState(false);
  const [imageUpload, setImageUpload] = useState<any>();
  const [resumeUpload, setResumeUpload] = useState<any>();
  const [loadingUpload, setLoadingUpload] = useState<boolean>(false);
  const [loadingUploadResume, setLoadingUploadResume] =
    useState<boolean>(false);
  const [isClick, setIsClick] = useState<boolean>(false);
  const { data: session } = useSession();
  const router = useRouter();
  async function handelUploadImage() {
    setLoadingUpload(true);
    setImageUpload(imageUpload);
    uploadImage(imageUpload)
      .then((course) => {
        updateUserAvatar(
          course.data.url,
          session?.user.id,
          session?.user.access_token
        )
          .then((course) => {
            if (session) {
              session.user.avatar = course.data.url;
            }
            setLoadingUpload(false);
          })
          .catch((err) => {
            setLoadingUpload(false);
            toast.error(err);
          });
      })
      .catch((err) => {
        setLoadingUpload(false);
        toast.error(err);
      });
  }

  async function handelUploadResume() {
    setLoadingUploadResume(true);
    setResumeUpload(resumeUpload);
    uploadResume(resumeUpload)
      .then((course) => {
        setLoadingUpload(false);
        updateResumeEmployee(
          { jwt: session?.user.access_token },
          {
            employeeId: employeeId,
            resume: course.data.url,
            completionLevel: 4,
          }
        )
          .then((course) => {
            setLoadingUploadResume(false);
          })
          .catch((err) => {
            setLoadingUploadResume(false);
            toast.error(err);
          });
      })
      .catch((err) => {
        setLoadingUploadResume(false);
        toast.error(err);
      });
  }

  const handleSubmit = () => {
    setIsClick(true);
    if (imageUpload && resumeUpload) {
      handelUploadImage();
      handelUploadResume();
      if (session) {
        session.user.employee.completionLevel = 4;
        router.replace("/");
      }
    }
  };

  const handleAvatarUpload = useCallback((file: any) => {
    setImageUpload(file);
  }, []);

  const handleResumeUpload = useCallback((file: any) => {
    setResumeUpload(file);
  }, []);

  return (
    <>
      <p className="title">Set up your professional profile</p>
      <div className="form">
        <div className="form-container">
          <div className="upload-image">
            <p>Profile Photo</p>
            <span>
              Please upload a high-quality profile photo. Freelancers with
              professional profile photos are prioritized and see more jobs with
              Toptal clients.
            </span>
            <div className="image-des">
              <div className="image">
                <div className="header-left__avatar">
                  <div className="avatar-img__upload">
                    <AvatarDropzone
                      onDrop={handleAvatarUpload}
                      imgDefult="https://res.cloudinary.com/hire-it/image/upload/v1681489265/Colorful_Abstract_Globe_Marketing_Logo_ovhooq.png"
                    />
                    {loadingUpload && (
                      <div className="loading-upload__avatar">
                        <CLoading />
                      </div>
                    )}
                  </div>
                </div>
              </div>
              <div className="des">
                <span>JPG / PNG file</span>
                <span>Minimum resolution: 380x380px</span>
                <span>Maximum file size: 25 MB</span>
              </div>
            </div>
            <div className="container-error">
              <p className="error">
                {!imageUpload && isClick ? "*Please complete this field." : ""}
              </p>
            </div>
          </div>

          <div className="upload-resume">
            <p>Resume</p>
            <p className="title">Please upload your resume.</p>
            <div className="resume-des">
              <div className="resume">
                <CButtonUploadPDF
                  onDrop={handleResumeUpload}
                  isLoading={loadingUploadResume}
                />
              </div>
              <div className="des">
                <p>PDF file</p>
                <p>Maximum file size: 5 MB</p>
              </div>
            </div>

            <div className="container-error">
              <p className="error">
                {!resumeUpload && isClick ? "*Please complete this field." : ""}
              </p>
            </div>
          </div>
          {resumeUpload && (
            <div className="des_pdf">
              <Link21 size="20" color="rgba(0, 0, 0, 0.2)" variant="Bold" />
              <p>{resumeUpload.name}</p>
              <Trash
                className="trash"
                size="24"
                color="#ff0000"
                onClick={() => {
                  setResumeUpload("");
                }}
              />
            </div>
          )}

          <div className="continue-bottom">
            <Button
              variant="contained"
              className="button"
              onClick={handleSubmit}
            >
              {loading ? (
                <CircularProgress color="inherit" size={16} />
              ) : (
                "Finish"
              )}
            </Button>
            <div className="description">
              <p>100% complete</p>
              <p>
                You’re almost done! One more step and you can start your
                personalized screening experience.
              </p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default FormProfileSetup;
