import { updateEmployeeFreelancingPreferences } from "@/apis/Employee";
import CTextErea from "@/common/components/controls/CTextArea";
import { formFreelancingPreferencesEmployee } from "@/types/models";
import { Button, CircularProgress } from "@mui/material";
import { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-toastify";
interface propsFormFreelancingPreferences {
  handleComplete: () => void;
  id?: number;
  employeeId?: number;
}

const FormFreelancingPreferences = ({
  handleComplete,
  id,
  employeeId,
}: propsFormFreelancingPreferences) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    watch,
    control,
  } = useForm<formFreelancingPreferencesEmployee>({
    reValidateMode: "onSubmit",
  });
  const [loading, setLoading] = useState(false);

  const handleUpdate: SubmitHandler<formFreelancingPreferencesEmployee> = (
    data
  ) => {
    updateEmployeeFreelancingPreferences({
      ...data,
      employeeId: employeeId,
      id: id,
      completionLevel: 3,
    })
      .then((data) => {
        setLoading(false);
        handleComplete();
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };

  return (
    <>
      <p className="title">Tell us about your freelancing preferences</p>
      <form className="form" onSubmit={handleSubmit(handleUpdate)}>
        <div className="form-container">
          <div className="textarea-about">
            <p>What types of projects would you like to work on?</p>
            <div className="container-error">
              <CTextErea
                control={control}
                {...register("interestedProductTypes", {
                  required: true,
                })}
                onBlur={(event) => {
                  event.target.value.length >= 255
                    ? setError("interestedProductTypes", {
                        type: "onBlur",
                        message: undefined,
                      })
                    : setError("interestedProductTypes", {
                        type: "onBlur",
                        message: "*Please complete this field.",
                      });
                }}
                minRows={5}
                className="textarea"
                placeholder="Share your thoughts about the types of projects you would like to work on."
                minCharacter={255}
              />
              <p className="error">
                {errors.interestedProductTypes?.type === "required"
                  ? "*Please complete this field."
                  : errors.interestedProductTypes && (
                      <span>{errors.interestedProductTypes.message}</span>
                    )}
              </p>
            </div>
          </div>
          <div className="textarea-about">
            <p>
              What interests you the most about building a freelance career?
            </p>
            <div className="container-error">
              <CTextErea
                control={control}
                {...register("careerInterests", {
                  required: true,
                })}
                onBlur={(event) => {
                  event.target.value.length >= 255
                    ? setError("careerInterests", {
                        type: "onBlur",
                        message: undefined,
                      })
                    : setError("careerInterests", {
                        type: "onBlur",
                        message: "*Please complete this field.",
                      });
                }}
                minRows={5}
                className="textarea"
                placeholder="Share your thoughts about what interests you the most about building a freelance career."
                minCharacter={255}
              />
              <p className="error">
                {errors.careerInterests?.type === "required"
                  ? "*Please complete this field."
                  : errors.careerInterests && (
                      <span>{errors.careerInterests.message}</span>
                    )}
              </p>
            </div>
          </div>
          <div className="textarea-about">
            <p>
              When working with a client remotely, what do you consider the key
              to success?
            </p>
            <div className="container-error">
              <CTextErea
                control={control}
                {...register("remoteSuccessKey", {
                  required: true,
                })}
                onBlur={(event) => {
                  event.target.value.length >= 255
                    ? setError("remoteSuccessKey", {
                        type: "onBlur",
                        message: undefined,
                      })
                    : setError("remoteSuccessKey", {
                        type: "onBlur",
                        message: "*Please complete this field.",
                      });
                }}
                minRows={5}
                className="textarea"
                minCharacter={255}
                placeholder="Share your thoughts about what you consider the key to success when working with a client remotely."
              />
              <p className="error">
                {errors.remoteSuccessKey?.type === "required"
                  ? "*Please complete this field."
                  : errors.remoteSuccessKey && (
                      <span>{errors.remoteSuccessKey.message}</span>
                    )}
              </p>
            </div>
          </div>
          <div className="continue-bottom">
            <Button variant="contained" className="button" type="submit">
              {loading ? (
                <CircularProgress color="inherit" size={16} />
              ) : (
                "Continue"
              )}
            </Button>
            <div className="description">
              <p>75% complete</p>
              <p>
                Once you’re done, you can start your personalized screening
                experience.
              </p>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default FormFreelancingPreferences;
