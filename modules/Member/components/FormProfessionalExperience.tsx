import { updateEmployeeProfessionalExperience } from "@/apis/Employee";
import CAutocompleteOption from "@/common/components/controls/CAutocompleteOption";
import CRadioButton from "@/common/components/controls/CRadioButton";
import CTextField from "@/common/components/controls/CTextField";
import {
  formGetStartEmployee,
  formProfessionalExperience,
} from "@/forms/employee";
import { Certification, Skill } from "@/types/models";
import {
  Button,
  CircularProgress,
  InputAdornment,
  TextField,
} from "@mui/material";
import React, { useEffect, useMemo, useRef, useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { BsCheck2 } from "react-icons/bs";
import { toast } from "react-toastify";
interface propsFormProfessionalExperience {
  handleComplete: () => void;
  policys: Certification[];
  commitments: string[];
  commitment: string | undefined;
  setCommitment: React.Dispatch<React.SetStateAction<string | undefined>>;
  skills: Skill[];
  positionName?: string;
  id?: number;
  employeeId?: number;
}
const FormProfessionalExperience = ({
  handleComplete,
  policys,
  commitments,
  commitment,
  setCommitment,
  skills,
  positionName,
  id,
  employeeId,
}: propsFormProfessionalExperience) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    watch,
    control,
  } = useForm<formProfessionalExperience>({ reValidateMode: "onSubmit" });
  const [loading, setLoading] = useState(false);
  const [skillPosition, setSkillPosition] = useState<
    {
      id: number;
      name: string;
    }[]
  >([]);

  const [certifications, setCertifications] = useState<
    {
      id: number;
      name: string;
    }[]
  >([]);

  useMemo(() => {
    let tempSkills: {
      id: number;
      name: string;
    }[] = [];

    skills.map((data) => {
      if (data.position?.positionName === positionName)
        tempSkills.push({ id: data.id, name: data.skillName });
    });
    setSkillPosition(tempSkills);

    let tempcertifications: {
      id: number;
      name: string;
    }[] = [];

    policys.map((data, index) => {
      tempcertifications.push({
        id: data.id,
        name: data.certificationName,
      });
    });
    setCertifications(tempcertifications);
  }, [skills, policys, positionName]);

  const handleUpdate: SubmitHandler<formProfessionalExperience> = (data) => {
    const listSkill: string[] = [];
    const listCertification: string[] = [];

    data.skills.map((v, i) => {
      let t = JSON.stringify(v);
      let skill = JSON.parse(t);
      listSkill.push(skill.id);
    });

    data.certifications.map((v, i) => {
      let t = JSON.stringify(v);
      let certification = JSON.parse(t);
      listCertification.push(certification.id);
    });

    updateEmployeeProfessionalExperience({
      ...data,
      employeeId: employeeId,
      id: id,
      skills: listSkill,
      certifications: listCertification,
      completionLevel: 2,
    })
      .then((data) => {
        setLoading(false);
        handleComplete();
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };

  return (
    <>
      <p className="title">Tell us about your professional experience</p>

      <form className="form" onSubmit={handleSubmit(handleUpdate)}>
        <div className="form-container">
          <div className="skills">
            <p>Skills (4 to 6 skills)</p>
            <div className="container-error">
              <CAutocompleteOption
                placeholder="Start typing, then select a skill"
                values={skillPosition}
                name={"skills"}
                validateLenght={true}
                multiple={true}
                control={control}
                isInline={true}
                clasName="skills-item"
                disableCloseOnSelect={true}
                ListboxProps={{ className: "cautocomplete" }}
                onBlurAutocomplete={(e) => {
                  const value = e.currentTarget.outerText.split("\n");
                  const values = value.filter((element) => element !== "");
                  values.length - 1 < 4 || values.length - 1 > 6
                    ? setError("skills", {
                        type: "onBlur",
                        message: "*Please add 4 to 6 areas of expertise.",
                      })
                    : setError("skills", {
                        type: "onBlur",
                        message: undefined,
                      });
                }}
                rules={{
                  required: true,
                  validate: async (value) => {
                    if (value.length < 4 || value.length > 6)
                      return "*Please add 4 to 6 areas of expertise.";
                    return true;
                  },
                }}
              />
              <p className="error">
                {errors.skills?.type === "required"
                  ? "*Please add 4 to 6 areas of expertise."
                  : errors.skills && <span>{errors.skills.message}</span>}
              </p>
            </div>
          </div>
          <div className="certification">
            <p>Certifications (optional)</p>

            <CAutocompleteOption
              clasName="reason-appling-item"
              placeholder="Select certifications from the list"
              values={certifications}
              name={"certifications"}
              multiple={true}
              control={control}
            />
            <span>
              Choose any certificates you hold from the following list of
              commonly requested certificates. Other certificates can be added
              to your profile upon joining the network. Note: You might be asked
              for the certification IDs later in the screening process, so
              please have them readily available (either in digital or print
              format).
            </span>
          </div>
          <div className="job-experience">
            <div className="title">
              <p>Years Of Experience</p>

              <div className="container-error">
                <TextField
                  placeholder="Enter a number"
                  type="number"
                  inputProps={{
                    step: 0.01,
                    min: 0,
                    pattern: "\\d+(\\.\\d{0,2})?",
                  }}
                  className="experience"
                  error={errors.yearsOfExperience?.message !== undefined}
                  {...register("yearsOfExperience", {
                    required: true,
                    onBlur(event) {
                      event.target.value
                        ? setError("yearsOfExperience", {
                            type: "onBlur",
                            message: undefined,
                          })
                        : setError("yearsOfExperience", {
                            type: "onBlur",
                            message: "*Experience rate cannot be empty",
                          });
                    },
                  })}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        {!!errors.yearsOfExperience &&
                        errors.yearsOfExperience?.message === undefined ? (
                          <BsCheck2 fontSize={18} color="#30d69a" />
                        ) : (
                          ""
                        )}
                      </InputAdornment>
                    ),
                  }}
                />
                <p className="error">
                  {errors.yearsOfExperience?.type === "required"
                    ? "*Experience cannot be empty"
                    : errors.yearsOfExperience && (
                        <span>{errors.yearsOfExperience.message}</span>
                      )}
                </p>
              </div>
            </div>
          </div>
          <div className="radio-commitment">
            <p>Which type of job commitment do you prefer?</p>
            <div>
              <div className="container-error">
                {commitments.map((data, index) => (
                  <CRadioButton
                    registerName={{
                      ...register("isFullTime", {
                        required: true,
                      }),
                    }}
                    onFocus={() => {
                      setError("isFullTime", {
                        type: "onBlur",
                        message: undefined,
                      });
                    }}
                    setState={setCommitment}
                    state={commitment}
                    name="isFullTime"
                    label={data}
                    item={data}
                    key={index}
                  />
                ))}
                <p className="error">
                  {errors.isFullTime?.type === "required" &&
                    "*Type of job commitment cannot be empty"}
                </p>
              </div>
            </div>
          </div>
          <div className="hourly-rate">
            <p>What’s your preferred hourly rate in U.S. dollars?</p>
            <div className="container-error">
              <TextField
                placeholder="Enter amount"
                inputProps={{
                  step: 0.01,
                  min: 0,
                  pattern: "\\d+(\\.\\d{0,2})?",
                }}
                type="number"
                className="hourly-rate"
                error={errors.hourlyRate?.message !== undefined}
                {...register("hourlyRate", {
                  required: true,
                  onBlur(event) {
                    event.target.value
                      ? setError("hourlyRate", {
                          type: "onBlur",
                          message: undefined,
                        })
                      : setError("hourlyRate", {
                          type: "onBlur",
                          message: "*Hourly rate cannot be empty",
                        });
                  },
                })}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">$</InputAdornment>
                  ),
                  endAdornment: (
                    <InputAdornment position="end">
                      {!!errors.hourlyRate &&
                      errors.hourlyRate?.message === undefined ? (
                        <BsCheck2 fontSize={18} color="#30d69a" />
                      ) : (
                        ""
                      )}
                    </InputAdornment>
                  ),
                }}
              />
              <p className="error">
                {errors.hourlyRate?.type === "required"
                  ? "*Hourly rate cannot be empty"
                  : errors.hourlyRate && (
                      <span>{errors.hourlyRate.message}</span>
                    )}
              </p>
            </div>
          </div>
          <div className="link-infor">
            <p>LinkedIn (optional)</p>
            <div className="container-error">
              <TextField
                placeholder="Your Linkedin profile URL"
                className="linkedIn"
                error={
                  errors.linkedIn?.message !== undefined &&
                  errors.linkedIn?.message !== ""
                }
                {...register("linkedIn", {
                  pattern: {
                    value: /^https?:\/\/(www\.)?linkedin\.com\/in\/[\w-]+\/?$/,
                    message: "*Please add a valid Linkedin profile URL.",
                  },
                  onBlur(event) {
                    const urllinkedIn = event.target.value;
                    const urllinkedInRegex =
                      /^https?:\/\/(www\.)?github\.com\/[\w-]+\/?$/;
                    !urllinkedIn.trim()
                      ? setError("linkedIn", {
                          type: "onBlur",
                          message: "",
                        })
                      : urllinkedInRegex.test(urllinkedIn)
                      ? setError("linkedIn", {
                          type: "onBlur",
                          message: undefined,
                        })
                      : setError("linkedIn", {
                          type: "onBlur",
                          message: "*Please add a valid Linkedin profile URL.",
                        });
                  },
                })}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      {!!errors.linkedIn &&
                      errors.linkedIn?.message === undefined &&
                      errors.linkedIn?.message !== "" ? (
                        <BsCheck2 fontSize={18} color="#30d69a" />
                      ) : (
                        ""
                      )}
                    </InputAdornment>
                  ),
                }}
              />
              <p className="error">
                {errors.linkedIn?.type === "required"
                  ? "*Please add a valid Linkedin profile URL."
                  : errors.linkedIn && <span>{errors.linkedIn.message}</span>}
              </p>
            </div>
          </div>
          <div className="link-infor">
            <p>GitHub (optional)</p>
            <div className="container-error">
              <TextField
                placeholder="Your GitHub profile URL"
                className="experience"
                error={
                  errors.gitHub?.message !== undefined &&
                  errors.gitHub?.message !== ""
                }
                {...register("gitHub", {
                  pattern: {
                    value: /^https?:\/\/(www\.)?github\.com\/[\w-]+\/?$/,
                    message: "*Please add a valid Github profile URL.",
                  },
                  onBlur(event) {
                    const urlGithub = event.target.value;
                    const urlGithubRegex =
                      /^https?:\/\/(www\.)?github\.com\/[\w-]+\/?$/;
                    !urlGithub.trim()
                      ? setError("gitHub", {
                          type: "onBlur",
                          message: "",
                        })
                      : urlGithubRegex.test(urlGithub)
                      ? setError("gitHub", {
                          type: "onBlur",
                          message: undefined,
                        })
                      : setError("gitHub", {
                          type: "onBlur",
                          message: "*Please add a valid Github profile URL.",
                        });
                  },
                })}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      {!!errors.gitHub &&
                      errors.gitHub?.message === undefined &&
                      errors.gitHub?.message !== "" ? (
                        <BsCheck2 fontSize={18} color="#30d69a" />
                      ) : (
                        ""
                      )}
                    </InputAdornment>
                  ),
                }}
              />
              <p className="error">
                {errors.gitHub?.type === "required"
                  ? "*Experience cannot be empty"
                  : errors.gitHub && <span>{errors.gitHub.message}</span>}
              </p>
            </div>
          </div>
          <div className="continue-bottom">
            <Button
              variant="contained"
              className="button"
              type="submit"
              // onClick={handleComplete}
            >
              {loading ? (
                <CircularProgress color="inherit" size={16} />
              ) : (
                "Continue"
              )}
            </Button>
            <div className="description">
              <p>50% complete</p>
              <p>
                You’re one step closer to unlocking access to working with the
                world’s top companies.
              </p>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default FormProfessionalExperience;
