import { updateEmployeeGettingStarted } from "@/apis/Employee";
import CAutocompleteOption from "@/common/components/controls/CAutocompleteOption";
import CRadioButton from "@/common/components/controls/CRadioButton";
import { formGetStartEmployee } from "@/types/models";
import {
  Button,
  CircularProgress,
  InputAdornment,
  TextField,
} from "@mui/material";
import { useSession } from "next-auth/react";
import React, { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { BsCheck2 } from "react-icons/bs";
import { toast } from "react-toastify";

interface propsFormGettingStarted {
  englishProficiencys: string[];
  setEnglishProficiency: React.Dispatch<
    React.SetStateAction<string | undefined>
  >;
  englishProficiency: string | undefined;
  reasons: { id: number; name: string }[];
  handleComplete: () => void;
  id?: number;
  employeeId?: number;
}

const FormGettingStarted = ({
  englishProficiencys,
  setEnglishProficiency,
  englishProficiency,
  reasons,
  handleComplete,
  id,
  employeeId,
}: propsFormGettingStarted) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    watch,
    control,
  } = useForm<formGetStartEmployee>({ reValidateMode: "onSubmit" });

  const [loading, setLoading] = useState(false);

  const handleUpdateProfile: SubmitHandler<formGetStartEmployee> = (data) => {
    setLoading(true);
    let temp1 = JSON.stringify(data.reasonApplying);
    let reasonApplying = JSON.parse(temp1);

    updateEmployeeGettingStarted({
      ...data,
      reasonApplying: reasonApplying.name,
      employeeId: employeeId,
      id: id,
      completionLevel: 1,
    })
      .then((data) => {
        setLoading(false);
        handleComplete();
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };

  return (
    <>
      <p className="title">Welcome to Toptal. Let’s get started!</p>
      <p>
        Your application should only take a few minutes. Based on the
        information you provide, our screening team will determine the best path
        for you going forward.
      </p>
      <form className="form" onSubmit={handleSubmit(handleUpdateProfile)}>
        <div className="form-container">
          <div className="full-name">
            <p>Full Legal Name</p>
            <div className="container-error">
              <TextField
                placeholder="e.g., Vo Minh Hieu"
                error={errors.fullName?.message !== undefined}
                {...register("fullName", {
                  required: true,
                  onBlur(event) {
                    event.target.value
                      ? setError("fullName", {
                          type: "onBlur",
                          message: undefined,
                        })
                      : setError("fullName", {
                          type: "onBlur",
                          message: "*Full Name cannot be empty",
                        });
                  },
                })}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      {!!errors.fullName &&
                      errors.fullName?.message === undefined ? (
                        <BsCheck2 fontSize={18} color="#30d69a" />
                      ) : (
                        ""
                      )}
                    </InputAdornment>
                  ),
                }}
              />
              <p className="error">
                {errors.fullName?.type === "required"
                  ? "*Full name cannot be empty"
                  : errors.fullName && <span>{errors.fullName.message}</span>}
              </p>
            </div>
          </div>
          <div className="location">
            <div className="title">
              <p>Location</p>
              <div className="container-error">
                <TextField
                  placeholder="Country"
                  error={errors.country?.message !== undefined}
                  {...register("country", {
                    required: true,
                    onBlur(event) {
                      event.target.value
                        ? setError("country", {
                            type: "onBlur",
                            message: undefined,
                          })
                        : setError("country", {
                            type: "onBlur",
                            message: "*Country cannot be empty",
                          });
                    },
                  })}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        {!!errors.country &&
                        errors.country?.message === undefined ? (
                          <BsCheck2 fontSize={18} color="#30d69a" />
                        ) : (
                          ""
                        )}
                      </InputAdornment>
                    ),
                  }}
                />
                <p className="error">
                  {errors.country?.type === "required"
                    ? "*Country cannot be empty"
                    : errors.country && <span>{errors.country.message}</span>}
                </p>
              </div>
            </div>
            <div className="title">
              <p>City</p>
              <div className="container-error">
                <TextField
                  placeholder="City"
                  className="city"
                  error={errors.city?.message !== undefined}
                  {...register("city", {
                    required: true,
                    onBlur(event) {
                      event.target.value
                        ? setError("city", {
                            type: "onBlur",
                            message: undefined,
                          })
                        : setError("city", {
                            type: "onBlur",
                            message: "*City cannot be empty",
                          });
                    },
                  })}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        {!!errors.city && errors.city?.message === undefined ? (
                          <BsCheck2 fontSize={18} color="#30d69a" />
                        ) : (
                          ""
                        )}
                      </InputAdornment>
                    ),
                  }}
                />
                <p className="error">
                  {errors.city?.type === "required"
                    ? "*City cannot be empty"
                    : errors.city && <span>{errors.city.message}</span>}
                </p>
              </div>
            </div>
          </div>
          <div className="citizenship">
            <p>Citizenship</p>
            <div className="container-error">
              <TextField
                placeholder="e.g., Viet Nam"
                error={errors.nationality?.message !== undefined}
                {...register("nationality", {
                  required: true,
                  onBlur(event) {
                    event.target.value
                      ? setError("nationality", {
                          type: "onBlur",
                          message: undefined,
                        })
                      : setError("nationality", {
                          type: "onBlur",
                          message: "*Citizenship cannot be empty",
                        });
                  },
                })}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      {!!errors.nationality &&
                      errors.nationality?.message === undefined ? (
                        <BsCheck2 fontSize={18} color="#30d69a" />
                      ) : (
                        ""
                      )}
                    </InputAdornment>
                  ),
                }}
              />
              <p className="error">
                {errors.nationality?.type === "required"
                  ? "*Citizenship cannot be empty"
                  : errors.nationality && (
                      <span>{errors.nationality.message}</span>
                    )}
              </p>
            </div>
          </div>
          <div className="radio-button-english-proficiency">
            <p>English Proficiency</p>
            <div>
              <div className="container-error">
                {englishProficiencys.map((data, index) => (
                  <CRadioButton
                    registerName={{
                      ...register("englishProficiency", {
                        required: true,
                      }),
                    }}
                    onFocus={() => {
                      setError("englishProficiency", {
                        type: "onBlur",
                        message: undefined,
                      });
                    }}
                    setState={setEnglishProficiency}
                    state={englishProficiency}
                    name="englishProficiency"
                    label={data}
                    item={data}
                    key={index}
                  />
                ))}
                <p className="error">
                  {errors.englishProficiency?.type === "required" &&
                    "*English proficiency cannot be empty"}
                </p>
              </div>
            </div>
          </div>
          <div className="reason-appling">
            <p>What’s the main reason you’re applying at Toptal?</p>
            <div className="container-error">
              <CAutocompleteOption
                clasName="reason-appling-item"
                placeholder="Select a reason form the list"
                values={reasons}
                name={"reasonApplying"}
                rules={{
                  required: true,
                }}
                onBlur={(e) => {
                  e.target.value
                    ? setError("reasonApplying", {
                        type: "onBlur",
                        message: undefined,
                      })
                    : setError("reasonApplying", {
                        type: "onBlur",
                        message: "*Reason you’re applying cannot be empty",
                      });
                }}
                control={control}
              />
              <p className="error">
                {errors.reasonApplying?.type === "required"
                  ? "*Reason you’re applying cannot be empty"
                  : errors.reasonApplying && (
                      <span>{errors.reasonApplying.message}</span>
                    )}
              </p>
            </div>
          </div>
          <div className="continue-bottom">
            <Button
              variant="contained"
              className="button"
              // onClick={handleComplete}
              type="submit"
            >
              {loading ? (
                <CircularProgress color="inherit" size={16} />
              ) : (
                "Continue"
              )}
            </Button>
            <div className="description">
              <p>25% complete</p>
              <p>
                You’re on your way to becoming part of the largest fully
                distributed workforce in the world!
              </p>
            </div>
          </div>
        </div>
      </form>
    </>
  );
};

export default FormGettingStarted;
