/* eslint-disable @next/next/no-img-element */
import CDropMenu from "@/common/components/controls/CDropMenu";
import CSteper from "@/common/components/controls/CSteper";
import withPageEditInfo from "@/utils/withPageEditInfo";
import { ArrowDown2, Home as HomeIcon, LogoutCurve, User } from "iconsax-react";
import { signOut, useSession } from "next-auth/react";
import React, { useEffect, useMemo, useState } from "react";
import FormFreelancingPreferences from "./FormFreelancingPreferences";
import FormGettingStarted from "./FormGettingStarted";
import FormProfessionalExperience from "./FormProfessionalExperience";
import FormProfileSetup from "./FormProfileSetup";
import { getStatusEmployee } from "@/services/employees";
import useSkills from "@/hooks/useSkills";
import CLoading from "@/common/components/controls/CLoading";
import useCertification from "@/hooks/useCertification";

const steps = [
  { title: "Getting Started", time: "1 min" },
  { title: "Professional Experience", time: "1 min" },
  { title: "Freelancing Preferences", time: "1 min" },
  { title: "Profile Setup", time: "1 min" },
];

const englishProficiencys = [
  "Native/Fluent",
  "Advanced",
  "Intermediate",
  "Basic",
];
const reasons = [
  { id: 1, name: "I want the freedom and flexibility of remote work." },
  {
    id: 2,
    name: "I want to officially be part of the top 3% of freelance talent.",
  },
  { id: 3, name: "I need extra work or income opportunities." },
  {
    id: 4,
    name: "I saw online reviews, posts, or job listings that convinced me to apply.",
  },
  { id: 5, name: "I aim to learn new technical skills and technologies." },
  { id: 6, name: "I was referred by a friend or colleague." },
  { id: 7, name: "I want a job where I can improve my English proficiency." },
  {
    id: 8,
    name: "I am looking for higher quality or more diverse freelance jobs.",
  },
  // { id: 9, name: "Other" },
];
const experiences = [
  "Less than 1 year",
  "1 to 2 years",
  "2 to 5 years",
  "More than 5 years",
];
const commitments = [
  "Full-time (40 hours/week)",
  // Freelancers with full-time availability are currently in hight demand and see more jobs with Toptal clients
  "Part-time (20 hours/week)",
  // "Hourly (Up to 10 hours/week)",
];

export default withPageEditInfo(["Employee"], () => {
  const [englishProficiency, setEnglishProficiency] = React.useState<string>();
  const [commitment, setCommitment] = React.useState<string>();
  const { data } = useSession();
  const [user, setUser] = useState<any>();
  const { skills, loading } = useSkills();
  const { certifications, loading: loadingCertifications } = useCertification();
  // Steper
  const [activeStep, setActiveStep] = React.useState(0);
  const [completed, setCompleted] = React.useState<{
    [k: number]: boolean;
  }>({});
  useMemo(() => {
    getStatusEmployee({ jwt: data?.user.access_token }, data?.user.id).then(
      (data) => {
        if (data) {
          setActiveStep(data.data[0].completionLevel);
          setUser(data.data[0]);
        }
      }
    );
  }, [data?.user.access_token, data?.user.id]);

  const handleNext = () => {
    if (!isLastStep()) {
      const newActiveStep = activeStep + 1;
      setActiveStep(newActiveStep);
    }
  };
  const handleComplete = () => {
    const newCompleted = completed;
    newCompleted[activeStep] = true;
    setCompleted(newCompleted);
    handleNext();
  };

  const isLastStep = () => {
    return activeStep === totalSteps() - 1;
  };

  const totalSteps = () => {
    return steps.length;
  };
  // Back
  const handleBack = () => {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  const handelLogOut = () => {
    signOut({
      callbackUrl: "/auth/login",
    });
  };

  return loading && loadingCertifications ? (
    <CLoading fullScreen />
  ) : (
    <>
      <div className="application_info_container">
        <div className="header">
          <div className="title">
            <img
              src="https://res.cloudinary.com/hire-it/image/upload/v1681221840/Logo_v_yiwkak.png"
              alt="logo"
            />
            <p>Your Toptal Application</p>
          </div>
          <div className="header-account__function">
            <CDropMenu
              arrMenu={[
                {
                  title: "My account",
                  href: "/auth/profile",
                  icon: <HomeIcon />,
                },

                {
                  title: "Log Out",
                  icon: <LogoutCurve />,
                  onClick: handelLogOut,
                },
              ]}
              icon={
                <div className="info">
                  <div className="border-icon">
                    <User color="#fff" size={10} />
                  </div>
                  <div className="info-user">
                    <p>{user?.users_permissions_user.fullName}</p>
                    <p className="position">{user?.position.positionName}</p>
                  </div>
                  <ArrowDown2 size="20" color="#ffffff" variant="Bold" />
                </div>
              }
            />
          </div>
        </div>
        <div className="east-container"></div>

        <div className="center-container">
          <CSteper
            steps={steps}
            initialState={activeStep}
            initialStateCompleted={completed}
          />

          {activeStep === 0 ? (
            <FormGettingStarted
              englishProficiency={englishProficiency}
              englishProficiencys={englishProficiencys}
              handleComplete={handleComplete}
              reasons={reasons}
              setEnglishProficiency={setEnglishProficiency}
              id={data?.user.id}
              employeeId={user && user.id}
            />
          ) : activeStep === 1 ? (
            <FormProfessionalExperience
              commitment={commitment}
              commitments={commitments}
              handleComplete={handleComplete}
              policys={certifications}
              setCommitment={setCommitment}
              skills={skills}
              positionName={user?.position.positionName}
              id={data?.user.id}
              employeeId={user && user.id}
            />
          ) : activeStep === 2 ? (
            <FormFreelancingPreferences
              handleComplete={handleComplete}
              id={data?.user.id}
              employeeId={user && user.id}
            />
          ) : (
            <FormProfileSetup
              handleComplete={handleComplete}
              id={data?.user.id}
              employeeId={user && user.id}
            />
          )}
          {/* <div>
            <Button
              color="inherit"
              disabled={activeStep === 0}
              onClick={handleBack}
              sx={{ mr: 1 }}
            >
              Back
            </Button>
          </div> */}
        </div>
      </div>
    </>
  );
});
