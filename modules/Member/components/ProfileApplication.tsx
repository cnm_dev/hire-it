/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import { Certification, Employee } from "@/types/models";
import { formatNormalDate } from "@/utils";
import {
  Airdrop,
  Ankr,
  ArchiveBook,
  ArrangeHorizontal,
  ArrangeHorizontalSquare,
  Bill,
  BookSaved,
  Cake,
  Gps,
  Link,
  Map,
  Map1,
  MoneyRecive,
  Reserve,
  Sagittarius,
  Task,
} from "iconsax-react";
import React from "react";

type ProfileApplicationProps = {
  employee: Employee;
};

const ProfileApplication = ({ employee }: ProfileApplicationProps) => {
  return (
    <div className="application-profile scroll">
      <div className="application-profile__header">
        <h4>Application Employee</h4>
      </div>
      <div className="application-profile__content">
        <div className="application-item profile">
          <div className="application-item__content profile">
            <img
              src={
                employee.users_permissions_user.avartar
                  ? employee.users_permissions_user.avartar
                  : image.image_default
              }
              alt=""
            />
            <div className="application__content">
              <h5>{employee.users_permissions_user.fullName}</h5>
              <span>{employee.users_permissions_user.email}</span>
              <span>Position: {employee.position.positionName}</span>
            </div>
          </div>
          {employee.resume && (
            <div className="application-item__content pdf">
              <a href={employee.resume} target="_blank" rel="noreferrer">
                <img src={image.dpf_logo} alt="" />
                <span>CV_{employee.users_permissions_user.fullName}</span>
              </a>
            </div>
          )}
        </div>
        <div className="application-item">
          <h4>Personal Infomation</h4>
          <div className="application-item__content">
            <div className="apllication-content">
              <p>
                <Cake size="20" color="#11315b" />
                Birthday
              </p>
              <p>
                {employee.dateOfBirth
                  ? formatNormalDate(employee.dateOfBirth)
                  : "No update"}
              </p>
            </div>
            <div className="apllication-content">
              <p>
                <Sagittarius size="20" color="#11315b" />
                Gender
              </p>
              <p>{employee.gender ? employee.gender : "No update"}</p>
            </div>
            <div className="apllication-content">
              <p>
                <Map size="20" color="#11315b" />
                Nationality
              </p>
              <p>{employee.nationality ? employee.nationality : "No update"}</p>
            </div>
            <div className="apllication-content">
              <p>
                <Map1 size="20" color="#11315b" />
                Country
              </p>
              <p>{employee.country ? employee.country : "No update"}</p>
            </div>
            <div className="apllication-content">
              <p>
                <Gps size="20" color="#11315b" />
                City
              </p>
              <p>{employee.city ? employee.city : "No update"}</p>
            </div>
            <div className="apllication-content">
              <p>
                <BookSaved size="20" color="#11315b" />
                English level
              </p>
              <p>
                {employee.englishProficiency
                  ? employee.englishProficiency
                  : "No update"}
              </p>
            </div>
            <div className="apllication-content">
              <p>
                <Link size="20" color="#11315b" />
                LinkedIn
              </p>
              <p>{employee.linkedIn ? employee.linkedIn : "No update"}</p>
            </div>
            <div className="apllication-content">
              <p>
                <Link size="20" color="#11315b" />
                GitHub
              </p>
              <a href={employee.gitHub} target="_blank" rel="noreferrer">
                {employee.gitHub ? employee.gitHub : "No update"}
              </a>
            </div>
          </div>
        </div>
        <div className="application-item">
          <h4>Working Infomation</h4>
          <div className="application-item__content">
            <div className="apllication-content">
              <p>
                <MoneyRecive size="20" color="#11315b" />
                Hourly Rate
              </p>
              <p>
                {employee.hourlyRate ? `${employee.hourlyRate}$` : "No update"}
              </p>
            </div>
            <div className="apllication-content">
              <p>
                <ArchiveBook size="20" color="#11315b" />
                Years Experience
              </p>
              <p>
                {employee.yearsOfExperience
                  ? `${employee.yearsOfExperience} year`
                  : "No update"}
              </p>
            </div>
            <div className="apllication-content">
              <p>
                <Airdrop size="20" color="#11315b" />
                Type working
              </p>
              <p>{employee.isRemote ? "Remote" : "Offline"}</p>
            </div>
            <div className="apllication-content">
              <p>
                <Task size="20" color="#11315b" />
                Skill
              </p>
              {employee.skills.length > 0
                ? employee.skills.map((skill: any, index: number) => (
                    <p key={index}>- {skill.skillName}</p>
                  ))
                : "No update"}
            </div>
          </div>
        </div>
        <div className="application-item">
          <h4>Reason Applying</h4>
          <div className="application-item__content double">
            <div className="apllication-content">
              <p>
                <Bill size="20" color="#11315b" />
                Certifications
              </p>
              {employee.certifications.length > 0
                ? employee.certifications.map(
                    (certification: any, index: number) => (
                      <p key={index}>- {certification.certificationName}</p>
                    )
                  )
                : "No update"}
            </div>
            <div className="apllication-content">
              <p>
                <Reserve size="20" color="#11315b" />
                Reason
              </p>
              <p>
                {employee.reasonApplying
                  ? employee.reasonApplying
                  : "No update"}
              </p>
            </div>
            <div className="apllication-content">
              <p>
                <Ankr size="20" color="#11315b" />
                Career Interests
              </p>
              <p>
                {employee.careerInterests
                  ? employee.careerInterests
                  : "No update"}
              </p>
            </div>
            <div className="apllication-content">
              <p>
                <ArrangeHorizontal size="20" color="#11315b" />
                Remote SuccessKey
              </p>
              <p>
                {employee.remoteSuccessKey
                  ? employee.remoteSuccessKey
                  : "No update"}
              </p>
            </div>
            <div className="apllication-content">
              <p>
                <ArrangeHorizontalSquare size="20" color="#11315b" />
                Interested Product Types
              </p>
              <p>
                {employee.interestedProductTypes
                  ? employee.interestedProductTypes
                  : "No update"}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProfileApplication;
