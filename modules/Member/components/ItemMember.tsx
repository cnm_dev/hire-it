/* eslint-disable @next/next/no-img-element */
import {
  createEmployeeChoose,
  deleteEmployeeChosse,
} from "@/apis/EmployeeChosse";
import image from "@/common/assets/image/all-image";
import CButton from "@/common/components/controls/CButton";
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import CTooltip from "@/common/components/controls/CTooltip";
import { averageStar } from "@/constants/employee";
import useCustomer from "@/hooks/useCustomer";
import { langs } from "@/mock/langs";
import { Employee, EmployeeChosse } from "@/types/models";
import { ArchiveAdd, Star1 } from "iconsax-react";
import { useSession } from "next-auth/react";
import Link from "next/link";
import React from "react";
import { mutate } from "swr";

interface PropItemMember {
  member: Employee;
  lang: "vn" | "en";
  isChosse: boolean;
  employeeChosse: EmployeeChosse[];
}

const ItemMember = ({
  member,
  lang,
  isChosse,
  employeeChosse,
}: PropItemMember) => {
  const { data: session } = useSession();

  const { customer } = useCustomer({
    customerId: session?.user.id,
  });
  const handelChosseEmployee = () => {
    if (employeeChosse.length === 0) {
      createEmployeeChoose({
        idCustomer: customer[0].id,
        idEmployee: member.id,
      }).then(() =>
        mutate(`/api/customers/${session?.user.id}/employee-chosse`)
      );
    } else {
      let listId: string[] = [];
      for (let i = 0; i < employeeChosse[0].employees.length; i++) {
        listId.push(employeeChosse[0].employees[i].id);
      }
      if (isChosse) {
        deleteEmployeeChosse({
          employeeChosseId: employeeChosse[0].id,
          listIdEmployee: listId.filter((id) => id !== member.id),
        }).then(() =>
          mutate(`/api/customers/${session?.user.id}/employee-chosse`)
        );
      } else {
        deleteEmployeeChosse({
          employeeChosseId: employeeChosse[0].id,
          listIdEmployee: [...listId, member.id],
        }).then(() =>
          mutate(`/api/customers/${session?.user.id}/employee-chosse`)
        );
      }
    }
  };

  return (
    <div className="member-user__item">
      {session?.user.type_user === "Customer" && (
        <div className="member-user__cloud">
          <CTooltip
            placement="bottom"
            title="Save employee"
            child={
              <span>
                <CButtonIcon
                  onclick={handelChosseEmployee}
                  icon={
                    <ArchiveAdd
                      size="32"
                      variant={`${isChosse ? "Bold" : "Bulk"}`}
                      color="#3eb28d"
                    />
                  }
                />
              </span>
            }
          />
        </div>
      )}
      <div className="member-user__status">
        <CTooltip
          placement="bottom"
          title={`Employee is ${member.statusWork}ing time`}
          child={<span className={member.statusWork}></span>}
        />
      </div>
      <div>
        <img
          src={
            member.users_permissions_user.avartar
              ? member.users_permissions_user.avartar
              : image.image_default
          }
          alt=""
        />
        <div className="member-item__skill">
          {member.skills.map((item: any, index: number) => (
            <CButton text={item.skillName} key={index} />
          ))}
        </div>
      </div>
      <div>
        <div className="member-item__job">
          <div className="member-job">
            <span>{langs.global.price[lang]}</span>
            <span>${member.hourlyRate}/1h</span>
          </div>
          <div className="member-job">
            <span>{langs.global.amount[lang]}</span>
            <span>
              {member.totalJob} {lang === "en" ? "Job" : "Dự án"}
            </span>
          </div>
        </div>
        <div className="member-item__account">
          <Link
            href={{
              pathname: "/member/[profileId]",
              query: { profileId: member.id },
            }}
          >
            <div className="member-account__information">
              <img
                className="avatar"
                src={
                  member.users_permissions_user.avartar
                    ? member.users_permissions_user.avartar
                    : image.image_default
                }
                alt=""
              />
              <span>{member.users_permissions_user.fullName}</span>
            </div>
          </Link>
          <div className="member-account__star">
            <Star1 variant="Bold" />
            <span>{member.previews && averageStar(member.previews)}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ItemMember;
