/* eslint-disable @next/next/no-img-element */
import CButton from "@/common/components/controls/CButton";
import CTextField from "@/common/components/controls/CTextField";
import React, { useEffect, useMemo, useState } from "react";

import {
  SearchNormal1,
  Grid2,
  Grid1,
  ArrowDown2,
  ArrowUp2,
} from "iconsax-react";
import CAutocomplete from "@/common/components/controls/CAutocomplete";
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import CCheckbox from "@/common/components/controls/CCheckbox";
import CPagination from "@/common/components/controls/CPagination";

// mock
import { all_api } from "@/mock/all_api";
import ItemMember from "./components/ItemMember";
import { TypeLang } from "../Home/components/HotMember";
import { langs } from "@/mock/langs";
import { todoRemainingSelector } from "@/redux/selector";
import { useDispatch, useSelector } from "react-redux";
import NotFoundValue from "../NotFoundValue";
import FilterMemberSlice from "@/redux/Slices/FilterMemberSlice";
import usePositions from "@/hooks/usePositions";
import CLoading from "@/common/components/controls/CLoading";
import { EmployeeChosse } from "@/types/models";
import { useRouter } from "next/router";

type TypeMemberProps = {
  lang: TypeLang["lang"];
  employeeChosse: EmployeeChosse[];
};

const Member = ({ lang, employeeChosse }: TypeMemberProps) => {
  const employees = useSelector(todoRemainingSelector);
  const router = useRouter();
  const [nameSearch, setnameSearch] = useState(`${router.query["name"] || ""}`);
  const [skillSearch, setSkillSearch] = useState(
    `${router.query["skill"] || ""}`
  );
  const [start, setStart] = useState<string>();
  const [sortPrice, setSortPrice] = useState<number>();
  const [priceTo, setPriceTo] = useState<string | number>();
  const [priceFrom, setPriceFrom] = useState<string | number>();
  const [experience, setExperience] = useState<string>();
  const [category, setCategory] = useState<string>();
  const [status, setStatus] = useState<string[]>([]);

  const { positions, loading } = usePositions();
  const dispatch = useDispatch();
  const employeesConfirm = useMemo(() => {
    return employees.filter(
      (employee) =>
        employee.status !== "wait" &&
        employee.status !== "cancel" &&
        employee.status !== "invited"
    );
  }, [employees]);

  const [pagination, SetPagination] = React.useState(1);
  const [activeGrid, setActiveGrid] = React.useState<number>(9);
  const [activeStatus, setActiveStatus] = React.useState<boolean>(true);
  const [activeCategory, setActiveCategory] = React.useState<boolean>(true);
  const [listmember, setListMember] = React.useState(
    employeesConfirm.slice(12)
  );

  useMemo(() => {
    dispatch(FilterMemberSlice.actions.memberName(""));
    dispatch(FilterMemberSlice.actions.start(0));
    dispatch(FilterMemberSlice.actions.sortPrice(0));
    dispatch(FilterMemberSlice.actions.priceForm(0));
    dispatch(FilterMemberSlice.actions.priceTo(Number.MAX_VALUE));
    dispatch(FilterMemberSlice.actions.skill("ALL"));
    dispatch(FilterMemberSlice.actions.experience(0));
    dispatch(FilterMemberSlice.actions.Category("ALL"));
  }, [dispatch]);

  const refMember = React.useRef<any>(null);

  const [selectedButton, setSelectedButton] = React.useState("button1");

  const handleClick = (button: string) => {
    setSelectedButton(button);
  };

  const handelActiveGrid = (number: number) => {
    setActiveGrid(number);
  };

  const handelActiveStatus = () => {
    setActiveStatus(!activeStatus);
  };

  const handelActiveCategory = () => {
    setActiveCategory(!activeCategory);
  };

  React.useEffect(() => {
    setListMember(
      employeesConfirm.slice((pagination - 1) * 12, pagination * 12)
    );
    refMember.current?.scrollIntoView();
  }, [employeesConfirm, pagination]);

  useEffect(() => {
    if (nameSearch) {
      dispatch(FilterMemberSlice.actions.memberName(nameSearch));
    }
    if (skillSearch) {
      dispatch(FilterMemberSlice.actions.skill(skillSearch));
    }
    if (status) {
      dispatch(FilterMemberSlice.actions.statusMember(status));
    }
  }, [dispatch, nameSearch, skillSearch, status]);

  useEffect(() => {
    if (
      !nameSearch &&
      !skillSearch &&
      !start &&
      !sortPrice &&
      !priceTo &&
      !priceFrom &&
      !experience &&
      !category
    ) {
      dispatch(FilterMemberSlice.actions.memberName(""));
      dispatch(FilterMemberSlice.actions.skill("ALL"));
      dispatch(FilterMemberSlice.actions.start(0));
      dispatch(FilterMemberSlice.actions.sortPrice(0));
      dispatch(FilterMemberSlice.actions.priceForm(0));
      dispatch(FilterMemberSlice.actions.priceTo(Number.MAX_VALUE));
      dispatch(FilterMemberSlice.actions.experience(0));
      dispatch(FilterMemberSlice.actions.Category("ALL"));
    }
  }, [
    category,
    dispatch,
    experience,
    nameSearch,
    priceFrom,
    priceTo,
    skillSearch,
    sortPrice,
    start,
  ]);

  return (
    <div id="member" ref={refMember}>
      <div className="member-header">
        <h3> {langs.member.listmember[lang]}</h3>
        <div className="member-header__statis">
          <CButton
            text={`${employeesConfirm.length}+ ${langs.global.client[lang]}`}
          />
        </div>
      </div>
      <div className="member-main">
        <div className="member-filter">
          <div className="member-filter__function">
            <div className="member-function__find">
              <SearchNormal1 size={20} />
              <CTextField
                defaultValue={nameSearch}
                onChange={(e) => {
                  setnameSearch(e.target.value);
                  dispatch(
                    FilterMemberSlice.actions.memberName(e.target.value)
                  );
                }}
                placeholder={langs.home.place_search[lang]}
              />
            </div>
            <CAutocomplete
              placeholder={langs.global.star[lang]}
              onChange={(e, v) => {
                const temp = v;
                dispatch(
                  FilterMemberSlice.actions.start(temp.label.split(" ")[0])
                );
                setStart(temp.label.split(" ")[0]);
              }}
              option={[
                { label: `5 ${langs.global.star[lang]}` },
                { label: `4 ${langs.global.star[lang]}` },
                { label: `3 ${langs.global.star[lang]}` },
                { label: `2 ${langs.global.star[lang]}` },
                { label: `1 ${langs.global.star[lang]}` },
              ]}
            />
            <CAutocomplete
              onChange={(e, v) => {
                if (v.label === "Price: Low to High") {
                  dispatch(FilterMemberSlice.actions.sortPrice(0));
                  setSortPrice(0);
                } else {
                  dispatch(FilterMemberSlice.actions.sortPrice(-1));
                  setSortPrice(-1);
                }
              }}
              placeholder={langs.global.price[lang]}
              option={[
                { label: langs.global.lowtohight[lang] },
                { label: langs.global.highttolow[lang] },
              ]}
            />
            <div
              className={`member-function__grid ${
                (activeGrid === 4 && "four") || (activeGrid === 9 && "nice")
              }`}
            >
              <CButtonIcon
                onclick={() => handelActiveGrid(4)}
                icon={<Grid2 size={28} variant="Bold" />}
              />
              <CButtonIcon
                onclick={() => handelActiveGrid(9)}
                icon={<Grid1 size={28} variant="Bold" />}
              />
            </div>
          </div>
          <div className="member-filter__status">
            <div className="member-filter__header">
              <h3>{langs.global.status[lang]}</h3>
              {activeStatus ? (
                <CButtonIcon onclick={handelActiveStatus} icon={<ArrowUp2 />} />
              ) : (
                <CButtonIcon
                  onclick={handelActiveStatus}
                  icon={<ArrowDown2 />}
                />
              )}
            </div>
            <div
              className={`member-status__main ${activeStatus ? "active" : ""}`}
            >
              <CCheckbox
                setLabel={setStatus}
                label={status}
                arrOption={[
                  {
                    value: "free",
                    label: langs.global.free[lang],
                  },
                  {
                    value: "work",
                    label: langs.global.work[lang],
                  },
                  {
                    value: "wait",
                    label: langs.global.wait[lang],
                  },
                ]}
              />
            </div>
          </div>
          <div className="member-filter__price">
            <div className="member-filter__header">
              <h3>{langs.global.price[lang]}</h3>
            </div>
            <div className="member-price__currency">
              <p>{langs.global.currency[lang]}: </p>
              <CAutocomplete
                className="autocomplete_currency"
                option={[{ label: "$" }, { label: "VNĐ" }]}
              />
              <div className="member-currency__input">
                <CTextField
                  className="text-price"
                  placeholder={langs.global.min[lang]}
                  onChange={(e) => {
                    setPriceFrom(e.target.value);
                    dispatch(
                      FilterMemberSlice.actions.priceForm(e.target.value)
                    );
                  }}
                />
                <span>{langs.global.to[lang]}</span>
                <CTextField
                  className="text-price"
                  placeholder={langs.global.max[lang]}
                  onChange={(e) => {
                    if (e.target.value === "") {
                      setPriceTo(Number.MAX_VALUE);
                      dispatch(
                        FilterMemberSlice.actions.priceTo(Number.MAX_VALUE)
                      );
                    } else {
                      setPriceTo(e.target.value);
                      dispatch(
                        FilterMemberSlice.actions.priceTo(e.target.value)
                      );
                    }
                  }}
                />
              </div>
            </div>
            <div className="member-filter__header">
              <h3>{langs.global.skillexp[lang]}</h3>
            </div>
            <div className="member-price__skill">
              <CAutocomplete
                defaultValue={{
                  label: skillSearch
                    ? skillSearch
                    : lang === "en"
                    ? "ALL"
                    : "Tất cả",
                }}
                className="member-skill__autocomplete"
                placeholder={langs.global.language[lang]}
                onChange={(e, v) => {
                  if (v.label === "Tất cả") {
                    dispatch(FilterMemberSlice.actions.skill("ALL"));
                    setSkillSearch(v.label);
                  } else {
                    dispatch(FilterMemberSlice.actions.skill(v.label));
                    setSkillSearch(v.label);
                  }
                }}
                option={lang === "en" ? all_api.skill : all_api.skill_vn}
              />
              <CAutocomplete
                onChange={(e, v) => {
                  const temp = v;
                  setExperience(temp.label.split(" ")[1]);
                  dispatch(
                    FilterMemberSlice.actions.experience(
                      temp.label.split(" ")[1]
                    )
                  );
                }}
                className="member-skill__autocomplete"
                placeholder={langs.global.year[lang]}
                option={[
                  {
                    label: `${langs.global.more[lang]} 3 ${langs.global.year[lang]}`,
                  },
                  {
                    label: `${langs.global.more[lang]} 2 ${langs.global.year[lang]}`,
                  },
                  {
                    label: `${langs.global.more[lang]} 1 ${langs.global.year[lang]}`,
                  },
                  {
                    label: `${langs.global.more[lang]} 6 ${langs.global.month[lang]}`,
                  },
                ]}
              />
            </div>
          </div>
          <div className="member-filter__category">
            <div className="member-filter__header">
              <h3>{langs.global.category[lang]}</h3>
              {activeCategory ? (
                <CButtonIcon
                  onclick={handelActiveCategory}
                  icon={<ArrowUp2 />}
                />
              ) : (
                <CButtonIcon
                  onclick={handelActiveCategory}
                  icon={<ArrowDown2 />}
                />
              )}
            </div>
            <div
              className={`member-categoru__main ${
                activeCategory ? "active" : ""
              }`}
            >
              <CButton
                onClick={() => {
                  handleClick("button1");
                  setCategory("ALL");
                  dispatch(FilterMemberSlice.actions.Category("ALL"));
                }}
                variant="outlined"
                className={selectedButton === "button1" ? "selected" : ""}
                // text={langs.global.dev[lang]}
                text={lang === "en" ? "ALL" : "Tất cả"}
              />
              {loading ? (
                <CLoading />
              ) : (
                positions.map((data, index) => (
                  <CButton
                    onClick={() => {
                      handleClick("button" + (index + 2));
                      setCategory(data.positionName);
                      dispatch(
                        FilterMemberSlice.actions.Category(data.positionName)
                      );
                    }}
                    key={index}
                    variant="outlined"
                    className={
                      selectedButton === "button" + (index + 2)
                        ? "selected"
                        : ""
                    }
                    // text={langs.global.dev[lang]}
                    text={data.positionName}
                  />
                ))
              )}
            </div>
          </div>
        </div>
        <div className="member-list">
          <div className="member-list__result">
            <div className="member-result">
              <h3>{langs.global.total[lang]}:</h3>
              <span>
                {employeesConfirm.length} {langs.global.staff[lang]}
              </span>
            </div>
            <div className="member-result">
              <h3>{langs.global.lastupdate[lang]}:</h3>
              <span>3 {langs.global.minago[lang]}</span>
            </div>
          </div>
          <div className="member-list__main">
            {listmember.length === 0 ? (
              <NotFoundValue />
            ) : (
              <div className="member-main__user">
                {listmember.map((member, index) => (
                  <ItemMember
                    isChosse={
                      employeeChosse.length > 0 &&
                      employeeChosse[0].employees.some(
                        (employee) => employee.id === member.id
                      )
                    }
                    employeeChosse={employeeChosse}
                    lang={lang}
                    member={member}
                    key={index}
                  />
                ))}
              </div>
            )}
            {listmember.length !== 0 && (
              <div className="member-main__pagination">
                <CPagination
                  SetPagination={SetPagination}
                  count={
                    employeesConfirm.length <= 12
                      ? 1
                      : Math.ceil(employeesConfirm.length / 12)
                  }
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Member;
