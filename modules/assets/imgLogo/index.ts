const imageLogo = {
  logo: [
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981420/KLTN/1_keqrwy.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981419/KLTN/1_si1drp.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981417/KLTN/3_osxqvq.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981417/KLTN/3_qubp7n.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981417/KLTN/2_ph0kix.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981417/KLTN/2_byx1uj.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/4_oywuii.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/4_yueixg.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981698/KLTN/5_cgxxhm.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/5_k2uxog.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/6_suiej1.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/6_xjfj3z.gif",
    },
  ],
  logoGif: [
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981420/KLTN/1_keqrwy.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981419/KLTN/1_b_wmrj43.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981417/KLTN/3_osxqvq.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981417/KLTN/3_b_fiy9jn.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981417/KLTN/2_ph0kix.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981417/KLTN/2_b_cygppi.gif",
    },

    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/4_oywuii.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/4_b_bskvzc.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981698/KLTN/5_cgxxhm.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/5_b_mqtbjs.gif",
    },
    {
      logo: "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/6_suiej1.png",
      logoGif:
        "https://res.cloudinary.com/hire-it/image/upload/v1679981418/KLTN/6_b_tggwed.gif",
    },
  ],
};
export default imageLogo;
