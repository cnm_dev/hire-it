import { JobPosting, PostingSave } from "@/types/models";
import React, { useState } from "react";
import NotFoundValue from "../NotFoundValue";
import PostItem from "./component/PostItem";
import CPagination from "@/common/components/controls/CPagination";
import { TypeLang } from "../Home/components/HotMember";
import CButton from "@/common/components/controls/CButton";

type HomePostingProps = {
  lang: TypeLang["lang"];
  postingSave: PostingSave[];
};
const SavePosting = ({ postingSave, lang }: HomePostingProps) => {
  const [pagination, SetPagination] = useState<number>(1);
  const [listPost, setListPost] = useState(
    postingSave && postingSave[0] && postingSave[0].job_postings.slice(6)
  );

  React.useEffect(() => {
    setListPost(
      postingSave &&
        postingSave[0] &&
        postingSave[0].job_postings.slice((pagination - 1) * 6, pagination * 6)
    );
  }, [postingSave, pagination]);

  return (
    <div>
      {postingSave && !postingSave[0] ? (
        <NotFoundValue />
      ) : postingSave[0] && postingSave[0].job_postings.length === 0 ? (
        <NotFoundValue />
      ) : (
        postingSave[0] && (
          <div className="home-posting">
            <div className="home-posting__header">
              <h3>Posting Save</h3>
              <CButton text={`${postingSave[0].job_postings.length} posting`} />
            </div>
            <div className="posting-list">
              <div className="posting-list__container">
                {listPost.map((postItem, index) => (
                  <PostItem key={index} postItem={postItem} lang={lang} />
                ))}
              </div>
              <div className="posting-list__pagination">
                <CPagination
                  SetPagination={SetPagination}
                  count={
                    postingSave.length <= 6
                      ? 1
                      : Math.ceil(postingSave.length / 6)
                  }
                />
              </div>
            </div>
          </div>
        )
      )}
    </div>
  );
};

export default SavePosting;
