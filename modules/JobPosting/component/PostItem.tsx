/* eslint-disable @next/next/no-img-element */
import CButton from "@/common/components/controls/CButton";
import { JobPosting } from "@/types/models";
import { intervalTime, timeLeaft, timeRemain } from "@/utils";
import Link from "next/link";
import React from "react";

interface PostItem {
  lang: "en" | "vn";
  postItem: JobPosting;
}

const PostItem = ({ postItem, lang }: PostItem) => {

  const getTime = (time: string) => {
    if (time.indexOf("s lefts") > 0) {
      return time.replace("s lefts", "giây trước");
    } else if (time.indexOf("minutes lefts") > 0) {
      return time.replace("minutes lefts", "phút trước");
    } else if (time.indexOf("hour lefts") > 0) {
      return time.replace("hour lefts", "giờ trước");
    } else if (time.indexOf("day lefts") > 0) {
      return time.replace("day lefts", "ngày trước");
    }
  };

  const getTimeCreate = (time: string) => {
    if (time.indexOf("Just recently") > 0) {
      return time.replace("Just recently", "Mới gần đây");
    } else if (time.indexOf("minutes ago") > 0) {
      return time.replace("minutes ago", "phút trước");
    } else if (time.indexOf("hour ago") > 0) {
      return time.replace("hour ago", "giờ trước");
    } else if (time.indexOf("day ago") > 0) {
      return time.replace("day ago", "ngày trước");
    }
  };

  return (
    <Link
      href={{
        pathname: "/job-posting/[postId]",
        query: { postId: postItem.id },
      }}
      className={`post-item ${
        timeRemain(postItem.timeRemain) < 0 ? "remain" : ""
      }`}
    >
      <div className="post-item__infor">
        <img src={postItem.image} alt="" />
        <div className="post-infor__container">
          <p>{postItem.jobPostingName} </p>
          <h4>{postItem.companyName}</h4>

          <div className="post-des__list">
            <CButton
              text={
                (lang === "en" ? "Wage" : "Tiền lương") + `: ${postItem.wage}$`
              }
            />
            <CButton
              text={
                (lang === "en" ? "Quantity" : "Số lượng") +
                `: ${postItem.quantity} ${lang === "en" ? "people" : "người"}`
              }
            />
            <CButton
              text={
                (lang === "en" ? "Exp" : "Số năm kinh nghiệm") +
                `: ${postItem.experience}`
              }
            />
            <CButton text={postItem.timework} />
          </div>
          <div className="post-des__list">
            <CButton text={postItem.email} />
            <CButton
              text={
                timeRemain(postItem.timeRemain) < 0
                  ? lang === "en"
                    ? "Expired"
                    : "Hết hạn"
                  : lang === "en"
                  ? timeLeaft(postItem.timeRemain)
                  : getTime(timeLeaft(postItem.timeRemain))
              }
            />
          </div>
        </div>
      </div>
      <div className="post-item__des">
        <p>
          {lang === "en" ? "Over" : "Hơn"} {postItem.wage}$
        </p>
        <span>
          {lang === "en"
            ? intervalTime(postItem.createdAt)
            : getTimeCreate(intervalTime(postItem.createdAt))}
        </span>
      </div>
    </Link>
  );
};

export default PostItem;
