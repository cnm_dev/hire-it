import CButtonIcon from "@/common/components/controls/CButtonIcon";
import CTextField from "@/common/components/controls/CTextField";
import { Add, Minus } from "iconsax-react";
import React from "react";
import { FieldErrors, FieldValues, UseFormRegister } from "react-hook-form";

type FormRequirementsProps = {
  register: UseFormRegister<FieldValues>;
  arrListFiedReq: string[];
  handelAddListFieldReq: () => void;
  handelRemoveListFieldReq: (id: string) => void;
  errors: FieldErrors<FieldValues>;
};

const FormRequirements = ({
  register,
  arrListFiedReq,
  handelAddListFieldReq,
  handelRemoveListFieldReq,
  errors,
}: FormRequirementsProps) => {
  return (
    <div className="form_step form-about">
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>Candidate Requirements</h4>
          <span>
            Describe the job requirements that your company needs to hire
          </span>
        </div>
        <div className="form-about__text">
          {arrListFiedReq.map((id) => (
            <div className="fieldItem_form" key={id}>
              <div className="fieldItem">
                <CTextField
                  registerName={{
                    ...register(`req_${id}`, { required: true }),
                  }}
                  name={`req_${id}`}
                  placeholder="Enter your Candidate Requirements"
                />
                <CButtonIcon
                  onclick={() => handelRemoveListFieldReq(id)}
                  icon={<Minus size="20" color="red" />}
                />
              </div>
              {errors[`req_${id}`]?.type === "required" && (
                <p role="alert">* Candidate Requirements is required</p>
              )}
            </div>
          ))}
          <div className="fieldItem">
            <CButtonIcon
              onclick={handelAddListFieldReq}
              icon={<Add size="20" color="green" />}
            />
            <span>Add new field Candidate Requirements</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormRequirements;
