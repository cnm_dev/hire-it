import image from "@/common/assets/image/all-image";
import AvatarDropzone from "@/common/components/controls/CDropzoneImg";
import CTextField from "@/common/components/controls/CTextField";
import React from "react";
import {
  Control,
  Controller,
  FieldErrors,
  FieldValues,
  UseFormRegister,
} from "react-hook-form";

type FormContactProps = {
  register: UseFormRegister<FieldValues>;
  control: Control<FieldValues, any>;
  errors: FieldErrors<FieldValues>;
};

const FormContact = ({ register, control, errors }: FormContactProps) => {
  return (
    <div className="form_step form-about">
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>Email company/ you</h4>
          <span>
            This email will be the place for candidates to contact you
          </span>
        </div>
        <div className="form-about__text">
          <CTextField
            type="email"
            registerName={{
              ...register("email", { required: true }),
            }}
            name="email"
            placeholder="Enter your Email"
          />
          {errors.email?.type === "required" && (
            <p role="alert">* Email is required</p>
          )}
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>Phone company/ you</h4>
          <span>
            This phone will be the place for candidates to contact you
          </span>
        </div>
        <div className="form-about__text">
          <CTextField
            registerName={{
              ...register("phone", { required: true }),
            }}
            name="phone"
            placeholder="Enter your Phone "
          />
          {errors.phone?.type === "required" && (
            <p role="alert">* Phone is required</p>
          )}
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>Image review company</h4>
          <span>
            You can post pictures to introduce your company. <br />
          </span>
        </div>
        <div className="form-about__text upload_img">
          <div className="avatar-img__upload">
            <Controller
              control={control}
              name="image"
              rules={{ required: true }}
              render={({ field }) => (
                <AvatarDropzone
                  imgDefult={
                    field.value
                      ? URL.createObjectURL(field.value)
                      : image.upload
                  }
                  onDrop={(file: any) => field.onChange(file)}
                />
              )}
            />
          </div>
          {errors.image?.type === "required" && (
            <p role="alert">* Image is required</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default FormContact;
