import CRadioButton from "@/common/components/controls/CRadioButton";
import CTextField from "@/common/components/controls/CTextField";
import { langs } from "@/mock/langs";
import React, { useState } from "react";
import {
  Control,
  Controller,
  FieldErrors,
  FieldValues,
  UseFormRegister,
} from "react-hook-form";

const genderRadio = [
  { label: "Male" },
  { label: "Female" },
  { label: "Not required" },
];

const timeWorkingRadio = [{ label: "Full time" }, { label: "Part time" }];

type FormGeneralProps = {
  register: UseFormRegister<FieldValues>;
  control: Control<FieldValues, any>;
  errors: FieldErrors<FieldValues>;
  lang: "en" | "vn";
};
const FormGeneral = ({ errors, register, control, lang }: FormGeneralProps) => {
  return (
    <div className="form_step form-about">
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.paymentAbout[lang]} ($)</h4>
          <span>{langs.posting.paymentAboutDes[lang]}</span>
        </div>
        <div className="form-about__text">
          <CTextField
            registerName={{
              ...register("pay", { required: true }),
            }}
            name="pay"
            type="number"
            placeholder={langs.posting.paymentAboutPlace[lang]}
          />
          {errors.pay?.type === "required" && (
            <p role="alert">* {langs.posting.paymentAboutError[lang]}</p>
          )}
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.recruitmentAbout[lang]}</h4>
          <span>{langs.posting.recruitmentAboutDes[lang]}</span>
        </div>
        <div className="form-about__text">
          <CTextField
            registerName={{
              ...register("number", { required: true }),
            }}
            name="number"
            type="number"
            placeholder={langs.posting.recruitmentAboutOlace[lang]}
          />
          {errors.number?.type === "required" && (
            <p role="alert">* {langs.posting.recruitmentAboutError[lang]}</p>
          )}
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.gendertAbout[lang]}</h4>
          <span>{langs.posting.gendertAboutDes[lang]}</span>
        </div>
        <div className="form-about__text">
          {genderRadio.map((gender, index) => (
            <Controller
              key={index}
              control={control}
              name="gender"
              rules={{ required: true }}
              render={({ field }) => (
                <CRadioButton
                  key={index}
                  onChange={(event) => field.onChange(event.target.value)}
                  setState={field.onChange}
                  state={field.value}
                  label={gender.label}
                  item={gender.label}
                />
              )}
            />
          ))}
          {errors.gender?.type === "required" && (
            <p role="alert">* {langs.posting.gendertAboutError[lang]}</p>
          )}
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.experienceAbout[lang]}</h4>
          <span>{langs.posting.experienceAboutDes[lang]}</span>
        </div>
        <div className="form-about__text">
          <CTextField
            registerName={{
              ...register("exp", { required: true }),
            }}
            name="exp"
            placeholder={langs.posting.experienceAboutPlace[lang]}
          />
          {errors.exp?.type === "required" && (
            <p role="alert">* {langs.posting.experienceAboutError[lang]}</p>
          )}
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.workTimeAbout[lang]}</h4>
          <span>{langs.posting.workTimeAboutDes[lang]}</span>
        </div>
        <div className="form-about__text">
          {timeWorkingRadio.map((time, index) => (
            <Controller
              key={index}
              control={control}
              name="time"
              rules={{ required: true }}
              render={({ field }) => (
                <CRadioButton
                  key={index}
                  onChange={(event) => field.onChange(event.target.value)}
                  setState={field.onChange}
                  state={field.value}
                  label={time.label}
                  item={time.label}
                />
              )}
            />
          ))}
          {errors.time?.type === "required" && (
            <p role="alert">* {langs.posting.workTimeAboutError[lang]}</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default FormGeneral;
