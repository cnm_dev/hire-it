import CButtonIcon from "@/common/components/controls/CButtonIcon";
import CTextField from "@/common/components/controls/CTextField";
import { Add, Minus } from "iconsax-react";
import React from "react";
import { FieldErrors, FieldValues, UseFormRegister } from "react-hook-form";

type FormDescriptionProps = {
  register: UseFormRegister<FieldValues>;
  arrListFiedADes: string[];
  handelAddListFieldDes: () => void;
  handelRemoveListFieldDes: (id: string) => void;
  errors: FieldErrors<FieldValues>;
};
const FormDescription = ({
  register,
  arrListFiedADes,
  handelAddListFieldDes,
  handelRemoveListFieldDes,
  errors,
}: FormDescriptionProps) => {
  return (
    <div className="form_step form-about">
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>Job description</h4>
          <span>Describe what you will do while working at the company</span>
        </div>
        <div className="form-about__text">
          {arrListFiedADes.map((id: string) => (
            <div className="fieldItem_form" key={id}>
              <div className="fieldItem">
                <CTextField
                  registerName={{
                    ...register(`des_${id}`, { required: true }),
                  }}
                  name={`des_${id}`}
                  placeholder="Enter your Job description"
                />
                <CButtonIcon
                  onclick={() => handelRemoveListFieldDes(id)}
                  icon={<Minus size="20" color="red" />}
                />
              </div>
              {errors[`des_${id}`]?.type === "required" && (
                <p role="alert">* Job description is required</p>
              )}
            </div>
          ))}
          <div className="fieldItem">
            <CButtonIcon
              onclick={handelAddListFieldDes}
              icon={<Add size="20" color="green" />}
            />
            <span>Add new field Job description</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormDescription;
