import CTextField from "@/common/components/controls/CTextField";
import DatePicker from "react-multi-date-picker";
import React from "react";
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import { Add, Minus } from "iconsax-react";
import {
  Control,
  Controller,
  FieldErrors,
  FieldValues,
  UseFormRegister,
} from "react-hook-form";
import { langs } from "@/mock/langs";
import { useSession } from "next-auth/react";

type FormAbout = {
  register: UseFormRegister<FieldValues>;
  arrListFiedAbout: string[];
  handelAddListFieldAbout: () => void;
  handelRemoveListFieldAbout: (id: string) => void;
  control: Control<FieldValues, any>;
  errors: FieldErrors<FieldValues>;
  lang: "en" | "vn";
};

const FormAbout = ({
  register,
  arrListFiedAbout,
  handelAddListFieldAbout,
  handelRemoveListFieldAbout,
  control,
  errors,
  lang,
}: FormAbout) => {
  const { data: session } = useSession();
  return (
    <div className="form_step form-about">
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.companyName[lang]}</h4>
          <span>{langs.posting.companyAbout[lang]}</span>
        </div>
        <div className="form-about__text">
          <CTextField
            defaultValue={session?.user?.customer.companyName}
            registerName={{ ...register("companyName", { required: true }) }}
            name="companyName"
            placeholder={langs.posting.placeholderInputCompanyName[lang]}
          />
          {errors.companyName?.type === "required" && (
            <p role="alert">* {langs.posting.errorInputCompanyName[lang]}</p>
          )}
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.jobTitle[lang]}</h4>
          <span>{langs.posting.jobTitleAbout[lang]}</span>
        </div>
        <div className="form-about__text">
          <CTextField
            registerName={{ ...register("title", { required: true }) }}
            name="title"
            placeholder={langs.posting.placeholderInputjobTitle[lang]}
          />
          {errors.title?.type === "required" && (
            <p role="alert">* {langs.posting.errorInputjobTitle[lang]}</p>
          )}
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.expiredTime[lang]}</h4>
          <span>{langs.posting.expiredAbout[lang]}</span>
        </div>
        <div className="form-about__text">
          <Controller
            control={control}
            name="date-input"
            rules={{ required: true }}
            render={({ field }) => (
              <DatePicker
                minDate={new Date()}
                value={field.value || field.onChange(new Date())}
                onChange={(date) => field.onChange(date?.toString())}
              />
            )}
          />
        </div>
      </div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.aboutCompany[lang]}</h4>
          <span>{langs.posting.aboutCompanyDes[lang]}</span>
        </div>
        <div className="form-about__text">
          {arrListFiedAbout.map((id) => (
            <div className="fieldItem_form" key={id}>
              <div className="fieldItem">
                <CTextField
                  registerName={{
                    ...register(`about_${id}`, { required: true }),
                  }}
                  name={`about_${id}`}
                  placeholder={langs.posting.placeholderInputAboutCompany[lang]}
                />
                <CButtonIcon
                  onclick={() => handelRemoveListFieldAbout(id)}
                  icon={<Minus size="20" color="red" />}
                />
              </div>
              {errors[`about_${id}`]?.type === "required" && (
                <p role="alert">
                  * {langs.posting.errorInputAboutCompany[lang]}
                </p>
              )}
            </div>
          ))}
          <div className="fieldItem">
            <CButtonIcon
              onclick={handelAddListFieldAbout}
              icon={<Add size="20" color="green" />}
            />
            <span>
              {lang === "en"
                ? "Add new field about company"
                : "Thêm mô tả về công ty"}
            </span>
          </div>
        </div>
      </div>
      <div className="line-separator"></div>
      <div className="form-about__field">
        <div className="form-about__title">
          <h4>{langs.posting.aboutLocationCompany[lang]}</h4>
          <span>{langs.posting.aboutLocationCompanyDes[lang]}</span>
        </div>
        <div className="form-about__text">
          <CTextField
            registerName={{
              ...register("location", { required: true }),
            }}
            name="location"
            placeholder={langs.posting.placeholderaboutLocationCompany[lang]}
          />
          {errors.location?.type === "required" && (
            <p role="alert">
              * {langs.posting.erroraboutLocationCompany[lang]}
            </p>
          )}
        </div>
      </div>
    </div>
  );
};

export default FormAbout;
