import { all_video } from "@/common/assets/videos";
import React from "react";

const FormDone = () => {
  return (
    <div className="form_step form-about form-done">
      <video src={all_video.post_done} muted autoPlay loop></video>
      <h4>You complete form Job Posting</h4>
      <span>
        Now press the post button to post or the back button to return to
        editing <br /> the content of the post
      </span>
    </div>
  );
};

export default FormDone;
