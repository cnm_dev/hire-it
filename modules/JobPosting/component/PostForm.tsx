import CButton from "@/common/components/controls/CButton";
import CSteper from "@/common/components/controls/CSteper";
import { randomString } from "@/utils";
import React, { useState } from "react";
import FormAbout from "./form/FormAbout";
import FormContact from "./form/FormContact";
import FormDescription from "./form/FormDescription";
import FormGeneral from "./form/FormGeneral";
import FormRequirements from "./form/FormRequirements";
import FormDone from "./form/FormDone";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { createJobPosting } from "@/apis/JobPosting";
import { useSession } from "next-auth/react";
import { mutate } from "swr";
import { Customer } from "@/types/models";
import { uploadImage } from "@/services/image";
import CSpinner from "@/common/components/controls/CSpinner";
import { createInformates } from "@/apis/Informates";

type TypePostFormProps = {
  setIsOpenModal: React.Dispatch<React.SetStateAction<boolean>>;
  customer: Customer;
  lang: "en" | "vn";
};

const PostForm = ({ setIsOpenModal, customer, lang }: TypePostFormProps) => {
  const { data: session } = useSession();

  const [loadingPost, setLoadingPost] = useState<boolean>(false);
  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm();
  const [number, setNumber] = useState<number>(-1);
  const [initialStateCompleted] = useState<{
    [k: number]: boolean;
  }>({});

  // state arrList
  const [listIdFieldAbout, setListIdFieldAbout] = useState<string[]>(
    Array.from({ length: 3 }, () => randomString(5))
  );
  const [listIdFieldDes, setListIdFieldDes] = useState<string[]>(
    Array.from({ length: 3 }, () => randomString(5))
  );
  const [listIdFieldReq, setListIdFieldReq] = useState<string[]>(
    Array.from({ length: 5 }, () => randomString(5))
  );

  /**
   * handel ADD and REMOVE item in array filed
   */
  const handelAddListFieldAbout = () => {
    if (listIdFieldAbout.length > 5) {
      toast.warning("Filed upload not more 6 item");
    } else {
      setListIdFieldAbout([...listIdFieldAbout, randomString(5)]);
    }
  };
  const handelRemoveListFieldAbout = (id: string) => {
    if (listIdFieldAbout.length < 2) {
      toast.warning("Filed upload not less 1 item");
    } else {
      setListIdFieldAbout(listIdFieldAbout.filter((idFiled) => idFiled !== id));
    }
  };
  const handelAddListFieldDes = () => {
    if (listIdFieldDes.length > 9) {
      toast.warning("Filed upload not more 10 item");
    } else {
      setListIdFieldDes([...listIdFieldDes, randomString(5)]);
    }
  };
  const handelRemoveListFieldDes = (id: string) => {
    if (listIdFieldDes.length < 4) {
      toast.warning("Filed upload not less 3 item");
    } else {
      setListIdFieldDes(listIdFieldDes.filter((idFiled) => idFiled !== id));
    }
  };
  const handelAddListFieldReq = () => {
    if (listIdFieldReq.length > 9) {
      toast.warning("Filed upload not more 10 item");
    } else {
      setListIdFieldReq([...listIdFieldReq, randomString(5)]);
    }
  };
  const handelRemoveListFieldReq = (id: string) => {
    if (listIdFieldReq.length < 4) {
      toast.warning("Filed upload not less 3 item");
    } else {
      setListIdFieldReq(listIdFieldReq.filter((idFiled) => idFiled !== id));
    }
  };

  /**
   *
   * @param value
   * @param id
   * @param arrField
   * @returns
   */
  const getValueArrField = (value: any, id: string, arrField: string[]) => {
    let valueArrField = "";
    arrField.map((idField, index) => {
      valueArrField +=
        (index !== 0 ? " \n " : "") + value[`${id}_${idField}`].trim();
    });
    return valueArrField;
  };

  /**
   *
   * @param value
   */
  const handelPostJob = (value: any) => {
    if (number < 4) {
      setNumber(number + 1);
      initialStateCompleted[number + 1] = true;
    } else {
      setLoadingPost(true);
      let image = "";
      uploadImage(
        value.image ? value.image : customer.users_permissions_user.avartar
      )
        .then((course) => {
          image = course.data.url;
          const requirement = getValueArrField(value, "req", listIdFieldReq);
          const description = getValueArrField(value, "des", listIdFieldDes);
          const aboutCompany = getValueArrField(
            value,
            "about",
            listIdFieldAbout
          );
          const dataJobPost = {
            jobPostingName: value.title,
            description,
            companyName: value.companyName,
            wage: Number(value.pay),
            requirement,
            aboutCompany,
            timeRemain: new Date(value["date-input"]),
            email: value.email,
            experience: value.exp,
            gender: value.gender,
            location: value.location,
            quantity: Number(value.number),
            phone: value.phone,
            timework: value.time,
            customerId: customer.id,
            image,
          };
          session &&
            createJobPosting({ ...dataJobPost })
              .then(() => {
                createInformates({
                  description: `Job Posting ${value.title} has been sent to admin.`,
                  title: "Post job",
                  route: "/job-posting",
                  customerId: customer.id,
                })
                  .then(() => {
                    createInformates({
                      description: `Just got a job posting from companay is ${value.companyName}.`,
                      title: "Post job",
                      route: "/manager/job-posting",
                      adminId: "1",
                    }).then(() => {
                      mutate("/api/job-postings");
                      toast.success("Post jop success");
                      setIsOpenModal(false);
                      setLoadingPost(false);
                    });
                  })
                  .catch((err) => {
                    toast.error(err.message);
                    setLoadingPost(false);
                  });
              })
              .catch((err) => {
                toast.error(err.message);
                setLoadingPost(false);
              });
        })
        .catch((err) => {
          toast.error(err.message);
          setLoadingPost(false);
        });
    }
  };

  return (
    <div className="post-form">
      <form onSubmit={handleSubmit(handelPostJob)}>
        <CSteper
          steps={
            lang === "en"
              ? [
                  { title: "About" },
                  { title: "General" },
                  { title: "Description" },
                  { title: "Requirements" },
                  { title: "Contact" },
                ]
              : [
                  { title: "Giới thiệu" },
                  { title: "Tổng quan" },
                  { title: "Mô tả" },
                  { title: "Yêu cầu" },
                  { title: "Hợp đồng" },
                ]
          }
          initialState={number + 1}
          initialStateCompleted={initialStateCompleted}
        />
        <div className="post-form__container">
          {number === -1 && (
            <FormAbout
              errors={errors}
              register={register}
              arrListFiedAbout={listIdFieldAbout}
              control={control}
              handelAddListFieldAbout={handelAddListFieldAbout}
              handelRemoveListFieldAbout={handelRemoveListFieldAbout}
              lang={lang && lang}
            />
          )}
          {number === 0 && (
            <FormGeneral
              errors={errors}
              register={register}
              control={control}
              lang={lang && lang}
            />
          )}
          {number === 1 && (
            <FormDescription
              errors={errors}
              register={register}
              arrListFiedADes={listIdFieldDes}
              handelAddListFieldDes={handelAddListFieldDes}
              handelRemoveListFieldDes={handelRemoveListFieldDes}
              // lang={lang && lang}
            />
          )}
          {number === 2 && (
            <FormRequirements
              errors={errors}
              register={register}
              arrListFiedReq={listIdFieldReq}
              handelAddListFieldReq={handelAddListFieldReq}
              handelRemoveListFieldReq={handelRemoveListFieldReq}
              // lang={lang && lang}
            />
          )}
          {number === 3 && (
            <FormContact
              errors={errors}
              control={control}
              register={register}
              // lang={lang && lang}
            />
          )}
          {number === 4 && <FormDone />}
        </div>
        <div className="post-form__btn">
          <CButton
            disabled={number < 0}
            onClick={() => {
              setNumber(number - 1);
              initialStateCompleted[number] = false;
            }}
            text={lang === "en" ? "Back" : "Quay lại"}
          />
          <CButton
            type="submit"
            disabled={loadingPost}
            text={
              loadingPost ? (
                <CSpinner size="small" />
              ) : number >= 4 ? (
                lang === "en" ? (
                  "Post"
                ) : (
                  "Đăng tin"
                )
              ) : lang === "en" ? (
                "Next"
              ) : (
                "Tiếp tục"
              )
            }
          />
        </div>
      </form>
    </div>
  );
};

export default PostForm;
