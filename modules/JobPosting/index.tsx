import CAutocomplete from "@/common/components/controls/CAutocomplete";
import CButton from "@/common/components/controls/CButton";
import CTextField from "@/common/components/controls/CTextField";
import {
  Add,
  CardAdd,
  Chart21,
  Heart,
  SearchNormal1,
  SmartHome,
} from "iconsax-react";
import { all_api } from "../../mock/all_api";
import React, { useEffect, useState } from "react";
import PostItem from "./component/PostItem";
import CPagination from "@/common/components/controls/CPagination";
import { TypeLang } from "../Home/components/HotMember";
import { langs } from "@/mock/langs";
import useJobPostings from "@/hooks/useJobPostings";
import CLoading from "@/common/components/controls/CLoading";
import { useDispatch } from "react-redux";
import JobPostingSlice from "@/redux/Slices/JobPostingSlice";
import JobPostingFilterSlice from "@/redux/Slices/JobPostingFilterSlice";
import { useSession } from "next-auth/react";
import CModal from "@/common/components/controls/CModal";
import PostForm from "./component/PostForm";
import { Customer, JobPosting } from "@/types/models";
import NotFoundValue from "../NotFoundValue";
import CDropMenu from "@/common/components/controls/CDropMenu";
import Link from "next/link";

type PostingProps = {
  lang: TypeLang["lang"];
  customer: Customer[];
};

const Posting = ({ lang, customer }: PostingProps) => {
  const [pagination, SetPagination] = React.useState<number>(1);
  const { jobpostings: jobpostingsOrigin, loading } = useJobPostings();
  const [jobpostings, setJobpostings] = useState<JobPosting[]>([]);
  const [listPost, setListPost] = React.useState(jobpostings.slice(6));
  const { data: session, status } = useSession();
  const [valueInput, setValueInput] = useState<string>("");
  const [valueOption, setValueOption] = useState<string>("");
  const dispatch = useDispatch();
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);

  useEffect(() => {
    if (!loading && jobpostingsOrigin) {
      setJobpostings(
        jobpostingsOrigin.filter(
          (jobposting) => jobposting.status === "confirm"
        )
      );
    }
  }, [loading, jobpostingsOrigin]);

  React.useEffect(() => {
    setListPost(jobpostings.slice((pagination - 1) * 6, pagination * 6));
    dispatch(JobPostingSlice.actions.listJobPosting(jobpostings));
    dispatch(JobPostingFilterSlice.actions.listPostingFilter(jobpostings));
  }, [jobpostings, pagination, dispatch]);

  useEffect(() => {
    if (valueOption) {
      valueOption === "All" &&
        setJobpostings(
          jobpostingsOrigin.filter(
            (jobposting) => jobposting.status === "confirm"
          )
        );
      valueOption === "Latest" &&
        setJobpostings(
          jobpostings
            .slice()
            .sort(
              (a, b) =>
                Number(new Date(a.createdAt)) - Number(new Date(b.createdAt))
            )
        );
      valueOption === "Most recent" &&
        setJobpostings(
          jobpostings
            .slice()
            .sort(
              (a, b) =>
                -Number(new Date(a.createdAt)) + Number(new Date(b.createdAt))
            )
        );
    }
  }, [jobpostings, jobpostingsOrigin, valueOption]);

  const handelSearch = () => {
    if (valueInput == "") {
      setJobpostings(
        jobpostingsOrigin.filter(
          (jobposting) => jobposting.status === "confirm"
        )
      );
    } else if (valueInput !== "") {
      setJobpostings(
        jobpostingsOrigin
          .filter((jobposting) => jobposting.status === "confirm")
          .filter(
            (jobposting) =>
              jobposting.companyName.includes(valueInput) ||
              jobposting.jobPostingName.includes(valueInput)
          )
      );
    }
  };
  return (
    <div className="posting">
      {loading ? (
        <CLoading fullScreen />
      ) : (
        <>
          <div className="posting-header">
            <h3>{langs.posting.jobpostL[lang]}</h3>
            <div className="posting-header__statis">
              <CButton
                text={`${jobpostings.length}+ ${langs.posting.posting[lang]}`}
              />
              <span>
                {jobpostings.length}+ {langs.posting.posting[lang]} /{" "}
                {langs.global.day[lang]}
              </span>
            </div>
          </div>
          <div className="posting-container">
            <div className="posting-filter">
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  gap: 10,
                  justifyContent: "space-between",
                }}
              >
                <div
                  style={{
                    width: "100%",
                    display: "flex",
                    gap: 10,
                  }}
                >
                  <div className="posting-filter__search">
                    <SearchNormal1 size="20" color="#9D9BB9" />
                    <CTextField
                      onChange={(e) => {
                        setValueInput(e.target.value);
                      }}
                      placeholder={langs.posting.placesearch[lang]}
                    />
                  </div>
                  <CButton
                    onClick={handelSearch}
                    text={lang === "vn" ? "Tìm việc ngay" : "Apply"}
                    color="success"
                    className="button-search"
                  />
                </div>
                {status === "authenticated" && (
                  <CDropMenu
                    arrMenu={
                      session.user.type_user === "Customer"
                        ? [
                            {
                              menu: true,
                              title: (
                                <CModal
                                  confirm={true}
                                  isOpenModal={isOpenModal}
                                  setIsOpenModal={setIsOpenModal}
                                  button={
                                    lang === "en" ? "Post job" : "Đăng tin"
                                  }
                                  modal={
                                    <PostForm
                                      setIsOpenModal={setIsOpenModal}
                                      customer={customer[0]}
                                      lang={lang}
                                    />
                                  }
                                />
                              ),
                              icon: <CardAdd size="24" color="#9f9c9c" />,
                            },
                            {
                              title: "My Postings",
                              href: "/job-posting/home",
                              icon: <SmartHome size="24" color="#9f9c9c" />,
                            },
                            {
                              title: (
                                <Link href="/job-posting/save">Post save</Link>
                              ),
                              icon: <Heart size="24" color="#9f9c9c" />,
                            },
                          ]
                        : [
                            {
                              title: (
                                <Link href="/job-posting/save">Post save</Link>
                              ),
                              icon: <Heart size="24" color="#9f9c9c" />,
                            },
                          ]
                    }
                    button={
                      <CButton
                        className="btn-jobPosting"
                        text={lang === "en" ? "Post job" : "Đăng tin"}
                      />
                    }
                  />
                )}
              </div>

              <div className="posting-filter__time">
                <Chart21
                  className="icon-time__post"
                  size="25"
                  color="#9D9BB9"
                  variant="Bold"
                />
                <span>{langs.global.sortby[lang]}</span>
                <CAutocomplete
                  onChange={(e, v) => {
                    setValueOption(v.label);
                  }}
                  option={[
                    { label: langs.global.all[lang] },
                    { label: langs.global.mostrecent[lang] },
                    { label: langs.global.latest[lang] },
                  ]}
                />
              </div>
            </div>
            {jobpostings.length === 0 ? (
              <NotFoundValue />
            ) : (
              <div className="posting-list">
                <div className="posting-list__container">
                  {listPost.map((postItem, index) => (
                    <PostItem key={index} postItem={postItem} lang={lang} />
                  ))}
                </div>
                <div className="posting-list__pagination">
                  <CPagination
                    SetPagination={SetPagination}
                    count={
                      jobpostingsOrigin.filter(
                        (jobposting) => jobposting.status === "confirm"
                      ).length <= 6
                        ? 1
                        : Math.ceil(
                            jobpostingsOrigin.filter(
                              (jobposting) => jobposting.status === "confirm"
                            ).length / 6
                          )
                    }
                  />
                </div>
              </div>
            )}
          </div>
        </>
      )}
    </div>
  );
};

export default Posting;
