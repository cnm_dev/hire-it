import { JobPosting } from "@/types/models";
import React, { useState } from "react";
import NotFoundValue from "../NotFoundValue";
import PostItem from "./component/PostItem";
import CPagination from "@/common/components/controls/CPagination";
import { TypeLang } from "../Home/components/HotMember";
import CButton from "@/common/components/controls/CButton";

type HomePostingProps = {
  lang: TypeLang["lang"];
  jobpostings: JobPosting[];
};
const HomePosting = ({ jobpostings, lang }: HomePostingProps) => {
  const [pagination, SetPagination] = useState<number>(1);
  const [listPost, setListPost] = useState(jobpostings.slice(6));

  React.useEffect(() => {
    setListPost(jobpostings.slice((pagination - 1) * 6, pagination * 6));
  }, [jobpostings, pagination]);

  return (
    <div>
      {jobpostings.length === 0 ? (
        <NotFoundValue />
      ) : (
        <div className="home-posting">
          <div className="home-posting__header">
            <h3>My Posting</h3>
            <CButton text={`${jobpostings.length} posting`} />
          </div>
          <div className="posting-list">
            <div className="posting-list__container">
              {listPost.map((postItem, index) => (
                <PostItem key={index} postItem={postItem} lang={lang} />
              ))}
            </div>
            <div className="posting-list__pagination">
              <CPagination
                SetPagination={SetPagination}
                count={
                  jobpostings.length <= 6
                    ? 1
                    : Math.ceil(jobpostings.length / 6)
                }
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default HomePosting;
