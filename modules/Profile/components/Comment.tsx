/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import CRating from "@/common/components/controls/CRating";
import React from "react";

interface PropComment {
  comment: {
    name: string;
    company?: string;
    star: number;
    time: string;
    avatar: any;
    comment: string;
  };
}

const Comment = ({ comment }: PropComment) => {
  return (
    <div id="comment">
      <img src={comment.avatar} alt="" />
      <div className="comment-content">
        <h5>{comment.name}</h5>
        <span>{comment.company}</span>
        <div className="comment-content__star">
          <CRating readOnly rate={comment.star} />
          <h5 className="number-star">{comment.star}</h5>
          <p>
            <span>|</span> {comment.time}
          </p>
        </div>
        <p>{comment.comment}</p>
      </div>
    </div>
  );
};

export default Comment;
