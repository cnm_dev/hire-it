/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @next/next/no-img-element */
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import React, { useState } from "react";
// assets
import CButton from "@/common/components/controls/CButton";
import CLoading from "@/common/components/controls/CLoading";
import CPagination from "@/common/components/controls/CPagination";
import CRating from "@/common/components/controls/CRating";
import useEmployee from "@/hooks/useEmployee";
import { langs } from "@/mock/langs";
import { intervalTime } from "@/utils";
import {
  ArrowDown2,
  ArrowUp2,
  Award,
  BrifecaseTimer,
  Clipboard,
  DirectboxNotif,
  EmptyWallet,
  Heart,
  I3Dcube,
  LikeShapes,
  Teacher,
} from "iconsax-react";
import { RxDotFilled } from "react-icons/rx";
import { TypeLang } from "../Home/components/HotMember";
import Comment from "./components/Comment";
import image from "@/common/assets/image/all-image";
import { averageStar } from "@/constants/employee";

interface ProfileProps {
  lang: TypeLang["lang"];
  profileId: string;
}

const Profile = ({ lang, profileId }: ProfileProps) => {
  const { employees, loading } = useEmployee({ profileId: profileId });
  const [activeHistory, setActiveHistory] = useState<boolean>(true);
  const [activeEducation, setActiveEducation] = useState<boolean>(true);
  const [activeSkill, setActiveSkill] = useState<boolean>(true);
  const [pagination, SetPagination] = useState<number>(1);
  const [listComment, setListComment] = useState<
    {
      name: string;
      company: string;
      star: number;
      time: string;
      avatar: string;
      comment: string;
    }[]
  >([]);
  const [since, setSince] = useState<string>("");

  React.useMemo(() => {
    if (employees[0]) {
      const t = new Date(employees[0]?.joinDate);
      setSince(t.getDate() + "/" + (t.getMonth() + 1) + "/" + t.getFullYear());
      if (employees[0].previews) {
        let temp: any[] = [];

        employees[0].previews.map((data) => {
          temp.push({
            name: data.customer.users_permissions_user.fullName,
            company: data.customer.companyName,
            star: data.rate,
            time: intervalTime(data.createdAt),
            avatar: data.customer.users_permissions_user.avartar,
            comment: data.content,
          });

          setListComment(temp.slice((pagination - 1) * 5, pagination * 5));
        });
      }
    }
  }, [employees, pagination]);

  const handelActiveInformation = (type: string) => {
    if (type === "history") {
      setActiveHistory(!activeHistory);
    } else if (type === "education") {
      setActiveEducation(!activeEducation);
    } else setActiveSkill(!activeSkill);
  };

  return (
    <>
      {loading ? (
        <CLoading fullScreen/>
      ) : (
        <div id="profile">
          <div className="profile-header">
            <div className="profile-header__left">
              <div className="header-left__avatar">
                <img
                  src={
                    employees[0].users_permissions_user.avartar
                      ? employees[0].users_permissions_user.avartar
                      : image.image_default
                  }
                  alt=""
                />
                {/* <LikeShapes size={30} variant="Bold" /> */}
              </div>
              <div className="header-leaft__infor">
                <div className="des">
                  <div className="name">
                    <p>{employees[0].users_permissions_user.fullName}</p>
                    <h5>
                      <Heart size="32" color="#ff7e82" variant="Bold" />{" "}
                      {employees[0].start}{" "}
                      <span>
                        ( {employees[0].previews?.length + " "} reviews )
                      </span>
                    </h5>
                  </div>
                  <div className="des-info">
                    <span className="position-since">
                      {employees[0].position.positionName} in{" "}
                      {employees[0].city}, {employees[0].country}
                    </span>
                    <RxDotFilled size={20} color="rgba(0,0,0,0.5)" />
                    <span className="position-since">Member since {since}</span>
                  </div>
                  <div className="introduce-yourself">
                    <span>
                      Mentored at Apple, Osandi has spent over ten years helping
                      teams create human-centered products that delight
                      customers. After a product role at Square, he led and
                      directed a remote team of five engineers, built and grew a
                      lifestyle brand, and cut his teeth as a designer in a
                      founder role. As one of Toptal's elites, he's changed the
                      way organizations solve problems for their users, as well
                      as produced world-class interfaces across B2B/B2C markets.
                      Osandi is currently innovating crypto fintech.
                    </span>
                  </div>
                </div>
                <div className="post-infor__list">
                  <div className="post-infor__item">
                    <EmptyWallet size={28} variant="Outline" />
                    <span>{employees[0].hourlyRate}$</span>
                  </div>

                  <div className="post-infor__item">
                    <Award size={28} variant="Outline" />
                    <span>Experience: {employees[0].yearsOfExperience}</span>
                  </div>
                  <div className="post-infor__item">
                    <BrifecaseTimer size={28} variant="Outline" />
                    <span>
                      {employees[0].isFullTime ? "All-time" : "Part-time"}
                    </span>
                  </div>
                  <div className="post-infor__item">
                    <DirectboxNotif size={20} variant="Outline" />
                    <span>{employees[0].users_permissions_user.email}</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className="status-work"
            style={{
              backgroundColor:
                employees[0].statusWork === "free"
                  ? "rgba(205,255,237,0.4)"
                  : "rgba(255,224,224,0.4)",
            }}
          >
            <p>
              {employees[0].users_permissions_user.fullName} is now
              <span
                style={{
                  color: employees[0].statusWork === "free" ? "#00cc83" : "red",
                }}
              >
                {employees[0].statusWork === "free"
                  ? " available "
                  : " unavailable "}
              </span>
              for hire
            </p>
          </div>

          <div className="profile-infor">
            <div className="profile-infor__skill">
              <div className="profile-infor__header">
                <I3Dcube size={28} /> <h4>{langs.profile.skill[lang]}</h4>{" "}
              </div>
              <div className="profile-skill">
                {employees[0].skills.map((skill, index) => (
                  <CButton key={index} text={skill.skillName} />
                ))}
              </div>
            </div>
            <div className="profile-infor__history">
              {employees[0].history_hire_employments &&
                employees[0].history_hire_employments.length > 0 && (
                  <>
                    <div className="profile-infor__header">
                      <Clipboard size={28} className="icon" />{" "}
                      <h4>{langs.profile.history[lang]}</h4>
                      <CButtonIcon
                        onclick={() => handelActiveInformation("history")}
                        icon={activeHistory ? <ArrowUp2 /> : <ArrowDown2 />}
                      />
                    </div>
                    <div
                      className={`profile-infor__context ${
                        !activeHistory ? "unactive" : ""
                      }`}
                    >
                      {employees[0].history_hire_employments?.map(
                        (data, index) => {
                          return (
                            <div className="profile-infor__content" key={index}>
                              <h4>{data.companyName}</h4>
                              <span>{data.workingPosition}</span>
                              <ul>
                                {data.descriptionJob &&
                                  data.descriptionJob
                                    .split("\n")
                                    .map((data, index) => (
                                      <li key={index}>{data}</li>
                                    ))}
                              </ul>
                              <span>Technologies: {data.technologie}</span>
                            </div>
                          );
                        }
                      )}
                    </div>
                  </>
                )}
            </div>
            <div
              className={`profile-infor__education ${
                activeEducation ? "active" : ""
              }`}
            >
              {employees[0].educations &&
                employees[0].educations.length > 0 && (
                  <>
                    <div className="profile-infor__header">
                      <Teacher size={28} />{" "}
                      <h4>{langs.profile.education[lang]}</h4>
                      <CButtonIcon
                        onclick={() => handelActiveInformation("education")}
                        icon={activeEducation ? <ArrowUp2 /> : <ArrowDown2 />}
                      />
                    </div>
                    <div
                      className={`profile-infor__context ${
                        !activeEducation ? "unactive" : ""
                      }`}
                    >
                      {employees[0].educations?.map((data, index) => {
                        return (
                          <div className="profile-context__text" key={index}>
                            <h4>{data.educationMajors}</h4>
                            <p>
                              {data.educationalInstitution} -{" "}
                              {data.educationaPlace}{" "}
                              <span>{data.educationTime}</span>
                            </p>
                          </div>
                        );
                      })}

                      {/* <div className="profile-context__text">
                        <h4>
                          Bachelor of Science Degree in Computer Engineering
                        </h4>
                        <p>
                          Georgia Institute of Technology - Atlanta, GA{" "}
                          <span>( 2006 - 2011 )</span>
                        </p>
                      </div> */}
                    </div>
                  </>
                )}
            </div>
          </div>
          <div className="profile-review">
            <div className="profile-review__header">
              <LikeShapes fontWeight={300} size={28} />
              <h4>{langs.global.reviews[lang]}</h4>
            </div>
            <div className="profile-review__filter">
              <div className="profile-review__star">
                <h4>
                  {employees[0].previews && employees[0].previews.length + " "}
                  {langs.profile.rvforstaff[lang]}
                </h4>
                <CRating
                  rate={
                    employees[0].previews &&
                    (employees[0].previews.length === 0
                      ? 5
                      : averageStar(employees[0].previews))
                  }
                  readOnly={true}
                />
                <span className="number-star">
                  {employees[0].previews &&
                    (employees[0].previews.length === 0
                      ? 5
                      : averageStar(employees[0].previews))}
                </span>
              </div>
            </div>
            <div className="profile-review__comment">
              {listComment.map((comment, index) => (
                <Comment key={index} comment={comment} />
              ))}
              <div className="profile-panination">
                {listComment.length > 0 ? (
                  <CPagination
                    SetPagination={SetPagination}
                    count={
                      listComment.length <= 5
                        ? 1
                        : Math.ceil(listComment.length / 5)
                    }
                  />
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default Profile;
