import CAutocomplete from "@/common/components/controls/CAutocomplete";
import CButton from "@/common/components/controls/CButton";
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import CTextField from "@/common/components/controls/CTextField";
import { ArrowDown2, ArrowUp2, Chart1, SearchNormal } from "iconsax-react";
import React, { useEffect, useState } from "react";
import ItemContract from "./components/ItemContract";
import { useSelector } from "react-redux";
import { contractsSelector } from "@/redux/selector";
import { Contract } from "@/types/models";
import CPagination from "@/common/components/controls/CPagination";
import NotFoundValue from "../NotFoundValue";

const Contract = () => {
  const contractsOrigin = useSelector(contractsSelector);
  const [contracts, setContracts] = useState<Contract[]>([]);
  /**
   * Filter
   */
  const [nameFilter, setNameFilter] = useState<string>("");
  const [sortFilter, setSortFilter] = useState<string>("All");
  const [priceFilter, setPriceFilter] = useState<string>("All");
  const [categoryFilter, setCategoryFilter] = useState<string>("all");

  useEffect(() => {
    if (
      !nameFilter &&
      sortFilter === "All" &&
      priceFilter === "All" &&
      categoryFilter === "all"
    ) {
      setContracts(contractsOrigin);
    }
    if (priceFilter !== "All") {
      let newContract;
      priceFilter === "Low to high" &&
        setContracts(
          (newContract = contractsOrigin
            .slice()
            .sort((a, b) => a.project.totalMoney - b.project.totalMoney))
        );
      priceFilter === "High to Low" &&
        setContracts(
          (newContract = contractsOrigin
            .slice()
            .sort((a, b) => -a.project.totalMoney + b.project.totalMoney))
        );

      if (categoryFilter === "all") {
        newContract && setContracts(newContract);
        if (nameFilter) {
          newContract &&
            setContracts(
              newContract.filter((contract) =>
                contract.project.projectName.toLowerCase().includes(nameFilter.toLowerCase())
              )
            );
        } else {
          newContract && setContracts(newContract);
        }
      } else {
        let newContractCate;
        newContract &&
          setContracts(
            (newContractCate = newContract.filter(
              (contract) => contract.status === categoryFilter
            ))
          );
        if (nameFilter) {
          newContractCate &&
            setContracts(
              newContractCate.filter((contract) =>
                contract.project.projectName.toLowerCase().includes(nameFilter.toLowerCase())
              )
            );
        } else {
          newContractCate && setContracts(newContractCate);
        }
      }
    } else if (sortFilter !== "All") {
      let newContract;
      sortFilter === "Latest" &&
        setContracts(
          (newContract = contractsOrigin
            .slice()
            .sort(
              (a, b) =>
                Number(new Date(a.createdAt)) - Number(new Date(b.createdAt))
            ))
        );

      sortFilter === "Recent" &&
        setContracts(
          (newContract = contractsOrigin
            .slice()
            .sort(
              (a, b) =>
                Number(new Date(b.createdAt)) - Number(new Date(a.createdAt))
            ))
        );

      if (categoryFilter === "all") {
        newContract && setContracts(newContract);
        if (nameFilter) {
          newContract &&
            setContracts(
              newContract.filter((contract) =>
                contract.project.projectName.toLowerCase().includes(nameFilter.toLowerCase())
              )
            );
        } else {
          newContract && setContracts(newContract);
        }
      } else {
        let newContractCate;
        newContract &&
          setContracts(
            (newContractCate = newContract.filter(
              (contract) => contract.status === categoryFilter
            ))
          );

        if (nameFilter) {
          newContractCate &&
            setContracts(
              newContractCate.filter((contract) =>
                contract.project.projectName.toLowerCase().includes(nameFilter.toLowerCase())
              )
            );
        } else {
          newContractCate && setContracts(newContractCate);
        }
      }
    } else if (nameFilter) {
      setContracts(
        contractsOrigin.filter((contract) =>
          contract.project.projectName.toLowerCase().includes(nameFilter.toLowerCase())
        )
      );
    } else if (categoryFilter) {
      if (categoryFilter === "all") {
        setContracts(contractsOrigin);
      } else
        setContracts(
          contractsOrigin.filter(
            (contract) => contract.status === categoryFilter
          )
        );
    }
  }, [categoryFilter, contractsOrigin, nameFilter, priceFilter, sortFilter]);

  const [pagination, SetPagination] = React.useState(1);
  const [listContract, setListContract] = React.useState<Contract[]>(
    contracts.slice(5)
  );
  React.useEffect(() => {
    setListContract(contracts.slice((pagination - 1) * 5, pagination * 5));
  }, [contracts, pagination]);

  const [showCategory, setShowCategory] = React.useState<boolean>(true);

  return (
    <div id="contract">
      <div className="contract-header">
        <h3>Manager Contract</h3>
      </div>
      {contractsOrigin.length === 0 ? (
        <NotFoundValue />
      ) : (
        <div className="contract-main">
          <div className="contract-filter">
            <div className="contract-filter__search">
              <SearchNormal />
              <CTextField
                placeholder="Search contract"
                onChange={(e) => {
                  setNameFilter(e.target.value);
                }}
              />
            </div>
            <div className="contract-filter__time">
              <Chart1 />
              <span>Sort by</span>
              <CAutocomplete
                defaultValue={{ label: sortFilter }}
                onChange={(e, v) => {
                  setPriceFilter("All");
                  setSortFilter(v.label);
                }}
                option={[
                  { label: "All" },
                  { label: "Recent" },
                  { label: "Latest" },
                ]}
              />
            </div>
            <div className="contract-filter__price">
              <span>Price:</span>
              <CAutocomplete
                defaultValue={{ label: priceFilter }}
                onChange={(e, v) => {
                  setSortFilter("All");
                  setPriceFilter(v.label);
                }}
                option={[
                  { label: "All" },
                  { label: "Low to high" },
                  { label: "High to Low" },
                ]}
              />
            </div>
          </div>
          <div className="contract-category">
            <div className="contract-category__header">
              <h4>Category</h4>
              <CButtonIcon
                onclick={() => setShowCategory(!showCategory)}
                icon={
                  showCategory ? (
                    <ArrowUp2 size={20} />
                  ) : (
                    <ArrowDown2 size={20} />
                  )
                }
              />
            </div>
            <div
              className={`contract-category__btn ${
                showCategory ? "active" : ""
              }`}
            >
              <CButton
                className={`${categoryFilter === "all" ? "active" : ""}`}
                onClick={() => setCategoryFilter("all")}
                variant="outlined"
                text="All"
              />
               <CButton
                className={`${categoryFilter === "active" ? "active" : ""}`}
                onClick={() => setCategoryFilter("active")}
                variant="outlined"
                text="Active"
              />
              <CButton
                className={`${categoryFilter === "wait" ? "active" : ""}`}
                onClick={() => setCategoryFilter("wait")}
                variant="outlined"
                text="Wait payment"
              />
              <CButton
                className={`${categoryFilter === "complete" ? "active" : ""}`}
                onClick={() => setCategoryFilter("complete")}
                variant="outlined"
                text="Complete"
              />
              <CButton
                className={`${categoryFilter === "cancel" ? "active" : ""}`}
                onClick={() => setCategoryFilter("cancel")}
                variant="outlined"
                text="Cancel"
              />
              <CButton
                className={`${categoryFilter === "end" ? "active" : ""}`}
                onClick={() => setCategoryFilter("end")}
                variant="outlined"
                text="End"
              />
              <CButton
                className={`${categoryFilter === "inactive" ? "active" : ""}`}
                onClick={() => setCategoryFilter("inactive")}
                variant="outlined"
                text="Inactive"
              />
            </div>
          </div>
          <div className="contract-list">
            <div className="contract-list__total">
              <h5>Total: </h5>
              <span>{contracts.length} Contract</span>
            </div>
            <div className="contract-list__item">
              {listContract.map((contract, index) => (
                <ItemContract contract={contract} key={index} />
              ))}
            </div>
          </div>
          {contracts.length !== 0 && (
            <div className="contract__pagination">
              <CPagination
                SetPagination={SetPagination}
                count={
                  contracts.length <= 5 ? 1 : Math.ceil(contracts.length / 5)
                }
              />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Contract;
