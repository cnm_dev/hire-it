/* eslint-disable @next/next/no-img-element */
import CSpinner from "@/common/components/controls/CSpinner";
import CTooltip from "@/common/components/controls/CTooltip";
import useHistoryContract from "@/hooks/useHistoryContract";
import { formatDate, formatNormalDate, formatNumber } from "@/utils";
import { ArchiveBook, Timer1 } from "iconsax-react";
import React from "react";
type HistoryContractProps = {
  contractId: string;
};
const HistoryContract = ({ contractId }: HistoryContractProps) => {
  const { historyContract, loading } = useHistoryContract({ contractId });

  return (
    <>
      {loading ? (
        <CSpinner />
      ) : historyContract.length === 0 ? (
        <span className="not-history-contract">Not value history</span>
      ) : (
        <div className="history-contract">
          <h4 className="title">History Contract</h4>
          <div className="history-contract__list">
            {historyContract.map((history, index) => (
              <div className="item-history" key={index}>
                <img
                  src="https://cdn.dribbble.com/userupload/4048235/file/original-afb96ed8db9e3e6c9b86b6bee010fa8e.png?compress=1&resize=1200x900"
                  alt=""
                />
                <div>
                  <div className="header">
                    <span className="type">{history.type} Contract</span>
                    <CTooltip
                      title={history.status}
                      child={
                        <span className={`status ${history.status}`}></span>
                      }
                    />
                  </div>
                  <span className="infor">
                    Toal: {formatNumber(history.totalMoney)}$
                  </span>

                  <span className="infor">
                    {history.endDate &&
                      `End: ${formatNormalDate(history.endDate)}`}
                  </span>

                  <span className="date">
                    <Timer1 size="18" />
                    {formatNormalDate(history.createdAt, true)}
                  </span>
                </div>
              </div>
            ))}
          </div>
        </div>
      )}
    </>
  );
};

export default HistoryContract;
