import { updateStatusContract } from "@/apis/Contract";
import { createContractEnd } from "@/apis/ContractEnd";
import { createContractHistory } from "@/apis/HistoryContract";
import { createInformates } from "@/apis/Informates";
import CButton from "@/common/components/controls/CButton";
import CRadioButton from "@/common/components/controls/CRadioButton";
import CSpinner from "@/common/components/controls/CSpinner";
import CTextField from "@/common/components/controls/CTextField";
import { emailInfomateCancel } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateCancelContract";
import { Contract } from "@/types/models";
import axios from "axios";
import { useSession } from "next-auth/react";
import React, { useState } from "react";
import { toast } from "react-toastify";
import { mutate } from "swr";

const radioEndContract = [
  { label: "Vấn đề liên quan đến hợp đồng." },
  { label: "Vấn đề liên quan đến nhân viên." },
  { label: "Vấn đề liên quan đến lý do cá nhân công ty." },
  { label: "Khác:" },
];
type EndContractProps = {
  setIsOpenModal: React.Dispatch<React.SetStateAction<boolean>>;
  contract: Contract;
};
const EndContract = ({ contract, setIsOpenModal }: EndContractProps) => {
  const [option, setOption] = useState<string>();
  const [valueOther, setvalueOther] = useState<string>("");
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { data: session } = useSession();

  const handelEndContract = () => {
    if (option) {
      setIsLoading(true);
      const variableContract = {
        contractId: contract.id,
        customerId: contract.project.customer.id,
        reason: option === "Khác:" ? valueOther : option,
      };
      createContractEnd(variableContract)
        .then(() => {
          updateStatusContract({
            contractId: contract.id,
            status: "pending",
          }).then(() => {
            createContractHistory({
              contractId: contract.id,
              dateUpdate: new Date(),
              endDate: contract.endDate,
              status: "wait",
              totalMoney: contract.project.totalMoney,
              type: "pending",
            }).then(async () => {
              const html = emailInfomateCancel({
                name: contract.project.customer.users_permissions_user.fullName,
                email: "Vohieu7k58@gmail.com",
                phoneNumber: "0377723460",
                contractId: contract.id,
                host: "https://hire-it-git-chanh-dev-update-project-hireit.vercel.app/contract",
              });
              await axios.post("/api/verifyEmail/sendEmail", {
                email: contract.project.customer.users_permissions_user.email,
                html,
              });
              createInformates({
                title: "Termination of contract",
                description: `We would like to inform you that the contract between us and you has been terminated.
                        Thank you for using our services.`,
                route: "/contract",
                customerId: contract.project.customer.id,
              });
              createInformates({
                title: "Customer request end Contract",
                description: `The customer is email ${contract.project.customer.users_permissions_user.email} has requested to end the contract ${contract.project.projectName}.`,
                route: "/manager/contract",
                adminId: "1",
              });
              toast.success("Send reason end contract complete");
              mutate(`/api/customers/${session?.user.id}/contracts`);
              setIsLoading(false);
              setIsOpenModal(false);
            });
          });
          setIsLoading(false);
        })
        .catch(() => {
          setIsLoading(false);
        });
    } else {
      toast.warning("Please select a reason to terminate the contract");
    }
  };

  return (
    <div className="end-contract">
      <h4>End Contract</h4>
      <p>Please select a reason to terminate the contract</p>
      <span className="des">
        We will contact you as soon as possible to resolve this issue
      </span>
      <div className="end-contract__container">
        {radioEndContract.map((item, index) => (
          <CRadioButton
            key={index}
            setState={setOption}
            label={item.label}
            item={item.label}
            state={option}
            name="end-contract"
          />
        ))}
        <CTextField
          onChange={(e) => setvalueOther(e.target.value)}
          disable={option === "Khác:" ? false : true}
          placeholder="Enter your reason"
        />
      </div>
      <div className="end-contract__btn">
        <CButton
          text="Cancel"
          onClick={() => {
            setIsOpenModal(false);
          }}
        />
        <CButton
          disabled={isLoading}
          onClick={handelEndContract}
          text={isLoading ? <CSpinner size="small" /> : "Submit"}
        />
      </div>
    </div>
  );
};

export default EndContract;
