import CButton from "@/common/components/controls/CButton";
import React from "react";

type StatusContractProps = {
  status: string;
};

const StatusContract = ({ status }: StatusContractProps) => {
  const colorProps = React.useMemo(() => {
    if (status === "active") {
      return "success";
    } else if (status === "complete" || status === "end") {
      return "inherit";
    } else if (status === "wait" || "extend") {
      return "warning";
    } else return "error";
  }, [status]);

  return (
    <CButton
      className="btn-status__contract"
      text={status}
      color={colorProps}
    />
  );
};

export default StatusContract;
