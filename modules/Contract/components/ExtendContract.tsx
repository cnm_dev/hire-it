import { updateContract } from "@/apis/Contract";
import { createContractHistory } from "@/apis/HistoryContract";
import { createInformates } from "@/apis/Informates";
import CButton from "@/common/components/controls/CButton";
import CSpinner from "@/common/components/controls/CSpinner";
import { emailInfomateExtendWait } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateExtendWaitContract";
import { Contract } from "@/types/models";
import { countDay, formatNumber, timeRemain } from "@/utils";
import axios from "axios";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React, { useEffect, useMemo, useRef, useState } from "react";
import DatePicker from "react-multi-date-picker";
import { toast } from "react-toastify";

type ExtendContractProps = {
  contract: Contract;
  setIsOpenModal: React.Dispatch<React.SetStateAction<boolean>>;
};

const ExtendContract = ({ contract, setIsOpenModal }: ExtendContractProps) => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const router = useRouter();
  const { data: session } = useSession();
  const datePickerRef: any = useRef();
  const nextEndDate = useMemo(() => {
    return new Date(new Date(contract.endDate).getTime() + 24 * 60 * 60 * 1000);
  }, [contract.endDate]);
  const [newTime, setNewTime] = useState<Date>(nextEndDate);

  const totalMoneyEmployee = useMemo(() => {
    let toalMoney = 0;
    contract.project.employees.forEach((employee) => {
      toalMoney += employee.hourlyRate;
    });
    return toalMoney;
  }, [contract.project.employees]);

  const expenseMoney = useMemo(() => {
    return totalMoneyEmployee * countDay(contract.endDate, newTime);
  }, [contract.endDate, newTime, totalMoneyEmployee]);

  useEffect(() => {
    if (datePickerRef.current) {
      const inputElement = datePickerRef.current.querySelector(".rmdp-input");
      if (inputElement) {
        inputElement.readOnly = true;
      }
    }
  }, []);

  return (
    <div className="extend-contract">
      <div className="extend-contract__title">
        <h4>Extend Contract</h4>
        <span>
          Please <u>select the date</u> you want to renew your next contract
          <br />
          After confirming, you will have <u>48 hours</u> to pay for the
          contract extension to complete the renewal procedure
        </span>
      </div>
      <div className="extend-contract__main">
        <h4>Description Contract</h4>
        <div className="extend-infor">
          <div className="extend-infor__item">
            <h5>Name contract: </h5>
            <span>{contract.project.projectName}</span>
          </div>
          <div className="extend-infor__item">
            <h5>Amount employee: </h5>
            <span>{contract.project.employees.length} (employees)</span>
          </div>
          <div className="extend-infor__item">
            <h5>Start date: </h5>
            <span>
              <DatePicker value={contract.startDate} disabled />
            </span>
          </div>
          <div className="extend-infor__item">
            <h5>End date: </h5>
            <span>
              <DatePicker value={contract.endDate} disabled />
            </span>
          </div>
          <div className="extend-infor__item">
            <h5>Amount money: </h5>
            <span>{formatNumber(contract.project.totalMoney)}$</span>
          </div>
        </div>
        <h4>Extend date contract</h4>
        <div className="extend-infor">
          <div className="extend-infor__item">
            <h5>New Date: </h5>
            <span>
              <DatePicker
                ref={datePickerRef}
                value={nextEndDate}
                minDate={nextEndDate}
                onChange={(date) => {
                  date && setNewTime(new Date(date.toString()));
                }}
              />
            </span>
            <span className="note">(*)</span>
          </div>
          <div className="extend-infor__item">
            <h5>Expense: </h5>
            <span>{formatNumber(expenseMoney)}$</span>
          </div>
          <div className="extend-infor__item">
            <h5>Amount money (new): </h5>
            <span>
              ${formatNumber(contract.project.totalMoney + expenseMoney)}$
            </span>
          </div>
        </div>
      </div>
      <div className="extend-contract__btn">
        <CButton text="Cancel" onClick={() => setIsOpenModal(false)} />
        <CButton
          disabled={isLoading}
          text={isLoading ? <CSpinner size="small" /> : "Confirm"}
          onClick={() => {
            setIsLoading(true);
            updateContract({ contractId: contract.id, isExtend: true })
              .then(() => {
                createContractHistory({
                  contractId: contract.id,
                  dateUpdate: new Date(),
                  endDate: newTime,
                  status: "wait",
                  totalMoney: contract.project.totalMoney + expenseMoney,
                  type: "extend",
                })
                  .then(() => {
                    const html = emailInfomateExtendWait({
                      name: contract.project.customer.users_permissions_user
                        .fullName,
                      email: "Vohieu7k58@gmail.com",
                      phoneNumber: "0377723460",
                      contractId: contract.id,
                      host: "https://hire-it-git-chanh-dev-update-project-hireit.vercel.app/contract",
                      endDate: contract.endDate,
                      endDateExtend: newTime,
                    });
                    axios.post("/api/verifyEmail/sendEmail", {
                      email:
                        contract.project.customer.users_permissions_user.email,
                      html,
                    });

                    createInformates({
                      title: "Contract extension",
                      description: `We are pleased to inform you that the contract extension has been successfully approved.
                      The contract between us will be extended for the agreed-upon duration. 
                      We appreciate your continued partnership and look forward to further collaboration. Thank you.`,
                      route: "/contract",
                      customerId: contract.project.customer.id,
                    });

                    setIsLoading(false);
                    toast.success("Confirm extend contract success");
                    localStorage.setItem("IdContract", contract.id);
                    session?.user.type_user === "Customer" &&
                      router.replace("/payment");
                  })
                  .catch((err) => {
                    toast.error(err.message);
                    setIsLoading(false);
                  });
              })
              .catch((err) => {
                toast.error(err.message);
                setIsLoading(false);
              });
          }}
        />
      </div>
    </div>
  );
};

export default ExtendContract;
