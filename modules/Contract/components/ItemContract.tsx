/* eslint-disable @next/next/no-img-element */
import CButton from "@/common/components/controls/CButton";
import { Clock } from "iconsax-react";
import Link from "next/link";
import React, { useState } from "react";
import StatusContract from "./StatusContract";
import { Contract } from "@/types/models";
import { formatDate, formatNumber, intervalTime } from "@/utils";
import { updateStatusContract } from "@/apis/Contract";
import { useRouter } from "next/router";
import { mutate } from "swr";
import { useSession } from "next-auth/react";
import { toast } from "react-toastify";
import CModal from "@/common/components/controls/CModal";
import ExtendContract from "./ExtendContract";
import { createContractHistory } from "@/apis/HistoryContract";
import HistoryContract from "./HistoryContract";
import date from "date-and-time";
import { createInformates } from "@/apis/Informates";
import { emailInfomateCancel } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateCancelContract";
import axios from "axios";
import COptionModel from "@/common/components/layout/COptionModel";
import EndContract from "./EndContract";

type ItemContractProps = {
  contract: Contract;
};

const ItemContract = ({ contract }: ItemContractProps) => {
  const { data: session } = useSession();
  const router = useRouter();
  const [isOpenModalExtend, setIsOpenModalExtend] = useState<boolean>(false);
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);

  return (
    <div className="item-contract">
      <div className="item-contract__infor">
        <div className="item-contract__img">
          <span className={contract.status}></span>
          <img
            src="https://cdn.dribbble.com/userupload/4048235/file/original-afb96ed8db9e3e6c9b86b6bee010fa8e.png?compress=1&resize=1200x900"
            alt=""
          />
        </div>
        <div className="item-infor">
          <div className="item-infor_status">
            <StatusContract status={contract.status} />
            {contract.isExtend && <StatusContract status={"extend"} />}
          </div>
          <h5>{contract.project.projectName}</h5>
          <div className="item-infor__content">
            {contract.startDate && (
              <span>Start: {formatDate(contract.startDate)} </span>
            )}
            {contract.endDate && (
              <span>End: {formatDate(contract.endDate)} </span>
            )}
            <span>{`${contract.project.employees.length} employee`} </span>
            <span>{formatNumber(contract.project.totalMoney)}$ </span>
          </div>
        </div>
      </div>
      <div className="item-contract__status">
        <div className="item-function">
          {contract.status === "wait" && (
            <>
              <Link
                href="/payment"
                onClick={() => {
                  localStorage.setItem("IdContract", contract.id);
                }}
              >
                <CButton variant="outlined" text="Payment" />
              </Link>
            </>
          )}
          {contract.status === "active" &&
            (contract.isExtend ? (
              <CButton
                variant="outlined"
                text="Payment Extend"
                onClick={() => {
                  localStorage.setItem("IdContract", contract.id);
                  router.replace("/payment");
                }}
              />
            ) : (
              <CModal
                isOpenModal={isOpenModalExtend}
                setIsOpenModal={setIsOpenModalExtend}
                confirm={true}
                button={<CButton variant="outlined" text="Extend" />}
                modal={
                  <ExtendContract
                    setIsOpenModal={setIsOpenModalExtend}
                    contract={contract}
                  />
                }
              />
            ))}
          {(contract.status === "active" || contract.status === "wait") && (
            <CModal
              setIsOpenModal={setIsOpenModal}
              isOpenModal={isOpenModal}
              modal={
                <EndContract
                  contract={contract}
                  setIsOpenModal={setIsOpenModal}
                />
              }
              button={<CButton variant="outlined" text="End Contract" />}
            />
          )}
          {contract.status === "inactive" && (
            <CButton
              variant="outlined"
              text="Active"
              onClick={() => {
                if (
                  contract.project.employees.some(
                    (employee) => employee.statusWork !== "free"
                  )
                ) {
                  toast.warning(
                    "This contract already exists the employee is working"
                  );
                } else {
                  localStorage.setItem("IdContract", contract.id);
                  updateStatusContract({
                    contractId: contract.id,
                    status: "wait",
                  }).then(() => {
                    createContractHistory({
                      contractId: contract.id,
                      dateUpdate: new Date(),
                      endDate: contract.endDate,
                      status: "wait",
                      totalMoney: contract.project.totalMoney,
                      type: "payment",
                    }).then(() => {
                      router.replace("/payment");
                    });
                  });
                }
              }}
            />
          )}
          <Link
            href={{
              pathname: "/contract/detail/[contractId]",
              query: { contractId: contract.id },
            }}
          >
            <CButton variant="outlined" text="Detail" />
          </Link>
        </div>
        <span className="item-function__time">
          <Clock size={17} />
          {intervalTime(contract.updatedAt)}
          <CModal
            button={<u>History</u>}
            modal={<HistoryContract contractId={contract.id} />}
          />
        </span>
      </div>
    </div>
  );
};

export default ItemContract;
