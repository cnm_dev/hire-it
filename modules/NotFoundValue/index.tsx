/* eslint-disable react/no-unescaped-entities */
/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import React from "react";

type NotFoundValueProps = {
  content?: any;
};
const NotFoundValue = ({ content }: NotFoundValueProps) => {
  return (
    <div className="not-found">
      <img src={image.not_found} alt="" />
      <div className="not-found__content">
        {content ? (
          content
        ) : (
          <>
            <h4>No results found</h4>
            <span>We couldn'n find value in system.</span>
          </>
        )}
      </div>
    </div>
  );
};

export default NotFoundValue;
