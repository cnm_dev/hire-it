import CButton from "@/common/components/controls/CButton";
import CSteper from "@/common/components/controls/CSteper";
import React, { useMemo, useState } from "react";
import QuickEmployeeHire from "./components/QuickEmployeeHire";
import QuickProjectHire from "./components/QuickProjectHire";
import QuickWorkHire from "./components/QuickWorkHire";
import QuickDoneHire from "./components/QuickDoneHire";
import { Employee } from "@/types/models";
import { useForm } from "react-hook-form";
import dateFormat from "dateformat";
import { createProject } from "@/apis/Project";
import { createProjectForm } from "@/forms/project";
import { useSession } from "next-auth/react";
import { converStringBoolean, randomString } from "@/utils";
import { toast } from "react-toastify";
import useCustomer from "@/hooks/useCustomer";
import { createContract } from "@/apis/Contract";
import CSpinner from "@/common/components/controls/CSpinner";
import { useRouter } from "next/router";
import { createContractHistory } from "@/apis/HistoryContract";
import { createInformates } from "@/apis/Informates";
import { Warning2 } from "iconsax-react";
import Link from "next/link";
import CLoading from "@/common/components/controls/CLoading";

export type QuickHireProps = {
  listEmployee: Employee[];
  handelDeleteEmployeeChosse: (isToast?: boolean) => void;
};

export type valueForm = {
  commitmentWork?: string;
  longProject?: string;
  remoteWork?: string;
  startWork?: string;
  typeProject?: string;
};

const QuickHire = ({
  listEmployee,
  handelDeleteEmployeeChosse,
}: QuickHireProps) => {
  const { handleSubmit, control } = useForm();
  const [number, setNumber] = useState<number>(-1);
  const [initialStateCompleted] = useState<{
    [k: number]: boolean;
  }>({});
  const { data: session } = useSession();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { customer, loading } = useCustomer({
    customerId: session?.user.id,
  });
  const [valueForm, setValueForm] = useState<valueForm>();
  const router = useRouter();

  const medium_money = useMemo(() => {
    let medium: number = 0;
    listEmployee.forEach((employee) => {
      medium += employee.hourlyRate;
    });
    return medium / listEmployee.length;
  }, [listEmployee]);

  const mount_money = useMemo(() => {
    let mount: number = 0;
    listEmployee.forEach((employee) => {
      mount += employee.hourlyRate;
    });
    return mount * 160;
  }, [listEmployee]);

  const listId = useMemo(() => {
    let listId: string[] = [];
    listEmployee.forEach((employee) => {
      listId.push(employee.id);
    });
    return listId;
  }, [listEmployee]);

  const handelPostJobnt = (value: valueForm) => {
    if (number < 2) {
      setNumber(number + 1);
      initialStateCompleted[number + 1] = true;
      if (number === 1) {
        setValueForm(value);
      }
    } else {
      setIsLoading(true);
      const project: createProjectForm = {
        totalMoney: undefined,
        projectName: undefined,
        openForRemote: undefined,
        commitment: undefined,
        projectLengthFrom: undefined,
        projectLengthTo: undefined,
        readyToStart: undefined,
        situation: undefined,
        listIDEmployee: [],
        customerId: "",
        status: "",
      };

      if (value.longProject && !loading) {
        const arrayLenght = value.longProject.split(",");
        const lengthFrom = Number.parseInt(arrayLenght[0]);
        const lengthTo = Number.parseInt(arrayLenght[1]);
        let projectName = `hireIT_${randomString(6)}`;
        project.totalMoney = mount_money;
        project.projectName = projectName;
        project.openForRemote = converStringBoolean(value.remoteWork);
        project.commitment = converStringBoolean(value.commitmentWork);
        project.projectLengthFrom = lengthFrom;
        project.projectLengthTo = lengthTo;
        project.readyToStart = value.startWork;
        project.situation = value.typeProject;
        project.customerId = customer[0].id;
        project.listIDEmployee = listId;
        createProject(project)
          .then((project) => {
            createContract({ projectId: project.data.id, status: "wait" })
              .then((contract) => {
                createContractHistory({
                  contractId: contract.data.id,
                  type: "create",
                  dateUpdate: new Date(),
                  status: "complete",
                  totalMoney: mount_money,
                }).then(() => {
                  createContractHistory({
                    contractId: contract.data.id,
                    type: "payment",
                    dateUpdate: new Date(),
                    status: "wait",
                    totalMoney: mount_money,
                  }).then(() => {
                    createInformates({
                      description: `Create contract ${projectName} success`,
                      title: "Create contract",
                      route: "/contract",
                      customerId: customer[0].id,
                    }).then(() => {
                      localStorage.setItem("IdContract", contract.data.id);
                      handelDeleteEmployeeChosse(false);
                      toast.success("Create contract complete");
                      setIsLoading(false);
                      router.replace("/payment");
                    });
                  });
                });
              })
              .catch((err) => {
                toast.error(err.message);
                setIsLoading(false);
              });
          })
          .catch((err) => {
            toast.error(err.message);
            setIsLoading(false);
          });
      }
    }
  };

  return (
    <div className="post-form">
      {!customer[0] ? (
        <CLoading />
      ) : !customer[0].companyName ||
        !customer[0].codeFax ||
        !customer[0].users_permissions_user.address ? (
        <div className="wanring-form">
          <span className="warning-infor">
            <Warning2 size="32" color="#FF8A65" />
            You have not updated your company information.
          </span>
          <div className="warning-btn">
            <Link href="/auth/profile">
              {" "}
              <CButton text="Update Profile" />
            </Link>
          </div>
        </div>
      ) : (
        <form onSubmit={handleSubmit(handelPostJobnt)}>
          <CSteper
            steps={[
              { title: "Employee" },
              { title: "Project" },
              { title: "Work" },
            ]}
            initialState={number + 1}
            initialStateCompleted={initialStateCompleted}
          />
          <div className="post-form__container">
            {number === -1 && <QuickEmployeeHire listEmployee={listEmployee} />}
            {number === 0 && <QuickProjectHire control={control} />}
            {number === 1 && <QuickWorkHire control={control} />}
            {number === 2 && (
              <QuickDoneHire
                medium_money={medium_money}
                mount_money={mount_money}
                valueForm={valueForm}
                listEmployee={listEmployee}
              />
            )}
          </div>
          <div className="post-form__btn">
            <CButton
              disabled={number < 0 || isLoading}
              onClick={() => {
                setNumber(number - 1);
                initialStateCompleted[number] = false;
              }}
              text="Back"
            />
            <CButton
              disabled={isLoading}
              type="submit"
              text={
                isLoading ? (
                  <CSpinner size="small" />
                ) : number >= 2 ? (
                  "Create"
                ) : (
                  "Next"
                )
              }
            />
          </div>
        </form>
      )}
    </div>
  );
};

export default QuickHire;
