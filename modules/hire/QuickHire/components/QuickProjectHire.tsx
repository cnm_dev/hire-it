import CRadioButton from "@/common/components/controls/CRadioButton";
import React, { useState } from "react";
import { Control, Controller, FieldValues } from "react-hook-form";

const typeProjecttRadio = [
  { label: "New idea or project" },
  { label: "Existing project that needs more resources" },
  { label: "Ongoing assistance or consultation" },
  { label: "None of the above, I'm just looking to learn more about Toptal" },
];

const longProjecttRadio = [
  { label: "Less than 1 week", value: "0,1" },
  { label: "1 to 4 weeks", value: "1,4" },
  { label: "1 to 3 months", value: "4,12" },
  { label: "3 to 6 months", value: "12,24" },
  { label: "Longer than 6 months", value: "24,99999" },
];

export type QuickProjectHireProps = {
  control: Control<FieldValues, any>;
};

const QuickProjectHire = ({ control }: QuickProjectHireProps) => {
  return (
    <div className="quick_container">
      <div className="quick-container_option">
        <div className="quick_header">
          <h4>What type of project are you hiring for?</h4>
          <span>
            Here is a list of employees who will be hired and create a contract
            after completing the steps to enter information
          </span>
        </div>
        <div className="quick_option">
          {typeProjecttRadio.map((type, index) => (
            <Controller
              key={index}
              control={control}
              name="typeProject"
              rules={{ required: true }}
              render={({ field }) => (
                <CRadioButton
                  key={index}
                  onChange={(event) => field.onChange(event.target.value)}
                  setState={field.onChange}
                  state={field.value}
                  label={type.label}
                  item={type.label}
                />
              )}
            />
          ))}
        </div>
      </div>
      <div className="quick-container_option">
        <div className="quick_header">
          <h4>How long do you need the developer?</h4>
          <span>
            How long is the project expected to hire staff to complete?
          </span>
        </div>
        <div className="quick_option">
          {longProjecttRadio.map((type, index) => (
            <Controller
              key={index}
              control={control}
              name="longProject"
              rules={{ required: true }}
              render={({ field }) => (
                <CRadioButton
                  key={index}
                  onChange={(event) => field.onChange(event.target.value)}
                  setState={field.onChange}
                  state={field.value}
                  label={type.label}
                  item={type.value}
                />
              )}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default QuickProjectHire;
