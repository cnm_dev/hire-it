import CRadioButton from "@/common/components/controls/CRadioButton";
import React, { useState } from "react";
import { QuickProjectHireProps } from "./QuickProjectHire";
import { Controller } from "react-hook-form";

const commitmentRadio = [
  { label: "Full time (40 or more hrs/week)", value: "false" },
  { label: "Part time (Less than 40 hrs/week)", value: "true" },
];

const remoteRadio = [
  { label: "No", value: "false" },
  { label: "Yes", value: "true" },
];

const startWorktRadio = [
  { label: "Immediately" },
  { label: "In 1 to 2 weeks" },
  { label: "More than 2 weeks from now" },
];

const QuickWorkHire = ({ control }: QuickProjectHireProps) => {
  const [, setTypeProject] = useState<string>();
  return (
    <div className="quick_container">
      <div className="quick-container_option">
        <div className="quick_header">
          <h4>
            What level of time commitment will you require from the developer?
          </h4>
          <span>In what form will the staff work?</span>
        </div>
        <div className="quick_option">
          {commitmentRadio.map((type, index) => (
            <Controller
            key={index}
            control={control}
            name="commitmentWork"
            rules={{ required: true }}
            render={({ field }) => (
              <CRadioButton
                key={index}
                onChange={(event) => field.onChange(event.target.value)}
                setState={field.onChange}
                state={field.value}
                label={type.label}
                item={type.value}
              />
            )}
          />
          ))}
        </div>
      </div>
      <div className="quick-container_option">
        <div className="quick_header">
          <h4>When do you need the developer to start?</h4>
          <span>When will the staff start working?</span>
        </div>
        <div className="quick_option">
          {startWorktRadio.map((type, index) => (
            <Controller
              key={index}
              control={control}
              name="startWork"
              rules={{ required: true }}
              render={({ field }) => (
                <CRadioButton
                  key={index}
                  onChange={(event) => field.onChange(event.target.value)}
                  setState={field.onChange}
                  state={field.value}
                  label={type.label}
                  item={type.label}
                />
              )}
            />
          ))}
        </div>
      </div>
      <div className="quick-container_option">
        <div className="quick_header">
          <h4>Are you open to working with a remote developer?</h4>
          <span>Will the staff work remotely or offline?</span>
        </div>
        <div className="quick_option">
          {remoteRadio.map((type, index) => (
            <Controller
            key={index}
            control={control}
            name="remoteWork"
            rules={{ required: true }}
            render={({ field }) => (
              <CRadioButton
                key={index}
                onChange={(event) => field.onChange(event.target.value)}
                setState={field.onChange}
                state={field.value}
                label={type.label}
                item={type.value}
              />
            )}
          />
          ))}
        </div>
      </div>
    </div>
  );
};

export default QuickWorkHire;
