/* eslint-disable @next/next/no-img-element */
import React from "react";
import { QuickHireProps } from "../QuickHire";
import { Employee } from "@/types/models";
import { Star1 } from "iconsax-react";

type QuickEmployeeHireProps = {
  listEmployee: QuickHireProps["listEmployee"];
};

const QuickEmployeeHire = ({ listEmployee }: QuickEmployeeHireProps) => {
  return (
    <div className="quick_container">
      <div className="quick_header only">
        <h4>List employee you want hire</h4>
        <span>
          Here is a list of employees who will be hired and create a contract
          after completing the steps to enter information
        </span>
      </div>
      <div className="quick_main">
        <div className="item-employee_list">
          {listEmployee.map((employee: Employee) => (
            <div className="item_employee" key={employee.id}>
              <img src={employee.users_permissions_user.avartar} alt="" />
              <div className="item-employee__infor">
                <div className="item-employee__start">
                  <Star1 variant="Bold" color="yellow" />
                  <span>{employee.start.toFixed(1)}</span>
                </div>
                <div className="item-employee__me">
                  <span>{employee.users_permissions_user.fullName}</span>
                  <span>{employee.users_permissions_user.email}</span>
                </div>
                <div className="item-employee__work">
                  <div className="employee-work">
                    <span>Position: {employee.position.positionName}</span>
                    <span>Hourly Rate: {employee.hourlyRate}</span>
                  </div>
                  <div className="employee-work">
                    <span>
                      Exp: {employee.yearsOfExperience}
                      {employee.yearsOfExperience < 1 ? "month" : "year"}
                    </span>
                    <span>{employee.isRemote ? "Remote" : "Offline"}</span>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default QuickEmployeeHire;
