/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useMemo } from "react";
import image from "@/common/assets/image/all-image";
import { valueForm } from "../QuickHire";
import { Employee } from "@/types/models";
import { CommitmentContract } from "@/constants/contract";
import { formatNumber } from "@/utils";

type QuickDoneHireProps = {
  valueForm?: valueForm;
  listEmployee: Employee[];
  medium_money: number;
  mount_money: number;
};

const QuickDoneHire = ({
  valueForm,
  listEmployee,
  medium_money,
  mount_money,
}: QuickDoneHireProps) => {
  const lenghProject = useMemo(() => {
    if (valueForm && valueForm.longProject) {
      const arrayLenght = valueForm.longProject.split(",");
      const lengthFrom = Number.parseInt(arrayLenght[0]);
      const lengthTo = Number.parseInt(arrayLenght[1]);
      if (lengthFrom === 0) {
        return "Less than 1 week";
      } else if (lengthFrom === 1 && lengthTo === 4) {
        return "1 to 4 weeks";
      } else if (lengthTo === 99999) {
        return "Longer than 6 months";
      } else return `${lengthFrom / 4} to ${lengthTo / 4} months`;
    }
  }, [valueForm]);

  return (
    <>
      {valueForm && lenghProject && (
        <div className="quick_container">
          <div className="quick_done">
            <img src={image.upload_contract} alt="" />
          </div>
          <span>
            <h4 className="title-done__quick">
              You complete write information description contract and chosse
              employee hire
            </h4>
            <div className="quick-done__over">
              <div className="quick-over__item">
                <div className="quick-item">
                  <h4>Mount Employee hire:</h4>
                  <span>{listEmployee.length} ( employee )</span>
                </div>
                <div className="quick-item">
                  <h4>Long project: </h4>
                  <span>{lenghProject}</span>
                </div>
                <div className="quick-item">
                  <h4>Time commitment: </h4>
                  <span>{CommitmentContract(valueForm.commitmentWork)}</span>
                </div>
                <div className="quick-item">
                  <h4>Type project:</h4>
                  <span>{valueForm.typeProject}</span>
                </div>
                <div className="quick-item">
                  <h4>Start work: </h4>
                  <span>{valueForm.startWork}</span>
                </div>
                <div className="quick-item">
                  <h4>Is remote: </h4>
                  <span>{valueForm.remoteWork ? "Remote" : "Offline"}</span>
                </div>
                <div className="quick-item">
                  <h4>Medium money: </h4>
                  <span> {formatNumber(medium_money, 2)} $ / employee</span>
                </div>
                <div className="quick-item">
                  <h4>Mount money: </h4>
                  <span>{formatNumber(mount_money, 2)} $</span>
                </div>
              </div>
            </div>
          </span>
        </div>
      )}
    </>
  );
};

export default QuickDoneHire;
