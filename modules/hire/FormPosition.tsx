/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import { positionSelector } from "@/redux/selector";
import { Button, CircularProgress } from "@mui/material";
import { ArrowRight2 } from "iconsax-react";
import Link from "next/link";
import React, { useEffect } from "react";
import RadioButtonCustomHire from "./components/RadioButtonCustomHire";
import { getStringStorage } from "@/utils";

const FormPosition = () => {
  const [selectedValue, setSelectedValue] = React.useState<
    string | undefined
  >();
  const [loading, setLoading] = React.useState(false);

  useEffect(() => {
    const position = getStringStorage(localStorage.getItem("Position"));
    setSelectedValue(position);
  }, []);

  const dataTemp = [
    {
      position: "Developers",
      description: "Software Developers, Data Scientists, DevOps, and QA",
    },
    {
      position: "Designers",
      description: "Web, Mobile, UI/UX, Branding, and Visual Designers",
    },
    {
      position: "Project Managers",
      description:
        "Digital Project Managers, IT Project Managers, Scrum Masters, and Agile Coaches",
    },
    {
      position: "Product Managers",
      description:
        "Digital Product Managers, Product Owners, and Business Analysts",
    },
    {
      position: "Finance Experts",
      description:
        "Financial Modelers, Fundraising Advisors, M&A and FP&A Experts",
    },
  ];

  const handelClick = () => {
    localStorage.setItem("Position", JSON.stringify(selectedValue));
    setLoading(false);
  };
  return (
    <div className="main-formPosition">
      <img src={image.logo_gif} alt="" width={250} height={250} />
      <div className="form-position-container">
        <div className="container_text">
          <p>
            Thanks for your interest in hiring through Toptal! Before we get
            started, we’d like to ask a few questions to better understand your
            business needs.
          </p>
        </div>
        <div className="form-radio-option">
          <p className="title">What role would you like to hire?</p>

          {dataTemp.map((data, index) => (
            <RadioButtonCustomHire
              key={index}
              item={data.position}
              name="position"
              state={selectedValue}
              setState={setSelectedValue}
              label={data.position}
              description={data.description}
            />
          ))}
        </div>
        <Button
          className="button"
          variant="contained"
          onClick={handelClick}
          disabled={selectedValue ? false : true}
          endIcon={
            loading ? (
              <CircularProgress size={18} sx={{ color: "#fff" }} />
            ) : (
              <ArrowRight2 size="20" color="#ffff" />
            )
          }
        >
          <Link href="/hire/quiz/company-size" className="next">
            Get Started
          </Link>
        </Button>
        <Link href="/auth/signup" className="freelancer_link">
          Looking for freelance work?
        </Link>
      </div>
    </div>
  );
};

export default FormPosition;
