import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "Budget",
  initialState: {
    budget: "",
  },
  reducers: {
    changeBudget: (state, action) => {
      // mutation
      state.budget = action.payload;
    },
  },
});
