import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "ProjectLength",
  initialState: {
    projectLength: "",
  },
  reducers: {
    changeProjectLength: (state, action) => {
      // mutation
      state.projectLength = action.payload;
    },
  },
});
