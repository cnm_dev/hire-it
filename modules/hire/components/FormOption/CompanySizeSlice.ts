import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "CompanySize",
  initialState: {
    companySize: "",
  },
  reducers: {
    changeCompanySize: (state, action) => {
      // mutation
      state.companySize = action.payload;
    },
  },
});
