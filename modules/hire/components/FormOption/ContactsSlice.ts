import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "Contacts",
  initialState: {
    contacts: "",
  },
  reducers: {
    changeContacts: (state, action) => {
      // mutation
      state.contacts = action.payload;
    },
  },
});
