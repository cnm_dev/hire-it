import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "Situation",
  initialState: {
    situation: "",
  },
  reducers: {
    changeSituation: (state, action) => {
      // mutation
      state.situation = action.payload;
    },
  },
});
