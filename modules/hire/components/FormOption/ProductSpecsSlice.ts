import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "ProductSpecs",
  initialState: {
    productSpecs: "",
  },
  reducers: {
    changeProductSpecs: (state, action) => {
      // mutation
      state.productSpecs = action.payload;
    },
  },
});
