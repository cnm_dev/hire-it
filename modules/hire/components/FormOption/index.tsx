/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import CLoading from "@/common/components/controls/CLoading";
import CRadioButton from "@/common/components/controls/CRadioButton";
import { Button, CircularProgress } from "@mui/material";
import { ArrowLeft2, ArrowRight2 } from "iconsax-react";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useState } from "react";

interface propsFormOption {
  options: {
    value: string;
    label?: string;
  }[];
  questionName: string;
  href: any;
  defaultValue?: string;
  showBack?: boolean;
  option: number;
}

const FormOption = ({
  options,
  questionName,
  href,
  defaultValue,
  showBack,
  option,
}: propsFormOption) => {
  const router = useRouter();
  const [selectedValue, setSelectedValue] = React.useState<string | undefined>(
    defaultValue
  );

  const [loading, setLoading] = useState(false);

  const handelClick = (option: number) => {
    switch (option) {
      case 1:
        localStorage.setItem("Budget", JSON.stringify(selectedValue));
        break;
      case 2:
        localStorage.setItem("Commitment", JSON.stringify(selectedValue));
        break;
      case 3:
        localStorage.setItem("CompanySize", JSON.stringify(selectedValue));
        break;
      case 4:
        localStorage.setItem("OpenForRemote", JSON.stringify(selectedValue));
        break;
      case 5:
        localStorage.setItem("ProductSpecs", JSON.stringify(selectedValue));
        break;
      case 6:
        localStorage.setItem("ProjectLength", JSON.stringify(selectedValue));
        break;
      case 7:
        localStorage.setItem("ReadyToStart", JSON.stringify(selectedValue));
        break;
      case 8:
        localStorage.setItem("Situation", JSON.stringify(selectedValue));
        break;
    }
    setLoading(false);
  };

  return (
    <div className="main-formOption">
      <img src={image.logo_gif} alt="" width={250} height={250} style={{}} />
      <div className="form-option-container">
        <div className="form-radio-option">
          <p className="title">{questionName}</p>
          {options.map((data, index) => (
            <CRadioButton
              key={index}
              item={data.value}
              name="position"
              state={selectedValue}
              setState={setSelectedValue}
              label={data.label || data.value}
            />
          ))}
        </div>
        <div className="container_button">
          {showBack ? (
            <Button
              className="button_back"
              variant="contained"
              onClick={() => router.back()}
              startIcon={<ArrowLeft2 size="20" color="rgba(0,0,0,0.7)" />}
            >
              Back
            </Button>
          ) : null}

          <Button
            className="button"
            variant="contained"
            disabled={selectedValue ? false : true}
            onClick={() => {
              handelClick(option);
              setLoading(true);
            }}
            endIcon={
              loading ? (
                <CircularProgress size={18} sx={{ color: "#fff" }} />
              ) : (
                <ArrowRight2 size="20" color="#ffff" />
              )
            }
          >
            <Link
              href={
                selectedValue === "1001 - 5000" ||
                selectedValue === "More than 5000"
                  ? "contacts"
                  : selectedValue === "New idea or project"
                  ? "product-specs"
                  : selectedValue === "Not sure on budget yet"
                  ? "../sorry"
                  : href
              }
              className="next"
            >
              Next
            </Link>
          </Button>
        </div>
      </div>
    </div>
  );
};

export default FormOption;
