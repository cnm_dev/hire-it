import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "Commitment",
  initialState: {
    commitment: "",
  },
  reducers: {
    changeCommitment: (state, action) => {
      // mutation
      state.commitment = action.payload;
    },
  },
});
