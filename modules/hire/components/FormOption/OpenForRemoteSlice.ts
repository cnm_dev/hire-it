import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "OpenForRemote",
  initialState: {
    openForRemote: "",
  },
  reducers: {
    changeOpenForRemote: (state, action) => {
      // mutation
      state.openForRemote = action.payload;
    },
  },
});
