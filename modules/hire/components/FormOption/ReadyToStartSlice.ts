import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "ReadyToStart",
  initialState: {
    readyToStart: "",
  },
  reducers: {
    changeReadyToStart: (state, action) => {
      // mutation
      state.readyToStart = action.payload;
    },
  },
});
