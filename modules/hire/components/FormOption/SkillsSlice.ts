import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "Skills",
  initialState: {
    skills: "",
  },
  reducers: {
    changeSkills: (state, action) => {
      // mutation
      state.skills = action.payload;
    },
  },
});
