import { createSlice } from "@reduxjs/toolkit";

export default createSlice({
  name: "Position",
  initialState: {
    position: "Developers",
  },
  reducers: {
    changePosition: (state, action) => {
      // mutation
      state.position = action.payload;
    },
  },
});
