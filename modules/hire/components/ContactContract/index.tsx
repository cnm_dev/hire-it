import { createProject } from "@/apis/Project";
import CButton from "@/common/components/controls/CButton";
import CSpinner from "@/common/components/controls/CSpinner";
import CTextField from "@/common/components/controls/CTextField";
import { valueAllOption } from "@/constants/contract";
import { RemoveStorageOption } from "@/constants/contract";
import useCustomer from "@/hooks/useCustomer";
import NotFoundValue from "@/modules/NotFoundValue";
import { emailInfomate } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateUsedServices";
import axios from "axios";
import { Session } from "next-auth";
import { useSession } from "next-auth/react";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { SubmitHandler, useForm } from "react-hook-form";
import { toast } from "react-toastify";
type ContactContractProps = {
  session: Session;
};

const ContactContract = ({ session }: ContactContractProps) => {
  const router = useRouter();

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const { customer, loading } = useCustomer({
    customerId: session.user.id,
  });
  const { register, handleSubmit } = useForm<{ email: string }>({
    reValidateMode: "onSubmit",
  });

  const onSubmit: SubmitHandler<{ email: string }> = (data) => {
    const valueStorage = valueAllOption();
    setIsLoading(true);
    createProject({
      skills: valueStorage.skills,
      commitment:
        valueStorage.commitment === "none"
          ? undefined
          : valueStorage.commitment,
      openForRemote:
        valueStorage.openForRemote === "none"
          ? undefined
          : valueStorage.openForRemote,
      projectLengthFrom: valueStorage.projectLengthFrom,
      projectLengthTo: valueStorage.projectLengthTo,
      projectName: valueStorage.projectName,
      readyToStart:
        valueStorage.readyToStart === "none"
          ? undefined
          : valueStorage.readyToStart,
      situation: valueStorage.situation,
      budgetFrom: valueStorage.budgetFrom,
      budgetTo: valueStorage.budgetTo,
      companySizeFrom: valueStorage.companySizeFrom,
      companySizeTo: valueStorage.companySizeTo,
      position: valueStorage.position,
      productSpecs: valueStorage.productSpecs,
      customerId: customer[0].id,
      totalMoney: 0,
      listIDEmployee: [],
      status: "draft",
    }).then(() => {
      const html = emailInfomate({
        name: customer[0].users_permissions_user.fullName,
        email: "Vohieu7k58@gmail.com",
        phoneNumber: "0377723460",
      });
      axios
        .post("/api/verifyEmail/sendEmail", {
          email: data.email,
          html,
        })
        .then(() => {
          setIsLoading(false);
          toast.success("Send email success");
          router.replace("/");
          RemoveStorageOption();
        })
        .catch((err) => {
          toast.error(err.message);
          setIsLoading(false);
        });
    });
  };

  return (
    <div className="contact-contract">
      <NotFoundValue
        content={
          <div className="content-contract">
            <h2>Thank you for using our service</h2>
            <span>
              The system is currently unable to generate a recommendation
              contract for you.
            </span>
            <span>
              Leave your email and we will contact you as soon as possible.
            </span>
            <div className="email-send__contract">
              <form onSubmit={handleSubmit(onSubmit)}>
                <CTextField
                  registerName={{
                    ...register("email", {
                      required: true,
                      pattern: {
                        value: /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/,
                        message: "*Invalid email format",
                      },
                    }),
                  }}
                  placeholder="Enter your email"
                  label="Email"
                  name="email"
                />
                <CButton
                  disabled={isLoading}
                  type="submit"
                  text={isLoading ? <CSpinner size="small" /> : "Subscribe"}
                />
              </form>
            </div>
          </div>
        }
      />
    </div>
  );
};

export default ContactContract;
