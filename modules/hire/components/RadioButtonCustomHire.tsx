import { Box, FormControlLabel, Radio } from "@mui/material";
import React from "react";

interface PropsRadio {
  item?: String;
  name?: string | undefined;
  state?: String;
  setState: React.Dispatch<React.SetStateAction<string | undefined>>;
  label?: string;
  description?: string;
}

const RadioButtonCustomHire = ({
  item,
  name,
  state,
  setState,
  label,
  description,
}: PropsRadio) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setState(event.target.value);
  };

  const handleBoxClick = () => {};

  return (
    <Box onClick={handleBoxClick}>
      <FormControlLabel
        className="radio-button-custom-hire"
        control={
          <div className="container">
            <div className="title-radio">
              <Radio
                size="small"
                checked={state === item}
                value={item}
                name={name}
                onChange={handleChange}
                color="primary"
              />

              <p className="label">{label}</p>
            </div>
            <p className="description">{description}</p>
          </div>
        }
        label=""
      />
    </Box>
  );
};

export default RadioButtonCustomHire;
