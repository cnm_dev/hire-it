/* eslint-disable @next/next/no-img-element */
import {
  HomeHashtag,
  Map1,
  Mobile,
  SmsTracking,
  UserSquare,
} from "iconsax-react";
import React, { useMemo } from "react";
import { Employee } from "@/types/models";
import ItemMemberDetail from "@/modules/ContractDetail/components/ItemMemberDetail";
import CLoading from "@/common/components/controls/CLoading";
import image from "@/common/assets/image/all-image";
import useCustomer from "@/hooks/useCustomer";
import { Session } from "next-auth";
import {
  BudgetContract,
  CommitmentContract,
  CompanySizeContract,
  NameContract,
  OpenForRemoteContract,
  PositionContract,
  PriceMoney,
  ProductSpecsContract,
  ProjectLengthContract,
  ReadyToStartContract,
  RemoveStorageOption,
  SituationContract,
  valueAllOption,
} from "@/constants/contract";
import CButton from "@/common/components/controls/CButton";
import { PDFDownloadLink } from "@react-pdf/renderer";
import { PDF } from "../../../../modules/review/components/pdf";
import { formatDate } from "@/utils";
import { createProject } from "@/apis/Project";
import { createContract } from "@/apis/Contract";
import { toast } from "react-toastify";
import { useRouter } from "next/router";
import { createContractHistory } from "@/apis/HistoryContract";
import { createInformates } from "@/apis/Informates";
import axios from "axios";
import { emailInfomate } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateUsedServices";
import { emailInfomateSuccess } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateCreateContractSuccess";

type ContractDetailProps = {
  employees: Employee[] | undefined;
  session: Session;
};

const ContractOption = ({ employees, session }: ContractDetailProps) => {
  const router = useRouter();
  const [isLoading, setIsLoading] = React.useState<boolean>(false);

  const { customer, loading } = useCustomer({
    customerId: session.user.id,
  });

  const priceMoney = useMemo(() => {
    if (employees) {
      return PriceMoney(employees);
    }
  }, [employees]);

  const listIDEmployee = useMemo(() => {
    let listId: string[] = [];
    if (employees) {
      employees.map((employee) => {
        listId.push(employee.id);
      });
    }
    return listId;
  }, [employees]);

  const handelAddProject = (status: "wait" | "inactive") => {
    setIsLoading(true);
    if (priceMoney) {
      const valueStorage = valueAllOption();
      createProject({
        ...valueStorage,
        customerId: customer[0].id,
        totalMoney: priceMoney + priceMoney * 0.05,
        listIDEmployee: listIDEmployee,
        status: "draft",
      })
        .then((course) => {
          if (course.data) {
            createContract({ projectId: course.data.id, status })
              .then((course) => {
                if (status === "wait") {
                  const html = emailInfomateSuccess({
                    name: customer[0].users_permissions_user.fullName,
                    email: "Vohieu7k58@gmail.com",
                    phoneNumber: "0377723460",
                    contractId: course.data.id,
                    totalMoney: priceMoney + priceMoney * 0.05,
                    host: "https://hire-it-git-chanh-dev-update-project-hireit.vercel.app/contract",
                  });
                  axios.post("/api/verifyEmail/sendEmail", {
                    email: customer[0].users_permissions_user.email,
                    html,
                  });
                  createContractHistory({
                    contractId: course.data.id,
                    type: "create",
                    dateUpdate: new Date(),
                    status: "complete",
                    totalMoney: priceMoney + priceMoney * 0.05,
                  }).then(() => {
                    createContractHistory({
                      contractId: course.data.id,
                      type: "payment",
                      dateUpdate: new Date(),
                      status: "wait",
                      totalMoney: priceMoney + priceMoney * 0.05,
                    }).then(() => {
                      createInformates({
                        title: "Create contract",
                        description: `Create contract ${valueStorage.projectName} success`,
                        route: "/contract",
                        customerId: customer[0].id,
                      }).then(() => {
                        setIsLoading(false);
                        localStorage.setItem("IdContract", course.data.id);
                        toast.success("Confirm contract example complete");
                        router.replace("/payment");
                        RemoveStorageOption();
                      });
                    });
                  });
                }
                if (status === "inactive") {
                  createContractHistory({
                    contractId: course.data.id,
                    type: "create",
                    dateUpdate: new Date(),
                    status: "complete",
                    totalMoney: priceMoney + priceMoney * 0.05,
                  }).then(() => {
                    createContractHistory({
                      contractId: course.data.id,
                      type: "inactive",
                      dateUpdate: new Date(),
                      status: "complete",
                      totalMoney: priceMoney + priceMoney * 0.05,
                    }).then(() => {
                      createInformates({
                        title: "Inactive contract",
                        description: `Inactive contract ${valueStorage.projectName} success`,
                        route: "/contract",
                        customerId: customer[0].id,
                      }).then(() => {
                        setIsLoading(false);
                        localStorage.setItem("IdContract", course.data.id);
                        toast.success("InActive contract example complete");
                        router.replace("/");
                        RemoveStorageOption();
                      });
                    });
                  });
                }
              })
              .catch((err) => {
                setIsLoading(false);
                toast.error(err.message);
              });
          }
        })
        .catch((err) => {
          setIsLoading(false);
          toast.error(err.message);
        });
    }
  };

  return (
    <div className="contract-detail">
      <img src={image.logo_gif} alt="" />
      <div className="contract-detail__header">
        <h3>Contract Information</h3>
      </div>
      <div className="contract-detail__container">
        <div className="contract-container__information">
          <h4>Your information</h4>
          {!loading && customer.length !== 0 ? (
            <div className="contract__information-detail">
              <div>
                <div className="contract-item__infor">
                  <span className="item-infor__title">
                    <HomeHashtag />
                    Company:
                  </span>
                  <span>{customer[0].companyName}</span>
                </div>
                <div className="contract-item__infor">
                  <span className="item-infor__title">
                    <SmsTracking /> Fax number:
                  </span>
                  <span>{customer[0].codeFax}</span>
                </div>
                <div className="contract-item__infor">
                  <span className="item-infor__title">
                    <Map1 /> Address:
                  </span>
                  <span>{customer[0].users_permissions_user.address}</span>
                </div>
              </div>
              <div>
                <div className="contract-item__infor">
                  <span className="item-infor__title">
                    <UserSquare />
                    Your name:
                  </span>
                  <span>{customer[0].users_permissions_user.fullName}</span>
                </div>
                <div className="contract-item__infor">
                  <span className="item-infor__title">
                    <Mobile />
                    Phone number:
                  </span>
                  <span>{customer[0].users_permissions_user.phoneNumber}</span>
                </div>
              </div>
            </div>
          ) : (
            <CLoading />
          )}
        </div>
        <div className="contract-container__preview">
          <h4>Preview Contract</h4>
          <div className="contract-preview__detail">
            <div className="contract-preview__des">
              <p>Description: </p>
              <div className="contract-preivew__list">
                <span>{PositionContract()}</span>
                <span>{SituationContract()}</span>
                <span>{CommitmentContract()}</span>
                <span>{OpenForRemoteContract()}</span>
                <span>{ProductSpecsContract()}</span>
                <span>{ReadyToStartContract()}</span>
                <span>{CompanySizeContract()}</span>
                <span>{ProjectLengthContract()}</span>
                <span>{BudgetContract()}</span>
              </div>
            </div>
            <div className="contract-preview__view">
              <p>View contract: </p>
              {employees && !loading && customer.length !== 0 && priceMoney ? (
                <PDFDownloadLink
                  document={
                    <PDF
                      date={formatDate(new Date())}
                      arrMember={employees}
                      inforCustomer={customer[0]}
                      description={[
                        PositionContract(),
                        SituationContract(),
                        CommitmentContract(),
                        OpenForRemoteContract(),
                        ProductSpecsContract(),
                        ReadyToStartContract(),
                        CompanySizeContract(),
                        ProjectLengthContract(),
                        BudgetContract(),
                      ]}
                      priceMoney={priceMoney}
                    />
                  }
                  fileName={NameContract()}
                >
                  <div className="contract-view__item">
                    <img
                      src="https://cdn.dribbble.com/users/1363206/screenshots/7188770/media/c9e56d0a3d39c082ba0e2f7b2421d252.jpg?compress=1&resize=1000x750&vertical=top"
                      alt=""
                    />
                    <div className="contract-view__infor">
                      <span>{NameContract()}</span>
                      <span>Recently ago</span>
                    </div>
                  </div>
                </PDFDownloadLink>
              ) : (
                <CLoading />
              )}
            </div>
          </div>
        </div>
        <div className="contract-container__member">
          <h4>List Member</h4>
          <div className="contract-member__list">
            {!employees ? (
              <CLoading />
            ) : (
              employees.map((employee) => (
                <ItemMemberDetail key={employee.id} employee={employee} />
              ))
            )}
          </div>
        </div>
        <div className="contract-container__cost">
          <h4>Contract cost</h4>
          {!priceMoney ? (
            <CLoading />
          ) : (
            <div className="contract-cost__detail">
              <div className="cost-detail__item">
                <span>Price</span>
                <span>{priceMoney.toFixed(2)}$</span>
              </div>
              <div className="cost-detail__item">
                <span>VAT (5%)</span>
                <span>{(priceMoney * 0.05).toFixed(2)}$</span>
              </div>
              <div className="cost-detail__item amount">
                <span>Amount Money</span>
                <span>{(priceMoney + priceMoney * 0.05).toFixed(2)}$</span>
              </div>
              <div className="cost-detail__item amount">
                <span>Amount paid</span>
                <span>0.00$</span>
              </div>
              <div className="cost-detail__item amount">
                <span>Discount</span>
                <span>0.00$</span>
              </div>
              <div className="cost-detail__item total">
                <span>Total money</span>
                <span>{(priceMoney + priceMoney * 0.05).toFixed(2)}$</span>
              </div>
            </div>
          )}
        </div>
        <div className="contract-container__btn">
          <CButton
            text="InActive"
            disabled={isLoading}
            onClick={() => handelAddProject("inactive")}
          />
          <CButton
            text="Payment"
            disabled={isLoading}
            onClick={() => handelAddProject("wait")}
          />
        </div>
      </div>
    </div>
  );
};

export default ContractOption;
