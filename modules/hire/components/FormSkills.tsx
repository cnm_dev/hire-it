/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import CAutocompleteOption from "@/common/components/controls/CAutocompleteOption";
import CLoading from "@/common/components/controls/CLoading";
import useSkills from "@/hooks/useSkills";
import { getStringStorage } from "@/utils";
import { Button, CircularProgress, TextField } from "@mui/material";
import { ArrowLeft2, ArrowRight2 } from "iconsax-react";
import { useRouter } from "next/router";
import React from "react";
import { SubmitHandler, useForm } from "react-hook-form";

interface propsFormOption {
  options?: {
    value: string;
  }[];
  questionName: string;
  href: any;
  showBack?: boolean;
}

const names = [
  {
    id: "1",
    positionName: "Designer",
  },
  {
    id: "2",
    positionName: "Developer",
  },
  {
    id: "3",
    positionName: "Finance Expert",
  },
  {
    id: "4",
    positionName: "Product Manager",
  },
  {
    id: "5",
    positionName: "Project Manager",
  },
];

const FormSkills = ({
  questionName,
  href,
  showBack,
}: propsFormOption) => {
  const { control, watch, handleSubmit } = useForm<any>({
    reValidateMode: "onSubmit",
  });
  const router = useRouter();
  const [selectedValue, setSelectedValue] = React.useState<
    string | undefined
  >();
  const [loadingNext, setLoadingNext] = React.useState(false);
  const [loadingSkip, setLoadingSkip] = React.useState(false);
  const listSKill = watch("skills", "");
  React.useMemo(() => {
    setLoadingSkip(false);
  }, []);

  const { loading, skills } = useSkills();
  const [customSkills, setCustomSkills] = React.useState<
    {
      id: number;
      name: string;
    }[]
  >([]);

  React.useEffect(() => {
    if (!loading) {
      let t: {
        id: number;
        name: string;
      }[] = [];
      const positionName = getStringStorage(localStorage.getItem("Position"));
      skills.map((data) => {

        if (data.position?.positionName === positionName) {
          t.push({ id: data.id, name: data.skillName });
        }
      });
      setCustomSkills(t);
    }
  }, [loading, skills]);

  const handelClick: SubmitHandler<any> = (data) => {
    localStorage.setItem("Skills", JSON.stringify(listSKill));
    setLoadingSkip(false);
    router.push(href);
  };

  const handelClickSkip = () => {
    localStorage.setItem("Skills", JSON.stringify([]));
    setLoadingSkip(false);
  };

  return loading ? (
    <CLoading fullScreen />
  ) : (
    <div className="main-formOption">
      <img src={image.logo_gif} alt="" width={250} height={250} style={{}} />
      <div className="form-option-container">
        <form onSubmit={handleSubmit(handelClick)}>
          <div className="form-radio-option">
            <p className="title">{questionName}</p>
            <CAutocompleteOption
              name="skills"
              open={true}
              control={control}
              placeholder="Desired areas of expertise (e.g., Agile, Scrum, Wordpress, etc.)"
              values={customSkills}
              multiple={true}
              isInline={true}
              clasName="skills-item"
              ListboxProps={{ className: "cautocomplete" }}
            />
          </div>
          <div className="container_button">
            {showBack ? (
              <Button
                className="button_back"
                variant="contained"
                onClick={() => router.back()}
                startIcon={<ArrowLeft2 size="20" color="rgba(0,0,0,0.7)" />}
              >
                Back
              </Button>
            ) : null}
            <div className="btn-right">
              <Button
                className="skip"
                variant="outlined"
                onClick={() => {
                  handelClickSkip();
                  setLoadingSkip(true);
                }}
              >
                {/* <Link href={href} className="next" style={{ color: "#000" }}> */}
                {loadingSkip ? (
                  <CircularProgress
                    size={18}
                    sx={{ color: "#0a3984", marginTop: 1 }}
                  />
                ) : (
                  "Skip"
                )}
                {/* </Link> */}
              </Button>
              <Button
                type="submit"
                className="button"
                variant="contained"
                disabled={listSKill ? false : true}
                onClick={() => {
                  setLoadingNext(true);
                }}
                sx={{ color: "#fff" }}
                endIcon={
                  loadingNext ? (
                    <CircularProgress size={18} sx={{ color: "#fff" }} />
                  ) : (
                    <ArrowRight2 size="20" color="#ffff" />
                  )
                }
              >
                {/* <Link href={href} className="next"> */}
                <span className="next">Next</span>

                {/* </Link> */}
              </Button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default FormSkills;
