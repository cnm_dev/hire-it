/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import CButton from "@/common/components/controls/CButton";
import CModal from "@/common/components/controls/CModal";
import { Contract, Employee, Review } from "@/types/models";
import { Star1 } from "iconsax-react";
import React, { useState } from "react";
import ModalReview from "./ModalReview";
import Link from "next/link";

type employeeProps = {
  employee: Employee;
  review?: Review | undefined;
  status?: string;
  contract?: Contract;
};
const ItemMemberDetail = ({
  contract,
  employee,
  review,
  status,
}: employeeProps) => {
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
  return (
    <div className="item-contract__detail">
      {!employee.users_permissions_user.avartar ? (
        <img src={image.image_default} alt="" />
      ) : (
        <img src={employee.users_permissions_user.avartar} alt="" />
      )}
      <div className="item-contract__infor">
        {review && status === "complete" && (
          <div className="item-review__star">
            <Star1 variant="Bold" size={16} />
            {review.rate}
          </div>
        )}
        <Link href={`/member/${employee.id}`}>
          <h5>{employee.users_permissions_user.fullName}</h5>
        </Link>
        <span>{employee.users_permissions_user.email}</span>
        {review && status === "complete" && (
          <div className="item-contract__review">
            <span>You review: {review.content}</span>
          </div>
        )}
        {!review && status === "complete" && contract && (
          <CModal
            isOpenModal={isOpenModal}
            setIsOpenModal={setIsOpenModal}
            button={<CButton text="Review" />}
            modal={
              <ModalReview
                contract={contract}
                setIsOpenModal={setIsOpenModal}
                employeeId={employee.id}
                avatar={employee.users_permissions_user.avartar}
                fullname={employee.users_permissions_user.fullName}
                email={employee.users_permissions_user.email}
              />
            }
          />
        )}
      </div>
    </div>
  );
};

export default ItemMemberDetail;
