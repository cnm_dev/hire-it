/* eslint-disable @next/next/no-img-element */
import { createPreview } from "@/apis/Preview";
import image from "@/common/assets/image/all-image";
import CButton from "@/common/components/controls/CButton";
import CRating from "@/common/components/controls/CRating";
import CTextField from "@/common/components/controls/CTextField";
import { Contract, Reviews } from "@/types/models";
import React from "react";
import { useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { mutate } from "swr";

type ModalReviewProps = {
  avatar: string;
  fullname: string;
  email: string;
  contract: Contract;
  employeeId: string;
  setIsOpenModal: React.Dispatch<React.SetStateAction<boolean>>;
};

const ModalReview = ({
  avatar,
  fullname,
  email,
  contract,
  employeeId,
  setIsOpenModal,
}: ModalReviewProps) => {
  const [star, setStar] = React.useState<number | null>(5);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<Reviews>();

  const handelSubmitReview = (value: Reviews) => {
    const customerId = contract.project.customer.id;
    const userId = contract.project.customer.users_permissions_user.id;
    star &&
      createPreview({
        content: value.content,
        contractId: contract.id,
        customerId,
        employeeId: employeeId,
        rate: star,
      })
        .then(() => {
          mutate(`/api/customers/${userId}/contracts/${contract.id}`);
          setIsOpenModal(false);
          toast.success("Thanks you for your review");
        })
        .catch((err) => {
          toast.error(err.message);
        });
  };
  return (
    <div className="review-container">
      <form onSubmit={handleSubmit(handelSubmitReview)}>
        <div>
          <div className="review-container__employee">
            {avatar ? (
              <img src={avatar} alt="" />
            ) : (
              <img src={image.image_default} alt="" />
            )}
            <div className="review-employee__information">
              <h5>{fullname}</h5>
              <span>{email}</span>
            </div>
          </div>
          <div className="review-container__content">
            <div className="review-container__star">
              <CRating setStar={setStar} readOnly={false} />
              <p>{Number(star).toFixed(1)}</p>
            </div>
            <CTextField
              registerName={{ ...register("content", { required: true }) }}
              placeholder="Enter your review of this employee"
              name="content"
            />
            <p className="error">
              {errors.content?.type === "required" && "Review is not null"}
            </p>
          </div>
        </div>
        <CButton type="submit" text="Post review" />
      </form>
    </div>
  );
};

export default ModalReview;
