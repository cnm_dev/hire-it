/* eslint-disable @next/next/no-img-element */
import {
  HomeHashtag,
  Map1,
  Mobile,
  SmsTracking,
  UserSquare,
} from "iconsax-react";
import React from "react";
import ItemMemberDetail from "./components/ItemMemberDetail";
import { formatDate } from "@/utils";
import {
  BudgetContract,
  CommitmentContract,
  CompanySizeContract,
  OpenForRemoteContract,
  PositionContract,
  ProductSpecsContract,
  ProjectLengthContract,
  ReadyToStartContract,
  SituationContract,
} from "@/constants/contract";
import { PDFDownloadLink } from "@react-pdf/renderer";
import { PDF } from "../review/components/pdf";
import { useSelector } from "react-redux";
import { contractsDetailSelector } from "@/redux/selector";

const ContractDetail = () => {
  const contract = useSelector(contractsDetailSelector);

  const reviewEmployee = (emailEmployee: string) => {
    if (contract.previews) {
      return contract.previews.find(
        (review) =>
          review.employee.users_permissions_user.username === emailEmployee
      );
    }
  };

  const totalMoney = React.useMemo(() => {
    return Number(contract.project.totalMoney);
  }, [contract.project.totalMoney]);

  return (
    <div className="contract-detail">
      <div className="contract-detail__header">
        <h3>Contract Information</h3>
      </div>
      <div className="contract-detail__container">
        <div className="contract-container__information">
          <h4>Your information</h4>
          <div className="contract__information-detail">
            <div>
              <div className="contract-item__infor">
                <span className="item-infor__title">
                  <HomeHashtag />
                  Company:
                </span>
                <span>{contract.project.customer.companyName}</span>
              </div>
              <div className="contract-item__infor">
                <span className="item-infor__title">
                  <SmsTracking /> Fax number:
                </span>
                <span>{contract.project.customer.codeFax}</span>
              </div>
              <div className="contract-item__infor">
                <span className="item-infor__title">
                  <Map1 /> Address:
                </span>
                <span>
                  {contract.project.customer.users_permissions_user.address}
                </span>
              </div>
            </div>
            <div>
              <div className="contract-item__infor">
                <span className="item-infor__title">
                  <UserSquare />
                  Your name:
                </span>
                <span>
                  {contract.project.customer.users_permissions_user.fullName}
                </span>
              </div>
              <div className="contract-item__infor">
                <span className="item-infor__title">
                  <Mobile />
                  Phone number:
                </span>
                <span>
                  {contract.project.customer.users_permissions_user.phoneNumber}
                </span>
              </div>
            </div>
          </div>
        </div>
        <div className="contract-container__preview">
          <h4>Preview Contract</h4>
          <div className="contract-preview__detail">
            <div className="contract-preview__des">
              <p>Description: </p>
              <div className="contract-preivew__list">
                {contract.project.position && (
                  <span>{PositionContract(contract.project.position)} </span>
                )}

                {contract.project.situation && (
                  <span>{SituationContract(contract.project.situation)} </span>
                )}

                {contract.project.commitment && (
                  <span>{CommitmentContract(contract.project.commitment)}</span>
                )}

                {contract.project.openForRemote && (
                  <span>
                    {OpenForRemoteContract(contract.project.openForRemote)}
                  </span>
                )}

                {contract.project.productSpecs && (
                  <span>
                    {ProductSpecsContract(contract.project.productSpecs)}
                  </span>
                )}

                {contract.project.readyToStart && (
                  <span>
                    {ReadyToStartContract(contract.project.readyToStart)}
                  </span>
                )}

                {contract.project.companySizeFrom !== null &&
                  contract.project.companySizeTo !== null && (
                    <span>
                      {CompanySizeContract(
                        contract.project.companySizeFrom,
                        contract.project.companySizeTo
                      )}
                    </span>
                  )}

                {contract.project.projectLengthFrom !== null &&
                  contract.project.projectLengthTo !== null && (
                    <span>
                      {ProjectLengthContract(
                        contract.project.projectLengthFrom,
                        contract.project.projectLengthTo
                      )}
                    </span>
                  )}

                {contract.project.budgetFrom !== null &&
                  contract.project.budgetTo !== null && (
                    <span>
                      {BudgetContract(
                        contract.project.budgetFrom,
                        contract.project.budgetTo
                      )}
                    </span>
                  )}
              </div>
            </div>
            <div className="contract-preview__view">
              <p>View contract: </p>
              <PDFDownloadLink
                document={
                  <PDF
                    date={formatDate(contract.project.publishedAt)}
                    arrMember={contract.project.employees}
                    inforCustomer={contract.project.customer}
                    description={[
                      contract.project.position &&
                        PositionContract(contract.project.position),
                      contract.project.situation &&
                        SituationContract(contract.project.situation),
                      contract.project.commitment &&
                        CommitmentContract(contract.project.commitment),
                      contract.project.openForRemote &&
                        OpenForRemoteContract(contract.project.openForRemote),
                      contract.project.productSpecs &&
                        ProductSpecsContract(contract.project.productSpecs),
                      contract.project.readyToStart &&
                        ReadyToStartContract(contract.project.readyToStart),
                      contract.project.companySizeFrom &&
                        contract.project.companySizeTo &&
                        CompanySizeContract(
                          contract.project.companySizeFrom,
                          contract.project.companySizeTo
                        ),
                      contract.project.projectLengthFrom &&
                        contract.project.projectLengthTo &&
                        ProjectLengthContract(
                          contract.project.projectLengthFrom,
                          contract.project.projectLengthTo
                        ),
                      contract.project.budgetFrom &&
                        contract.project.budgetTo &&
                        BudgetContract(
                          contract.project.budgetFrom,
                          contract.project.budgetTo
                        ),
                    ]}
                    priceMoney={contract.project.totalMoney}
                  />
                }
                fileName={contract.project.projectName}
              >
                <div className="contract-view__item">
                  <img
                    src="https://cdn.dribbble.com/users/1363206/screenshots/7188770/media/c9e56d0a3d39c082ba0e2f7b2421d252.jpg?compress=1&resize=1000x750&vertical=top"
                    alt=""
                  />
                  <div className="contract-view__infor">
                    <span>{contract.project.projectName}</span>
                    <span>Recently ago</span>
                  </div>
                </div>
              </PDFDownloadLink>
            </div>
          </div>
        </div>
        <div className="contract-container__member">
          <h4>List Member</h4>
          <div className="contract-member__list">
            {contract.project.employees.map((employee) => (
              <ItemMemberDetail
                status={contract.status}
                key={employee.id}
                contract={contract}
                employee={employee}
                review={reviewEmployee(employee.users_permissions_user.email)}
              />
            ))}
          </div>
        </div>
        <div className="contract-container__cost">
          <h4>Contract cost</h4>
          <div className="contract-cost__detail">
            <div className="cost-detail__item">
              <span>Price</span>
              <span>{totalMoney.toFixed(2)}$</span>
            </div>
            <div className="cost-detail__item">
              <span>VAT (5%)</span>
              <span>{(totalMoney * 0.05).toFixed(2)}$</span>
            </div>
            <div className="cost-detail__item amount">
              <span>Amount Money</span>
              <span>{(totalMoney + totalMoney * 0.05).toFixed(2)}$</span>
            </div>
            <div className="cost-detail__item amount">
              <span>Amount paid</span>
              {contract.status !== "active" &&
              contract.status !== "complete" ? (
                <span>0.00$</span>
              ) : (
                <span>{(totalMoney + totalMoney * 0.05).toFixed(2)}$</span>
              )}
            </div>
            <div className="cost-detail__item amount">
              <span>Discount</span>
              <span>0.00$</span>
            </div>
            <div className="cost-detail__item total">
              <span>Total money</span>
              {contract.status !== "active" &&
              contract.status !== "complete" ? (
                <span>{(totalMoney + totalMoney * 0.05).toFixed(2)}$</span>
              ) : (
                <span>0.00$</span>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ContractDetail;
