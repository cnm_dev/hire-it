/* eslint-disable react-hooks/rules-of-hooks */
import { langs } from "@/mock/langs";
import React from "react";
import { TypeLang } from "../Home/components/HotMember";
import CardPayment from "./components/CardPayment";
import PreviewContract from "./components/PreviewContract";
import useContract from "@/hooks/useContract";
import CLoading from "@/common/components/controls/CLoading";

type PaymentProps = {
  lang: TypeLang["lang"];
  idContract: string;
  customerId: string;
};

const index = ({ lang, idContract, customerId }: PaymentProps) => {
  const { loading, contract } = useContract({
    customerId: customerId,
    contractId: idContract,
  });

  return (
    <div id="payment">
      {loading && contract.length === 0 ? (
        <CLoading />
      ) : (
        <>
          <div className="payment-left">
            <h2>{langs.payment.contract[lang]}</h2>
            <div className="payment-left__bank">
              <h4>{langs.payment.choosemethod[lang]} </h4>
              <CardPayment />
            </div>
            <div className="payment-left__infor">
              <h4>{langs.payment.yourinfor[lang]}</h4>
              <ul>
                <li>
                  {langs.payment.namecompany[lang]}:
                  <span>{contract[0].project.customer.companyName}</span>
                </li>
                <li>
                  {langs.payment.name[lang]}:
                  <span>
                    {
                      contract[0].project.customer.users_permissions_user
                        .fullName
                    }
                  </span>
                </li>
                <li>
                  {langs.payment.faxcode[lang]}:
                  <span>{contract[0].project.customer.codeFax}</span>
                </li>
                <li>
                  {langs.payment.location[lang]}:
                  <span>
                    {
                      contract[0].project.customer.users_permissions_user
                        .address
                    }
                  </span>
                </li>
              </ul>
            </div>
            <div className="payment-left__contract">
              <h4>{langs.payment.contractterm[lang]}</h4>
              <ul>
                <li>
                  IT employees with experience and sufficient qualifications to
                  meet the job requirements of the client.
                </li>
                <li>
                  The minimum lease term is 3 months and the maximum is 1 year.
                </li>
                <li>The client may extend the lease contract if necessary.</li>
                <li>
                  The client may request changes to the job or scope of work if
                  necessary.
                </li>
                <li>
                  The salary of the IT employee is agreed upon in advance and is
                  paid monthly or at the agreed-upon interval.
                </li>
                <li>
                  The fee for hiring IT labor includes salary, incidental fees
                  related to the job, and other costs related to hiring
                  employees.
                </li>
                <li>
                  The client is responsible for fully paying salaries and fees
                  for hiring IT labor.
                </li>
                <li>
                  The two parties may terminate the contract when there is an
                  agreement on payment of wages and fees.
                </li>
                <li>
                  If either party violates the terms of the contract, the other
                  party has the right to terminate the contract.
                </li>
                <li>
                  Both parties commit to maintaining confidentiality of
                  information exchanged.
                </li>
              </ul>
            </div>
          </div>
          <div className="payment-right">
            <PreviewContract
              contractId={contract[0].id}
              project={contract[0].project}
              lang={lang}
              contract={contract[0]}
            />
          </div>
        </>
      )}
    </div>
  );
};

export default index;
