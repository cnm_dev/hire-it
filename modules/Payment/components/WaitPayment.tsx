/* eslint-disable @next/next/no-img-element */
import image from "@/common/assets/image/all-image";
import { useRouter } from "next/router";
import React, { useState } from "react";

const WaitPayment = () => {
  const router = useRouter();
  const [vnp_ResponseCode] = useState(`${router.query["vnp_ResponseCode"] || ""}`);

  console.log(vnp_ResponseCode);
  

  return (
    <div className="wait-payment">
      <img src={image.Payment} alt="" />;
    </div>
  );
};

export default WaitPayment;
