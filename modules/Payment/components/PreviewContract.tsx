import { updateContract } from "@/apis/Contract";
import { createContractHistory } from "@/apis/HistoryContract";
import { createInformates } from "@/apis/Informates";
import { updateProject } from "@/apis/Project";
import CButton from "@/common/components/controls/CButton";
import CButtonIcon from "@/common/components/controls/CButtonIcon";
import CCheckbox from "@/common/components/controls/CCheckbox";
import { informate_payment, url_payment } from "@/constants/payment";
import useHistoryContract from "@/hooks/useHistoryContract";
import { langs } from "@/mock/langs";
import { TypeLang } from "@/modules/Home/components/HotMember";
import { emailInfomateComplete } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateCreateContractComplete";
import { emailInfomateExtend } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateExtendContract";
import { Contract, Project } from "@/types/models";
import { addWeekstoDate, formatDate, formatNumber } from "@/utils";
import { Link } from "@mui/material";
import axios from "axios";
import { useRouter } from "next/router";
import React, { useEffect, useMemo, useState } from "react";
import { CiTwitter, CiFacebook, CiInstagram, CiYoutube } from "react-icons/ci";
import { toast } from "react-toastify";

type PreviewContractProps = {
  lang: TypeLang["lang"];
  project: Project;
  contractId: string;
  contract: Contract;
};

const PreviewContract = ({
  lang,
  project,
  contractId,
  contract,
}: PreviewContractProps) => {
  const router = useRouter();

  const { historyContract, loading } = useHistoryContract({
    contractId: contract.id,
  });

  const [isConfirm, setIsConfirm] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isToastDisplayed, setIsToastDisplayed] = useState(false);

  const historyContractExtend = useMemo(() => {
    if (!loading && historyContract && historyContract.length > 0) {
      return historyContract.filter((history) => history.type === "extend");
    }
  }, [historyContract, loading]);

  const handelUpdateContract = () => {
    console.log(historyContractExtend);

    if (contract.isExtend === true) {
      setIsLoading(true);
      historyContractExtend &&
        historyContractExtend.length > 0 &&
        updateProject({
          projectId: project.id,
          totalMoney: historyContractExtend[0].totalMoney,
        })
          .then(() => {
            const html = emailInfomateExtend({
              name: contract.project.customer.users_permissions_user.fullName,
              email: "Vohieu7k58@gmail.com",
              phoneNumber: "0377723460",
              contractId: contract.id,
              host: "https://hire-it-git-chanh-dev-update-project-hireit.vercel.app/contract",
              endDate: contract.endDate,
              endDateExtend: historyContractExtend[0].endDate,
            });
            axios.post("/api/verifyEmail/sendEmail", {
              email: contract.project.customer.users_permissions_user.email,
              html,
            });
            updateContract({
              contractId: contract.id,
              endDate: historyContractExtend[0].endDate,
              isExtend: false,
            })
              .then(() => {
                createContractHistory({
                  contractId: contract.id,
                  status: "complete",
                  dateUpdate: new Date(),
                  endDate: historyContractExtend[0].endDate,
                  totalMoney: historyContractExtend[0].totalMoney,
                  type: "extend",
                })
                  .then(() => {
                    createInformates({
                      description: `Payment extend contract ${contract.project.projectName} success`,
                      title: "Payment extend contract",
                      route: "/contract",
                      customerId: contract.project.customer.id,
                    }).then(() => {
                      toast.success("Payment extend contract success");
                      setIsLoading(false);
                      localStorage.removeItem("IdContract");
                      router.replace("/contract");
                    });
                  })
                  .catch((err) => {
                    setIsLoading(false);
                    toast.error(err.message);
                  });
              })
              .catch((err) => {
                setIsLoading(false);
                toast.error(err.message);
              });
          })
          .catch((err) => {
            setIsLoading(false);
            toast.error(err.message);
          });
    } else {
      const nowDate = new Date();
      const endDate = addWeekstoDate(nowDate, project.projectLengthTo);
      setIsLoading(true);
      updateContract({
        contractId: contractId,
        status: "active",
        startDate: new Date(),
        endDate,
      })
        .then(async () => {
          const html = emailInfomateComplete({
            name: contract.project.customer.users_permissions_user.fullName,
            email: "Vohieu7k58@gmail.com",
            phoneNumber: "0377723460",
            contractId: contract.id,
            host: "https://hire-it-git-chanh-dev-update-project-hireit.vercel.app/contract",
          });

          await axios
            .post("/api/verifyEmail/sendEmail", {
              email: contract.project.customer.users_permissions_user.email,
              html,
            })
            .then(() => {
              createContractHistory({
                contractId: contractId,
                dateUpdate: new Date(),
                endDate: endDate,
                status: "complete",
                totalMoney: project.totalMoney,
                type: "payment",
              }).then(() => {
                createInformates({
                  description: `Payment contract ${contract.project.projectName} success`,
                  title: "Payment contract",
                  route: "/contract",
                  customerId: contract.project.customer.id,
                }).then(() => {
                  setIsLoading(false);
                  toast.success("Payment contract complete");
                  localStorage.removeItem("IdContract");
                  router.replace("/contract");
                });
              });
            });
        })
        .catch((err) => {
          setIsLoading(false);
          toast.error(err.message);
        });
    }
  };

  const handelPayment = () => {
    window.location.href = url_payment(
      project.totalMoney,
      new Date(),
      "a",
      "https://hire-it-ten.vercel.app/payment"
    );
  };

  const vnp_ResponseCode = useMemo(() => {
    return router.query["vnp_ResponseCode"];
  }, [router.query]);

  useEffect(() => {
    if (vnp_ResponseCode && !isToastDisplayed && historyContractExtend) {
      if (vnp_ResponseCode === "00") {
        setIsToastDisplayed(true);
        toast.success(informate_payment(vnp_ResponseCode));
        handelUpdateContract();
      } else {
        setIsToastDisplayed(true);
        toast.error(informate_payment(vnp_ResponseCode));
        router.replace("/payment");
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isToastDisplayed, loading, historyContractExtend]);

  return (
    <div id="preview-contract">
      <div className="payment-right__header">
        <h4>{langs.payment.precontract[lang]}</h4>
      </div>
      <div className="payment-right__description">
        <span>{langs.global.des[lang]}: </span>
        <ul>
          <li>Name: {project.projectName}</li>
          {contract.isExtend !== true ? (
            <>
              <li>Hire employee position {project.position}</li>
              <li>Client: {project.employees.length} employee </li>
              <li>
                Work style: {project.openForRemote ? "Remote" : "Offline"}
              </li>
            </>
          ) : (
            !loading &&
            historyContractExtend &&
            historyContractExtend.length > 0 && (
              <>
                <li>Start date: {formatDate(contract.startDate)}</li>
                <li>End date: {formatDate(contract.endDate)}</li>
                <li>
                  Extend date: {formatDate(historyContractExtend[0].endDate)}
                </li>
              </>
            )
          )}
        </ul>
      </div>
      {contract.isExtend !== true ? (
        <>
          <div className="payment-right__information">
            <span>{langs.global.price[lang]}</span>
            <p>{formatNumber((project.totalMoney * 100) / 105)}$</p>
          </div>
          <div className="payment-right__information">
            <span>VAT ( 5% )</span>
            <p>{formatNumber((project.totalMoney * 5) / 105)}$</p>
          </div>
          <div className="payment-right__money">
            <span>{langs.payment.amountmoney[lang]}</span>
            <p>{formatNumber(project.totalMoney)}$</p>
          </div>
        </>
      ) : (
        !loading &&
        historyContractExtend &&
        historyContractExtend.length > 0 && (
          <>
            <div className="payment-right__information">
              <span>{langs.global.price[lang]}</span>
              <p>
                {formatNumber(
                  historyContractExtend[0].totalMoney - project.totalMoney
                )}
                $
              </p>
            </div>
            <div className="payment-right__money">
              <span>{langs.payment.amountmoney[lang]}</span>
              <p>
                {formatNumber(
                  historyContractExtend[0].totalMoney - project.totalMoney
                )}
                $
              </p>
            </div>
          </>
        )
      )}

      <div className="payment-right__confirm">
        <CCheckbox
          setValue={setIsConfirm}
          arrOption={[
            {
              value: "agree",
              label: langs.payment.check[lang],
            },
          ]}
        />
      </div>
      <div className="payment-btn">
        <CButton
          onClick={handelPayment}
          disabled={!isConfirm || isLoading}
          text={langs.payment.payment[lang]}
        />
      </div>
      <p>
        {langs.payment.help[lang]}? <Link>{langs.payment.support[lang]}</Link>
      </p>
      <div className="payment-right__media">
        <div className="payment-media__btn">
          <CButtonIcon icon={<CiTwitter />} />
          <span>Twitter</span>
        </div>
        <div className="payment-media__btn">
          <CButtonIcon icon={<CiFacebook />} />
          <span>Facebook</span>
        </div>
        <div className="payment-media__btn">
          <CButtonIcon icon={<CiInstagram />} />
          <span>Instagram</span>
        </div>
        <div className="payment-media__btn">
          <CButtonIcon icon={<CiYoutube />} />
          <span>Youtube</span>
        </div>
      </div>
    </div>
  );
};

export default PreviewContract;
