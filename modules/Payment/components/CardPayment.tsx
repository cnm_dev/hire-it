/* eslint-disable @next/next/no-img-element */
import * as React from "react";
import DialogTitle from "@mui/material/DialogTitle";
import Dialog from "@mui/material/Dialog";
import image from "@/common/assets/image/all-image";
import CButton from "@/common/components/controls/CButton";

const list_card = [
  {
    nameBank: "ATM VIETINBANK",
    status: "Connected",
    numberBank: "9704 15** **** **84",
    name: "NGUYEN MINH CHANH",
  },
  {
    nameBank: "ATM ACB",
    status: "Waiting",
    numberBank: "4501 10** **** **91",
    name: "VO MINH HIEU",
  },
  {
    nameBank: "VNPay",
    status: "Connected",
  },
];

type ListCard = {
  nameBank: string;
  status: string;
  numberBank?: string;
  name?: string;
};
export interface SimpleDialogProps {
  open: boolean;
  selectedBank: ListCard;
  onClose: (value: any) => void;
  setSelectedBank: any;
}

function SimpleDialog(props: SimpleDialogProps) {
  const { onClose, selectedBank, open, setSelectedBank } = props;

  const handleClose = (card: ListCard) => {
    setSelectedBank(card);
    onClose(selectedBank);
  };

  return (
    <Dialog onClose={() => handleClose(selectedBank)} open={open}>
      <DialogTitle>Choose payment method</DialogTitle>
      <div className="dialog-listPayment">
        {list_card.map((card, index) => (
          <div
            onClick={() => {
              handleClose(card);
            }}
            key={index}
            className="dialog-payment__container"
          >
            <div className="card-payment">
              <img
                src={card.nameBank === "VNPay" ? image.VNPay : image.card_atm}
                alt=""
              />
              <div className="card-payment__container">
                <div className="card-payment__header">
                  <h3>{card.nameBank}</h3>
                  <span>{card.status}</span>
                </div>
                <div className="card-payment__infor">
                  <h3>{card.numberBank}</h3>
                  <h3>{card.name}</h3>
                </div>
              </div>
            </div>
          </div>
        ))}
        <div className="card-payment__add">
          <CButton
            variant="outlined"
            onClick={() => handleClose(selectedBank)}
            text="Add new Card"
          />
        </div>
      </div>
    </Dialog>
  );
}

export default function CardPayment() {
  const [open, setOpen] = React.useState(false);
  const [selectedBank, setSelectedBank] = React.useState(list_card[2]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className="dialog-payment">
      <div className="dialog-payment__container" onClick={handleClickOpen}>
        <div className="card-payment">
          <img
            src={
              selectedBank.nameBank === "VNPay" ? image.VNPay : image.card_atm
            }
            alt=""
          />
          <div className="card-payment__container">
            <div className="card-payment__header">
              <h3>{selectedBank.nameBank}</h3>
              <span>{selectedBank.status}</span>
            </div>
            <div className="card-payment__infor">
              <h3>{selectedBank.numberBank}</h3>
              <h3>{selectedBank.name}</h3>
            </div>
          </div>
        </div>
      </div>
      <SimpleDialog
        setSelectedBank={setSelectedBank}
        selectedBank={selectedBank}
        open={open}
        onClose={handleClose}
      />
    </div>
  );
}
