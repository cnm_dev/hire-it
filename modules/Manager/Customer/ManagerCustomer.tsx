/* eslint-disable react/jsx-key */
import CPagination from "@/common/components/controls/CPagination";
import NotFoundValue from "@/modules/NotFoundValue";
import { Customer } from "@/types/models";
import React, { useMemo } from "react";
import CTable from "@/common/components/controls/CTable";
import CDropMenu from "@/common/components/controls/CDropMenu";
import { InfoCircle, MoreCircle, MoreSquare, TickCircle } from "iconsax-react";
import { Button } from "@mui/material";
import { updateBlockUser } from "@/apis/Accounts";
import { toast } from "react-toastify";
import { mutate } from "swr";
import FilterManagerCustomer from "../FilterManager/FilterManagerCustomer";
import { useSelector } from "react-redux";
import { todoCustomerAdminSelector } from "@/redux/selector";
import COptionModel from "@/common/components/layout/COptionModel";
import { createInformates } from "@/apis/Informates";
type ManagerCustomerProps = {
  customers: Customer[];
};

const handelBlockUser = (idUser: string, block: boolean) => {
  updateBlockUser({
    idUser: idUser,
    block,
  })
    .then(() => {
      toast.success(`${block ? "Block" : "Unlock"} is success"`);
      mutate("/api/customers");
    })
    .catch((err) => toast.error(err.message));
};

const ManagerCustomer = () => {
  const customers = useSelector(todoCustomerAdminSelector);
  const [pagination, SetPagination] = React.useState(1);

  const [lisCustomer, setListCustomer] = React.useState(customers.slice(10));
  React.useEffect(() => {
    setListCustomer(customers.slice((pagination - 1) * 10, pagination * 10));
  }, [customers, pagination]);

  const rows = useMemo(() => {
    let rowCustomers: any[] = [];

    lisCustomer.map((customer) => {
      rowCustomers.push([
        customer.id,
        customer.users_permissions_user.email,
        customer.users_permissions_user.fullName,
        customer.companyName,
        customer.users_permissions_user.blocked ? (
          <InfoCircle size="24" color="#FF8A65" />
        ) : (
          <TickCircle size="24" color="#51cb1f" />
        ),
        customer.codeFax,
        <COptionModel
          title={customer.users_permissions_user.blocked ? "UnLock?" : "Block?"}
          handel={() => {
            customer.users_permissions_user.blocked
              ? handelBlockUser(customer.users_permissions_user.id, false)
              : handelBlockUser(customer.users_permissions_user.id, true);
          }}
          notice=""
          buttonOpen={
            <Button
              size="small"
              variant="outlined"
              color={
                customer.users_permissions_user.blocked ? "primary" : "error"
              }
            >
              {customer.users_permissions_user.blocked ? "UnLock" : "Block"}
            </Button>
          }
        />,
      ]);
    });
    return rowCustomers;
  }, [lisCustomer]);
  return (
    <>
      <div className="manager">
        <div className="manager-filter">
          <FilterManagerCustomer />
        </div>
        <div className="manager-header">
          <h3>Customer list</h3>
        </div>
      </div>
      {customers.length === 0 ? (
        <NotFoundValue />
      ) : (
        <div className="manager">
          <>
            <div className="manager-list">
              {rows && (
                <CTable
                  rows={rows}
                  label={[
                    "Id",
                    "Email",
                    "Full name",
                    "Company name",
                    "Status block",
                    "Code Fax",
                    "Function",
                  ]}
                />
              )}
            </div>
            {customers.length !== 0 && (
              <div className="member-manager__pagination">
                <CPagination
                  SetPagination={SetPagination}
                  count={
                    customers.length <= 10
                      ? 1
                      : Math.ceil(customers.length / 10)
                  }
                />
              </div>
            )}
          </>
        </div>
      )}
    </>
  );
};

export default ManagerCustomer;
