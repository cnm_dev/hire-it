/* eslint-disable react-hooks/rules-of-hooks */
import CAutocomplete from "@/common/components/controls/CAutocomplete";
import CButton from "@/common/components/controls/CButton";
import CTextField from "@/common/components/controls/CTextField";
import { SearchNormal1 } from "iconsax-react";
import React, { useMemo, useState } from "react";
import { all_api } from "../../../mock/all_api";
import { useDispatch } from "react-redux";
import FilterEmployeeAdminSlice from "@/redux/Slices/FilterEmployeeAdminSlice";
const index = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [selectedButton, setSelectedButton] = useState("button");
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const dispatch = useDispatch();
  const handleClick = (button: string) => {
    setSelectedButton(button);
  };

  return (
    <div className="manager-filter__container">
      <div className="manager-filter__search">
        <SearchNormal1 size={20} />
        <CTextField
          placeholder="Find staff"
          onChange={(e) => {
            dispatch(
              FilterEmployeeAdminSlice.actions.employeeName(e.target.value)
            );
          }}
        />
      </div>

      <div className="manager-filter__btn">
        <h4>Status:</h4>
        <div className="manager-btn__list">
          <div className="button-search">
            <CButton
              variant="outlined"
              text="ALL"
              className={selectedButton === "button" ? "selected" : ""}
              onClick={() => {
                handleClick("button");
                dispatch(FilterEmployeeAdminSlice.actions.status("all"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Wait"
              className={selectedButton === "button1" ? "selected" : ""}
              onClick={() => {
                handleClick("button1");
                dispatch(FilterEmployeeAdminSlice.actions.status("wait"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Invited"
              className={selectedButton === "button2" ? "selected" : ""}
              onClick={() => {
                handleClick("button2");
                dispatch(FilterEmployeeAdminSlice.actions.status("invited"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Confirm"
              className={selectedButton === "button3" ? "selected" : ""}
              onClick={() => {
                handleClick("button3");
                dispatch(FilterEmployeeAdminSlice.actions.status("confirm"));
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default index;
