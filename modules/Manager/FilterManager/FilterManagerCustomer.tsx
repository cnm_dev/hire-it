import CButton from "@/common/components/controls/CButton";
import CTextField from "@/common/components/controls/CTextField";
import FilterCustomerAdminSlice from "@/redux/Slices/FilterCustomerAdminSlice";
import { SearchNormal1 } from "iconsax-react";
import { useState } from "react";
import { useDispatch } from "react-redux";
const FilterManagerCustomer = () => {
  const [selectedButton, setSelectedButton] = useState("button");
  const dispatch = useDispatch();
  const handleClick = (button: string) => {
    setSelectedButton(button);
  };
  return (
    <div className="manager-filter__container">
      <div className="manager-filter__search">
        <SearchNormal1 size={20} />
        <CTextField
          placeholder="Find customer"
          onChange={(e) => {
            dispatch(
              FilterCustomerAdminSlice.actions.customerName(e.target.value)
            );
          }}
        />
      </div>
      <div className="manager-filter__btn">
        <h4>Status:</h4>
        <div className="manager-btn__list">
          <div className="button-search">
            <CButton
              variant="outlined"
              text="ALL"
              className={selectedButton === "button" ? "selected" : ""}
              onClick={() => {
                handleClick("button");
                dispatch(FilterCustomerAdminSlice.actions.status("all"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Active"
              className={selectedButton === "button1" ? "selected" : ""}
              onClick={() => {
                handleClick("button1");
                dispatch(FilterCustomerAdminSlice.actions.status("active"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Block"
              className={selectedButton === "button2" ? "selected" : ""}
              onClick={() => {
                handleClick("button2");
                dispatch(FilterCustomerAdminSlice.actions.status("block"));
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default FilterManagerCustomer;
