/* eslint-disable react-hooks/rules-of-hooks */
import CButton from "@/common/components/controls/CButton";
import CTextField from "@/common/components/controls/CTextField";
import FilterContractAdminSlice from "@/redux/Slices/FilterContractAdminSlice";
import { SearchNormal1 } from "iconsax-react";
import { useMemo, useState } from "react";
import DatePicker from "react-multi-date-picker";
import { useDispatch } from "react-redux";
const FilterManagerContracts = () => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [selectedButton, setSelectedButton] = useState("button");
  const [dateFrom, setDateFrom] = useState<Date>(() => {
    const currentDate = new Date();
    // Thiết lập ngày thành 1
    currentDate.setDate(1);
    // Trừ đi ba tháng
    currentDate.setMonth(currentDate.getMonth() - 3);
    return currentDate;
  });
  const [dateTo, setDateTo] = useState<Date>(new Date());
  // eslint-disable-next-line react-hooks/rules-of-hooks
  const dispatch = useDispatch();
  const handleClick = (button: string) => {
    setSelectedButton(button);
  };

  return (
    <div className="manager-filter__container">
      <div className="manager-filter__search">
        <SearchNormal1 size={20} />
        <CTextField
          placeholder="Find customer name"
          onChange={(e) => {
            dispatch(
              FilterContractAdminSlice.actions.contractName(e.target.value)
            );
          }}
        />
      </div>
      <div className="manager-filter__date">
        <h4>Date From: </h4>
        <span>
          <DatePicker
            value={dateFrom}
            maxDate={dateTo}
            onChange={(date) => {
              date && setDateFrom(new Date(date.toString()));
              date &&
                dispatch(
                  FilterContractAdminSlice.actions.dateFrom(
                    new Date(date.toString()).toISOString()
                  )
                );
            }}
          />
        </span>
      </div>
      <div className="manager-filter__date">
        <h4>Date To: </h4>
        <span>
          <DatePicker
            value={dateTo}
            minDate={dateFrom}
            onChange={(date) => {
              date && setDateTo(new Date(date.toString()));
              date &&
                dispatch(
                  FilterContractAdminSlice.actions.dateTo(
                    new Date(date.toString()).toISOString()
                  )
                );
            }}
          />
        </span>
      </div>

      <div className="manager-filter__btn">
        <h4>Status:</h4>
        <div className="manager-btn__list">
          <div className="button-search">
            <CButton
              variant="outlined"
              text="ALL"
              className={selectedButton === "button" ? "selected" : ""}
              onClick={() => {
                handleClick("button");
                dispatch(FilterContractAdminSlice.actions.status("all"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Active"
              className={selectedButton === "button1" ? "selected" : ""}
              onClick={() => {
                handleClick("button1");
                dispatch(FilterContractAdminSlice.actions.status("active"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Wait"
              className={selectedButton === "button2" ? "selected" : ""}
              onClick={() => {
                handleClick("button2");
                dispatch(FilterContractAdminSlice.actions.status("wait"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="End"
              className={selectedButton === "button3" ? "selected" : ""}
              onClick={() => {
                handleClick("button3");
                dispatch(FilterContractAdminSlice.actions.status("end"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Pending"
              className={selectedButton === "button4" ? "selected" : ""}
              onClick={() => {
                handleClick("button4");
                dispatch(FilterContractAdminSlice.actions.status("pending"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Cancel"
              className={selectedButton === "button5" ? "selected" : ""}
              onClick={() => {
                handleClick("button5");
                dispatch(FilterContractAdminSlice.actions.status("cancel"));
              }}
            />
          </div>
          <div className="button-search">
            <CButton
              variant="outlined"
              text="Complete"
              className={selectedButton === "button6" ? "selected" : ""}
              onClick={() => {
                handleClick("button6");
                dispatch(FilterContractAdminSlice.actions.status("complete"));
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default FilterManagerContracts;
