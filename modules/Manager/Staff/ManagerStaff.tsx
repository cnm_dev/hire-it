import { updateBlockUser } from "@/apis/Accounts";
import { updateStatusEmployee } from "@/apis/Employee";
import CDropMenu from "@/common/components/controls/CDropMenu";
import CPagination from "@/common/components/controls/CPagination";
import CTable from "@/common/components/controls/CTable";
import COptionModel from "@/common/components/layout/COptionModel";
import NotFoundValue from "@/modules/NotFoundValue";
import { todoEmployeeAdminSelector } from "@/redux/selector";
import { Employee } from "@/types/models";
import { formatDate } from "@/utils";
import { Button } from "@mui/material";
import {
  BagCross,
  ClipboardTick,
  InfoCircle,
  MoreCircle,
  TickCircle,
} from "iconsax-react";
import React, { useMemo } from "react";
import { useSelector } from "react-redux";
import { toast } from "react-toastify";
import { mutate } from "swr";
import FilterManagerStaff from "../FilterManager/FilterManagerStaff";
import { createInformates } from "@/apis/Informates";
import CModal from "@/common/components/controls/CModal";
import ProfileApplication from "@/modules/Member/components/ProfileApplication";
const ManagerStaff = () => {
  const employees = useSelector(todoEmployeeAdminSelector);
  const [pagination, SetPagination] = React.useState(1);
  const [listmember, setListMember] = React.useState<Employee[]>(
    employees.slice(10)
  );
  React.useEffect(() => {
    setListMember(
      employees
        .filter((data) => {
          return data.status !== "cancel";
        })
        .slice((pagination - 1) * 10, pagination * 10)
    );
  }, [employees, pagination]);

  const handelUpdateStatus = (employeeId: string, status: string) => {
    updateStatusEmployee({
      employeeId,
      status: status === "wait" ? "invited" : "confirm",
      joinDate: new Date(),
    })
      .then(() => {
        createInformates({
          title: "Job Application Approval",
          description: `We have reviewed your application and found it to be a good fit for our company.
          We will soon notify you via Gmail regarding the next steps of the recruitment process.
          Thank you sincerely for your interest.`,
          route: "/",
          employeeId: employeeId,
        });
        mutate("/api/employees");
        toast.success("Approve staff complete");
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };

  const handelRejected = (employeeId: string) => {
    updateStatusEmployee({
      employeeId,
      status: "cancel",
    })
      .then(() => {
        createInformates({
          title: "Job Application Rejection",
          description: `We regret to inform you that we cannot proceed with your job application as it does not meet our requirements and criteria.
          Thank you for your interest and the time you invested in the application process.`,
          route: "/",
          employeeId: employeeId,
        });

        mutate("/api/employees");
        toast.success("Approve staff complete");
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };

  const handelBlockUser = (idUser: string, block: boolean) => {
    updateBlockUser({
      idUser: idUser,
      block,
    })
      .then(() => {
        toast.success(`${block ? "Block" : "Unlock"} is success"`);
        mutate("/api/employees");
      })
      .catch((err) => toast.error(err.message));
  };

  const rows = useMemo(() => {
    let rowMembers: any[] = [];

    listmember.map((employee: Employee | any, index) => {
      rowMembers.push([
        employee.id,
        <CModal
          key={index}
          modal={<ProfileApplication employee={employee} />}
          button={
            <span className="modal-profile__application">
              {employee.users_permissions_user.email}
            </span>
          }
        />,
        employee.users_permissions_user.fullName,
        employee.users_permissions_user.blocked ? (
          <InfoCircle size="24" color="#FF8A65" />
        ) : (
          <TickCircle size="24" color="#51cb1f" />
        ),
        employee.status,
        formatDate(employee.createdAt),
        employee.status === "confirm" ? (
          <COptionModel
            title={
              employee.users_permissions_user.blocked ? "UnLock?" : "Block?"
            }
            handel={() => {
              employee.users_permissions_user.blocked
                ? handelBlockUser(employee.users_permissions_user.id, false)
                : handelBlockUser(employee.users_permissions_user.id, true);
            }}
            notice=""
            buttonOpen={
              <Button
                size="small"
                color={
                  !employee.users_permissions_user.blocked ? "error" : "primary"
                }
                variant="outlined"
              >
                {employee.users_permissions_user.blocked ? "UnLock" : "Block"}
              </Button>
            }
          />
        ) : (
          <div className="funtion">
            <COptionModel
              title="Approve?"
              handel={() => handelUpdateStatus(employee.id, employee.status)}
              notice=""
              buttonOpen={
                <Button size="small" variant="outlined">
                  Approve
                </Button>
              }
            />
            <COptionModel
              title="Rejected"
              handel={() => handelRejected(employee.id)}
              notice=""
              buttonOpen={
                <Button size="small" variant="outlined" color="error">
                  Rejected
                </Button>
              }
            />
          </div>
        ),
      ]);
    });
    return rowMembers;
  }, [listmember]);

  return (
    <>
      <div className="manager">
        <div className="manager-filter">
          <FilterManagerStaff />
        </div>
        <div className="manager-header">
          <h3>Staff list</h3>
        </div>
      </div>
      {employees.length === 0 ? (
        <NotFoundValue />
      ) : (
        <div className="manager">
          <div className="manager-list">
            {rows && (
              <CTable
                rows={rows}
                label={[
                  "Id",
                  "Email",
                  "Full name",
                  "Status block",
                  "Status",
                  "Join company",
                  "Function",
                ]}
              />
            )}
          </div>
          {employees.length !== 0 && (
            <div className="member-manager__pagination">
              <CPagination
                SetPagination={SetPagination}
                count={
                  employees.length <= 10 ? 1 : Math.ceil(employees.length / 10)
                }
              />
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default ManagerStaff;
