import CChart from "@/common/components/controls/CChart";
import CLoading from "@/common/components/controls/CLoading";
import {
  totalContractYear,
  totalMoneyContractYear,
} from "@/constants/contract";
import { all_api } from "@/mock/all_api";
import { Contract } from "@/types/models";
import React, { useMemo } from "react";
interface CharContractProps {
  contracts: Contract[];
}
const ChartContract = ({ contracts }: CharContractProps) => {
  const datasets = useMemo(() => {
    if (contracts.length !== 0) {
      const ObjectMoneyContract = totalContractYear(contracts);

      return [
        {
          label: "Total contract Active",
          data: ObjectMoneyContract.arrSizeActive,
          fill: false,
          borderColor: "rgb(46, 220, 84)",
          tension: 0.1,
        },
        {
          label: "Total  contract Wait",
          data: ObjectMoneyContract.arrSizeWait,
          fill: false,
          borderColor: "rgb(220, 214, 46)",
          tension: 0.1,
        },
        {
          label: "Total contract Cancel",
          data: ObjectMoneyContract.arrSizeCancel,
          fill: false,
          borderColor: "rgb(220, 46, 46)",
          tension: 0.1,
        },
        {
          label: "Total contract Complete",
          data: ObjectMoneyContract.arrSizeComplete,
          fill: false,
          borderColor: "rgb(75, 192, 192)",
          tension: 0.1,
        },
      ];
    }
  }, [contracts]);
  return datasets ? (
    <div className="admin-contract__chart">
      <CChart datasets={datasets} labels={all_api.chart_month} />
    </div>
  ) : (
    <CLoading fullScreen />
  );
};

export default ChartContract;
