import CButton from "@/common/components/controls/CButton";
import CModal from "@/common/components/controls/CModal";
import CPagination from "@/common/components/controls/CPagination";
import CTable from "@/common/components/controls/CTable";
import NotFoundValue from "@/modules/NotFoundValue";
import { Project } from "@/types/models";
import { formatNormalDate } from "@/utils";
import React, { useMemo } from "react";
import QuickUpdate from "./QuickUpdate";
import useSkills from "@/hooks/useSkills";
import CLoading from "@/common/components/controls/CLoading";
type ManagerProject = {
  projects: Project[];
};
const ManagerProject = ({ projects }: ManagerProject) => {
  const [pagination, SetPagination] = React.useState(1);
  const [listProject, setlistProject] = React.useState<Project[]>(
    projects.slice(10)
  );
  const { skills, loading } = useSkills();

  React.useEffect(() => {
    setlistProject(projects.slice((pagination - 1) * 10, pagination * 10));
  }, [pagination, projects]);

  const rows = useMemo(() => {
    let rowMembers: any[] = [];

    listProject.map((project: Project, index) => {
      rowMembers.push([
        project.id,
        project.projectName,
        project.situation,
        project.employees && `${project.employees.length} (employee)`,
        project.customer.companyName,
        formatNormalDate(project.createdAt),
        project.status === "draft" ? (
          <span
            style={{
              fontWeight: "600",
              color: "#d1d651",
              textTransform: "capitalize",
            }}
          >
            {project.status}
          </span>
        ) : (
          <span
            style={{
              fontWeight: "600",
              color: "#39d398",
              textTransform: "capitalize",
            }}
          >
            active
          </span>
        ),
        <div key={index} className="">
          {project.status === "draft" && (
            <CModal
              confirm
              modal={
                loading ? (
                  <CLoading />
                ) : (
                  <QuickUpdate skills={skills} project={project} />
                )
              }
              button={<CButton variant="outlined" text="Update"></CButton>}
            />
          )}
        </div>,
      ]);
    });
    return rowMembers;
  }, [listProject, loading, skills]);

  return (
    <>
      <div className="manager">
        <div className="manager-filter"></div>
        <div className="manager-header">
          <h3>Project list</h3>
        </div>
      </div>
      {projects.length === 0 ? (
        <NotFoundValue />
      ) : (
        <div className="manager">
          <div className="manager-list">
            {rows && (
              <CTable
                rows={rows}
                label={[
                  "Id",
                  "Project Name",
                  "Situation",
                  "Number Employee",
                  "Customer",
                  "Date create",
                  "Status",
                  "Function",
                ]}
              />
            )}
          </div>
          {projects.length !== 0 && (
            <div className="member-manager__pagination">
              <CPagination
                SetPagination={SetPagination}
                count={
                  projects.length <= 10 ? 1 : Math.ceil(projects.length / 10)
                }
              />
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default ManagerProject;
