/* eslint-disable @next/next/no-img-element */
import { createContract } from "@/apis/Contract";
import { filterEmployee } from "@/apis/Employees";
import { createContractHistory } from "@/apis/HistoryContract";
import { createInformates } from "@/apis/Informates";
import { updateProject } from "@/apis/Project";
import CAutocomplete from "@/common/components/controls/CAutocomplete";
import CAutocompleteOption, {
  Option,
} from "@/common/components/controls/CAutocompleteOption";
import CButton from "@/common/components/controls/CButton";
import CRadioButton from "@/common/components/controls/CRadioButton";
import CSpinner from "@/common/components/controls/CSpinner";
import CTextField from "@/common/components/controls/CTextField";
import { STRAPI_API_TOKEN } from "@/constants";
import { emailInfomateSuccess } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateCreateContractSuccess";
import { Employee, Project, Skill } from "@/types/models";
import { formatNumber } from "@/utils";
import axios from "axios";
import { Star1 } from "iconsax-react";
import React, { useMemo, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { toast } from "react-toastify";
import { mutate } from "swr";

type QuickUpdate = {
  project?: Project;
  skills?: Skill[];
};

const remoteRadio = [
  {
    label: "Online",
  },
  {
    label: "Remote",
  },
];

const commitedRadio = [
  {
    label: "Full time (40h/week)",
  },
  {
    label: "Part time (Less 40h/week)",
  },
];

const QuickUpdate = ({ project, skills }: QuickUpdate) => {
  const { control, handleSubmit } = useForm();
  const [render, setRender] = useState(false);
  const [loading, setLoading] = useState(false);
  const [listMember, setListmember] = useState<Employee[]>([]);
  const [valueForm, setValueForm] = useState<any>();
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const handelFilterEmployee = (value: any) => {
    setLoading(true);
    let listIdSkill: string[] = [];
    value.skills.map((skill: { id: string; name: string }) => {
      listIdSkill.push(skill.id);
    });
    filterEmployee(
      {
        hourlyRateFrom: Number(value.budgetFrom),
        hourlyRateTo: Number(value.budgetTo),
        isFullTime: value.commitment === "Full time (40h/week)" ? true : false,
        isRemote: value.remoteRadio === "Online" ? false : true,
        positionName: value.position,
        status: "confirm",
        skills: listIdSkill,
      },
      { jwt: STRAPI_API_TOKEN }
    )
      .then((course) => {
        setValueForm(value);
        setListmember(course.data);
        setLoading(false);
        setRender(true);
      })
      .catch((err) => {
        setLoading(false);
        toast.error(err.message);
      });
  };

  const [position, setPosition] = useState(project && project.position);

  const listSkill = React.useMemo(() => {
    let t: {
      id: number;
      name: string;
    }[] = [];
    if (skills) {
      skills.map((data) => {
        if (data.position?.positionName === position) {
          t.push({ id: data.id, name: data.skillName });
        }
      });
    }
    return t;
  }, [position, skills]);

  const modifiedSkills: Option[] | undefined = useMemo(() => {
    if (project) {
      return project.skills.map((skill) => {
        return { id: skill.id, name: skill.skillName };
      });
    }
  }, [project]);

  const listResultEmployee = useMemo(() => {
    if (listMember && render && valueForm) {
      if (listMember.length <= Number(valueForm.companySizeTo)) {
        return listMember;
      } else
        return listMember
          .sort(() => Math.random() - 0.5)
          .slice(0, Number(valueForm.companySizeTo));
    }
  }, [listMember, render, valueForm]);

  const totalMoney = useMemo(() => {
    if (listResultEmployee && valueForm) {
      let money = 0;
      listResultEmployee.map((employee) => {
        money +=
          employee.hourlyRate * 8 * 5 * Number(valueForm.projectLengthTo);
      });
      return money;
    }
  }, [listResultEmployee, valueForm]);

  const handelUpdateProject = () => {
    setIsLoading(true);
    let listIdSkill: string[] = [];
    valueForm.skills.map((skill: { id: string; name: string }) => {
      listIdSkill.push(skill.id);
    });

    let listIdEmployee: string[] = [];
    listResultEmployee?.map((employee) => {
      listIdEmployee.push(employee.id);
    });
    if (project) {
      updateProject({
        projectId: project.id,
        budgetFrom: Number(valueForm.budgetFrom),
        budgetTo: Number(valueForm.budgetTo),
        commitment:
          valueForm.commitment === "Full time (40h/week)" ? true : false,
        companySizeFrom: Number(valueForm.companySizeFrom),
        companySizeTo: Number(valueForm.companySizeTo),
        openForRemote: valueForm.remoteRadio === "Online" ? true : false,
        position: valueForm.position,
        projectLengthFrom: Number(valueForm.projectLengthFrom),
        projectLengthTo: Number(valueForm.projectLengthTo),
        totalMoney: totalMoney && totalMoney,
        situation: valueForm.situation,
        skills: listIdSkill,
        listIDEmployee: listIdEmployee,
        status: "active",
      })
        .then(() => {
          createContract({ projectId: project.id, status: "wait" })
            .then((course) => {
              const html = emailInfomateSuccess({
                name: project.customer.users_permissions_user.fullName,
                email: "Vohieu7k58@gmail.com",
                phoneNumber: "0377723460",
                contractId: course.data.id,
                totalMoney: totalMoney ? totalMoney : project.totalMoney,
                host: "https://hire-it-git-chanh-dev-update-project-hireit.vercel.app/contract",
              });
              axios.post("/api/verifyEmail/sendEmail", {
                email: project.customer.users_permissions_user.email,
                html,
              });
              totalMoney &&
                createContractHistory({
                  contractId: course.data.id,
                  type: "create",
                  dateUpdate: new Date(),
                  status: "complete",
                  totalMoney: totalMoney,
                })
                  .then(() => {
                    totalMoney &&
                      createContractHistory({
                        contractId: course.data.id,
                        type: "payment",
                        dateUpdate: new Date(),
                        status: "wait",
                        totalMoney: totalMoney,
                      }).then(() => {
                        createInformates({
                          title: `Update contract ${project.projectName} complete`,
                          description: `Contract name is ${project.projectName} update information complete. You can payment contract in 48h`,
                          route: "/contract",
                          type: "default",
                          customerId: project.customer.id,
                        })
                          .then(() => {
                            toast.success(
                              "Update and create contract complete"
                            );
                            mutate("/api/projects");
                            setIsLoading(false);
                          })
                          .catch((err) => {
                            setIsLoading(false);
                            toast.error(err.message);
                          });
                      });
                  })
                  .catch((err) => {
                    setIsLoading(false);
                    toast.error(err.message);
                  });
            })
            .catch((err) => {
              setIsLoading(false);
              toast.error(err.message);
            });
        })
        .catch((err) => {
          setIsLoading(false);
          toast.error(err.message);
        });
    }
  };
  return (
    <div className="form-update">
      <h3>Create and update Contract</h3>
      {render ? (
        listMember.length < Number(valueForm.companySizeFrom) ? (
          <div className="form-result__null">
            <span>Not found count employee</span>
            <div className="form-result__item btn">
              <CButton text="Back" onClick={() => setRender(false)} />
            </div>
          </div>
        ) : (
          <div className="form-result">
            <div className="form-result__item">
              <h4>List member</h4>
              <div className="result-item">
                <div className="quick_main">
                  <div className="item-employee_list">
                    {listResultEmployee &&
                      listResultEmployee.map((employee: Employee) => {
                        return (
                          <div className="item_employee" key={employee.id}>
                            <img
                              src={employee.users_permissions_user.avartar}
                              alt=""
                            />
                            <div className="item-employee__infor">
                              <div className="item-employee__start">
                                <Star1 variant="Bold" color="yellow" />
                                <span>{employee.start.toFixed(1)}</span>
                              </div>
                              <div className="item-employee__me">
                                <span>
                                  {employee.users_permissions_user.fullName}
                                </span>
                                <span>
                                  {employee.users_permissions_user.email}
                                </span>
                              </div>
                              <div className="item-employee__work">
                                <div className="employee-work">
                                  <span>
                                    Position: {employee.position.positionName}
                                  </span>
                                  <span>
                                    Hourly Rate: {employee.hourlyRate}
                                  </span>
                                </div>
                                <div className="employee-work">
                                  <span>
                                    Exp: {employee.yearsOfExperience}
                                    {employee.yearsOfExperience < 1
                                      ? "month"
                                      : "year"}
                                  </span>
                                  <span>
                                    {employee.isRemote ? "Remote" : "Offline"}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      })}
                  </div>
                </div>
              </div>
            </div>
            <div className="form-result__item">
              <h4>Total Money</h4>
              <div className="result-item">
                <div className="payment-right__information">
                  <span>Money</span>
                  <p>{totalMoney && formatNumber(totalMoney)}$</p>
                </div>
                <div className="payment-right__information">
                  <span>VAT ( 5% )</span>
                  <p>{totalMoney && formatNumber((totalMoney * 5) / 100)}$</p>
                </div>
                <div className="payment-right__money">
                  <span>Count Money</span>
                  <p>{totalMoney && formatNumber((totalMoney * 105) / 100)}$</p>
                </div>
              </div>
            </div>
            <div className="form-result__item btn">
              <CButton
                disabled={isLoading}
                text="Back"
                onClick={() => setRender(false)}
              />
              <CButton
                disabled={isLoading}
                text={isLoading ? <CSpinner size="small" /> : "Create"}
                onClick={handelUpdateProject}
              />
            </div>
          </div>
        )
      ) : (
        <form onSubmit={handleSubmit(handelFilterEmployee)}>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Name project:</h4>
            </div>
            <div className="form-item__text">
              <Controller
                control={control}
                name="projectName"
                rules={{ required: true }}
                defaultValue={project && project.projectName}
                render={({ field }) => {
                  return (
                    <CTextField
                      disable={!!(project && project.projectName)}
                      defaultValue={project && project.projectName}
                    />
                  );
                }}
              />
            </div>
          </div>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Position hire:</h4>
            </div>
            <div className="form-item__text">
              <Controller
                control={control}
                name="position"
                rules={{ required: true }}
                defaultValue={project && project.position}
                render={({ field }) => {
                  return (
                    <CAutocomplete
                      defaultValue={project && { label: field.value }}
                      onChange={(e, v) => {
                        setPosition(v.label);
                        field.onChange(v.label);
                      }}
                      option={[
                        { label: "Developers" },
                        { label: "Designers" },
                        { label: "Finance Expert" },
                        { label: "Product Manager" },
                        { label: "Project Manager" },
                      ]}
                    />
                  );
                }}
              />
            </div>
          </div>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Position hire:</h4>
            </div>
            <div className="form-item__text option">
              <CAutocompleteOption
                defaultValue={modifiedSkills && modifiedSkills}
                name="skills"
                open={true}
                control={control}
                placeholder="Desired areas of expertise (e.g., Agile, Scrum, Wordpress, etc.)"
                values={listSkill}
                multiple={true}
                isInline={true}
                clasName="skills-item"
                ListboxProps={{ className: "cautocomplete" }}
              />
            </div>
          </div>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Situation project:</h4>
            </div>
            <div className="form-item__text">
              <Controller
                control={control}
                name="situation"
                rules={{ required: true }}
                defaultValue={project && project.situation}
                render={({ field }) => {
                  return (
                    <CTextField
                      onChange={(e) => field.onChange(e.target.value)}
                      defaultValue={
                        field.value ? field.value : project && project.situation
                      }
                    />
                  );
                }}
              />
            </div>
          </div>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Open For Remote: </h4>
            </div>
            <div className="form-item__text radio">
              {remoteRadio.map((remote, index) => (
                <Controller
                  key={index}
                  control={control}
                  name="remoteRadio"
                  rules={{ required: true }}
                  defaultValue={
                    project && (project.openForRemote ? "Online" : "Remote")
                  }
                  render={({ field }) => (
                    <CRadioButton
                      key={index}
                      onChange={(event) => field.onChange(event.target.value)}
                      setState={field.onChange}
                      state={field.value}
                      label={remote.label}
                      item={remote.label}
                    />
                  )}
                />
              ))}
            </div>
          </div>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Type work: </h4>
            </div>
            <div className="form-item__text radio">
              {commitedRadio.map((commited, index) => (
                <Controller
                  key={index}
                  control={control}
                  name="commitment"
                  rules={{ required: true }}
                  defaultValue={
                    project &&
                    (project.commitment
                      ? "Full time (40h/week)"
                      : " Part time (Less 40h/week)")
                  }
                  render={({ field }) => (
                    <CRadioButton
                      key={index}
                      onChange={(event) => field.onChange(event.target.value)}
                      setState={field.onChange}
                      state={field.value}
                      label={commited.label}
                      item={commited.label}
                    />
                  )}
                />
              ))}
            </div>
          </div>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Budget:</h4>
            </div>
            <div className="form-item__text double">
              <span>From</span>
              <Controller
                control={control}
                name="budgetFrom"
                rules={{ required: true }}
                defaultValue={project && project.budgetFrom}
                render={({ field }) => {
                  return (
                    <CTextField
                      type="number"
                      onChange={(e) => {
                        field.onChange(e.target.value);
                      }}
                      defaultValue={
                        field.value
                          ? field.value
                          : project && project.budgetFrom
                      }
                    />
                  );
                }}
              />

              <span>To</span>
              <Controller
                control={control}
                name="budgetTo"
                rules={{ required: true }}
                defaultValue={project && project.budgetTo}
                render={({ field }) => {
                  return (
                    <CTextField
                      type="number"
                      onChange={(e) => field.onChange(e.target.value)}
                      defaultValue={
                        field.value ? field.value : project && project.budgetTo
                      }
                    />
                  );
                }}
              />
              <span>* ($)</span>
            </div>
          </div>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Company size:</h4>
            </div>
            <div className="form-item__text double">
              <span>From</span>
              <Controller
                control={control}
                name="companySizeFrom"
                rules={{ required: true }}
                defaultValue={project && project.companySizeFrom}
                render={({ field }) => {
                  return (
                    <CTextField
                      type="number"
                      onChange={(e) => field.onChange(e.target.value)}
                      defaultValue={
                        field.value
                          ? field.value
                          : project && project.companySizeFrom
                      }
                    />
                  );
                }}
              />
              <span>To</span>
              <Controller
                control={control}
                name="companySizeTo"
                rules={{ required: true }}
                defaultValue={project && project.companySizeTo}
                render={({ field }) => {
                  return (
                    <CTextField
                      type="number"
                      onChange={(e) => field.onChange(e.target.value)}
                      defaultValue={
                        field.value
                          ? field.value
                          : project && project.companySizeTo
                      }
                    />
                  );
                }}
              />
              <span>* (employees)</span>
            </div>
          </div>
          <div className="form-update__item">
            <div className="form-item__title">
              <h4>Project Length:</h4>
            </div>
            <div className="form-item__text double">
              <span>From</span>
              <Controller
                control={control}
                name="projectLengthFrom"
                rules={{ required: true }}
                defaultValue={project && project.projectLengthFrom}
                render={({ field }) => {
                  return (
                    <CTextField
                      type="number"
                      onChange={(e) => field.onChange(e.target.value)}
                      defaultValue={
                        field.value
                          ? field.value
                          : project && project.projectLengthFrom
                      }
                    />
                  );
                }}
              />
              <span>To</span>
              <Controller
                control={control}
                name="projectLengthTo"
                rules={{ required: true }}
                defaultValue={project && project.projectLengthTo}
                render={({ field }) => {
                  return (
                    <CTextField
                      type="number"
                      onChange={(e) => field.onChange(e.target.value)}
                      defaultValue={
                        field.value
                          ? field.value
                          : project && project.projectLengthTo
                      }
                    />
                  );
                }}
              />
              <span>* (week)</span>
            </div>
          </div>

          <div className="form-update__btn">
            <CButton
              type="submit"
              text={loading ? <CSpinner size="small" /> : "Filter"}
            />
          </div>
        </form>
      )}
    </div>
  );
};

export default QuickUpdate;
