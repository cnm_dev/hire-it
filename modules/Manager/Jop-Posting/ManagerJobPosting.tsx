import CPagination from "@/common/components/controls/CPagination";
import CTable from "@/common/components/controls/CTable";
import NotFoundValue from "@/modules/NotFoundValue";
import React, { useMemo } from "react";
import { JobPosting } from "@/types/models";
import { formatDate } from "@/utils";
import CButton from "@/common/components/controls/CButton";
import CModal from "@/common/components/controls/CModal";
import JobDetail from "@/modules/JobDetail";
import { TypeLang } from "@/modules/Home/components/HotMember";
import { updateJopPosting } from "@/apis/JobPosting";
import jobPosting from "@/pages/api/customers/[id]/job-posting";
import { mutate } from "swr";
import { toast } from "react-toastify";
import SpamJobPosting from "./SpamJobPosting";
import { createInformates } from "@/apis/Informates";
import { Button } from "@mui/material";
import { Box1, BoxRemove, BoxTime } from "iconsax-react";
import CBadge from "@/common/components/controls/CBadge";

type ManagerJobPostingType = {
  jobpostings: JobPosting[];
  lang: TypeLang["lang"];
};
const ManagerJobPosting = ({ jobpostings, lang }: ManagerJobPostingType) => {
  const [pagination, SetPagination] = React.useState(1);
  const [listJobpostingOrigin, setListJobpostingOrigin] =
    React.useState<JobPosting[]>(jobpostings);
  const [listJobPosting, setListJobPosting] = React.useState<JobPosting[]>(
    listJobpostingOrigin.slice(10)
  );
  const [showJob, setShowJob] = React.useState<string>("");

  React.useEffect(() => {
    setListJobPosting(
      listJobpostingOrigin.slice((pagination - 1) * 10, pagination * 10)
    );
  }, [listJobpostingOrigin, pagination]);

  React.useEffect(() => {
    if (showJob === "") {
      setListJobpostingOrigin(jobpostings);
    } else if (showJob === "block") {
      setListJobpostingOrigin(
        jobpostings.filter((job) => job.status === "block")
      );
    } else if (showJob === "confirm") {
      setListJobpostingOrigin(
        jobpostings.filter((job) => job.status === "confirm")
      );
    } else {
      setListJobpostingOrigin(
        jobpostings.filter((job) => job.status === "pending")
      );
    }
  }, [showJob, jobpostings]);

  const handelBlockJobPosting = (
    idJposting: string,
    status: string,
    jobposting: JobPosting
  ) => {
    updateJopPosting({ idJposting: idJposting, status: status })
      .then(() => {
        mutate("/api/job-postings");
        toast.success(`${status ? "Block" : "Unlock"} status success`);
        createInformates({
          customerId: jobposting.customer.id,
          title: `${status ? "Block" : "Unlock"} Jobposting`,
          description: `Posting name is ${jobposting.jobPostingName} has been ${
            status ? "Block" : "Unlock"
          }`,
          route: "/job-posting",
        });
      })
      .catch(() => {});
  };

  const handelApproveJobPosting = (
    idJposting: string,
    status: string,
    jobposting: JobPosting
  ) => {
    updateJopPosting({ idJposting: idJposting, status: status })
      .then(() => {
        mutate("/api/job-postings");
        toast.success("Confirm job posting success");
        createInformates({
          customerId: jobposting.customer.id,
          title: "Approve Job Posting",
          description: `Posting name is ${jobposting.jobPostingName} has been approve`,
          route: "/job-posting",
        });
      })
      .catch(() => {});
  };

  const rows = useMemo(() => {
    let rowJobPostings: any[] = [];
    listJobPosting.map((jobposting: JobPosting, index) => {
      let colorStatus: string = "";
      if (jobposting.status === "confirm") {
        colorStatus = "#00b894";
      } else if (jobposting.status === "pending") {
        colorStatus = "#0984e3";
      } else {
        colorStatus = "#e17055";
      }
      rowJobPostings.push([
        jobposting.id,
        jobposting.jobPostingName,
        <span key={index} style={{ color: colorStatus, fontWeight: "600" }}>
          {jobposting.status}
        </span>,
        jobposting.customer.users_permissions_user.fullName,
        jobposting.companyName,
        formatDate(jobposting.createdAt),
        <div className="funtion" key={index}>
          {jobposting.status === "pending" && (
            <CButton
              color="success"
              variant="outlined"
              text="Approve"
              onClick={() => {
                handelApproveJobPosting(jobposting.id, "confirm", jobposting);
              }}
            />
          )}
          {jobposting.status !== "pending" && (
            <CButton
              color={`${jobposting.status === "block" ? "success" : "error"}`}
              variant="outlined"
              text={`${jobposting.status === "block" ? "Unlock" : "Block"}`}
              onClick={() =>
                handelBlockJobPosting(
                  jobposting.id,
                  jobposting.status === "block" ? "confirm" : "block",
                  jobposting
                )
              }
            />
          )}
          {jobposting.posting_spams.length > 0 && (
            <CModal
              modal={
                <SpamJobPosting
                  jobposting={jobposting}
                  listSpam={jobposting.posting_spams}
                />
              }
              button={
                <CButton color="warning" variant="outlined" text="Reason" />
              }
            />
          )}
          <CModal
            modal={
              <div className="modal-detail__posting">
                <JobDetail
                  postingSave={[]}
                  lang={lang}
                  postId={jobposting.id}
                  isShowHeader={false}
                />
              </div>
            }
            button={<CButton variant="outlined" text="Detail" />}
          />
        </div>,
      ]);
    });
    return rowJobPostings;
  }, [lang, listJobPosting]);

  return (
    <>
      <div className="manager">
        <div className="manager-header">
          <h3>Project list</h3>
          <div className="">
            <Button
              className={`button-showchart ${
                showJob === "confirm" ? "active" : ""
              }`}
              sx={{ color: "#000" }}
              endIcon={
                showJob === "confirm" ? (
                  <Box1 size="32" color="#2ccce4" />
                ) : (
                  <CBadge
                    color="error"
                    numberInfo={
                      jobpostings.filter(
                        (posting) => posting.status === "confirm"
                      ).length
                    }
                    icon={<BoxTime size="32" color="#2ccce4" />}
                  />
                )
              }
              onClick={() => {
                showJob === "confirm" ? setShowJob("") : setShowJob("confirm");
              }}
            >
              {showJob === "confirm" ? "List Job" : "Job Confirm"}
            </Button>
            <Button
              className={`button-showchart ${
                showJob === "block" ? "active" : ""
              }`}
              sx={{ color: "#000" }}
              endIcon={
                showJob === "block" ? (
                  <Box1 size="32" color="#2ccce4" />
                ) : (
                  <CBadge
                    color="error"
                    numberInfo={
                      jobpostings.filter((job) => job.status === "block").length
                    }
                    icon={<BoxRemove size="32" color="#2ccce4" />}
                  />
                )
              }
              onClick={() => {
                showJob === "block" ? setShowJob("") : setShowJob("block");
              }}
            >
              {showJob === "block" ? "List Job" : "Job Block"}
            </Button>
            <Button
              className={`button-showchart ${
                showJob === "pending" ? "active" : ""
              }`}
              sx={{ color: "#000" }}
              endIcon={
                showJob === "pending" ? (
                  <Box1 size="32" color="#2ccce4" />
                ) : (
                  <CBadge
                    color="error"
                    numberInfo={
                      jobpostings.filter(
                        (posting) => posting.status === "pending"
                      ).length
                    }
                    icon={<BoxTime size="32" color="#2ccce4" />}
                  />
                )
              }
              onClick={() => {
                showJob === "pending" ? setShowJob("") : setShowJob("pending");
              }}
            >
              {showJob === "pending" ? "List Job" : "Job Pending"}
            </Button>
          </div>
        </div>
      </div>
      {jobpostings.length === 0 ? (
        <NotFoundValue />
      ) : (
        <div className="manager">
          <div className="manager-list">
            {rows && (
              <CTable
                rows={rows}
                label={[
                  "Id",
                  "Job Posting Name",
                  "Status",
                  "Customer",
                  "Company",
                  "Date create",
                  "Function",
                ]}
              />
            )}
          </div>
          {listJobpostingOrigin.length !== 0 && (
            <div className="member-manager__pagination">
              <CPagination
                SetPagination={SetPagination}
                count={
                  listJobpostingOrigin.length <= 10
                    ? 1
                    : Math.ceil(listJobpostingOrigin.length / 10)
                }
              />
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default ManagerJobPosting;
