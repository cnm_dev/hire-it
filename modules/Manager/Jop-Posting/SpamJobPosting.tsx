/* eslint-disable @next/next/no-img-element */
import { createInformates } from "@/apis/Informates";
import { updateJopPosting } from "@/apis/JobPosting";
import CButton from "@/common/components/controls/CButton";
import { JobPosting, PostingSpam } from "@/types/models";
import { intervalTime } from "@/utils";
import { Clock } from "iconsax-react";
import React, { useMemo } from "react";
import { toast } from "react-toastify";
import { mutate } from "swr";
type SpamJobPostingType = {
  listSpam: PostingSpam[];
  jobposting: JobPosting;
};
const SpamJobPosting = ({ listSpam, jobposting }: SpamJobPostingType) => {
  const handelBlockJobPosting = (
    idJposting: string,
    status: string,
    jobposting: JobPosting
  ) => {
    updateJopPosting({ idJposting: idJposting, status: status })
      .then(() => {
        toast.success(`Block status success`);
        createInformates({
          customerId: jobposting.customer.id,
          title: `Block Jobposting`,
          description: `Posting name is ${jobposting.jobPostingName} has been Block`,
          route: "/job-posting",
        });
        mutate("/api/job-postings");
      })
      .catch(() => {});
  };

  return (
    <div className="detail-spam">
      <h4>Spam Posting</h4>
      {listSpam.map((spam, index) => (
        <div key={index} className="detail-spam__item">
          <img
            src={
              spam.employee
                ? spam.employee.users_permissions_user.avartar
                : spam.customer.users_permissions_user.avartar
            }
            alt=""
          />
          <div className="detail-spam__container">
            <div className="detail-item__content">
              <p>
                <h5>Feedback {index + 1}: </h5>
                {spam.Reason}
              </p>
              <span>
                <Clock size={17} />
                {intervalTime(spam.createdAt)}
              </span>
            </div>
          </div>
        </div>
      ))}

      <div className="detail-spam__btn">
        {jobposting.status !== "block" && (
          <CButton
            text="Block Posting"
            onClick={() =>
              handelBlockJobPosting(jobposting.id, "block", jobposting)
            }
          />
        )}
      </div>
    </div>
  );
};

export default SpamJobPosting;
