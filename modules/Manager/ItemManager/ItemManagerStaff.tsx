import CButton from "@/common/components/controls/CButton";
import { Employee } from "@/types/models";
import React from "react";
import { formatDate } from "@/utils";
import { deleteEmployee, updateStatusEmployee } from "@/apis/Employee";
import { mutate } from "swr";
import { toast } from "react-toastify";
import date from "date-and-time";

type ItemManagerStaffProp = {
  employee: any | Employee;
};

const ItemManagerStaff = ({ employee }: ItemManagerStaffProp) => {
  const handelUpdateStatus = (employeeId: string) => {
    updateStatusEmployee({ employeeId, status: "confirm" })
      .then(() => {
        mutate("/api/employees");
        toast.success("Approve staff complete");
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };
  const handelDeleteEmployee = (employeeId: string) => {
    deleteEmployee({ employeeId })
      .then(() => {
        mutate("/api/employees");
        toast.success("Remove staff complete");
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };
  return (
    <div className="manager_item">
      <div className="item-manager">
        <div className="item-manager__column ">{employee.id}</div>
        <div className="item-manager__column col-4">
          {employee.users_permissions_user.email}
        </div>
        <div className="item-manager__column col-3">
          {employee.users_permissions_user.fullName}
        </div>
        <div className={`item-manager__column col-2 ${employee.status}`}>
          {employee.status}
        </div>
        <div className="item-manager__column col-3">
          {formatDate(employee.createdAt)}
        </div>
      </div>
      <div className="item-manager__function">
        <CButton variant="text" color="warning" text="Edit" />
        <CButton
          variant="text"
          disabled={
            employee.projects.length > 0 &&
            employee.projects[0].contracts.length > 0 &&
            (employee.projects[0].contracts[0].status === "wait" ||
              (employee.projects[0].contracts[0].status === "active" &&
                date
                  .subtract(
                    new Date(employee.projects[0].contracts[0].endDate),
                    new Date()
                  )
                  .toSeconds() > 0))
          }
          color="error"
          text="Remove"
          onClick={() => handelDeleteEmployee(employee.id)}
        />
        <CButton
          variant="text"
          disabled={employee.status !== "wait" ? true : false}
          color="success"
          text="Approve"
          onClick={() => handelUpdateStatus(employee.id)}
        />
      </div>
    </div>
  );
};

export default ItemManagerStaff;
