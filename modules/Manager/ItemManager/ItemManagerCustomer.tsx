import CButton from "@/common/components/controls/CButton";
import { Customer } from "@/types/models";
import React from "react";

type ItemManagerCustomerProp = {
  customer: Customer;
};

const ItemManagerCustomer = ({ customer }: ItemManagerCustomerProp) => {
  return (
    <div className="manager_item">
      <div className="item-manager">
        <div className="item-manager__column ">{customer.id}</div>
        <div className="item-manager__column col-4">
          {customer.users_permissions_user.email}
        </div>
        <div className="item-manager__column col-3">
          {customer.users_permissions_user.fullName}
        </div>
        <div className="item-manager__column col-3">{customer.companyName}</div>
        <div className="item-manager__column col-2">{customer.codeFax}</div>
      </div>
      <div className="item-manager__function">
        <CButton variant="text" color="warning" text="Edit" />
        <CButton variant="text" color="error" text="Remove" />
        <CButton variant="text" color="success" text="Approve" />
      </div>
    </div>
  );
};

export default ItemManagerCustomer;
