import { deleteUser, updateBlockUser } from "@/apis/Accounts";
import CButton from "@/common/components/controls/CButton";
import { Users_permissions_user } from "@/types/models";
import React from "react";
import { toast } from "react-toastify";
import { mutate } from "swr";

type ItemManagerAccountProp = {
  account: Users_permissions_user;
};

const ItemManagerAccount = ({ account }: ItemManagerAccountProp) => {
  const handelBlockUser = (idUser: string, block: boolean) => {
    updateBlockUser({
      idUser: idUser,
      block,
    })
      .then(() => {
        toast.success(`${block ? "Block" : "Unlock"} is success"`);
        mutate("/api/accounts");
      })
      .catch((err) => toast.error(err.message));
  };
  const handelDeleteUser = (idUser: string) => {
    deleteUser({ idUser })
      .then(() => {
        toast.success("Delete account success");
        mutate("/api/accounts");
      })
      .catch((err) => {
        toast.error(err.message);
      });
  };
  return (
    <div className={`manager_item ${account.blocked ? "block" : ""}`}>
      <div className="item-manager">
        <div className="item-manager__column ">{account.id}</div>
        <div className="item-manager__column col-4">{account.username}</div>
        <div className="item-manager__column col-4">{account.fullName}</div>
        <div className="item-manager__column col-4">
          {account.type_user.typeUserName}
        </div>
        <div className="item-manager__column col-3">{account.phoneNumber}</div>
      </div>
      <div className="item-manager__function">
        <CButton variant="text" color="warning" text="Edit" />
        <CButton
          variant="text"
          color="error"
          text="Remove"
          onClick={() => handelDeleteUser(account.id)}
        />
        {account.blocked ? (
          <CButton
            variant="text"
            color="success"
            text="Unlock"
            onClick={() => handelBlockUser(account.id, false)}
          />
        ) : (
          <CButton
            variant="text"
            color="success"
            text="Block"
            onClick={() => handelBlockUser(account.id, true)}
          />
        )}
      </div>
    </div>
  );
};

export default ItemManagerAccount;
