/* eslint-disable react/jsx-key */
import CModal from "@/common/components/controls/CModal";
import CPagination from "@/common/components/controls/CPagination";
import CTable from "@/common/components/controls/CTable";
import ExtendContract from "@/modules/Contract/components/ExtendContract";
import NotFoundValue from "@/modules/NotFoundValue";
import { Contract } from "@/types/models";
import { formatNumber } from "@/utils";
import { Button } from "@mui/material";
import {
  ChartSquare,
  DeviceMessage,
  DocumentText1,
  SearchNormal1,
} from "iconsax-react";
import React, { useMemo, useState } from "react";
import FilterManagerContracts from "../FilterManager/FilterManagerContracts";
import { useSelector } from "react-redux";
import { todoContractAdminSelector } from "@/redux/selector";
import COptionModel from "@/common/components/layout/COptionModel";
import { mutate } from "swr";
import { toast } from "react-toastify";
import { createContractHistory } from "@/apis/HistoryContract";
import { updateStatusContract } from "@/apis/Contract";
import Link from "next/link";
import ChartContract from "../Chart/ChartContract";
import { createInformates } from "@/apis/Informates";
import axios from "axios";
import { emailInfomateSuccess } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateCreateContractSuccess";
import { emailInfomateCancel } from "@/pages/api/verifyEmail/htmlEmail/EmailInfomateCancelContract";
import ContractEnd from "./ContractEnd";
import CBadge from "@/common/components/controls/CBadge";

const ManagerContract = () => {
  const contracts = useSelector(todoContractAdminSelector);
  const [showChart, SetShowChart] = React.useState<string>("");
  const [pagination, SetPagination] = React.useState(1);
  const [listContracts, setListContracts] = React.useState(contracts.slice(10));

  const [isOpenModalExtend, setIsOpenModalExtend] =
    React.useState<boolean>(false);
  React.useEffect(() => {
    if (contracts) {
      setListContracts(
        contracts
          .filter((data) => {
            return data.status !== "inactive";
          })
          .slice((pagination - 1) * 10, pagination * 10)
      );
    }
  }, [contracts, pagination]);

  const rows = useMemo(() => {
    let rowAccount: any[] = [];
    listContracts.map((contract: Contract) => {
      let styleContract = "";
      if (contract.status === "active") {
        styleContract = "#51cb1f";
      } else if (contract.status === "cancel") {
        styleContract = "rgb(220, 46, 46)";
      } else if (contract.status === "wait" || contract.status === "pending") {
        styleContract = "#e4da24";
      } else styleContract = "#1b5198";

      rowAccount.push([
        contract.id,
        contract.project.customer.users_permissions_user.fullName,
        contract.createdAt,
        contract.endDate,
        `${formatNumber(contract.project.totalMoney)} $`,
        <span
          key={contract.id}
          style={{ color: styleContract, fontWeight: "500" }}
        >
          {contract.status}
        </span>,
        ,
        contract.status === "active" ? (
          <div className="funtion">
            <CModal
              isOpenModal={isOpenModalExtend}
              setIsOpenModal={setIsOpenModalExtend}
              confirm={true}
              button={
                <Button size="small" variant="outlined">
                  Extend
                </Button>
              }
              modal={
                <ExtendContract
                  setIsOpenModal={setIsOpenModalExtend}
                  contract={contract}
                />
              }
            />
            <COptionModel
              title="Termination of contract?"
              handel={() => {
                updateStatusContract({
                  contractId: contract.id,
                  status: "end",
                }).then(() => {
                  createContractHistory({
                    contractId: contract.id,
                    dateUpdate: new Date(),
                    endDate: contract.endDate,
                    status: "complete",
                    totalMoney: contract.project.totalMoney,
                    type: "end",
                  }).then(() => {
                    createInformates({
                      title: "Termination of contract",
                      description: `We would like to inform you that the contract between us and you has been terminated. 
                      We will send you a detailed notification via your email.
                      Thank you.`,
                      route: "/contract",
                      customerId: contract.project.customer.id,
                    });

                    const html = emailInfomateCancel({
                      name: contract.project.customer.users_permissions_user
                        .fullName,
                      email: "Vohieu7k58@gmail.com",
                      phoneNumber: "0377723460",
                      contractId: contract.id,
                      host: "https://hire-it-git-chanh-dev-update-project-hireit.vercel.app/contract",
                    });
                    axios.post("/api/verifyEmail/sendEmail", {
                      email:
                        contract.project.customer.users_permissions_user.email,
                      html,
                    });
                    toast.success("End contract complete");
                    mutate(`/api/contracts`);
                  });
                });
              }}
              notice=""
              buttonOpen={
                <Button size="small" variant="outlined" color="error">
                  End
                </Button>
              }
            />
          </div>
        ) : (
          <div className="funtion">
            {contract.contract_ends.length > 0 && (
              <CModal
                modal={<ContractEnd contract={contract} />}
                button={
                  <Button size="small" variant="outlined">
                    Reason
                  </Button>
                }
              />
            )}
            <Link
              href={{
                pathname: "/contract/detail/[contractId]",
                query: { contractId: contract.id },
              }}
            >
              <Button size="small" variant="outlined" color="info">
                Detail
              </Button>
            </Link>
          </div>
        ),
      ]);
    });
    return rowAccount;
  }, [isOpenModalExtend, listContracts]);

  const listContractEnd = useMemo(() => {
    return contracts.filter((contract) => contract.contract_ends.length > 0);
  }, [contracts]);

  const rowsEnd = useMemo(() => {
    let rowAccount: any[] = [];
    listContractEnd.map((contract: Contract) => {
      let styleContract = "";
      if (contract.status === "active") {
        styleContract = "#51cb1f";
      } else if (contract.status === "cancel") {
        styleContract = "rgb(220, 46, 46)";
      } else if (contract.status === "wait" || contract.status === "pending") {
        styleContract = "#e4da24";
      } else styleContract = "#1b5198";

      rowAccount.push([
        contract.id,
        contract.project.customer.users_permissions_user.fullName,
        contract.createdAt,
        contract.endDate,
        `${formatNumber(contract.project.totalMoney)} $`,
        <span
          key={contract.id}
          style={{ color: styleContract, fontWeight: "500" }}
        >
          {contract.status}
        </span>,
        ,
        contract.status === "active" ? (
          <div className="funtion">
            <CModal
              isOpenModal={isOpenModalExtend}
              setIsOpenModal={setIsOpenModalExtend}
              confirm={true}
              button={
                <Button size="small" variant="outlined">
                  Extend
                </Button>
              }
              modal={
                <ExtendContract
                  setIsOpenModal={setIsOpenModalExtend}
                  contract={contract}
                />
              }
            />
            <COptionModel
              title="Termination of contract?"
              handel={() => {
                updateStatusContract({
                  contractId: contract.id,
                  status: "end",
                }).then(() => {
                  createContractHistory({
                    contractId: contract.id,
                    dateUpdate: new Date(),
                    endDate: contract.endDate,
                    status: "complete",
                    totalMoney: contract.project.totalMoney,
                    type: "end",
                  }).then(() => {
                    createInformates({
                      title: "Termination of contract",
                      description: `We would like to inform you that the contract between us and you has been terminated. 
                      We will send you a detailed notification via your email.
                      Thank you.`,
                      route: "/contract",
                      customerId: contract.project.customer.id,
                    });

                    const html = emailInfomateCancel({
                      name: contract.project.customer.users_permissions_user
                        .fullName,
                      email: "Vohieu7k58@gmail.com",
                      phoneNumber: "0377723460",
                      contractId: contract.id,
                      host: "https://hire-it-git-chanh-dev-update-project-hireit.vercel.app/contract",
                    });
                    axios.post("/api/verifyEmail/sendEmail", {
                      email:
                        contract.project.customer.users_permissions_user.email,
                      html,
                    });
                    toast.success("End contract complete");
                    mutate(`/api/contracts`);
                  });
                });
              }}
              notice=""
              buttonOpen={
                <Button size="small" variant="outlined" color="error">
                  End
                </Button>
              }
            />
          </div>
        ) : (
          <div className="funtion">
            {contract.contract_ends.length > 0 && (
              <CModal
                modal={<ContractEnd contract={contract} />}
                button={
                  <Button size="small" variant="outlined">
                    Reason
                  </Button>
                }
              />
            )}
            <Link
              href={{
                pathname: "/contract/detail/[contractId]",
                query: { contractId: contract.id },
              }}
            >
              <Button size="small" variant="outlined" color="info">
                Detail
              </Button>
            </Link>
          </div>
        ),
      ]);
    });
    return rowAccount;
  }, [isOpenModalExtend, listContractEnd]);

  return (
    <>
      <div className="manager">
        <div className="manager-filter">
          <FilterManagerContracts />
        </div>
        <div className="manager-header">
          <h3>Contract {showChart ? showChart : "list"}</h3>
          <div className="">
            <Button
              className={`button-showchart ${
                showChart === "chart" ? "active" : ""
              }`}
              sx={{ color: "#000" }}
              endIcon={
                showChart === "chart" ? (
                  <DocumentText1 size="32" color="#2ccce4" />
                ) : (
                  <ChartSquare size="32" color="#2ccce4" />
                )
              }
              onClick={() => {
                showChart === "chart"
                  ? SetShowChart("")
                  : SetShowChart("chart");
              }}
            >
              {showChart === "chart" ? "List contract" : "Show Chart"}
            </Button>
            <Button
              className={`button-showchart ${
                showChart === "end" ? "active" : ""
              }`}
              sx={{ color: "#000" }}
              endIcon={
                showChart === "end" ? (
                  <DocumentText1 size="32" color="#2ccce4" />
                ) : (
                  <CBadge
                    color="error"
                    numberInfo={
                      listContractEnd.filter(
                        (contract) => contract.status === "pending"
                      ).length
                    }
                    icon={<DeviceMessage size="32" color="#2ccce4" />}
                  />
                )
              }
              onClick={() => {
                showChart === "end" ? SetShowChart("") : SetShowChart("end");
              }}
            >
              {showChart === "end" ? "List contract" : "Request End"}
            </Button>
          </div>
        </div>
      </div>
      {contracts.length === 0 ? (
        <NotFoundValue />
      ) : (
        <div className="manager">
          {showChart === "chart" ? (
            <div>
              <ChartContract contracts={contracts} />
              <>
                <h4 style={{ marginTop: "20px", textAlign: "center" }}>
                  Detail list contract
                </h4>
                <div className="manager-list">
                  {rows && (
                    <CTable
                      rows={rows}
                      label={[
                        "Id",
                        "Customer name",
                        "Create date",
                        "Expiration date ",
                        "Total",
                        "Status",
                        "Function",
                      ]}
                    />
                  )}
                </div>
                <div className="member-manager__pagination">
                  <CPagination
                    SetPagination={SetPagination}
                    count={
                      contracts.length <= 10
                        ? 1
                        : Math.ceil(contracts.length / 10)
                    }
                  />
                </div>
              </>
            </div>
          ) : showChart === "end" ? (
            <>
              <div className="manager-list">
                {rowsEnd && (
                  <CTable
                    rows={rowsEnd}
                    label={[
                      "Id",
                      "Customer name",
                      "Create date",
                      "Expiration date ",
                      "Total",
                      "Status",
                      "Function",
                    ]}
                  />
                )}
              </div>
              <div className="member-manager__pagination">
                <CPagination
                  SetPagination={SetPagination}
                  count={
                    listContractEnd.length <= 10
                      ? 1
                      : Math.ceil(listContractEnd.length / 10)
                  }
                />
              </div>
            </>
          ) : (
            <>
              <div className="manager-list">
                {rows && (
                  <CTable
                    rows={rows}
                    label={[
                      "Id",
                      "Customer name",
                      "Create date",
                      "Expiration date ",
                      "Total",
                      "Status",
                      "Function",
                    ]}
                  />
                )}
              </div>
              <div className="member-manager__pagination">
                <CPagination
                  SetPagination={SetPagination}
                  count={
                    contracts.length <= 10
                      ? 1
                      : Math.ceil(contracts.length / 10)
                  }
                />
              </div>
            </>
          )}
        </div>
      )}
    </>
  );
};

export default ManagerContract;
