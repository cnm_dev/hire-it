/* eslint-disable @next/next/no-img-element */
import { updateContract } from "@/apis/Contract";
import { createContractHistory } from "@/apis/HistoryContract";
import { createInformates } from "@/apis/Informates";
import CButton from "@/common/components/controls/CButton";
import { Contract } from "@/types/models";
import { intervalTime } from "@/utils";
import { Clock } from "iconsax-react";
import React from "react";
import { toast } from "react-toastify";
import { mutate } from "swr";
type ContractEndProps = {
  contract: Contract;
};
const ContractEnd = ({ contract }: ContractEndProps) => {
  const [isLoading, setIsLoading] = React.useState<boolean>(false);
  const handelActiveContract = () => {
    setIsLoading(true);
    updateContract({
      contractId: contract.id,
      status: "active",
      startDate: contract.startDate,
      endDate: contract.endDate,
    })
      .then(() => {
        createContractHistory({
          contractId: contract.id,
          status: "complete",
          dateUpdate: new Date(),
          totalMoney: contract.project.totalMoney,
          type: "active",
          endDate: contract.endDate,
        });
        createInformates({
          title: "Contract is active again",
          description: `Contract ${contract.project.projectName} is active again`,
          customerId: contract.project.customer.id,
          route: `/contract/detail/${contract.id}`,
        })
          .then(() => {
            toast.success("Active contract success");
            mutate("/api/customers/undefined/contracts");
            setIsLoading(false);
          })
          .catch(() => {
            setIsLoading(false);
          });
      })
      .catch(() => {
        setIsLoading(false);
      });
  };
  const handelEndContract = () => {
    setIsLoading(true);
    updateContract({
      contractId: contract.id,
      status: "end",
      startDate: contract.startDate,
      endDate: contract.endDate,
    })
      .then(() => {
        createContractHistory({
          contractId: contract.id,
          status: "complete",
          dateUpdate: new Date(),
          totalMoney: contract.project.totalMoney,
          type: "end",
          endDate: contract.endDate,
        });
        createInformates({
          title: "Request end Contract approved",
          description: `Request end Contract ${contract.project.projectName} is approved`,
          customerId: contract.project.customer.id,
          route: `/contract/detail/${contract.id}`,
        })
          .then(() => {
            toast.success("End contract success");
            mutate("/api/customers/undefined/contracts");
            setIsLoading(false);
          })
          .catch(() => {
            setIsLoading(false);
          });
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  return (
    <div className="detail-spam">
      <h4>End Contract</h4>
      <div className="detail-spam__item">
        <img
          src={contract.project.customer.users_permissions_user.avartar}
          alt=""
        />
        <div className="detail-spam__container">
          {contract.contract_ends.map((contract_end, index) => (
            <div key={index} className="detail-item__content">
              <p>
                <h5>Feedback {index + 1}: </h5>
                {contract_end.reason}
              </p>
              <span>
                <Clock size={17} />
                {intervalTime(contract_end.createdAt)}
              </span>
            </div>
          ))}
        </div>
      </div>
      {contract.status === "pending" && (
        <div className="detail-spam__btn">
          <CButton
            disabled={isLoading}
            text="Active Contract"
            onClick={handelActiveContract}
          />
          <CButton
            disabled={isLoading}
            text="End Contract"
            onClick={handelEndContract}
          />
        </div>
      )}
    </div>
  );
};

export default ContractEnd;
