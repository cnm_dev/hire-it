/* eslint-disable jsx-a11y/alt-text */
import image from "@/common/assets/image/all-image";
import { Customer, Employee } from "@/types/models";
import {
  Document,
  Page,
  Text,
  Image,
  PDFViewer,
  StyleSheet,
  Font,
} from "@react-pdf/renderer";

Font.register({
  family: "Roboto",
  fonts: [
    {
      src: "https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmSU5vAx05IsDqlA.ttf",
    },
    {
      src: "https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmEU9vAx05IsDqlA.ttf",
    },
    {
      src: "https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmWUlvAx05IsDqlA.ttf",
    },
    {
      src: "https://fonts.gstatic.com/s/roboto/v30/KFOlCnqEu92Fr1MmYUtvAx05IsDqlA.ttf",
    },
    {
      src: "https://fonts.gstatic.com/s/roboto/v30/KFOjCnqEu92Fr1Mu51TjARc9AMX6lJBP.ttf",
      fontWeight: "normal",
      fontStyle: "italic",
    },
    {
      src: "https://fonts.gstatic.com/s/roboto/v30/KFOjCnqEu92Fr1Mu51S7ABc9AMX6lJBP.ttf",
      fontWeight: "normal",
      fontStyle: "italic",
    },
    {
      src: "https://fonts.gstatic.com/s/roboto/v30/KFOjCnqEu92Fr1Mu51TzBhc9AMX6lJBP.ttf",
      fontWeight: "normal",
      fontStyle: "italic",
    },
    {
      src: "https://fonts.gstatic.com/s/roboto/v30/KFOjCnqEu92Fr1Mu51TLBBc9AMX6lJBP.ttf",
      fontWeight: "normal",
      fontStyle: "italic",
    },
  ],
});

const styles = StyleSheet.create({
  body: {
    width: "100%",
    height: "100%",
    fontFamily: "Roboto",
  },
});

type PDFProps = {
  arrMember : Employee[],
  inforCustomer: Customer,
  description: Array<string | undefined | any>
  priceMoney: number,
  date: string 
}

export const PDF = ({arrMember, inforCustomer, description, priceMoney, date}:PDFProps) => {
  
  return (
    <Document style={styles.body}>
      <Page style={{padding: "20px", position: "relative"}}>
        <div className="header-contract">
          <Text
            style={{
              textAlign: "center",
              fontSize: "15px",
              fontWeight: 500,
            }}
            wrap={false}
          >
            CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM
          </Text>
          <Text
            wrap={false}
            style={{
              textAlign: "center",
              marginTop: "2px",
              fontSize: "15px",
              fontWeight: 500,
            }}
          >
            Độc lập - Tự do - Hạnh phúc
          </Text>
          <Text
            style={{ textAlign: "center", marginTop: "10px", fontSize: "15px" }}
          >
            ---------------------------------
          </Text>
        </div>
        <div className="">
          <Text
            style={{
              textAlign: "center",
              marginTop: "10px",
              fontSize: "18px",
              fontWeight: "semibold",
            }}
            wrap={false}
          >
            HỢP ĐỒNG THAM KHẢO THUÊ NHÂN LỰC CNTT
          </Text>
          <Text
            style={{
              textAlign: "right",
              marginRight: "40px",
              marginTop: "10px",
              fontSize: "13px",
            }}
            wrap={false}
          >
            Hồ Chí Minh, {date}
          </Text>
          <div>
            <Text
              style={{
                marginLeft: "15px",
                marginTop: "20px",
                fontSize: "13px",
                fontWeight: "semibold",
              }}
              wrap={false}
            >
              Công ty chúng tôi:
              <Text>   HireIT.io</Text>
            </Text>
          </div>
          <div>
            <Text
              style={{ marginLeft: "15px", marginTop: "5px", fontSize: "12px" }}
              wrap={false}
            >
              Địa chỉ :
              <Text>                          12 Nguyễn Văn Bảo phường 4, Gò Vấp, Hồ Chí Minh</Text>
            </Text>
          </div>
          <div>
            <Text
              style={{ marginLeft: "15px", marginTop: "5px", fontSize: "12px" }}
              wrap={false}
            >
              Số điện thoại :               <Text>0123456789</Text>
            </Text>
          </div>
          <div>
            <Text
              style={{ marginLeft: "15px", marginTop: "5px", fontSize: "12px" }}
              wrap={false}
            >
              Số fax :                           <Text>99999999</Text>
            </Text>
          </div>
          <div>
            <Text style={{
                marginLeft: "15px",
                marginTop: "10px",
                fontSize: "13px",
                fontWeight: "semibold",
              }} wrap={false}>Bên tiếp nhận:          <Text>{inforCustomer.companyName}</Text></Text>
           
          </div>
          <div>
            <Text style={{ marginLeft: "15px", marginTop: "5px", fontSize: "12px" }} wrap={false}>Địa chỉ :                           <Text>{inforCustomer.users_permissions_user.address}</Text></Text>
          </div>
          <div>
            <Text style={{ marginLeft: "15px", marginTop: "5px", fontSize: "12px" }} wrap={false}>Số điện thoại :                <Text>{inforCustomer.users_permissions_user.phoneNumber}</Text></Text>
          </div>
          <div>
            <Text style={{ marginLeft: "15px", marginTop: "5px", fontSize: "12px" }} wrap={false}>Số fax :                            <Text>{inforCustomer.codeFax}</Text></Text>
          </div>
          <div>
            <Text style={{ marginLeft: "15px", marginTop: "5px", fontSize: "12px" }} wrap={false}>Người đại diện:              <Text>{inforCustomer.users_permissions_user.fullName}</Text></Text>
          </div>
        </div>
        <div>
          <Text style={{marginTop: "15px", fontSize: "13px", fontWeight: "semibold", marginLeft: "15px", fontStyle: "italic"}}>Hai bên ký kết hợp đồng với nội dung:</Text>
          <div>
            {description.map((des, index) => (
              des && <Text key={index} style={{marginLeft: "15px", fontSize: "12px", marginTop: "5px", color: "#393838"}}>- {des}</Text>
            ))}
            <Text style={{marginLeft: "15px", fontSize: "12px", marginTop: "5px", color: "#393838"}}>- Tổng tiền: {priceMoney.toFixed(2)}$</Text>
            <Text style={{marginLeft: "15px", fontSize: "12px", marginTop: "5px", color: "#393838"}}>- VAT(10%): {(priceMoney * 0.05).toFixed(2)}$</Text>
            <Text style={{marginLeft: "15px", fontSize: "12px", marginTop: "5px", color: "#393838"}}>- Giảm giá: 0.00$</Text>
            <Text style={{marginLeft: "15px", fontSize: "12px", marginTop: "5px", color: "#393838", fontWeight: "semibold"}}>- Tổng tiền cần trả: {(priceMoney + priceMoney * 0.05).toFixed(2)}$</Text>
            <Text style={{marginLeft: "15px", fontSize: "10px", marginTop: "5px", color: "#393838", fontWeight: "semibold",fontStyle: "italic"}}>* Lưu ý: Tổng số tiền trên chỉ là số liệu tham khảo, khoản tiền có thể phát sinh tùy theo nhiều yếu tố như trợ cập, hỗ trợ,...</Text>

          </div>
          <Text style={{marginTop: "15px", fontSize: "13px", fontWeight: "semibold", marginLeft: "15px", fontStyle: "italic"}}>Cụ thể:</Text>
          {arrMember.map((member, index) => (
            <div key={index}>
              <Text style={{marginLeft: "15px", fontSize: "13px", marginTop: "10px", fontStyle: "italic"}}>- Nhân viên {index + 1}:</Text>
              <div>
                <Text style={{marginTop: "5px", marginLeft: "40px", color: "#393838", fontSize: "12px"}}>+ Tên: {member.users_permissions_user.fullName}</Text>
                <Text style={{marginTop: "5px", marginLeft: "40px", color: "#393838", fontSize: "12px"}}>+ Chuyên ngành: {member.position.positionName}</Text>
                <Text style={{marginTop: "5px", marginLeft: "40px", color: "#393838", fontSize: "12px"}}>+ Địa chỉ: {member.users_permissions_user.address}</Text>
                <Text style={{marginTop: "5px", marginLeft: "40px", color: "#393838", fontSize: "12px"}}>+ Số điện thoại: {member.users_permissions_user.phoneNumber}</Text>
                <Text style={{marginTop: "5px", marginLeft: "40px", color: "#393838", fontSize: "12px"}}>+ Email: {member.users_permissions_user.email}</Text>
              </div>
            </div>
          ))}
        </div>
        <div>
          <Text style={{fontSize: "13px", marginTop: "20px", textAlign: "right", marginRight: "40px"}}>Đại diện bên cung ứng</Text>
          <Text style={{fontSize: "13px", marginTop: "10px", textAlign: "right", marginRight: "65px"}}> Chánh & Hiếu</Text>
        </div>
      </Page>
    </Document>
  );
};
const PDFView = ({arrMember, inforCustomer, description, priceMoney, date}: PDFProps) => {
  return (
    <PDFViewer>
      <PDF date={date} arrMember={arrMember} inforCustomer={inforCustomer} description={description} priceMoney={priceMoney}/>
    </PDFViewer>
  );
};
export default PDFView;
