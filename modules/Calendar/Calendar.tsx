import CBigCalendar, {
  TypeEvent,
} from "@/common/components/controls/CBigCalendar";
import { Project } from "@/types/models";
import React, { useMemo } from "react";

export type ContractEmployee = {
  id: string;
  projects: Project[];
};

type CalendarProps = {
  contracts: ContractEmployee[];
};

const Calendar = ({ contracts }: CalendarProps) => {
  const events = useMemo(() => {
    if (contracts && contracts.length > 0) {
      let event: TypeEvent[] = [];
      if (contracts[0].projects) {
        contracts[0].projects.map((project) => {
          let evetItem: TypeEvent = {};
          evetItem.title = project.projectName;
          evetItem.start = new Date(project.contracts[0].startDate);
          evetItem.end = new Date(project.contracts[0].endDate);
          evetItem.type = project.contracts[0].status;
          event.push(evetItem);
        });
      }
      return event;
    } else return [];
  }, [contracts]);

  function eventStyleGetter(event: TypeEvent) {
    let backgroundColor = "#3174ad";
    if (event.type !== "active") {
      backgroundColor = "#c9c7c7";
    }
    const style = {
      backgroundColor: backgroundColor,
      borderRadius: "5px",
      opacity: 0.8,
      color: "white",
      border: "0px",
      display: "block",
    };
    return {
      style: style,
    };
  }
  
  return (
    <div className="calendar-container">
      <CBigCalendar events={events} eventPropGetter={eventStyleGetter} />
    </div>
  );
};

export default Calendar;
